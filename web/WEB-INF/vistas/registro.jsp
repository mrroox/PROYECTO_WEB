<%-- 
    Document   : registro
    Created on : 07-nov-2017, 14:22:09
    Author     : Felipe Quezada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="/PROYECTO_WEB"><i class="glyphicon glyphicon-knight"></i>Rakiduam</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="/PROYECTO_WEB">Home </a></li>
                    <li role="presentation"><a href="login" style="background-color:#ffffff;">Login </a></li>
                    <li class="active" role="presentation"><a href="registro">Registro </a></li>
                    <li role="presentation"><a href="contacto">Contacto </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Nuevo Registro</h1>
