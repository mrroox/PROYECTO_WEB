<%-- 
    Document   : contacto
    Created on : 18-oct-2017, 9:55:25
    Author     : Felipe Quezada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="/PROYECTO_WEB"><i class="glyphicon glyphicon-knight"></i>Rakiduam</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="/PROYECTO_WEB">Home </a></li>
                    <li role="presentation"><a href="login" style="background-color:#ffffff;">Login </a></li>
                    <li role="presentation"><a href="registro">Registro </a></li>
                    <li class="active" role="presentation"><a href="contacto">Contacto </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="contact-clean">
        <form method="post">
            <h2 class="text-center">Contact us</h2>
            <div class="form-group has-success has-feedback">
                <input class="form-control" type="text" name="name" placeholder="Name"><i class="form-control-feedback glyphicon glyphicon-ok" aria-hidden="true"></i></div>
            <div class="form-group has-error has-feedback">
                <input class="form-control" type="email" name="email" placeholder="Email"><i class="form-control-feedback glyphicon glyphicon-remove" aria-hidden="true"></i>
                <p class="help-block">Please enter a correct email address.</p>
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="14" name="message" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">send </button>
            </div>
        </form>
    </div>
