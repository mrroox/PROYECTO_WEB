<%-- 
    Document   : login
    Created on : 17-oct-2017, 22:04:38
    Author     : Felipe Quezada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="/PROYECTO_WEB"><i class="glyphicon glyphicon-knight"></i>Rakiduam</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-right">
                    <li  role="presentation"><a href="/PROYECTO_WEB">Home </a></li>
                    <li class="active" role="presentation"><a href="login" style="background-color:#ffffff;">Login </a></li>
                    <li role="presentation"><a href="registro">Registro </a></li>
                    <li role="presentation"><a href="contacto">Contacto </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="login-dark">
        <form action="loginuser" method="POST">
            <h2 class="sr-only">Login Form</h2>
            <h1 class="text-center">Iniciar Sesión</h1>
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group">
                <input class="form-control" type="text" name="user" placeholder="Usuario">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit">Log In</button>
            </div><a href="#" class="forgot">Forgot your email or password?</a>
        </form>
    </div>
