<%-- 
    Document   : vista_paciente
    Created on : 03-12-2017, 19:38:08
    Author     : unknown
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Rakiduam - Ficha Paciente</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="assets/V2/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLES-->
        <link href="assets/V2/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
        <!-- MORRIS CHART STYLES-->
        <link href="assets/V2/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <link href="assets/V2/css/custom.css" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="assets/js/main.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="intranet">Terapeuta</a> 
                </div>


                <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: left;
                     font-size: 16px;"> Vista Paciente   &nbsp;
                </div>
                <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: right;
                     font-size: 16px;"> ${terapeuta.getPersona().getNombre()} ${terapeuta.getPersona().getApellido_paterno()} &nbsp; 
                    <a href="intranet/u/out" class="btn btn-danger square-btn-adjust">Logout</a> </div>
            </nav>   
            <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li class="text-center">
                            <img src="assets/V2/img/personaB.png" class="user-image img-responsive"/>
                        </li>

                        <li>
                            <a  href="#tab1"><i class="fa fa-desktop fa-3x"></i>Información Personal</a>
                        </li>
                        <li>
                            <a  href="#tab2"><i class="fa fa-dashboard fa-3x"></i>Historial de Consultas</a>
                        </li>
                        <li>
                            <a  href="#tab3"><i class="fa fa-qrcode fa-3x"></i>Nuevo Registro y Dispositivo</a>
                        </li>
                        <li>
                            <a  href="#tab4"><i class="fa fa-qrcode fa-3x"></i>Nuevo Registro para Dispositivo</a>
                        </li> 
                        <li>
                            <a  href="#tab5"><i class="fa fa-bar-chart-o fa-3x"></i>Seguimiento Historico</a>
                        </li> 
                        <li>
                            <a  href="#tab6"><i class="fa fa-bar-chart-o fa-3x"></i>Seguimiento por Periodo</a>
                        </li> 


                    </ul>

                </div>

            </nav>  
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div id="tab1" class="col-md-12">
                            <h2>Rakiduam </h2>   
                            <h3>Ficha del Paciente : ${paciente.getPersona().getRun()}</h3>
                            <form role="form">
                                <div class="form-group">
                                    <a class="btn btn-success">Editar</a>
                                </div>
                                <fieldset disabled="disabled">

                                    <div class="form-group">
                                        <label for="disabledSelect">Run</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getRun()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Nombre</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getNombre()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Apellido Paterno</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getApellido_paterno()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Apellido Materno</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getApellido_materno()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Apellido Materno</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getApellido_materno()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Fecha Nacimiento</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getFecha_nacimiento()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Fecha Nacimiento</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getSexo()}" disabled />
                                    </div>
                                    </hr>
                                    <div class="form-group">
                                        <label for="disabledSelect">Dirección Calle</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getDireccion().getCalle()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Dirección Numero</label>
                                        <input class="form-control" id="disabledInput" type="text" placeholder="${paciente.getPersona().getDireccion().getNumero()}" disabled />
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledSelect">Region</label>
                                        <select id="disabledSelect" class="form-control">
                                            <option>Disabled select</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <a href="agregar_paciente" class="btn btn-success">Guardar</a>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div id="tab2" class="col-md-12">
                            <h2>Historial de Consultas</h2>   
                            <h5> </h5>
                            </hr>

                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    Lista de Pacientes
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Fecha Evaluacion</th>
                                                    <th>Observaciones</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="i" items="${pacientes}">
                                                    <tr>
                                                        <td>

                                                        </td>
                                                        <td>${i.getPersona().getRun()}</td>
                                                        <td>${i.getPersona().getNombre()}</td>

                                                    </tr>
                                                </c:forEach>  
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div id="tab5" class="col-md-12">
                            <h2>Seguimiento Historico</h2>   
                            <h5> </h5>
                            <hr />

                            <div class="row"> 


                                <div class="col-md-6 col-sm-12 col-xs-12">                     
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Datos Historicos 1
                                        </div>
                                        <div class="panel-body">
                                            <div id="morris-bar-chart"></div>
                                        </div>
                                    </div>            
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">                     
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Datos Historicos 2
                                        </div>
                                        <div class="panel-body">
                                            <div id="morris-area-chart"></div>
                                        </div>
                                    </div>            
                                </div> 

                            </div>
                            <div class="form-group">
                                <a class="btn btn-success">Generar Reporte Historico</a>
                            </div>

                        </div>
                        <!--End Advanced Tables -->
                        <div id="tab6" class="col-md-12">
                            <h2>Seguimiento Periodico</h2>   
                            <h5> </h5>
                            <hr />
                            <!-- /. ROW  -->
                            <div class="row">                     

                                <div class="col-md-6 col-sm-12 col-xs-12">                     
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Datos Periodo 1
                                        </div>
                                        <div class="panel-body">
                                            <div id="morris-donut-chart"></div>
                                        </div>
                                    </div>            
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">                     
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Datos Periodo 2
                                        </div>
                                        <div class="panel-body">
                                            <div id="morris-line-chart"></div>
                                        </div>
                                    </div>            
                                </div> 

                            </div>
                            <div class="form-group">
                                <a class="btn btn-success">Generar Reporte Periodo</a>
                            </div>
                            <!-- /. ROW  -->
                        </div> 
                    </div>    
                </div>    


            </div>
            <!-- /. ROW  -->
            <hr />

        </div>
        <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="assets/V2/js/jquery-1.10.2.js"></script>

        <script src="assets/V2/js/bootstrap.min.js"></script>
        <!-- METISMENU SCRIPTS -->
        <script src="assets/V2/js/jquery.metisMenu.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="assets/V2/js/custom.js"></script>
        <!-- MORRIS CHART SCRIPTS -->
        <script src="assets/V2/js/morris/raphael-2.1.0.min.js"></script>
        <script src="assets/V2/js/morris/morris.js"></script>


    </body>
</html>
