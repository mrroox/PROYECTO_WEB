<%-- 
    Document   : register_terapeuta
    Created on : 18-oct-2017, 9:43:59
    Author     : Felipe Quezada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="/PROYECTO_WEB"><i class="glyphicon glyphicon-knight"></i>Rakiduam</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="/PROYECTO_WEB">Home </a></li>
                    <li role="presentation"><a href="login" style="background-color:#ffffff;">Login </a></li>
                    <li class="active" role="presentation"><a href="registro">Registro </a></li>
                    <li role="presentation"><a href="contacto">Contacto </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="form-container">
            <form action="RegistroServlet" method="post">
                <h2 class="text-center">Registrar Terapeuta</h2>
                <div class="form-group">
                    <input class="form-control" type="text" name="run" placeholder="Run">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="nombre" placeholder="Nombre">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="apellido_paterno" placeholder="Apellido Paterno">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="apellido_materno" placeholder="Apellido Materno">
                </div>
                <div class="form-group">
                    <input class="form-control" type="email" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <div>
                        <label for="sexo"> Sexo  &nbsp;</label>
                        <input type="radio" name="sexo" value="Femenino" />Femenino 
                        <input type="radio" name="sexo" value="Masculino" />Masculino
                        </br>
                    </div>
                </div>
                <div class="form-group">


                    <label for="fnacimiento"> F.Nacimiento  &nbsp;</label>
                    <input type="date" name="fnacimiento" /> 

                </div>

                <div class="form-group">
                    <input class="form-control" type="text" name="calle" placeholder="Calle">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="numero" placeholder="Numero">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="movil" placeholder="Telefono Movil">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="redfija" placeholder="Red Fija">
                </div>
                <div class="form-group">
                    <label for="pais"> Pais &nbsp;</label>
                    <select class="form-control" type="text" name="pais">
                        <option value="chile">Chile</option>

                    </select>
                </div>
                <div class="form-group">
                    <!--                        <input class="form-control" type="text" name="region" placeholder="Region">-->
                    <label for="region"> Region  &nbsp;</label>
                    <select class="form-control" type="text" name="region">
                        <option value="Tarapacá">Tarapacá</option>
                        <option value="Antofagasta">Antofagasta</option>
                        <option value="Atacama">Atacama</option>
                        <option value="Coquimbo">Coquimbo</option>
                        <option value="Valparaíso">Valparaíso</option>
                        <option value="O’Higgins">O’Higgins</option>
                        <option value="Maule">Maule</option>
                        <option value="Bío-Bío">Bío-Bío</option>
                        <option value="La Araucanía">La Araucanía</option>
                        <option value="Los Lagos">Los Lagos</option>
                        <option value="Aysén">Aysén</option>
                        <option value="Magallanes">Magallanes</option>
                        <option value="Metropolitana de Santiago">Metropolitana de Santiago</option>
                        <option value="Arica y Parinacota">Arica y Parinacota</option>


                    </select>
                    </br>

                </div>
                <div class="form-group">
                    <input class="form-control" type="password" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <input class="form-control" type="password" name="password-repeat" placeholder="Password (repeat)">
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label class="control-label">
                            <input type="checkbox">I agree to the license terms.</label>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit">Registrar </button>
                </div><a href="login.jsp" class="already">¿Ya tienes una cuenta? Inicia Sesión Aqui!.</a></form>
        </div>
    </div>
    <div class="footer-basic">
        <footer>
            <div class="social"><a href="#"><i class="icon ion-social-instagram"></i></a><a href="#"><i class="icon ion-social-snapchat"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-facebook"></i></a></div>
            <ul class="list-inline">
                <li><a href="#">Home</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="#">Privacy Policy</a></li>
            </ul>
            <p class="copyright">Company Name © 2017</p>
        </footer>
    </div>
