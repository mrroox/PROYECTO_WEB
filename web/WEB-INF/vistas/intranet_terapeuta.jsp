<%-- 
    Document   : intranet_terapeuta
    Created on : 07-nov-2017, 10:06:21
    Author     : Felipe Quezada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Intranet Terapeuta</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="assets/V2/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLES-->
        <link href="assets/V2/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
        <link href="assets/V2/css/custom.css" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="assets/js/main.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="intranet">Terapeuta</a> 
                </div>
                <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: left;
                     font-size: 16px;"> Intranet Terapeuta &nbsp;
                </div>
                <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: right;
                     font-size: 16px;"> ${terapeuta.getPersona().getNombre()} ${terapeuta.getPersona().getApellido_paterno()} &nbsp; 
                    <a href="intranet/u/out" class="btn btn-danger square-btn-adjust">Logout</a> </div>
            </nav>   
            <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li class="text-center">
                            <img src="assets/V2/img/personaB.png" class="user-image img-responsive"/>
                        </li>
                        <li>
                            <a  href="#tab1"><i class="fa fa-desktop fa-3x"></i> Portada</a>
                        </li>
                        <li>
                            <a  href="#tab2"><i class="fa fa-dashboard fa-3x"></i> Terapeuta</a>
                        </li>
                        </li>	
                        <li  >
                            <a  href="#tab3"><i class="fa fa-table fa-3x"></i> Pacientes</a>
                        </li>
                    </ul>

                </div>

            </nav>  
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div id="tab1" class="col-md-12">
                            <h2> Rakiduam</h2>   
                            <h5>Bienvenido ${terapeuta.getPersona().getNombre()} ${terapeuta.getPersona().getApellido_paterno()} , nos encanta tenerte de regreso!. </h5>
                            <p>
                                Llenar con descripccion


                            </p>
                        </div>
                        <div id="tab2" class="col-md-12">
                            <h2>Mis Datos Personales</h2> 
                            <div class="form-group">
                                <a class="btn btn-success">Editar</a>
                            </div>
                            <fieldset disabled="disabled">
                                <div class="form-group">
                                    <label for="disabledSelect">Run</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getRun()}" disabled />
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">Nombre</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getNombre()}" disabled />
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">Apellido Paterno</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getApellido_paterno()}" disabled />
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">Apellido Materno</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getApellido_materno()}" disabled />
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">Apellido Materno</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getApellido_materno()}" disabled />
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">Fecha Nacimiento</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getFecha_nacimiento()}" disabled />
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">Fecha Nacimiento</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getSexo()}" disabled />
                                </div>
                                </hr>
                                <div class="form-group">
                                    <label for="disabledSelect">Dirección Calle</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getDireccion().getCalle()}" disabled />
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">Dirección Numero</label>
                                    <input class="form-control" id="disabledInput" type="text" placeholder="${terapeuta.getPersona().getDireccion().getNumero()}" disabled />
                                </div>
                                <div class="form-group">
                                    <a href="" class="btn btn-success">Guardar</a>
                                </div>

                            </fieldset >
                        </div>

                        <div id="tab3" class="col-md-12">
                            <h2>Mi Lista de Pacientes</h2>   
                            <div class="form-group">
                                <a href="agregar_paciente" class="btn btn-success">Agregar Nuevo Paciente</a>
                            </div>
                            </hr>
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    Lista de Pacientes
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Acciones</th>
                                                    <th>Run</th>
                                                    <th>Email</th>
                                                    <th>Nombre</th>
                                                    <th>Apellido Paterno</th>
                                                    <th>Apellido Materno</th>
                                                    <th>Fecha Nacimiento</th>
                                                    <th>Sexo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="i" items="${pacientes}">
                                                    <tr>
                                                        <td>
                                                            <div class="col-sm-offset-2 col-sm-10">
                                                                <div class="form-group">        

                                                                    <a href="ver_paciente?id=${i.getPersona().getRun()}" class="btn btn-primary btn-xs">Ver</a>

                                                                    <a href="borrar_paciente?id=${i.getPersona().getRun()}" class="btn btn-danger btn-xs">Borrar</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>${i.getPersona().getRun()}</td>
                                                        <td>${i.getPersona().getEmail()}</td>
                                                        <td>${i.getPersona().getNombre()}</td>
                                                        <td>${i.getPersona().getApellido_paterno()}</td>
                                                        <td>${i.getPersona().getApellido_materno()}</td>
                                                        <td>${i.getPersona().getFecha_nacimiento()}</td>
                                                        <td>${i.getPersona().getSexo()}</td>
                                                    </tr>
                                                </c:forEach>  
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <!--End Advanced Tables -->

                        </div>    
                    </div>    


                </div>
                <!-- /. ROW  -->
                <hr />

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="assets/V2/js/jquery-1.10.2.js"></script>

        <script src="assets/V2/js/bootstrap.min.js"></script>
        <!-- METISMENU SCRIPTS -->
        <script src="assets/V2/js/jquery.metisMenu.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="assets/V2/js/custom.js"></script>


    </body>
</html>
