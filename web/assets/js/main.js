$(document).ready(function () {
    $('ul.nav li a:first').addClass('active-menu');
    $('.row .col-md-12').hide();
    $('.row .col-md-12:first').show();

    $('ul.nav li a').click(function () {
        $('ul.nav li a').removeClass('active-menu');
        $(this).addClass('active-menu');
        $('.row .col-md-12').hide();

        var activeTab = $(this).attr('href');
        console.log(activeTab);
        $(activeTab).show();
        return false;

    });



});
