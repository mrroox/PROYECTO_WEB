package poblamiento;

import org.apache.commons.lang3.RandomUtils;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import orm.Clienteterapeuta;
import orm.Paciente;
import orm.Persona;

/**
 *
 * Version 0.0.1.
 *
 * @author Felipe Quezada
 */
public class Utilidades {

    /**
     * Valida rut comprobando el digito verificador
     *
     * @param vrut Rut a comprobar
     * @param vverificador Supuesto digito verificador del rut
     * @return True si es que el digito verificador es correcto y false si no lo
     * es
     */
    public static boolean validarRut(String vrut, String vverificador) {
        boolean flag;
        String rut = vrut.trim();

        String posibleVerificador = vverificador.trim();
        int cantidad = rut.length();
        int factor = 2;
        int suma = 0;
        String verificador;

        for (int i = cantidad; i > 0; i--) {
            if (factor > 7) {
                factor = 2;
            }
            suma += (Integer.parseInt(rut.substring((i - 1), i))) * factor;
            factor++;

        }
        verificador = String.valueOf(11 - suma % 11);
        flag = verificador.equals(posibleVerificador) || (verificador.equals("10")) && (posibleVerificador.toLowerCase().equals("k")) || (verificador.equals("11") && posibleVerificador.equals("0"));
        return flag;
    }

    /**
     * Genera un rut aleatorio entre 10 millones y 21 millones con digito
     * verificador
     *
     * @return rut y su digito verificador segun modulo 11
     */
    public static String getRandomRut() {
        int randomNum = new Random().nextInt((21000000 - 10000000) + 1) + 10000000;
        String rut = String.valueOf(randomNum);
        int cantidad = rut.length();
        int factor = 2;
        int suma = 0;
        String verificador;
        for (int i = cantidad; i > 0; i--) {
            if (factor > 7) {
                factor = 2;
            }
            suma += (Integer.parseInt(rut.substring((i - 1), i))) * factor;
            factor++;

        }
        verificador = String.valueOf(11 - suma % 11);
        if (verificador.equals("11")) {
            verificador = "0";
        } else if (verificador.equals("10")) {
            verificador = "K";
        }
        return rut + verificador;
    }

    /**
     * Genera un apellido aleatorio.
     *
     * @return Apellido aleatorio
     */
    public static String genRandomApellido() {
        ArrayList<String> firstNames = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("files/nombre.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    firstNames.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Genera un apellido paterno aleatorio.
     *
     * @return Apellido aleatorio
     */
    public static String genRandomApellidoPaterno() {
        ArrayList<String> firstNames = new ArrayList<>();
        try (
                BufferedReader br = new BufferedReader(new FileReader("src/java/poblamiento/config/nombre.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    firstNames.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int fnRandom = ThreadLocalRandom.current().nextInt(0, firstNames.size());

        return firstNames.get(fnRandom);
    }

    /**
     * Genera un apellido paterno aleatorio.
     *
     * @return Apellido Materno aleatorio
     */
    public static String getRandomNameMaterno() {
        ArrayList<String> firstLastNames = new ArrayList<>();
        try (
                BufferedReader br = new BufferedReader(new FileReader("src/java/poblamiento/config/apellido.txt"))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    firstLastNames.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int fnRandom = ThreadLocalRandom.current().nextInt(0, firstLastNames.size());

        return firstLastNames.get(fnRandom);

    }

    /**
     * Genera una fecha aleatoria
     *
     * @return Una fecha aleatoria entre 1/1/2017 y el 31/12/2017
     */
    public static Date getRandomDate() {
        GregorianCalendar gc = new GregorianCalendar();

        int year = 2017;

        gc.set(gc.YEAR, year);

        int dayOfYear = RandomUtils.nextInt(1, gc.getActualMaximum(gc.DAY_OF_YEAR));

        gc.set(gc.DAY_OF_YEAR, dayOfYear);
        return gc.getTime();
    }

    /**
     * Lee el contenido de un archivo y lo entrega como string
     *
     * @param filePath Ruta del archivo
     * @return El contenido del archivo
     */
    public static String readFile(String filePath) {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

            String line;

            while ((line = br.readLine()) != null) {
                builder.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    /**
     *Metodo para leer desde archivo personas.csv
     * @return ArrayList de Clientesterapeuta
     */
    public static ArrayList<Clienteterapeuta> leerfromCSVPersonas() {
        //Primero tengo que leer la ruta de  los archivos Cliente.csv
        String ruta = "src/java/poblamiento/config/Personas.csv";
        String line = "";

        ArrayList<Clienteterapeuta> cliente = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ruta))) {
            while ((line = br.readLine()) != null) {

                String[] columna = line.split(",");

                System.out.println("" + columna[0] + " - " + columna[1]);

                if (!columna[0].equals("")) {
                    Clienteterapeuta C = new Clienteterapeuta();
                    Persona P = new Persona();

                    P.setNombre(columna[2]);
                    P.setApellido_paterno(columna[0]);
                    P.setApellido_materno(columna[1]);
                    P.setEmail(columna[3]);
                    P.setFecha_nacimiento(columna[4]);
                    if (columna[5] == "1") {
                        P.setSexo("masculino");
                    } else if (columna[5] == "0") {
                        P.setSexo("femenino");
                    }

                    C.setPersona(P);
                    C.setPassword(columna[6]);

                    cliente.add(C);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return cliente;
    }

    /**
     * Lee un archivo csv que consiente una lista de clientes
     *
     * @return ArrayList de Objetos orm Cliente.
     */
    public static ArrayList<PersonaPoblamiento> leerfromCSVClientes() {
        //Primero tengo que leer la ruta de  los archivos Cliente.csv
        String ruta = "src/java/poblamiento/config/Personas.csv";
        String line = "";
        String nombre = "";
        String a_paterno = "";
        String a_materno = "";
        String Email = "";
        String f_nacimiento = "";
        String Sexo = "";
        String pass = "";
        int cont = 0;
        ArrayList<PersonaPoblamiento> APP = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ruta))) {
            while ((line = br.readLine()) != null) {

                String[] columna = line.split(",");

                System.out.println("" + columna[0] + " - " + columna[1] + " - " + columna[2] + " - " + columna[3] + " - " + columna[4] + " - " + columna[5]);

                if (!columna[0].equals("")) {

                    nombre = columna[2];
                    a_paterno = columna[0];
                    a_materno = columna[1];
                    Email = columna[3];
                    f_nacimiento = columna[4];
                    if (columna[5].equals("1")) {
                        Sexo = "masculino";
                    } else if (columna[5].equals("0")) {
                        Sexo = "femenino";
                    }

                    pass = columna[6];
                    PersonaPoblamiento PP = new PersonaPoblamiento(Utilidades.getRandomRut(), nombre, a_paterno, a_materno, Email, f_nacimiento, Sexo, pass);

                    APP.add(PP);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return APP;
    }

    /**
     * Lee un archivo csv que contiene las regiones
     *
     * @return ArrayList de String
     */
    public static ArrayList<String> leerfromCSVRegiones() {

        String ruta = "src/java/poblamiento/config/regiones.csv";
        String line = "";
        String SplitBy = ",";
        ArrayList<String> regiones = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ruta))) {
            while ((line = br.readLine()) != null) {

                String[] columna = line.split(SplitBy);
                System.out.println("" + columna[0]);
                if (!columna[0].equals("")) {
                    regiones.add(columna[0]);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return regiones;
    }

    /**
     * Lee un archivo csv que contiene una lista de Productos
     *
     * @return ArrayList de Objetos Producto
     */
    public static ArrayList<Paciente> leerfromCSVPaciente() {

        String descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.";
        String ruta = "src/java/poblamiento/config/Paciente.csv";
        String line = "";

        ArrayList<Paciente> paciente = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ruta))) {
            while ((line = br.readLine()) != null) {

                String[] columna = line.split(",");

                System.out.println("" + columna[0] + " - " + columna[1]);

                if (!columna[0].equals("")) {
                    Paciente C = new Paciente();
                    Persona P = new Persona();

                    P.setNombre(columna[1]);
                    P.setApellido_paterno(columna[0]);
                    P.setApellido_materno(columna[0]);
                    P.setFecha_nacimiento(columna[3]);
                    if (columna[4] == "0") {
                        P.setSexo("femenino");
                    } else if (columna[4] == "1") {
                        P.setSexo("masculino");
                    }
                    C.setPersona(P);

                    paciente.add(C);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return paciente;

    }

    /**
     * Genera numero de telefonos random
     *
     * @return String numero generado.
     */
    public static String randomNumerosTelefonicos() {
        Random rng = new Random();

//int dig3 = rng.nextInt(900)+100; //siempre 3 digitos
        int numero = rng.nextInt(900000000) + 90000000;
        String num = "" + numero;

        return num;

    }

    /**
     * Lee un archivo txt que conteine las regiones.
     *
     * @return String de region al azar.
     */
    public static String TXTRegionesRandom() {

        String ruta = "src/java/poblamiento/config/regiones.txt";

        ArrayList<String> firstNames = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ruta))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    System.out.println(" lectura region " + line);
                    firstNames.add(line);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Hola soy el metodo CSVRegionesRandom");
            System.out.println(e);
        }
        int fnRandom = ThreadLocalRandom.current().nextInt(0, firstNames.size());

        return firstNames.get(fnRandom);
    }

    /**
     * Genera un numero al azar 1 ó 0.
     *
     * @return valor entero 1 ó 0.
     */
    public static int RandomCeroUno() {
        Random rng = new Random();

        int numero = rng.nextInt(2) + 0;

        return numero;

    }

    /**
     * Generara valores aleatorios para el precio de producto
     *
     * @return int con valor de producto
     */
    public static int RandomPrecio() {
        Random rng = new Random();
        int num = rng.nextInt(100000) + 100;
        return num;

    }

    /**
     * Lee un archivo txt que contiene la lista de fechas
     *
     * @return String de la fecha random
     */
    public static String randomTXTListaFechas() {

        String ruta = "src/java/poblamiento/config/ListaFechas.txt";

        ArrayList<String> fechas = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ruta))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    fechas.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

            System.out.println(e);
        }
        int fnRandom = ThreadLocalRandom.current().nextInt(0, fechas.size());

        return fechas.get(fnRandom);

    }

    /**
     * Lee un archivo txt que contiene la lista de fechas
     *
     * @return String de la fecha.
     */
    public static ArrayList<String> TXTListaFechas() {

        String ruta = "src/java/poblamiento/ListaFechas.txt";

        ArrayList<String> fechas = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ruta))) {
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                if (line != null) {
                    fechas.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Hola soy el metodo TXTListaFechas");
            System.out.println(e);
        }

        return fechas;

    }

}
