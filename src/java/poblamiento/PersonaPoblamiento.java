/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poblamiento;

/**
 *
 * @author Felipe Quezada
 */
class PersonaPoblamiento {

    @Override
    public String toString() {
        return "PersonaPoblamiento{" + "run=" + run + ", nombre=" + nombre + ", a_paterno=" + a_paterno + ", a_materno=" + a_materno + ", email=" + email + ", f_nacimiento=" + f_nacimiento + ", sexo=" + sexo + ", pass=" + pass + '}';
    }

    public String getRun() {
        return run;
    }

    public String getNombre() {
        return nombre;
    }

    public String getA_paterno() {
        return a_paterno;
    }

    public String getA_materno() {
        return a_materno;
    }

    public String getEmail() {
        return email;
    }

    public String getF_nacimiento() {
        return f_nacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public String getPass() {
        return pass;
    }
    private String run;
    private String nombre;
    private String a_paterno;
    private String a_materno;
    private String email;
    private String f_nacimiento;
    private String sexo;
    private String pass;

    PersonaPoblamiento(String randomRun, String Nombre, String a_p, String a_m, String Email, String f_n, String Sexo, String password) {
        this.run = randomRun;
        this.nombre = Nombre;
        this.a_paterno = a_p;
        this.a_materno = a_m;
        this.email = Email;
        this.f_nacimiento = f_n;
        this.sexo = Sexo;
        this.pass = password;

    }

}
