package poblamiento;

import business.ExNihilo;
import business.Search;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import orm.Apoderado;
import orm.Clienteterapeuta;
import orm.Direccion;
import orm.Expedientemedico;
import orm.Paciente;
import orm.Pais;
import orm.Persona;
import orm.Region;
import orm.Telefono;

/**
 * Version 0.0.1.
 *
 * @author Felipe Quezada
 */
public class CreateData {

    public static void main(String[] args) {

//Llenar Pais
//ExNihilo.crearPais("chile");
//llenarRegiones();
////        cargarClientesterapeutas();
////Metodos de prueba del funcionamiento.
////       cargarUnClienteTerapeuta();
////        cargarSimplePersona();
////Metodo para crear Clienteterapeuta, paciente y apoderado.
//       asignarClienteterapeutaPaciente();
    }

    /**
     * Metodo que ingresa una lista de clientes a la base de datos. Se lee un
     * archivo .csv a travez del metodo leerfromCSVClientes, del objeto
     * Utilidades
     */
    public static void cargarClientesterapeutas() {
        ArrayList<PersonaPoblamiento> arregloPersonas = new ArrayList<>();

        arregloPersonas = Utilidades.leerfromCSVClientes();

        for (int i = 0; i < arregloPersonas.size(); i++) {
            //Se crea un objeto telefono
            Telefono T = new Telefono();
            T.setCelular(Utilidades.randomNumerosTelefonicos());
            T.setRedfija(Utilidades.randomNumerosTelefonicos());

            //Se crea un objeto direccion
            Direccion D = new Direccion();
            D.setCalle("Francisco Salazar");
            D.setNumero("" + 300);

            Search.obtenerPais("chile");
            D.setPais(Search.obtenerPais("chile"));

            Search.obtenerRegion(Utilidades.TXTRegionesRandom());

            D.setRegion(Search.obtenerRegion(Utilidades.TXTRegionesRandom()));

            if (ExNihilo.crearClienteterapeuta(arregloPersonas.get(i).getRun(),
                    arregloPersonas.get(i).getNombre(),
                    arregloPersonas.get(i).getA_paterno(),
                    arregloPersonas.get(i).getA_materno(),
                    arregloPersonas.get(i).getEmail(),
                    arregloPersonas.get(i).getF_nacimiento(),
                    arregloPersonas.get(i).getSexo(), arregloPersonas.get(i).getPass(), T, D)) {
                System.out.println("Persona agregada :D");
            } else {
                System.out.println("Fallo agregacion D:");
            }

        }

    }

    /**
     * Metodo que sierve para ingresar las regiones a a la base de datos. Se lee
     * un archivo .csv a travez del metodo leerfromCSVRegiones desde el objeto
     * Utilidades.
     */
    public static void llenarRegiones() {

        for (int i = 0; i < Utilidades.leerfromCSVRegiones().size(); i++) {
            ExNihilo.crearRegion(Utilidades.leerfromCSVRegiones().get(i));
        }
    }

    /**
     *
     */
    public static void cargarSimplePersona() {
        //Se crea un objeto telefono
        Telefono T = new Telefono();
        T.setCelular(Utilidades.randomNumerosTelefonicos());
        T.setRedfija(Utilidades.randomNumerosTelefonicos());

        //Se crea un objeto direccion
        Direccion D = new Direccion();
        D.setCalle("Francisco Salazar");
        D.setNumero("" + 300);

        //Se crea un objeto Pais
        Pais Pais = new Pais();
        Pais.setNombre("Chile");
        D.setPais(Pais);

        //Se crea un objeto Region
        Region R = new Region();
        R.setNombre(Utilidades.TXTRegionesRandom());
        D.setRegion(R);

        ExNihilo.crearSimplePersona("987654321", "Simple Persona", "ApellidoPaterno Simple Persona", "ApellidoMaterno Simple Persona", "holamundoPersona@hola.com", "14-12-2017", "masculino", T, D);

    }

    /**
     *
     */
    public static void cargarUnClienteTerapeuta() {
        //Se crea un objeto telefono
        Telefono T = new Telefono();
        T.setCelular(Utilidades.randomNumerosTelefonicos());
        T.setRedfija(Utilidades.randomNumerosTelefonicos());

        //Se crea un objeto direccion
        Direccion D = new Direccion();
        D.setCalle("Francisco Salazar");
        D.setNumero("" + 300);

        //Se crea un objeto Pais
        D.setPais(Search.obtenerPais("chile"));

        //Se crea un objeto Region
        D.setRegion(Search.obtenerRegion(Utilidades.TXTRegionesRandom()));

        ExNihilo.crearClienteterapeuta(ExNihilo.crearPersona("1234556789123123", "Clienteterapeuta", "ApellidoPaterno", "ApellidoMaterno", "holamundo@hola.com", "14-11-2017", "masculino", T, D), "clav123123");

    }

    /**
     *
     */
    public static void asignarClienteterapeutaPaciente() {

        /*Logica del codigo:
        Tengo que buscar un terapeuta, crear una paciente, y crear un nuevo apoderado
        luego el paciente es asignado al terapeuta.   
         */
        ArrayList<PersonaPoblamiento> arregloPersonas = new ArrayList<>();

        arregloPersonas = Utilidades.leerfromCSVClientes();

        for (int i = 0; i < arregloPersonas.size(); i++) {
            //Se crea un objeto telefono
            Telefono T = new Telefono();
            T.setCelular(Utilidades.randomNumerosTelefonicos());
            T.setRedfija(Utilidades.randomNumerosTelefonicos());

            //Se crea un objeto direccion
            Direccion D = new Direccion();
            D.setCalle("Francisco Salazar");
            D.setNumero("" + i);

            Search.obtenerPais("chile");
            D.setPais(Search.obtenerPais("chile"));

            Search.obtenerRegion(Utilidades.TXTRegionesRandom());

            D.setRegion(Search.obtenerRegion(Utilidades.TXTRegionesRandom()));

            //Crear Persona Terapeuta.
            Persona Persona = ExNihilo.crearPersona(arregloPersonas.get(i).getRun(),
                    arregloPersonas.get(i).getNombre(),
                    arregloPersonas.get(i).getA_paterno(),
                    arregloPersonas.get(i).getA_materno(),
                    arregloPersonas.get(i).getEmail(),
                    arregloPersonas.get(i).getF_nacimiento(),
                    arregloPersonas.get(i).getSexo(), T, D);

            //crear Terapeuta.
            Clienteterapeuta Terapeuta = ExNihilo.crearClienteterapeuta(Persona, arregloPersonas.get(i).getPass());
            for (int j = 0; j < 10; j++) {
                int rand = ThreadLocalRandom.current().nextInt(0, arregloPersonas.size());
                int rand2 = ThreadLocalRandom.current().nextInt(0, arregloPersonas.size());
                //Se crea un objeto telefono
                Telefono T1 = new Telefono();
                T1.setCelular(Utilidades.randomNumerosTelefonicos());
                T1.setRedfija(Utilidades.randomNumerosTelefonicos());

                //Se crea un objeto direccion
                Direccion D1 = new Direccion();
                D1.setCalle("Calle Falsa");
                D1.setNumero("" + rand + 123);

                D1.setPais(Search.obtenerPais("chile"));

                D1.setRegion(Search.obtenerRegion(Utilidades.TXTRegionesRandom()));

                //Crear Persona Paciente.
                Persona Persona2Paciente = ExNihilo.crearPersona(arregloPersonas.get(rand).getRun(),
                        arregloPersonas.get(rand).getNombre(),
                        arregloPersonas.get(rand).getA_paterno(),
                        arregloPersonas.get(rand).getA_materno(),
                        arregloPersonas.get(rand).getEmail(),
                        arregloPersonas.get(rand).getF_nacimiento(),
                        arregloPersonas.get(rand).getSexo(), T1, D1);

                //crear Expediente
                Expedientemedico expediente = ExNihilo.crearExpedienteMedico("Observacion HOLA MUNDO", "Patologia HOLA MUNDO");

                //crear Paciente.
                Paciente Paciente = ExNihilo.crearPaciente(Persona2Paciente, expediente);

                //crear Apoderado.
                //Se crea un objeto telefono
                Telefono T2 = new Telefono();
                T2.setCelular(Utilidades.randomNumerosTelefonicos());
                T2.setRedfija(Utilidades.randomNumerosTelefonicos());

                //Se crea un objeto direccion
                Direccion D2 = new Direccion();
                D2.setCalle("Calle Falsa Dos");
                D2.setNumero("" + rand + 123);

                D2.setPais(Search.obtenerPais("chile"));

                D2.setRegion(Search.obtenerRegion(Utilidades.TXTRegionesRandom()));
                Persona Persona2Apoderado = ExNihilo.crearPersona(arregloPersonas.get(rand2).getRun(),
                        arregloPersonas.get(rand2).getNombre(),
                        arregloPersonas.get(rand2).getA_paterno(),
                        arregloPersonas.get(rand2).getA_materno(),
                        arregloPersonas.get(rand2).getEmail(),
                        arregloPersonas.get(rand2).getF_nacimiento(),
                        arregloPersonas.get(rand2).getSexo(), T2, D2);

                Apoderado Apoderado = ExNihilo.crearApoderado(Persona2Apoderado);
                //asignar Apoderado a Paciente.
                ExNihilo.asignarApoderadoPaciente(Apoderado, Paciente);
                //asignar Paciente a Terapeuta.
                ExNihilo.asignarPacienteClienteTerapeuta(Terapeuta, Paciente, "2017-11-14");
            }

        }

    }

}
