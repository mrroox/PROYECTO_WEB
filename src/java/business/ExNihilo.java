package business;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.*;

/**
 * Ex nihilo es una locución latina traducible por "de la nada" o "desde la
 * nada". En filosofía y teología, suele emplearse en la expresión creatio ex
 * nihilo, haciendo referencia a aquello que se crea a partir de la nada. Dentro
 * del ámbito científico, el concepto fue utilizado por la teoría de la
 * generación espontánea para explicar la aparición de larvas de insectos y de
 * gusanos en la materia en descomposición.
 *
 * Clase para crear nuevas entidades.
 *
 * Version 0.0.1.
 *
 * @author Felipe Quezada
 */
public class ExNihilo {

    /**
     * Asigna un Apoderado a un Paciente.
     *
     * @param lormApoderado Objeto Apoderado
     * @param lormPaciente Objeto Paciente
     * @return true si es que se ha asignado correctamente.
     */
    public static boolean asignarApoderadoPaciente(Apoderado lormApoderado, Paciente lormPaciente) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {

                Apoderado_paciente lormApoderado_paciente = orm.Apoderado_pacienteDAO.createApoderado_paciente();
                lormApoderado_paciente.setApoderado(lormApoderado);
                lormApoderado_paciente.setPaciente(lormPaciente);
                Apoderado_pacienteDAO.save(lormApoderado_paciente);

                t.commit();

                return true;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            System.out.println("Error 2 =" + e);
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Asigna un Paciente a un Cliente Terapeuta.
     *
     * @param lormClienteTerapeuta Objeto ClienteTerpeuta
     * @param lormPaciente Objeto Paciente
     * @param fecha String valor de fecha
     * @return true si la asignacion se realizo con exito.
     */
    public static boolean asignarPacienteClienteTerapeuta(Clienteterapeuta lormClienteTerapeuta, Paciente lormPaciente, String fecha) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                Clienteterapeuta_paciente lormClienteterapeuta_Paciente = orm.Clienteterapeuta_pacienteDAO.createClienteterapeuta_paciente();
                lormClienteterapeuta_Paciente.setClienteterapeuta(lormClienteTerapeuta);
                lormClienteterapeuta_Paciente.setPaciente(lormPaciente);
                lormClienteterapeuta_Paciente.setFecha_ingreso(fecha);
                Clienteterapeuta_pacienteDAO.save(lormClienteterapeuta_Paciente);

                t.commit();

                return true;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            System.out.println("Error 2 =" + e);
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Asigna un Pacienta a un Terapeuta.
     *
     * @param lormTerapeuta Objeto Terapeuta
     * @param lormPaciente Objeto Paciente
     * @param fecha String fecha
     * @return true si es la asignacion sea ha realizado correctamente.
     */
    public static boolean asignarPacienteTerapeuta(Terapeuta lormTerapeuta, Paciente lormPaciente, String fecha) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                Paciente_terapeuta lormPacienteTerapeuta = Paciente_terapeutaDAO.createPaciente_terapeuta();
                lormPacienteTerapeuta.setPaciente(lormPaciente);
                lormPacienteTerapeuta.setTerapeuta(lormTerapeuta);
                lormPacienteTerapeuta.setFecha_ingreso(fecha);
                Paciente_terapeutaDAO.save(lormPacienteTerapeuta);

                t.commit();

                return true;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            System.out.println("Error 2 =" + e);
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Crea una nueva Persona
     *
     * @param run String run de persona
     * @param nombre String nombre de persona
     * @param a_paterno String apellido paterno 
     * @param a_materno String apellido materno
     * @param email String Email
     * @param f_nacimiento String fecha nacimiento
     * @param sexo String sexo
     * @param T Objeto Telefono
     * @param D Objeto Direccion
     * @return true si se ha creado satiscatoriamente, false si no.
     */
    public static boolean crearSimplePersona(String run, String nombre,
            String a_paterno, String a_materno, String email,
            String f_nacimiento, String sexo, Telefono T, Direccion D) {

        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                //Se intancia una Persona.
                orm.Persona lormPersona = orm.PersonaDAO.createPersona();

                lormPersona.setRun(run);
                lormPersona.setNombre(nombre);
                lormPersona.setApellido_paterno(a_paterno);
                lormPersona.setApellido_materno(a_materno);
                lormPersona.setEmail(email);
                lormPersona.setFecha_nacimiento(f_nacimiento);
                lormPersona.setSexo(sexo);

                //Direccion
                lormPersona.setTelefono(T);
                lormPersona.setDireccion(D);
                //Telefono
                orm.TelefonoDAO.save(T);
                orm.DireccionDAO.save(D);

                orm.PersonaDAO.save(lormPersona);

                t.commit();

                return true;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            System.out.println("Error 2 =" + e);
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Crea una nueva Persona.
     *
     * @param run String run Person 
     * @param nombre String nombre Persona
     * @param a_paterno String apellido paterno
     * @param a_materno String apellido materno
     * @param email String Email
     * @param f_nacimiento String fecha nacimiento
     * @param sexo String Sexo
     * @param T Objeto Telefono
     * @param D Objeto Direccion
     * @return Objeto de tipo Persona.
     */
    public static orm.Persona crearPersona(String run, String nombre,
            String a_paterno, String a_materno, String email,
            String f_nacimiento, String sexo, Telefono T, Direccion D) {

        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                //Se intancia una Persona.
                orm.Persona lormPersona = orm.PersonaDAO.createPersona();

                lormPersona.setRun(run);
                lormPersona.setNombre(nombre);
                lormPersona.setApellido_paterno(a_paterno);
                lormPersona.setApellido_materno(a_materno);
                lormPersona.setEmail(email);
                lormPersona.setFecha_nacimiento(f_nacimiento);
                lormPersona.setSexo(sexo);

                //Direccion
                lormPersona.setTelefono(T);
                lormPersona.setDireccion(D);
                //Telefono
                orm.TelefonoDAO.save(T);
                orm.DireccionDAO.save(D);

                orm.PersonaDAO.save(lormPersona);

                t.commit();

                return lormPersona;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return null;
            }
        } catch (PersistentException e) {
            System.out.println("Error 2 =" + e);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Se crea un nuevo Cliente terapeuta, para crearlo se necesita tambien
     * crear una persona.
     *
     * @param run String run
     * @param nombre String nombre Clienteterapeuta
     * @param a_paterno String apellido paterno Clienteterapeuta
     * @param a_materno String apellido materno Clienteterapeuta
     * @param email String email
     * @param f_nacimiento String fecha nacimiento
     * @param sexo String sexo
     * @param pass String password
     * @param T Objeto Telefono
     * @param D Objeto Direccon
     * @return true si se ha creado satiscatoriamente, false si no.
     */
    public static boolean crearClienteterapeuta(String run, String nombre,
            String a_paterno, String a_materno, String email,
            String f_nacimiento, String sexo, String pass, Telefono T, Direccion D) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                //Se instancia una Persona.
                orm.Persona lormPersona = orm.PersonaDAO.createPersona();

                //Se instancia un Clienteterapeuta.
                orm.Clienteterapeuta lormClienteterapeuta = orm.ClienteterapeutaDAO.createClienteterapeuta();

                lormPersona.setRun(run);
                lormPersona.setNombre(nombre);
                lormPersona.setApellido_paterno(a_paterno);
                lormPersona.setApellido_materno(a_materno);
                lormPersona.setEmail(email);
                lormPersona.setFecha_nacimiento(f_nacimiento);
                lormPersona.setSexo(sexo);

                orm.TelefonoDAO.save(T);
                orm.DireccionDAO.save(D);

                lormPersona.setTelefono(T);
                lormPersona.setDireccion(D);

                lormClienteterapeuta.setPassword(pass);
                lormPersona.setClienteterapeuta(lormClienteterapeuta);

                orm.PersonaDAO.save(lormPersona);

                t.commit();

                return true;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            System.out.println("Error 2 =" + e);
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Crea un nuevo Clienteterapeuta.
     *
     * @param lormPersona Objeto Persona
     * @param pass String password
     * @return verdadero si se ha creado satisfactoriamente.
     */
    public static Clienteterapeuta crearClienteterapeuta(Persona lormPersona, String pass) {

        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {

                //Se instancia un Clienteterapeuta.
                orm.Clienteterapeuta lormClienteterapeuta = orm.ClienteterapeutaDAO.createClienteterapeuta();

                lormClienteterapeuta.setPassword(pass);
                lormClienteterapeuta.setPersona(lormPersona);
                orm.ClienteterapeutaDAO.save(lormClienteterapeuta);

                t.commit();

                return lormClienteterapeuta;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return null;
            }
        } catch (PersistentException e) {
            System.out.println("Error 2 =" + e);
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Crear un nuevo Paciente.
     *
     * @param run String run Paciente
     * @param nombre String nombre Paciente
     * @param a_paterno String apellido paterno
     * @param a_materno String apellido materno
     * @param email String email
     * @param f_nacimiento String fechanacimiento
     * @param sexo String sexo
     * @param T Objeto Telefono
     * @param D Objeto Direccion
     * @return
     */
    public static boolean crearPaciente(String run, String nombre,
            String a_paterno, String a_materno, String email,
            String f_nacimiento, String sexo, Telefono T, Direccion D) {

        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                //Se instancia una Persona.
                orm.Persona lormPersona = orm.PersonaDAO.createPersona();

                //Se instancia un Paciente.
                orm.Paciente lormPaciente = orm.PacienteDAO.createPaciente();

                lormPersona.setRun(run);
                lormPersona.setNombre(nombre);
                lormPersona.setApellido_paterno(a_paterno);
                lormPersona.setApellido_materno(a_materno);
                lormPersona.setEmail(email);
                lormPersona.setFecha_nacimiento(f_nacimiento);
                lormPersona.setSexo(sexo);

                //Direccion
                lormPersona.setTelefono(T);
                lormPersona.setDireccion(D);
                //Telefono
                orm.TelefonoDAO.save(T);
                orm.DireccionDAO.save(D);
                //La persona se asigna como Paciente.
                lormPersona.setPaciente(lormPaciente);

                orm.PersonaDAO.save(lormPersona);

                t.commit();

                return true;

            } catch (Exception e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Crea un nuevo Paciente a partir de una persona.
     *
     * @param lormPersona Objeto Persona
     * @param expediente Objeto Expediente
     * @return Objeto de tipo Persona.
     */
    public static Paciente crearPaciente(Persona lormPersona, Expedientemedico expediente) {

        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                orm.Paciente lormPaciente = orm.PacienteDAO.createPaciente();
                //Al paciente se le asigna como persona.
                lormPaciente.setPersona(lormPersona);
                lormPaciente.setExpedientemedico(expediente);
                orm.PacienteDAO.save(lormPaciente);

                t.commit();

                return lormPaciente;

            } catch (Exception e) {
                t.rollback();
                return null;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     *Metodo que crea un expediente Medico
     * 
     * @param observaciones String observaciones
     * @param patologia String patologia
     * @return 
     */
    public static Expedientemedico crearExpedienteMedico(String observaciones, String patologia) {

        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                orm.Expedientemedico lormExpedientemedico = orm.ExpedientemedicoDAO.createExpedientemedico();

                lormExpedientemedico.setObservacion(observaciones);
                lormExpedientemedico.setPatologia(patologia);

                orm.ExpedientemedicoDAO.save(lormExpedientemedico);

                t.commit();

                return lormExpedientemedico;

            } catch (Exception e) {
                t.rollback();
                return null;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Crea un nuevo Apoderado.
     *
     * @param run String run Apoderado
     * @param nombre String nombre Apoderado
     * @param a_paterno String apellido paterno
     * @param a_materno String apellido materno
     * @param email String email
     * @param f_nacimiento String fecha nacimiento
     * @param sexo String sexo
     * @param T Objeto telefono
     * @param D Objeto direccion
     * @return
     */
    public static boolean crearApoderado(String run, String nombre,
            String a_paterno, String a_materno, String email,
            String f_nacimiento, String sexo, Telefono T, Direccion D) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                //Se instancia una Persona.
                orm.Persona lormPersona = orm.PersonaDAO.createPersona();
                //Se instancia un Apoderado.
                orm.Apoderado lormApoderado = orm.ApoderadoDAO.createApoderado();

                lormPersona.setRun(run);
                lormPersona.setNombre(nombre);
                lormPersona.setApellido_paterno(a_paterno);
                lormPersona.setApellido_materno(a_materno);
                lormPersona.setEmail(email);
                lormPersona.setFecha_nacimiento(f_nacimiento);
                lormPersona.setSexo(sexo);

                //Direccion
                lormPersona.setTelefono(T);
                lormPersona.setDireccion(D);
                //Telefono
                orm.TelefonoDAO.save(T);
                orm.DireccionDAO.save(D);
                //La persona se asigna como Apoderado.
                lormApoderado.setPersona(lormPersona);

                orm.PersonaDAO.save(lormPersona);

                t.commit();

                return true;

            } catch (Exception e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Crea un nuevo Apoderado.
     *
     * @param lormPersona Objeto de tipo Persona
     * @return Objeto Apoderado.
     */
    public static Apoderado crearApoderado(Persona lormPersona) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {

                //Se instancia un Apoderado.
                orm.Apoderado lormApoderado = orm.ApoderadoDAO.createApoderado();

                //La persona se asigna como Apoderado.
                lormApoderado.setPersona(lormPersona);

                orm.PersonaDAO.save(lormPersona);

                t.commit();

                return lormApoderado;

            } catch (Exception e) {
                t.rollback();
                return null;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Crea una nueva Organizacion.
     *
     * @param rut String rut organizacion
     * @param nombre String nombre de organizacion
     * @param descripcion String descripcion
     * @param razon_social  String razon social de organizacion
     * @param fax String fax de oraganizacion
     * @param email String email
     * @param web String direccion de pagina web
     * @param telefono Objeto Telefono 
     * @param direccion Objeto Direccion
     * @return Retorna un booleando indicando si la creacion fue correcta.
     */
    public static boolean crearOrganizacion(String rut, String nombre,
            String descripcion, String razon_social, String fax, String email,
            String web, Telefono telefono, Direccion direccion) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
//                if (Search.buscarProducto(nombre) == null) {

                orm.Organizacion lormOrganizacion = orm.OrganizacionDAO.createOrganizacion();
                lormOrganizacion.setRut(rut);
                lormOrganizacion.setNombre(nombre);
                lormOrganizacion.setDescripcion(descripcion);
                lormOrganizacion.setRazon_social(razon_social);
                lormOrganizacion.setFax(fax);
                lormOrganizacion.setEmail(email);
                lormOrganizacion.setWeb(web);
                lormOrganizacion.setTelefono(telefono);
                lormOrganizacion.setDireccion(direccion);

                orm.OrganizacionDAO.save(lormOrganizacion);
                t.commit();
                return true;
//                } else {
//                    t.rollback();
//                    return false;
//                }
            } catch (Exception e) {
                System.out.println(e);
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            System.out.println(e);
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Crea una nueva Organizacion.
     *
     * @param rut String rut organizacion   
     * @param nombre String nombre organizacion
     * @param descripcion String descripcion
     * @param razon_social String
     * @param fax String 
     * @param email String 
     * @param web String web
     * @param telefono Objeto Telefono
     * @param direccion Objeto Direccion
     * @return Retorna un booleando indicando si la creacion fue correcta.
     */
    public static Organizacion crearOrganizacionOb(String rut, String nombre,
            String descripcion, String razon_social, String fax, String email,
            String web, Telefono telefono, Direccion direccion) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                if (Search.buscarOrganizacion(rut) == null) {

                    orm.Organizacion lormOrganizacion = orm.OrganizacionDAO.createOrganizacion();
                    lormOrganizacion.setRut(rut);
                    lormOrganizacion.setNombre(nombre);
                    lormOrganizacion.setDescripcion(descripcion);
                    lormOrganizacion.setRazon_social(razon_social);
                    lormOrganizacion.setFax(fax);
                    lormOrganizacion.setEmail(email);
                    lormOrganizacion.setWeb(web);
                    lormOrganizacion.setTelefono(telefono);
                    lormOrganizacion.setDireccion(direccion);

                    orm.OrganizacionDAO.save(lormOrganizacion);
                    t.commit();
                    return lormOrganizacion;
                } else {
                    t.rollback();
                    return null;
                }
            } catch (Exception e) {
                System.out.println(e);
                t.rollback();
                return null;
            }
        } catch (PersistentException e) {
            System.out.println(e);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Se crea un nuevo Terapeuta.
     *
     * @param run
     * @param nombre
     * @param a_paterno
     * @param a_materno
     * @param email
     * @param f_nacimiento
     * @param sexo
     * @param T
     * @param D
     * @param password
     * @return
     */
    public static boolean crearTerapeuta(String run, String nombre,
            String a_paterno, String a_materno, String email,
            String f_nacimiento, String sexo, Telefono T, Direccion D, String password) {

        //Aqui es donde necesito asignarle la organizacion al terapeuta.
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                //Se instancia una Persona.
                orm.Persona lormPersona = orm.PersonaDAO.createPersona();

                //Se instancia un Terapeuta.
                // orm.Clienteterapeuta lormClienteterapeuta = orm.ClienteterapeutaDAO.createClienteterapeuta();
                orm.Terapeuta lormTerapeuta = orm.TerapeutaDAO.createTerapeuta();

                lormPersona.setRun(run);
                lormPersona.setNombre(nombre);
                lormPersona.setApellido_paterno(a_paterno);
                lormPersona.setApellido_materno(a_materno);
                lormPersona.setEmail(email);
                lormPersona.setFecha_nacimiento(f_nacimiento);
                lormPersona.setSexo(sexo);

                //Direccion
                lormPersona.setTelefono(T);
                lormPersona.setDireccion(D);
                //Telefono
                orm.TelefonoDAO.save(T);
                orm.DireccionDAO.save(D);

                lormTerapeuta.setPassword(password);
                //La persona se asigna como Terapeuta
                lormPersona.setTerapeuta(lormTerapeuta);

                orm.PersonaDAO.save(lormPersona);

                t.commit();

                return true;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return false;

            }
        } catch (PersistentException e) {
            System.out.println("Error 2 = " + e);
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Se crea un nuevo Terapeuta.
     *
     * @param lormOrganizacion
     * @param lormPersona
     * @param password
     * @return obtejero Terapeuta
     */
    public static Terapeuta crearTerapeuta(Organizacion lormOrganizacion, Persona lormPersona, String password) {

        //Aqui es donde necesito asignarle la organizacion al terapeuta.
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                //Se instancia un Terapeuta.
                //orm.Clienteterapeuta lormClienteterapeuta = orm.ClienteterapeutaDAO.createClienteterapeuta();
                orm.Terapeuta lormTerapeuta = orm.TerapeutaDAO.createTerapeuta();

                lormTerapeuta.setPassword(password);
                //Al terapeuta se le asigana como persona.
                lormTerapeuta.setPersona(lormPersona);
                //Al terapeuta se le asigna una Organizacion.
                lormTerapeuta.setOrganizacion(lormOrganizacion);
                orm.TerapeutaDAO.save(lormTerapeuta);

                t.commit();

                return lormTerapeuta;

            } catch (Exception e) {
                System.out.println("Error 1 = " + e);
                t.rollback();
                return null;

            }
        } catch (PersistentException e) {
            System.out.println("Error 2 = " + e);
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Crea un nuevo dispositivo
     *
     * @param nombre
     * @param descripcion
     * @param num_serie
     * @param tipo
     * @param datos_dispositivo
     * @return
     */
    public static boolean crearDispositivo(String nombre, String descripcion, String num_serie, Tipodispositivo tipo, Datosdispositivo datos_dispositivo) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                orm.Dispositivo lormDispositivo = orm.DispositivoDAO.createDispositivo();

                lormDispositivo.setNombre(nombre);
                lormDispositivo.setDescripcion(descripcion);
                lormDispositivo.setNumserie(num_serie);
                lormDispositivo.setTipodispositivo(tipo);
                lormDispositivo.setDotosdispositivo(datos_dispositivo);

                orm.DispositivoDAO.save(lormDispositivo);

                t.commit();
                return true;

            } catch (Exception e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Crea Telefono
     *
     * @param celular
     * @param redfija
     * @return
     */
    public static boolean crearTelefono(String celular, String redfija) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {

                orm.Telefono Telefono = orm.TelefonoDAO.createTelefono();
                Telefono.setCelular(celular);
                Telefono.setRedfija(redfija);
                orm.TelefonoDAO.save(Telefono);
                t.commit();
                return true;

            } catch (Exception e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Crea Direccion
     *
     * @param calle
     * @param numero
     * @param region
     * @param pais
     * @return
     */
    public static boolean crearDireccion(String calle, String numero, Region region, Pais pais) {

        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                orm.Direccion Direccion = orm.DireccionDAO.createDireccion();
                Direccion.setCalle(calle);
                Direccion.setNumero(numero);

                Direccion.setRegion(region);
                Direccion.setPais(pais);

                orm.DireccionDAO.save(Direccion);
                t.commit();
                return true;

            } catch (Exception e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Crea Region
     *
     * @param nombre
     * @return
     */
    public static boolean crearRegion(String nombre) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {

                orm.Region Region = orm.RegionDAO.createRegion();
                Region.setNombre(nombre);

                orm.RegionDAO.save(Region);
                t.commit();
                return true;

            } catch (Exception e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Crea Pais
     *
     * @param nombre
     * @return
     */
    public static boolean crearPais(String nombre) {
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {

                orm.Pais Pais = orm.PaisDAO.createPais();
                Pais.setNombre(nombre);

                orm.PaisDAO.save(Pais);
                t.commit();
                return true;

            } catch (Exception e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     *
     * @return
     */
    public static boolean crearCargo() {

        return false;
    }

}
