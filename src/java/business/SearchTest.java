package business;

import static business.Search.obtenerListaPacienteCLienteTerapeuta;
import java.util.HashSet;
import java.util.Iterator;
import orm.Clienteterapeuta;
import orm.Clienteterapeuta_paciente;
import orm.Paciente;

/**
 *
 * @author Felipe Quezada
 */
public class SearchTest {

    public static void main(String[] args) {
        String testRun1 = "209744864";

        if (Search.buscarClienteTerapeuta(testRun1) == null) {
            System.out.println("Busqueda con resultado Null");

        } else {

            System.out.println("El resultado es = " + Search.buscarClienteTerapeuta(testRun1).getPassword());
            System.out.println(Search.buscarClienteTerapeuta(testRun1).clienteterapeuta_paciente.getCollection().toString());

            obtenerListaPacienteCLienteTerapeuta(Search.buscarClienteTerapeuta(testRun1));

//
//            System.out.println(" - Lista de mandado con " + Search.buscarClienteTerapeuta(testRun1).clienteterapeuta_paciente.getCollection().size());
//
//            // Definir Iterator para extraer/imprimir valores 
//            for (Iterator it2 = Search.buscarClienteTerapeuta(testRun1).clienteterapeuta_paciente.getCollection().iterator(); it2.hasNext();) {
//
//                Clienteterapeuta_paciente x = (Clienteterapeuta_paciente) it2.next();
//                System.out.println(x.getClienteterapeuta().getPersona().getNombre() + " : " + x.getClienteterapeuta().getPersona().getRun());
//                System.out.println(x.getPaciente().getPersona().getNombre() + " : " + x.getPaciente().getPersona().getRun());
//
//            }
        }

    }
}
