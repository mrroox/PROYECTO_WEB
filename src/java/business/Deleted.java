package business;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.Paciente;

/**
 * Version 0.0.1.
 *
 * @author unknown
 */
public class Deleted {
    
    public static void main(String[] args){
        String run="209536668";
    Paciente  P = Search.obtenerPaciente(run);
                if (Deleted.eliminarPaciente(P)) {
                    System.out.println("paciente Eliminado");
            
        }else{
                
                    System.out.println("Paciente NO eliminado");}
    }

    /**
     * Elimina un Paciente.
     *
     * @param lormPaciente Objeto tipo Paciente.
     * @return Objeto de tipo Persona.
     */
    public static boolean eliminarPaciente(Paciente lormPaciente) {
        boolean estado = false;
        try {
            PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();

            try {
                System.out.println("Paciente => "+lormPaciente.toString());
                orm.PacienteDAO.delete(lormPaciente);
                t.commit();
                    estado = true;

            } catch (Exception e) {
                System.out.println("ERROR 1 "+e);
                t.rollback();
                estado = false;
            }
        } catch (PersistentException e) {
            System.out.println("ERRO 2 "+e);
            e.printStackTrace();
        }
        return estado;

    }

}
