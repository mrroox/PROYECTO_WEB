package business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import orm.Clienteterapeuta;
import orm.ClienteterapeutaDAO;
import orm.Clienteterapeuta_paciente;
import orm.Organizacion;
import orm.OrganizacionDAO;
import orm.Paciente;
import orm.PacienteDAO;
import orm.Pais;
import orm.PaisDAO;
import orm.Persona;
import orm.Region;

/**
 * Clase que sirve para realizar busquedas de Entidades.
 *
 * Version 0.0.1.
 *
 * @author Felipe Quezada
 */
public class Search {

    private static final int ROW_COUNT = 100;
    
    
    /**
     * Busca un Paciente.
     * 
     * @param run
     * @return 
     */
    
    public static Paciente obtenerPaciente(String run){
        if (run != null) {
            String query = "Paciente.persona.run = '" + run + "'";
            try {
                return PacienteDAO.loadPacienteByQuery(query, null);
            } catch (PersistentException e) {
                e.printStackTrace();
            }
        }
        return null;
    
    
    }

    /**
     * Busca todos los Pacientes asosiados a un Clienteterapeuta.
     *
     * @param clienteterapeuta ArralyList de Paciente.
     * @return ArrayList<> de tipo Pacientes.
     */
    public static ArrayList<Paciente> obtenerListaPacienteCLienteTerapeuta(Clienteterapeuta clienteterapeuta) {

        // Definir un ArrayList para lista Objetos Paciente.
        ArrayList<Paciente> listaPacientes = new ArrayList<>();
        //Se instancia una nueva Collection de Objetos.
        Collection lista = buscarClienteTerapeuta(clienteterapeuta.getPersona().getRun()).clienteterapeuta_paciente.getCollection();

        // Definir Iterator para extraer/imprimir valores 
        for (Iterator it2 = lista.iterator(); it2.hasNext();) {

            Clienteterapeuta_paciente x = (Clienteterapeuta_paciente) it2.next();
//            System.out.println(x.getClienteterapeuta().getPersona().getNombre() + " : " + x.getClienteterapeuta().getPersona().getRun());
//            System.out.println(x.getPaciente().getPersona().getNombre() + " : " + x.getPaciente().getPersona().getRun());
            listaPacientes.add(x.getPaciente());
        }

        return listaPacientes;
    }

    /**
     * Busca una Region
     *
     * @param nombre String del nombre de la region a buscar.
     * @return
     */
    public static Region obtenerRegion(String nombre) {
        if (nombre != null) {
            String query = "Region.nombre='" + nombre + "'";
            try {
                return orm.RegionDAO.loadRegionByQuery(query, null);

            } catch (PersistentException e) {
                System.out.println("Error Obeteniendo Region " + e);

                e.printStackTrace();
            }
        }
        return null;

    }

    /**
     * Busca un pais.
     *
     * @param nombre String del nombre del pasi a buscar.
     * @return objeto orm.Pais
     */
    public static Pais obtenerPais(String nombre) {
        if (nombre != null) {
            String query = "Pais.nombre='" + nombre + "'";
            try {
                
                return PaisDAO.loadPaisByQuery(query, null);
                
            } catch (PersistentException e) {
                System.out.println("Error en metodo obtenerPais = " + e);
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Busca una direccion de una Persona.
     *
     * @param persona
     * @return objeto direccion de la Persona.
     */
    public static orm.Direccion obtenerDireccion(Persona persona) {

        if (persona != null) {
            String query = "Persona.run='" + persona.getRun() + "'";
            try {
                return orm.DireccionDAO.loadDireccionByQuery(query, null);

            } catch (PersistentException e) {
                System.out.println("Error en metodo obtenerDireccion = " + e);
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Busca la el Telefono de una Persona.
     *
     * @param persona
     * @return
     */
    public static orm.Telefono obtenerTelefono(Persona persona) {

        if (persona != null) {
            String query = "Persona.run='" + persona.getRun() + "'";
            try {
                return orm.TelefonoDAO.loadTelefonoByQuery(query, null);

            } catch (PersistentException e) {

                e.printStackTrace();
            }
        }
        return null;

    }

    /**
     * Busca un Cliente Terapeuta
     *
     * @param run del Cliente terapeuta.
     * @return Objeto Cliente terapeuta, null si no existe.
     */
    public static Clienteterapeuta buscarClienteTerapeuta(String run) {

        if (run != null) {
            String query = "Clienteterapeuta.persona.run = '" + run + "'";
            try {
                return ClienteterapeutaDAO.loadClienteterapeutaByQuery(query, null);
            } catch (PersistentException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     *
     *
     * @return
     */
    public static Organizacion buscarOrganizacion(String rut) {
        if (rut != null) {
            String query = "Organizacion.rut='" + rut + "'";
            try {
                return OrganizacionDAO.loadOrganizacionByQuery(query, null);
            } catch (PersistentException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    /**
     * Busca todos los ClienteTerapeuta.
     *
     * @return arreglo de ClineteTerapeuta registrados.
     */
    public static Clienteterapeuta[] buscarListaTotalClienteTerapeuta() {
        orm.Clienteterapeuta[] terapeuta = null;
        try {

            System.out.println("Listing Clienteterapeuta...");
            orm.Clienteterapeuta[] Clienteterapeuta = orm.ClienteterapeutaDAO.listClienteterapeutaByQuery(null, null);
            int length = Math.min(Clienteterapeuta.length, ROW_COUNT);
            for (int i = 0; i < length; i++) {
                System.out.println(Clienteterapeuta[i].getPersona().getNombre());
            }
            terapeuta = Clienteterapeuta;
            System.out.println(length + " record(s) retrieved.");
        } catch (PersistentException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return terapeuta;
    }

    /**
     * Busca una arreglo de ClienteTerapeuta
     *
     * @param run String correspondiente al run de un ClineteTerapeuta.
     * @return Arreglo de ClienteTerapeuta con ese run.
     */
    public static Clienteterapeuta[] buscarListaClienteTerapeuta(String run) {
        orm.Clienteterapeuta[] terapeuta = null;
        try {
            String query = "Clienteterapeuta.persona.run=" + run;
            System.out.println("Listing Clienteterapeuta...");
            orm.Clienteterapeuta[] Clienteterapeuta = orm.ClienteterapeutaDAO.listClienteterapeutaByQuery(query, null);
            int length = Math.min(Clienteterapeuta.length, ROW_COUNT);
            for (int i = 0; i < length; i++) {
                System.out.println(Clienteterapeuta[i].getPersona().getNombre());
            }
            terapeuta = Clienteterapeuta;
            System.out.println(length + " record(s) retrieved.");
        } catch (PersistentException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return terapeuta;
    }

    /**
     *
     * @param nombre
     * @return
     */
    static Paciente[] buscarPasiente(Clienteterapeuta CTerapeuta, String nombre) {
        orm.Paciente[] Paciente = null;
        String query = "Paciente.nombre=" + nombre;
        if (CTerapeuta != null) {
            try {

                System.out.println("Listing Paciente...");
                orm.Paciente[] ormPaciente = orm.PacienteDAO.listPacienteByQuery(query, null);
                int length = Math.min(Paciente.length, ROW_COUNT);
                for (int i = 0; i < length; i++) {
                    System.out.println(ormPaciente[i]);
                }

                System.out.println(length + " record(s) retrieved.");

                return ormPaciente;

            } catch (PersistentException ex) {
                Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        return null;
    }

    public static Region[] buscarRegion(String nombre) {
        orm.Region[] region = null;
        try {

            String query = "Region.nombre='" + nombre + "'";
            System.out.println("Listing region...");
            orm.Region[] regiones = orm.RegionDAO.listRegionByQuery(query, null);
            int length = Math.min(regiones.length, ROW_COUNT);
            for (int i = 0; i < length; i++) {
                System.out.println(regiones[i].getNombre());
            }
            region = regiones;
            System.out.println(length + " record(s) retrieved.");
        } catch (PersistentException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return region;

    }

    static Object buscarPais(String nombre) {
        orm.Pais[] pais = null;
        try {

            String query = "Pais.nombre='" + nombre + "'";
            System.out.println("Listing Pais...");
            orm.Pais[] paises = orm.PaisDAO.listPaisByQuery(query, null);
            int length = Math.min(paises.length, ROW_COUNT);
            for (int i = 0; i < length; i++) {
                System.out.println(paises[i].getNombre());
            }
            pais = paises;
            System.out.println(length + " record(s) retrieved.");
        } catch (PersistentException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pais;

    }

}
