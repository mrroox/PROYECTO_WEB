/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Apoderado_pacienteCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression apoderadoId;
    public final AssociationExpression apoderado;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public Apoderado_pacienteCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        apoderadoId = new IntegerExpression("apoderado.id", this);
        apoderado = new AssociationExpression("apoderado", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
    }

    public Apoderado_pacienteCriteria(PersistentSession session) {
        this(session.createCriteria(Apoderado_paciente.class));
    }

    public Apoderado_pacienteCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public ApoderadoCriteria createApoderadoCriteria() {
        return new ApoderadoCriteria(createCriteria("apoderado"));
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public Apoderado_paciente uniqueApoderado_paciente() {
        return (Apoderado_paciente) super.uniqueResult();
    }

    public Apoderado_paciente[] listApoderado_paciente() {
        java.util.List list = super.list();
        return (Apoderado_paciente[]) list.toArray(new Apoderado_paciente[list.size()]);
    }
}
