/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PaisDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final CollectionExpression direccion;

    public PaisDetachedCriteria() {
        super(orm.Pais.class, orm.PaisCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        direccion = new CollectionExpression("ORM_Direccion", this.getDetachedCriteria());
    }

    public PaisDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.PaisCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        direccion = new CollectionExpression("ORM_Direccion", this.getDetachedCriteria());
    }

    public DireccionDetachedCriteria createDireccionCriteria() {
        return new DireccionDetachedCriteria(createCriteria("ORM_Direccion"));
    }

    public Pais uniquePais(PersistentSession session) {
        return (Pais) super.createExecutableCriteria(session).uniqueResult();
    }

    public Pais[] listPais(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Pais[]) list.toArray(new Pais[list.size()]);
    }
}
