/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Paciente_terapeutaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_ingreso;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;

    public Paciente_terapeutaDetachedCriteria() {
        super(orm.Paciente_terapeuta.class, orm.Paciente_terapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_ingreso = new StringExpression("fecha_ingreso", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
    }

    public Paciente_terapeutaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Paciente_terapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_ingreso = new StringExpression("fecha_ingreso", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public TerapeutaDetachedCriteria createTerapeutaCriteria() {
        return new TerapeutaDetachedCriteria(createCriteria("terapeuta"));
    }

    public Paciente_terapeuta uniquePaciente_terapeuta(PersistentSession session) {
        return (Paciente_terapeuta) super.createExecutableCriteria(session).uniqueResult();
    }

    public Paciente_terapeuta[] listPaciente_terapeuta(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Paciente_terapeuta[]) list.toArray(new Paciente_terapeuta[list.size()]);
    }
}
