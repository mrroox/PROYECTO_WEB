/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Datosdispocitivo_datosdeusodiarioDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression datosdispocitivoId;
    public final AssociationExpression datosdispocitivo;
    public final IntegerExpression datosdeusodiarioId;
    public final AssociationExpression datosdeusodiario;

    public Datosdispocitivo_datosdeusodiarioDetachedCriteria() {
        super(orm.Datosdispocitivo_datosdeusodiario.class, orm.Datosdispocitivo_datosdeusodiarioCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        datosdispocitivoId = new IntegerExpression("datosdispocitivo.id", this.getDetachedCriteria());
        datosdispocitivo = new AssociationExpression("datosdispocitivo", this.getDetachedCriteria());
        datosdeusodiarioId = new IntegerExpression("datosdeusodiario.id", this.getDetachedCriteria());
        datosdeusodiario = new AssociationExpression("datosdeusodiario", this.getDetachedCriteria());
    }

    public Datosdispocitivo_datosdeusodiarioDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Datosdispocitivo_datosdeusodiarioCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        datosdispocitivoId = new IntegerExpression("datosdispocitivo.id", this.getDetachedCriteria());
        datosdispocitivo = new AssociationExpression("datosdispocitivo", this.getDetachedCriteria());
        datosdeusodiarioId = new IntegerExpression("datosdeusodiario.id", this.getDetachedCriteria());
        datosdeusodiario = new AssociationExpression("datosdeusodiario", this.getDetachedCriteria());
    }

    public DatosdispositivoDetachedCriteria createDatosdispocitivoCriteria() {
        return new DatosdispositivoDetachedCriteria(createCriteria("datosdispocitivo"));
    }

    public DatosdeusodiarioDetachedCriteria createDatosdeusodiarioCriteria() {
        return new DatosdeusodiarioDetachedCriteria(createCriteria("datosdeusodiario"));
    }

    public Datosdispocitivo_datosdeusodiario uniqueDatosdispocitivo_datosdeusodiario(PersistentSession session) {
        return (Datosdispocitivo_datosdeusodiario) super.createExecutableCriteria(session).uniqueResult();
    }

    public Datosdispocitivo_datosdeusodiario[] listDatosdispocitivo_datosdeusodiario(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Datosdispocitivo_datosdeusodiario[]) list.toArray(new Datosdispocitivo_datosdeusodiario[list.size()]);
    }
}
