/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Patron {

    public Patron() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_PATRON_JUEGO_PATRON) {
            return ORM_juego_patron;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private Integer nivel;

    private Integer nivelmaximo;

    private Integer tiemporespuesta;

    private java.util.Set ORM_juego_patron = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setNivel(int value) {
        setNivel(new Integer(value));
    }

    public void setNivel(Integer value) {
        this.nivel = value;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivelmaximo(int value) {
        setNivelmaximo(new Integer(value));
    }

    public void setNivelmaximo(Integer value) {
        this.nivelmaximo = value;
    }

    public Integer getNivelmaximo() {
        return nivelmaximo;
    }

    public void setTiemporespuesta(int value) {
        setTiemporespuesta(new Integer(value));
    }

    public void setTiemporespuesta(Integer value) {
        this.tiemporespuesta = value;
    }

    public Integer getTiemporespuesta() {
        return tiemporespuesta;
    }

    private void setORM_Juego_patron(java.util.Set value) {
        this.ORM_juego_patron = value;
    }

    private java.util.Set getORM_Juego_patron() {
        return ORM_juego_patron;
    }

    public final orm.Juego_patronSetCollection juego_patron = new orm.Juego_patronSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PATRON_JUEGO_PATRON, orm.ORMConstants.KEY_JUEGO_PATRON_PATRON, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
