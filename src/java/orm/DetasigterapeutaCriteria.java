/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DetasigterapeutaCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression observaciones;
    public final StringExpression fecha_asignacion;
    public final StringExpression fecha_devolucion;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final CollectionExpression asigdispterapeuta_detasigterapeuta;

    public DetasigterapeutaCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        observaciones = new StringExpression("observaciones", this);
        fecha_asignacion = new StringExpression("fecha_asignacion", this);
        fecha_devolucion = new StringExpression("fecha_devolucion", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
        asigdispterapeuta_detasigterapeuta = new CollectionExpression("ORM_Asigdispterapeuta_detasigterapeuta", this);
    }

    public DetasigterapeutaCriteria(PersistentSession session) {
        this(session.createCriteria(Detasigterapeuta.class));
    }

    public DetasigterapeutaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public Asigdispterapeuta_detasigterapeutaCriteria createAsigdispterapeuta_detasigterapeutaCriteria() {
        return new Asigdispterapeuta_detasigterapeutaCriteria(createCriteria("ORM_Asigdispterapeuta_detasigterapeuta"));
    }

    public Detasigterapeuta uniqueDetasigterapeuta() {
        return (Detasigterapeuta) super.uniqueResult();
    }

    public Detasigterapeuta[] listDetasigterapeuta() {
        java.util.List list = super.list();
        return (Detasigterapeuta[]) list.toArray(new Detasigterapeuta[list.size()]);
    }
}
