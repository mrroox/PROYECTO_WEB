/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Clienteterapeuta_paciente {

    public Clienteterapeuta_paciente() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_CLIENTETERAPEUTA_PACIENTE_CLIENTETERAPEUTA) {
            this.clienteterapeuta = (orm.Clienteterapeuta) owner;
        } else if (key == orm.ORMConstants.KEY_CLIENTETERAPEUTA_PACIENTE_PACIENTE) {
            this.paciente = (orm.Paciente) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private String fecha_ingreso;

    private orm.Clienteterapeuta clienteterapeuta;

    private orm.Paciente paciente;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFecha_ingreso(String value) {
        this.fecha_ingreso = value;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setClienteterapeuta(orm.Clienteterapeuta value) {
        if (clienteterapeuta != null) {
            clienteterapeuta.clienteterapeuta_paciente.remove(this);
        }
        if (value != null) {
            value.clienteterapeuta_paciente.add(this);
        }
    }

    public orm.Clienteterapeuta getClienteterapeuta() {
        return clienteterapeuta;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Clienteterapeuta(orm.Clienteterapeuta value) {
        this.clienteterapeuta = value;
    }

    private orm.Clienteterapeuta getORM_Clienteterapeuta() {
        return clienteterapeuta;
    }

    public void setPaciente(orm.Paciente value) {
        if (paciente != null) {
            paciente.clienteterapeuta_paciente.remove(this);
        }
        if (value != null) {
            value.clienteterapeuta_paciente.add(this);
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Paciente(orm.Paciente value) {
        this.paciente = value;
    }

    private orm.Paciente getORM_Paciente() {
        return paciente;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
