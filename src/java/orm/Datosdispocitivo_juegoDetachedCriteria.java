/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Datosdispocitivo_juegoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression datosdispocitivoId;
    public final AssociationExpression datosdispocitivo;
    public final IntegerExpression juegoId;
    public final AssociationExpression juego;

    public Datosdispocitivo_juegoDetachedCriteria() {
        super(orm.Datosdispocitivo_juego.class, orm.Datosdispocitivo_juegoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        datosdispocitivoId = new IntegerExpression("datosdispocitivo.id", this.getDetachedCriteria());
        datosdispocitivo = new AssociationExpression("datosdispocitivo", this.getDetachedCriteria());
        juegoId = new IntegerExpression("juego.id", this.getDetachedCriteria());
        juego = new AssociationExpression("juego", this.getDetachedCriteria());
    }

    public Datosdispocitivo_juegoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Datosdispocitivo_juegoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        datosdispocitivoId = new IntegerExpression("datosdispocitivo.id", this.getDetachedCriteria());
        datosdispocitivo = new AssociationExpression("datosdispocitivo", this.getDetachedCriteria());
        juegoId = new IntegerExpression("juego.id", this.getDetachedCriteria());
        juego = new AssociationExpression("juego", this.getDetachedCriteria());
    }

    public DatosdispositivoDetachedCriteria createDatosdispocitivoCriteria() {
        return new DatosdispositivoDetachedCriteria(createCriteria("datosdispocitivo"));
    }

    public JuegoDetachedCriteria createJuegoCriteria() {
        return new JuegoDetachedCriteria(createCriteria("juego"));
    }

    public Datosdispocitivo_juego uniqueDatosdispocitivo_juego(PersistentSession session) {
        return (Datosdispocitivo_juego) super.createExecutableCriteria(session).uniqueResult();
    }

    public Datosdispocitivo_juego[] listDatosdispocitivo_juego(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Datosdispocitivo_juego[]) list.toArray(new Datosdispocitivo_juego[list.size()]);
    }
}
