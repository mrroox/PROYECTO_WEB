/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Datosdispocitivo_juegoDAO {

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_juegoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego getDatosdispocitivo_juegoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdispocitivo_juegoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_juegoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego getDatosdispocitivo_juegoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdispocitivo_juegoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdispocitivo_juego) session.load(orm.Datosdispocitivo_juego.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego getDatosdispocitivo_juegoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdispocitivo_juego) session.get(orm.Datosdispocitivo_juego.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdispocitivo_juego) session.load(orm.Datosdispocitivo_juego.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego getDatosdispocitivo_juegoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdispocitivo_juego) session.get(orm.Datosdispocitivo_juego.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_juego(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdispocitivo_juego(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_juego(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdispocitivo_juego(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego[] listDatosdispocitivo_juegoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdispocitivo_juegoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego[] listDatosdispocitivo_juegoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdispocitivo_juegoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_juego(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_juego as Datosdispocitivo_juego");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_juego(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_juego as Datosdispocitivo_juego");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdispocitivo_juego", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego[] listDatosdispocitivo_juegoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDatosdispocitivo_juego(session, condition, orderBy);
            return (Datosdispocitivo_juego[]) list.toArray(new Datosdispocitivo_juego[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego[] listDatosdispocitivo_juegoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDatosdispocitivo_juego(session, condition, orderBy, lockMode);
            return (Datosdispocitivo_juego[]) list.toArray(new Datosdispocitivo_juego[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_juegoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_juegoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Datosdispocitivo_juego[] datosdispocitivo_juegos = listDatosdispocitivo_juegoByQuery(session, condition, orderBy);
        if (datosdispocitivo_juegos != null && datosdispocitivo_juegos.length > 0) {
            return datosdispocitivo_juegos[0];
        } else {
            return null;
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Datosdispocitivo_juego[] datosdispocitivo_juegos = listDatosdispocitivo_juegoByQuery(session, condition, orderBy, lockMode);
        if (datosdispocitivo_juegos != null && datosdispocitivo_juegos.length > 0) {
            return datosdispocitivo_juegos[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_juegoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdispocitivo_juegoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_juegoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdispocitivo_juegoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_juegoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_juego as Datosdispocitivo_juego");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_juegoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_juego as Datosdispocitivo_juego");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdispocitivo_juego", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego createDatosdispocitivo_juego() {
        return new orm.Datosdispocitivo_juego();
    }

    public static boolean save(orm.Datosdispocitivo_juego datosdispocitivo_juego) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(datosdispocitivo_juego);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Datosdispocitivo_juego datosdispocitivo_juego) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(datosdispocitivo_juego);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdispocitivo_juego datosdispocitivo_juego) throws PersistentException {
        try {
            if (datosdispocitivo_juego.getDatosdispocitivo() != null) {
                datosdispocitivo_juego.getDatosdispocitivo().datosdispocitivo_juego.remove(datosdispocitivo_juego);
            }

            if (datosdispocitivo_juego.getJuego() != null) {
                datosdispocitivo_juego.getJuego().datosdispocitivo_juego.remove(datosdispocitivo_juego);
            }

            return delete(datosdispocitivo_juego);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdispocitivo_juego datosdispocitivo_juego, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (datosdispocitivo_juego.getDatosdispocitivo() != null) {
                datosdispocitivo_juego.getDatosdispocitivo().datosdispocitivo_juego.remove(datosdispocitivo_juego);
            }

            if (datosdispocitivo_juego.getJuego() != null) {
                datosdispocitivo_juego.getJuego().datosdispocitivo_juego.remove(datosdispocitivo_juego);
            }

            try {
                session.delete(datosdispocitivo_juego);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Datosdispocitivo_juego datosdispocitivo_juego) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(datosdispocitivo_juego);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Datosdispocitivo_juego datosdispocitivo_juego) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(datosdispocitivo_juego);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_juego loadDatosdispocitivo_juegoByCriteria(Datosdispocitivo_juegoCriteria datosdispocitivo_juegoCriteria) {
        Datosdispocitivo_juego[] datosdispocitivo_juegos = listDatosdispocitivo_juegoByCriteria(datosdispocitivo_juegoCriteria);
        if (datosdispocitivo_juegos == null || datosdispocitivo_juegos.length == 0) {
            return null;
        }
        return datosdispocitivo_juegos[0];
    }

    public static Datosdispocitivo_juego[] listDatosdispocitivo_juegoByCriteria(Datosdispocitivo_juegoCriteria datosdispocitivo_juegoCriteria) {
        return datosdispocitivo_juegoCriteria.listDatosdispocitivo_juego();
    }
}
