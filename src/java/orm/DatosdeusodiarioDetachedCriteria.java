/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosdeusodiarioDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha;
    public final DoubleExpression tiempototaluso;
    public final StringExpression nivelmaximo;
    public final IntegerExpression totalpatronesrealizados;
    public final CollectionExpression datosdispocitivo_datosdeusodiario;

    public DatosdeusodiarioDetachedCriteria() {
        super(orm.Datosdeusodiario.class, orm.DatosdeusodiarioCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha = new StringExpression("fecha", this.getDetachedCriteria());
        tiempototaluso = new DoubleExpression("tiempototaluso", this.getDetachedCriteria());
        nivelmaximo = new StringExpression("nivelmaximo", this.getDetachedCriteria());
        totalpatronesrealizados = new IntegerExpression("totalpatronesrealizados", this.getDetachedCriteria());
        datosdispocitivo_datosdeusodiario = new CollectionExpression("ORM_Datosdispocitivo_datosdeusodiario", this.getDetachedCriteria());
    }

    public DatosdeusodiarioDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DatosdeusodiarioCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha = new StringExpression("fecha", this.getDetachedCriteria());
        tiempototaluso = new DoubleExpression("tiempototaluso", this.getDetachedCriteria());
        nivelmaximo = new StringExpression("nivelmaximo", this.getDetachedCriteria());
        totalpatronesrealizados = new IntegerExpression("totalpatronesrealizados", this.getDetachedCriteria());
        datosdispocitivo_datosdeusodiario = new CollectionExpression("ORM_Datosdispocitivo_datosdeusodiario", this.getDetachedCriteria());
    }

    public Datosdispocitivo_datosdeusodiarioDetachedCriteria createDatosdispocitivo_datosdeusodiarioCriteria() {
        return new Datosdispocitivo_datosdeusodiarioDetachedCriteria(createCriteria("ORM_Datosdispocitivo_datosdeusodiario"));
    }

    public Datosdeusodiario uniqueDatosdeusodiario(PersistentSession session) {
        return (Datosdeusodiario) super.createExecutableCriteria(session).uniqueResult();
    }

    public Datosdeusodiario[] listDatosdeusodiario(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Datosdeusodiario[]) list.toArray(new Datosdeusodiario[list.size()]);
    }
}
