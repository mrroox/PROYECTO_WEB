/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Asigdisporganizacion_dispositivo {

    public Asigdisporganizacion_dispositivo() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_ASIGDISPORGANIZACION_DISPOSITIVO_ASIGDISPORGANIZACION) {
            this.asigdisporganizacion = (orm.Asigdisporganizacion) owner;
        } else if (key == orm.ORMConstants.KEY_ASIGDISPORGANIZACION_DISPOSITIVO_DISPOSITIVO) {
            this.dispositivo = (orm.Dispositivo) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Asigdisporganizacion asigdisporganizacion;

    private orm.Dispositivo dispositivo;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setAsigdisporganizacion(orm.Asigdisporganizacion value) {
        if (asigdisporganizacion != null) {
            asigdisporganizacion.asigdisporganizacion_dispositivo.remove(this);
        }
        if (value != null) {
            value.asigdisporganizacion_dispositivo.add(this);
        }
    }

    public orm.Asigdisporganizacion getAsigdisporganizacion() {
        return asigdisporganizacion;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Asigdisporganizacion(orm.Asigdisporganizacion value) {
        this.asigdisporganizacion = value;
    }

    private orm.Asigdisporganizacion getORM_Asigdisporganizacion() {
        return asigdisporganizacion;
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (dispositivo != null) {
            dispositivo.asigdisporganizacion_dispositivo.remove(this);
        }
        if (value != null) {
            value.asigdisporganizacion_dispositivo.add(this);
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Dispositivo(orm.Dispositivo value) {
        this.dispositivo = value;
    }

    private orm.Dispositivo getORM_Dispositivo() {
        return dispositivo;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
