/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegionDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final CollectionExpression direccion;

    public RegionDetachedCriteria() {
        super(orm.Region.class, orm.RegionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        direccion = new CollectionExpression("ORM_Direccion", this.getDetachedCriteria());
    }

    public RegionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.RegionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        direccion = new CollectionExpression("ORM_Direccion", this.getDetachedCriteria());
    }

    public DireccionDetachedCriteria createDireccionCriteria() {
        return new DireccionDetachedCriteria(createCriteria("ORM_Direccion"));
    }

    public Region uniqueRegion(PersistentSession session) {
        return (Region) super.createExecutableCriteria(session).uniqueResult();
    }

    public Region[] listRegion(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Region[]) list.toArray(new Region[list.size()]);
    }
}
