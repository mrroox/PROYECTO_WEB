/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Clienteterapeuta_dispositivoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;

    public Clienteterapeuta_dispositivoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this);
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
    }

    public Clienteterapeuta_dispositivoCriteria(PersistentSession session) {
        this(session.createCriteria(Clienteterapeuta_dispositivo.class));
    }

    public Clienteterapeuta_dispositivoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public ClienteterapeutaCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaCriteria(createCriteria("clienteterapeuta"));
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public Clienteterapeuta_dispositivo uniqueClienteterapeuta_dispositivo() {
        return (Clienteterapeuta_dispositivo) super.uniqueResult();
    }

    public Clienteterapeuta_dispositivo[] listClienteterapeuta_dispositivo() {
        java.util.List list = super.list();
        return (Clienteterapeuta_dispositivo[]) list.toArray(new Clienteterapeuta_dispositivo[list.size()]);
    }
}
