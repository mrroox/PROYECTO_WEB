/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsigdispclienteDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_asignacion;
    public final StringExpression observaciones;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final CollectionExpression detalleasignacion_asigdispcliente;

    public AsigdispclienteDetachedCriteria() {
        super(orm.Asigdispcliente.class, orm.AsigdispclienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        detalleasignacion_asigdispcliente = new CollectionExpression("ORM_Detalleasignacion_asigdispcliente", this.getDetachedCriteria());
    }

    public AsigdispclienteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.AsigdispclienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        detalleasignacion_asigdispcliente = new CollectionExpression("ORM_Detalleasignacion_asigdispcliente", this.getDetachedCriteria());
    }

    public ClienteterapeutaDetachedCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaDetachedCriteria(createCriteria("clienteterapeuta"));
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public Detalleasignacion_asigdispclienteDetachedCriteria createDetalleasignacion_asigdispclienteCriteria() {
        return new Detalleasignacion_asigdispclienteDetachedCriteria(createCriteria("ORM_Detalleasignacion_asigdispcliente"));
    }

    public Asigdispcliente uniqueAsigdispcliente(PersistentSession session) {
        return (Asigdispcliente) super.createExecutableCriteria(session).uniqueResult();
    }

    public Asigdispcliente[] listAsigdispcliente(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Asigdispcliente[]) list.toArray(new Asigdispcliente[list.size()]);
    }
}
