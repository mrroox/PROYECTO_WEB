/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Datosusogeneral {

    public Datosusogeneral() {
    }

    private int id;

    private Integer tiempototaluso;

    private Integer nivelmaximologrado;

    private Integer totalpatronesrealizados;

    private orm.Datosdispositivo datosdispositivo;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setTiempototaluso(int value) {
        setTiempototaluso(new Integer(value));
    }

    public void setTiempototaluso(Integer value) {
        this.tiempototaluso = value;
    }

    public Integer getTiempototaluso() {
        return tiempototaluso;
    }

    public void setNivelmaximologrado(int value) {
        setNivelmaximologrado(new Integer(value));
    }

    public void setNivelmaximologrado(Integer value) {
        this.nivelmaximologrado = value;
    }

    public Integer getNivelmaximologrado() {
        return nivelmaximologrado;
    }

    public void setTotalpatronesrealizados(int value) {
        setTotalpatronesrealizados(new Integer(value));
    }

    public void setTotalpatronesrealizados(Integer value) {
        this.totalpatronesrealizados = value;
    }

    public Integer getTotalpatronesrealizados() {
        return totalpatronesrealizados;
    }

    public void setDatosdispositivo(orm.Datosdispositivo value) {
        if (this.datosdispositivo != value) {
            orm.Datosdispositivo ldatosdispositivo = this.datosdispositivo;
            this.datosdispositivo = value;
            if (value != null) {
                datosdispositivo.setDatosusogeneral(this);
            }
            if (ldatosdispositivo != null && ldatosdispositivo.getDatosusogeneral() == this) {
                ldatosdispositivo.setDatosusogeneral(null);
            }
        }
    }

    public orm.Datosdispositivo getDatosdispositivo() {
        return datosdispositivo;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
