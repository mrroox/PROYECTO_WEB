/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Asigdispterapeuta_detasigterapeutaDAO {

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeuta_detasigterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta getAsigdispterapeuta_detasigterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdispterapeuta_detasigterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeuta_detasigterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta getAsigdispterapeuta_detasigterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdispterapeuta_detasigterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdispterapeuta_detasigterapeuta) session.load(orm.Asigdispterapeuta_detasigterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta getAsigdispterapeuta_detasigterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdispterapeuta_detasigterapeuta) session.get(orm.Asigdispterapeuta_detasigterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdispterapeuta_detasigterapeuta) session.load(orm.Asigdispterapeuta_detasigterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta getAsigdispterapeuta_detasigterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdispterapeuta_detasigterapeuta) session.get(orm.Asigdispterapeuta_detasigterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta_detasigterapeuta(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdispterapeuta_detasigterapeuta(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta_detasigterapeuta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdispterapeuta_detasigterapeuta(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta[] listAsigdispterapeuta_detasigterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta[] listAsigdispterapeuta_detasigterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta_detasigterapeuta(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta_detasigterapeuta as Asigdispterapeuta_detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta_detasigterapeuta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta_detasigterapeuta as Asigdispterapeuta_detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdispterapeuta_detasigterapeuta", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta[] listAsigdispterapeuta_detasigterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryAsigdispterapeuta_detasigterapeuta(session, condition, orderBy);
            return (Asigdispterapeuta_detasigterapeuta[]) list.toArray(new Asigdispterapeuta_detasigterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta[] listAsigdispterapeuta_detasigterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryAsigdispterapeuta_detasigterapeuta(session, condition, orderBy, lockMode);
            return (Asigdispterapeuta_detasigterapeuta[]) list.toArray(new Asigdispterapeuta_detasigterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Asigdispterapeuta_detasigterapeuta[] asigdispterapeuta_detasigterapeutas = listAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy);
        if (asigdispterapeuta_detasigterapeutas != null && asigdispterapeuta_detasigterapeutas.length > 0) {
            return asigdispterapeuta_detasigterapeutas[0];
        } else {
            return null;
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Asigdispterapeuta_detasigterapeuta[] asigdispterapeuta_detasigterapeutas = listAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy, lockMode);
        if (asigdispterapeuta_detasigterapeutas != null && asigdispterapeuta_detasigterapeutas.length > 0) {
            return asigdispterapeuta_detasigterapeutas[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateAsigdispterapeuta_detasigterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispterapeuta_detasigterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdispterapeuta_detasigterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispterapeuta_detasigterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta_detasigterapeuta as Asigdispterapeuta_detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispterapeuta_detasigterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta_detasigterapeuta as Asigdispterapeuta_detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdispterapeuta_detasigterapeuta", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta createAsigdispterapeuta_detasigterapeuta() {
        return new orm.Asigdispterapeuta_detasigterapeuta();
    }

    public static boolean save(orm.Asigdispterapeuta_detasigterapeuta asigdispterapeuta_detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(asigdispterapeuta_detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Asigdispterapeuta_detasigterapeuta asigdispterapeuta_detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(asigdispterapeuta_detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdispterapeuta_detasigterapeuta asigdispterapeuta_detasigterapeuta) throws PersistentException {
        try {
            if (asigdispterapeuta_detasigterapeuta.getAsigdispterapeuta() != null) {
                asigdispterapeuta_detasigterapeuta.getAsigdispterapeuta().asigdispterapeuta_detasigterapeuta.remove(asigdispterapeuta_detasigterapeuta);
            }

            if (asigdispterapeuta_detasigterapeuta.getDetasigterapeuta() != null) {
                asigdispterapeuta_detasigterapeuta.getDetasigterapeuta().asigdispterapeuta_detasigterapeuta.remove(asigdispterapeuta_detasigterapeuta);
            }

            return delete(asigdispterapeuta_detasigterapeuta);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdispterapeuta_detasigterapeuta asigdispterapeuta_detasigterapeuta, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (asigdispterapeuta_detasigterapeuta.getAsigdispterapeuta() != null) {
                asigdispterapeuta_detasigterapeuta.getAsigdispterapeuta().asigdispterapeuta_detasigterapeuta.remove(asigdispterapeuta_detasigterapeuta);
            }

            if (asigdispterapeuta_detasigterapeuta.getDetasigterapeuta() != null) {
                asigdispterapeuta_detasigterapeuta.getDetasigterapeuta().asigdispterapeuta_detasigterapeuta.remove(asigdispterapeuta_detasigterapeuta);
            }

            try {
                session.delete(asigdispterapeuta_detasigterapeuta);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Asigdispterapeuta_detasigterapeuta asigdispterapeuta_detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(asigdispterapeuta_detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Asigdispterapeuta_detasigterapeuta asigdispterapeuta_detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(asigdispterapeuta_detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta_detasigterapeuta loadAsigdispterapeuta_detasigterapeutaByCriteria(Asigdispterapeuta_detasigterapeutaCriteria asigdispterapeuta_detasigterapeutaCriteria) {
        Asigdispterapeuta_detasigterapeuta[] asigdispterapeuta_detasigterapeutas = listAsigdispterapeuta_detasigterapeutaByCriteria(asigdispterapeuta_detasigterapeutaCriteria);
        if (asigdispterapeuta_detasigterapeutas == null || asigdispterapeuta_detasigterapeutas.length == 0) {
            return null;
        }
        return asigdispterapeuta_detasigterapeutas[0];
    }

    public static Asigdispterapeuta_detasigterapeuta[] listAsigdispterapeuta_detasigterapeutaByCriteria(Asigdispterapeuta_detasigterapeutaCriteria asigdispterapeuta_detasigterapeutaCriteria) {
        return asigdispterapeuta_detasigterapeutaCriteria.listAsigdispterapeuta_detasigterapeuta();
    }
}
