/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DetalleasignacionDAO {

    public static Detalleasignacion loadDetalleasignacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion getDetalleasignacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDetalleasignacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion loadDetalleasignacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion getDetalleasignacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDetalleasignacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion loadDetalleasignacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Detalleasignacion) session.load(orm.Detalleasignacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion getDetalleasignacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Detalleasignacion) session.get(orm.Detalleasignacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion loadDetalleasignacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Detalleasignacion) session.load(orm.Detalleasignacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion getDetalleasignacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Detalleasignacion) session.get(orm.Detalleasignacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDetalleasignacion(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDetalleasignacion(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion[] listDetalleasignacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDetalleasignacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion[] listDetalleasignacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDetalleasignacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion as Detalleasignacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion as Detalleasignacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Detalleasignacion", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion[] listDetalleasignacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDetalleasignacion(session, condition, orderBy);
            return (Detalleasignacion[]) list.toArray(new Detalleasignacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion[] listDetalleasignacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDetalleasignacion(session, condition, orderBy, lockMode);
            return (Detalleasignacion[]) list.toArray(new Detalleasignacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion loadDetalleasignacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion loadDetalleasignacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion loadDetalleasignacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Detalleasignacion[] detalleasignacions = listDetalleasignacionByQuery(session, condition, orderBy);
        if (detalleasignacions != null && detalleasignacions.length > 0) {
            return detalleasignacions[0];
        } else {
            return null;
        }
    }

    public static Detalleasignacion loadDetalleasignacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Detalleasignacion[] detalleasignacions = listDetalleasignacionByQuery(session, condition, orderBy, lockMode);
        if (detalleasignacions != null && detalleasignacions.length > 0) {
            return detalleasignacions[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDetalleasignacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDetalleasignacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetalleasignacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDetalleasignacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetalleasignacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion as Detalleasignacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetalleasignacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion as Detalleasignacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Detalleasignacion", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion createDetalleasignacion() {
        return new orm.Detalleasignacion();
    }

    public static boolean save(orm.Detalleasignacion detalleasignacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(detalleasignacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Detalleasignacion detalleasignacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(detalleasignacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Detalleasignacion detalleasignacion) throws PersistentException {
        try {
            if (detalleasignacion.getDispositivo() != null) {
                detalleasignacion.getDispositivo().detalleasignacion.remove(detalleasignacion);
            }

            orm.Detalleasignacion_asigdispcliente[] lDetalleasignacion_asigdispclientes = detalleasignacion.detalleasignacion_asigdispcliente.toArray();
            for (int i = 0; i < lDetalleasignacion_asigdispclientes.length; i++) {
                lDetalleasignacion_asigdispclientes[i].setDetalleasignacion(null);
            }
            return delete(detalleasignacion);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Detalleasignacion detalleasignacion, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (detalleasignacion.getDispositivo() != null) {
                detalleasignacion.getDispositivo().detalleasignacion.remove(detalleasignacion);
            }

            orm.Detalleasignacion_asigdispcliente[] lDetalleasignacion_asigdispclientes = detalleasignacion.detalleasignacion_asigdispcliente.toArray();
            for (int i = 0; i < lDetalleasignacion_asigdispclientes.length; i++) {
                lDetalleasignacion_asigdispclientes[i].setDetalleasignacion(null);
            }
            try {
                session.delete(detalleasignacion);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Detalleasignacion detalleasignacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(detalleasignacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Detalleasignacion detalleasignacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(detalleasignacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion loadDetalleasignacionByCriteria(DetalleasignacionCriteria detalleasignacionCriteria) {
        Detalleasignacion[] detalleasignacions = listDetalleasignacionByCriteria(detalleasignacionCriteria);
        if (detalleasignacions == null || detalleasignacions.length == 0) {
            return null;
        }
        return detalleasignacions[0];
    }

    public static Detalleasignacion[] listDetalleasignacionByCriteria(DetalleasignacionCriteria detalleasignacionCriteria) {
        return detalleasignacionCriteria.listDetalleasignacion();
    }
}
