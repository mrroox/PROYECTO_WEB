/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ExpedientemedicoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression patologia;
    public final StringExpression observacion;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public ExpedientemedicoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        patologia = new StringExpression("patologia", this);
        observacion = new StringExpression("observacion", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
    }

    public ExpedientemedicoCriteria(PersistentSession session) {
        this(session.createCriteria(Expedientemedico.class));
    }

    public ExpedientemedicoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public Expedientemedico uniqueExpedientemedico() {
        return (Expedientemedico) super.uniqueResult();
    }

    public Expedientemedico[] listExpedientemedico() {
        java.util.List list = super.list();
        return (Expedientemedico[]) list.toArray(new Expedientemedico[list.size()]);
    }
}
