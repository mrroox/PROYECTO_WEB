/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Persona {

    public Persona() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_PERSONA_APODERADO) {
            return ORM_apoderado;
        }

        return null;
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_PERSONA_TELEFONO) {
            this.telefono = (orm.Telefono) owner;
        } else if (key == orm.ORMConstants.KEY_PERSONA_DIRECCION) {
            this.direccion = (orm.Direccion) owner;
        } else if (key == orm.ORMConstants.KEY_PERSONA_PACIENTE) {
            this.paciente = (orm.Paciente) owner;
        } else if (key == orm.ORMConstants.KEY_PERSONA_CLIENTETERAPEUTA) {
            this.clienteterapeuta = (orm.Clienteterapeuta) owner;
        } else if (key == orm.ORMConstants.KEY_PERSONA_TERAPEUTA) {
            this.terapeuta = (orm.Terapeuta) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private String run;

    private String nombre;

    private String apellido_paterno;

    private String apellido_materno;

    private String fecha_nacimiento;

    private String email;

    private String sexo;

    private orm.Direccion direccion;

    private orm.Telefono telefono;

    private orm.Paciente paciente;

    private orm.Clienteterapeuta clienteterapeuta;

    private java.util.Set ORM_apoderado = new java.util.HashSet();

    private orm.Terapeuta terapeuta;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setRun(String value) {
        this.run = value;
    }

    public String getRun() {
        return run;
    }

    public void setNombre(String value) {
        this.nombre = value;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellido_paterno(String value) {
        this.apellido_paterno = value;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_materno(String value) {
        this.apellido_materno = value;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setFecha_nacimiento(String value) {
        this.fecha_nacimiento = value;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getEmail() {
        return email;
    }

    public void setSexo(String value) {
        this.sexo = value;
    }

    public String getSexo() {
        return sexo;
    }

    public void setTelefono(orm.Telefono value) {
        if (telefono != null) {
            telefono.persona.remove(this);
        }
        if (value != null) {
            value.persona.add(this);
        }
    }

    public orm.Telefono getTelefono() {
        return telefono;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Telefono(orm.Telefono value) {
        this.telefono = value;
    }

    private orm.Telefono getORM_Telefono() {
        return telefono;
    }

    public void setDireccion(orm.Direccion value) {
        if (direccion != null) {
            direccion.persona.remove(this);
        }
        if (value != null) {
            value.persona.add(this);
        }
    }

    public orm.Direccion getDireccion() {
        return direccion;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Direccion(orm.Direccion value) {
        this.direccion = value;
    }

    private orm.Direccion getORM_Direccion() {
        return direccion;
    }

    public void setPaciente(orm.Paciente value) {
        if (this.paciente != value) {
            orm.Paciente lpaciente = this.paciente;
            this.paciente = value;
            if (value != null) {
                paciente.setPersona(this);
            }
            if (lpaciente != null && lpaciente.getPersona() == this) {
                lpaciente.setPersona(null);
            }
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    public void setClienteterapeuta(orm.Clienteterapeuta value) {
        if (this.clienteterapeuta != value) {
            orm.Clienteterapeuta lclienteterapeuta = this.clienteterapeuta;
            this.clienteterapeuta = value;
            if (value != null) {
                clienteterapeuta.setPersona(this);
            }
            if (lclienteterapeuta != null && lclienteterapeuta.getPersona() == this) {
                lclienteterapeuta.setPersona(null);
            }
        }
    }

    public orm.Clienteterapeuta getClienteterapeuta() {
        return clienteterapeuta;
    }

    private void setORM_Apoderado(java.util.Set value) {
        this.ORM_apoderado = value;
    }

    private java.util.Set getORM_Apoderado() {
        return ORM_apoderado;
    }

    public final orm.ApoderadoSetCollection apoderado = new orm.ApoderadoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_APODERADO, orm.ORMConstants.KEY_APODERADO_PERSONA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setTerapeuta(orm.Terapeuta value) {
        if (this.terapeuta != value) {
            orm.Terapeuta lterapeuta = this.terapeuta;
            this.terapeuta = value;
            if (value != null) {
                terapeuta.setPersona(this);
            }
            if (lterapeuta != null && lterapeuta.getPersona() == this) {
                lterapeuta.setPersona(null);
            }
        }
    }

    public orm.Terapeuta getTerapeuta() {
        return terapeuta;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
