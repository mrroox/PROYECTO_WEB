/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TelefonoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression redfija;
    public final StringExpression celular;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final CollectionExpression persona;

    public TelefonoDetachedCriteria() {
        super(orm.Telefono.class, orm.TelefonoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        redfija = new StringExpression("redfija", this.getDetachedCriteria());
        celular = new StringExpression("celular", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        persona = new CollectionExpression("ORM_Persona", this.getDetachedCriteria());
    }

    public TelefonoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.TelefonoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        redfija = new StringExpression("redfija", this.getDetachedCriteria());
        celular = new StringExpression("celular", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        persona = new CollectionExpression("ORM_Persona", this.getDetachedCriteria());
    }

    public OrganizacionDetachedCriteria createOrganizacionCriteria() {
        return new OrganizacionDetachedCriteria(createCriteria("organizacion"));
    }

    public PersonaDetachedCriteria createPersonaCriteria() {
        return new PersonaDetachedCriteria(createCriteria("ORM_Persona"));
    }

    public Telefono uniqueTelefono(PersistentSession session) {
        return (Telefono) super.createExecutableCriteria(session).uniqueResult();
    }

    public Telefono[] listTelefono(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Telefono[]) list.toArray(new Telefono[list.size()]);
    }
}
