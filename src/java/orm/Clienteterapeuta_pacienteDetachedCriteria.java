/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Clienteterapeuta_pacienteDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_ingreso;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public Clienteterapeuta_pacienteDetachedCriteria() {
        super(orm.Clienteterapeuta_paciente.class, orm.Clienteterapeuta_pacienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_ingreso = new StringExpression("fecha_ingreso", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public Clienteterapeuta_pacienteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Clienteterapeuta_pacienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_ingreso = new StringExpression("fecha_ingreso", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public ClienteterapeutaDetachedCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaDetachedCriteria(createCriteria("clienteterapeuta"));
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public Clienteterapeuta_paciente uniqueClienteterapeuta_paciente(PersistentSession session) {
        return (Clienteterapeuta_paciente) super.createExecutableCriteria(session).uniqueResult();
    }

    public Clienteterapeuta_paciente[] listClienteterapeuta_paciente(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Clienteterapeuta_paciente[]) list.toArray(new Clienteterapeuta_paciente[list.size()]);
    }
}
