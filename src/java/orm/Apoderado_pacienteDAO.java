/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Apoderado_pacienteDAO {

    public static Apoderado_paciente loadApoderado_pacienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadApoderado_pacienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente getApoderado_pacienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getApoderado_pacienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadApoderado_pacienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente getApoderado_pacienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getApoderado_pacienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Apoderado_paciente) session.load(orm.Apoderado_paciente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente getApoderado_pacienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Apoderado_paciente) session.get(orm.Apoderado_paciente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Apoderado_paciente) session.load(orm.Apoderado_paciente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente getApoderado_pacienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Apoderado_paciente) session.get(orm.Apoderado_paciente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryApoderado_paciente(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryApoderado_paciente(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryApoderado_paciente(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryApoderado_paciente(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente[] listApoderado_pacienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listApoderado_pacienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente[] listApoderado_pacienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listApoderado_pacienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryApoderado_paciente(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Apoderado_paciente as Apoderado_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryApoderado_paciente(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Apoderado_paciente as Apoderado_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Apoderado_paciente", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente[] listApoderado_pacienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryApoderado_paciente(session, condition, orderBy);
            return (Apoderado_paciente[]) list.toArray(new Apoderado_paciente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente[] listApoderado_pacienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryApoderado_paciente(session, condition, orderBy, lockMode);
            return (Apoderado_paciente[]) list.toArray(new Apoderado_paciente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadApoderado_pacienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadApoderado_pacienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Apoderado_paciente[] apoderado_pacientes = listApoderado_pacienteByQuery(session, condition, orderBy);
        if (apoderado_pacientes != null && apoderado_pacientes.length > 0) {
            return apoderado_pacientes[0];
        } else {
            return null;
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Apoderado_paciente[] apoderado_pacientes = listApoderado_pacienteByQuery(session, condition, orderBy, lockMode);
        if (apoderado_pacientes != null && apoderado_pacientes.length > 0) {
            return apoderado_pacientes[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateApoderado_pacienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateApoderado_pacienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateApoderado_pacienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateApoderado_pacienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateApoderado_pacienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Apoderado_paciente as Apoderado_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateApoderado_pacienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Apoderado_paciente as Apoderado_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Apoderado_paciente", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente createApoderado_paciente() {
        return new orm.Apoderado_paciente();
    }

    public static boolean save(orm.Apoderado_paciente apoderado_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(apoderado_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Apoderado_paciente apoderado_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(apoderado_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Apoderado_paciente apoderado_paciente) throws PersistentException {
        try {
            if (apoderado_paciente.getApoderado() != null) {
                apoderado_paciente.getApoderado().apoderado_paciente.remove(apoderado_paciente);
            }

            if (apoderado_paciente.getPaciente() != null) {
                apoderado_paciente.getPaciente().apoderado_paciente.remove(apoderado_paciente);
            }

            return delete(apoderado_paciente);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Apoderado_paciente apoderado_paciente, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (apoderado_paciente.getApoderado() != null) {
                apoderado_paciente.getApoderado().apoderado_paciente.remove(apoderado_paciente);
            }

            if (apoderado_paciente.getPaciente() != null) {
                apoderado_paciente.getPaciente().apoderado_paciente.remove(apoderado_paciente);
            }

            try {
                session.delete(apoderado_paciente);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Apoderado_paciente apoderado_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(apoderado_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Apoderado_paciente apoderado_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(apoderado_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Apoderado_paciente loadApoderado_pacienteByCriteria(Apoderado_pacienteCriteria apoderado_pacienteCriteria) {
        Apoderado_paciente[] apoderado_pacientes = listApoderado_pacienteByCriteria(apoderado_pacienteCriteria);
        if (apoderado_pacientes == null || apoderado_pacientes.length == 0) {
            return null;
        }
        return apoderado_pacientes[0];
    }

    public static Apoderado_paciente[] listApoderado_pacienteByCriteria(Apoderado_pacienteCriteria apoderado_pacienteCriteria) {
        return apoderado_pacienteCriteria.listApoderado_paciente();
    }
}
