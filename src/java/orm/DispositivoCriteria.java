/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DispositivoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final StringExpression descripcion;
    public final StringExpression numserie;
    public final IntegerExpression tipodispositivoId;
    public final AssociationExpression tipodispositivo;
    public final IntegerExpression dotosdispositivoId;
    public final AssociationExpression dotosdispositivo;
    public final CollectionExpression clienteterapeuta_dispositivo;
    public final CollectionExpression dispositivo_organizacion;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final CollectionExpression detalleasignacion;
    public final IntegerExpression detasigterapeutaId;
    public final AssociationExpression detasigterapeuta;
    public final CollectionExpression asigdisporganizacion_dispositivo;

    public DispositivoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        nombre = new StringExpression("nombre", this);
        descripcion = new StringExpression("descripcion", this);
        numserie = new StringExpression("numserie", this);
        tipodispositivoId = new IntegerExpression("tipodispositivo.id", this);
        tipodispositivo = new AssociationExpression("tipodispositivo", this);
        dotosdispositivoId = new IntegerExpression("dotosdispositivo.id", this);
        dotosdispositivo = new AssociationExpression("dotosdispositivo", this);
        clienteterapeuta_dispositivo = new CollectionExpression("ORM_Clienteterapeuta_dispositivo", this);
        dispositivo_organizacion = new CollectionExpression("ORM_Dispositivo_organizacion", this);
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this);
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this);
        detalleasignacion = new CollectionExpression("ORM_Detalleasignacion", this);
        detasigterapeutaId = new IntegerExpression("detasigterapeuta.id", this);
        detasigterapeuta = new AssociationExpression("detasigterapeuta", this);
        asigdisporganizacion_dispositivo = new CollectionExpression("ORM_Asigdisporganizacion_dispositivo", this);
    }

    public DispositivoCriteria(PersistentSession session) {
        this(session.createCriteria(Dispositivo.class));
    }

    public DispositivoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public TipodispositivoCriteria createTipodispositivoCriteria() {
        return new TipodispositivoCriteria(createCriteria("tipodispositivo"));
    }

    public DatosdispositivoCriteria createDotosdispositivoCriteria() {
        return new DatosdispositivoCriteria(createCriteria("dotosdispositivo"));
    }

    public Clienteterapeuta_dispositivoCriteria createClienteterapeuta_dispositivoCriteria() {
        return new Clienteterapeuta_dispositivoCriteria(createCriteria("ORM_Clienteterapeuta_dispositivo"));
    }

    public Dispositivo_organizacionCriteria createDispositivo_organizacionCriteria() {
        return new Dispositivo_organizacionCriteria(createCriteria("ORM_Dispositivo_organizacion"));
    }

    public AsigdisporganizacionCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionCriteria(createCriteria("asigdisporganizacion"));
    }

    public DetalleasignacionCriteria createDetalleasignacionCriteria() {
        return new DetalleasignacionCriteria(createCriteria("ORM_Detalleasignacion"));
    }

    public DetasigterapeutaCriteria createDetasigterapeutaCriteria() {
        return new DetasigterapeutaCriteria(createCriteria("detasigterapeuta"));
    }

    public Asigdisporganizacion_dispositivoCriteria createAsigdisporganizacion_dispositivoCriteria() {
        return new Asigdisporganizacion_dispositivoCriteria(createCriteria("ORM_Asigdisporganizacion_dispositivo"));
    }

    public Dispositivo uniqueDispositivo() {
        return (Dispositivo) super.uniqueResult();
    }

    public Dispositivo[] listDispositivo() {
        java.util.List list = super.list();
        return (Dispositivo[]) list.toArray(new Dispositivo[list.size()]);
    }
}
