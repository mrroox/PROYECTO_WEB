/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ClienteterapeutaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression password;
    public final IntegerExpression personaId;
    public final AssociationExpression persona;
    public final IntegerExpression personadireccionid;
    public final IntegerExpression evaluacionId;
    public final AssociationExpression evaluacion;
    public final CollectionExpression clienteterapeuta_paciente;
    public final IntegerExpression asigdispclienteId;
    public final AssociationExpression asigdispcliente;
    public final CollectionExpression clienteterapeuta_dispositivo;

    public ClienteterapeutaDetachedCriteria() {
        super(orm.Clienteterapeuta.class, orm.ClienteterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        password = new StringExpression("password", this.getDetachedCriteria());
        personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
        persona = new AssociationExpression("persona", this.getDetachedCriteria());
        personadireccionid = new IntegerExpression("personadireccionid", this.getDetachedCriteria());
        evaluacionId = new IntegerExpression("evaluacion.id", this.getDetachedCriteria());
        evaluacion = new AssociationExpression("evaluacion", this.getDetachedCriteria());
        clienteterapeuta_paciente = new CollectionExpression("ORM_Clienteterapeuta_paciente", this.getDetachedCriteria());
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this.getDetachedCriteria());
        asigdispcliente = new AssociationExpression("asigdispcliente", this.getDetachedCriteria());
        clienteterapeuta_dispositivo = new CollectionExpression("ORM_Clienteterapeuta_dispositivo", this.getDetachedCriteria());
    }

    public ClienteterapeutaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.ClienteterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        password = new StringExpression("password", this.getDetachedCriteria());
        personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
        persona = new AssociationExpression("persona", this.getDetachedCriteria());
        personadireccionid = new IntegerExpression("personadireccionid", this.getDetachedCriteria());
        evaluacionId = new IntegerExpression("evaluacion.id", this.getDetachedCriteria());
        evaluacion = new AssociationExpression("evaluacion", this.getDetachedCriteria());
        clienteterapeuta_paciente = new CollectionExpression("ORM_Clienteterapeuta_paciente", this.getDetachedCriteria());
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this.getDetachedCriteria());
        asigdispcliente = new AssociationExpression("asigdispcliente", this.getDetachedCriteria());
        clienteterapeuta_dispositivo = new CollectionExpression("ORM_Clienteterapeuta_dispositivo", this.getDetachedCriteria());
    }

    public PersonaDetachedCriteria createPersonaCriteria() {
        return new PersonaDetachedCriteria(createCriteria("persona"));
    }

    public EvaluacionDetachedCriteria createEvaluacionCriteria() {
        return new EvaluacionDetachedCriteria(createCriteria("evaluacion"));
    }

    public Clienteterapeuta_pacienteDetachedCriteria createClienteterapeuta_pacienteCriteria() {
        return new Clienteterapeuta_pacienteDetachedCriteria(createCriteria("ORM_Clienteterapeuta_paciente"));
    }

    public AsigdispclienteDetachedCriteria createAsigdispclienteCriteria() {
        return new AsigdispclienteDetachedCriteria(createCriteria("asigdispcliente"));
    }

    public Clienteterapeuta_dispositivoDetachedCriteria createClienteterapeuta_dispositivoCriteria() {
        return new Clienteterapeuta_dispositivoDetachedCriteria(createCriteria("ORM_Clienteterapeuta_dispositivo"));
    }

    public Clienteterapeuta uniqueClienteterapeuta(PersistentSession session) {
        return (Clienteterapeuta) super.createExecutableCriteria(session).uniqueResult();
    }

    public Clienteterapeuta[] listClienteterapeuta(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Clienteterapeuta[]) list.toArray(new Clienteterapeuta[list.size()]);
    }
}
