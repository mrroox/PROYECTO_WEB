/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CargoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final CollectionExpression terapeuta_cargo;

    public CargoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        nombre = new StringExpression("nombre", this);
        terapeuta_cargo = new CollectionExpression("ORM_Terapeuta_cargo", this);
    }

    public CargoCriteria(PersistentSession session) {
        this(session.createCriteria(Cargo.class));
    }

    public CargoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public Terapeuta_cargoCriteria createTerapeuta_cargoCriteria() {
        return new Terapeuta_cargoCriteria(createCriteria("ORM_Terapeuta_cargo"));
    }

    public Cargo uniqueCargo() {
        return (Cargo) super.uniqueResult();
    }

    public Cargo[] listCargo() {
        java.util.List list = super.list();
        return (Cargo[]) list.toArray(new Cargo[list.size()]);
    }
}
