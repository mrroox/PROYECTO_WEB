/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Detalleasignacion_asigdispclienteCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression detalleasignacionId;
    public final AssociationExpression detalleasignacion;
    public final IntegerExpression asigdispclienteId;
    public final AssociationExpression asigdispcliente;

    public Detalleasignacion_asigdispclienteCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        detalleasignacionId = new IntegerExpression("detalleasignacion.id", this);
        detalleasignacion = new AssociationExpression("detalleasignacion", this);
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this);
        asigdispcliente = new AssociationExpression("asigdispcliente", this);
    }

    public Detalleasignacion_asigdispclienteCriteria(PersistentSession session) {
        this(session.createCriteria(Detalleasignacion_asigdispcliente.class));
    }

    public Detalleasignacion_asigdispclienteCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DetalleasignacionCriteria createDetalleasignacionCriteria() {
        return new DetalleasignacionCriteria(createCriteria("detalleasignacion"));
    }

    public AsigdispclienteCriteria createAsigdispclienteCriteria() {
        return new AsigdispclienteCriteria(createCriteria("asigdispcliente"));
    }

    public Detalleasignacion_asigdispcliente uniqueDetalleasignacion_asigdispcliente() {
        return (Detalleasignacion_asigdispcliente) super.uniqueResult();
    }

    public Detalleasignacion_asigdispcliente[] listDetalleasignacion_asigdispcliente() {
        java.util.List list = super.list();
        return (Detalleasignacion_asigdispcliente[]) list.toArray(new Detalleasignacion_asigdispcliente[list.size()]);
    }
}
