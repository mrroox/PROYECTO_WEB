/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Clienteterapeuta_dispositivo {

    public Clienteterapeuta_dispositivo() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_CLIENTETERAPEUTA_DISPOSITIVO_CLIENTETERAPEUTA) {
            this.clienteterapeuta = (orm.Clienteterapeuta) owner;
        } else if (key == orm.ORMConstants.KEY_CLIENTETERAPEUTA_DISPOSITIVO_DISPOSITIVO) {
            this.dispositivo = (orm.Dispositivo) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Clienteterapeuta clienteterapeuta;

    private orm.Dispositivo dispositivo;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setClienteterapeuta(orm.Clienteterapeuta value) {
        if (clienteterapeuta != null) {
            clienteterapeuta.clienteterapeuta_dispositivo.remove(this);
        }
        if (value != null) {
            value.clienteterapeuta_dispositivo.add(this);
        }
    }

    public orm.Clienteterapeuta getClienteterapeuta() {
        return clienteterapeuta;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Clienteterapeuta(orm.Clienteterapeuta value) {
        this.clienteterapeuta = value;
    }

    private orm.Clienteterapeuta getORM_Clienteterapeuta() {
        return clienteterapeuta;
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (dispositivo != null) {
            dispositivo.clienteterapeuta_dispositivo.remove(this);
        }
        if (value != null) {
            value.clienteterapeuta_dispositivo.add(this);
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Dispositivo(orm.Dispositivo value) {
        this.dispositivo = value;
    }

    private orm.Dispositivo getORM_Dispositivo() {
        return dispositivo;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
