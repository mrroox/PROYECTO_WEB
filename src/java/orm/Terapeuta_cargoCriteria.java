/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Terapeuta_cargoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final IntegerExpression cargoId;
    public final AssociationExpression cargo;

    public Terapeuta_cargoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        terapeutaId = new IntegerExpression("terapeuta.id", this);
        terapeuta = new AssociationExpression("terapeuta", this);
        cargoId = new IntegerExpression("cargo.id", this);
        cargo = new AssociationExpression("cargo", this);
    }

    public Terapeuta_cargoCriteria(PersistentSession session) {
        this(session.createCriteria(Terapeuta_cargo.class));
    }

    public Terapeuta_cargoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public TerapeutaCriteria createTerapeutaCriteria() {
        return new TerapeutaCriteria(createCriteria("terapeuta"));
    }

    public CargoCriteria createCargoCriteria() {
        return new CargoCriteria(createCriteria("cargo"));
    }

    public Terapeuta_cargo uniqueTerapeuta_cargo() {
        return (Terapeuta_cargo) super.uniqueResult();
    }

    public Terapeuta_cargo[] listTerapeuta_cargo() {
        java.util.List list = super.list();
        return (Terapeuta_cargo[]) list.toArray(new Terapeuta_cargo[list.size()]);
    }
}
