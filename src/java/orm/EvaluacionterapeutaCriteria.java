/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EvaluacionterapeutaCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_evaluacion;
    public final StringExpression observacion;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public EvaluacionterapeutaCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha_evaluacion = new StringExpression("fecha_evaluacion", this);
        observacion = new StringExpression("observacion", this);
        terapeutaId = new IntegerExpression("terapeuta.id", this);
        terapeuta = new AssociationExpression("terapeuta", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
    }

    public EvaluacionterapeutaCriteria(PersistentSession session) {
        this(session.createCriteria(Evaluacionterapeuta.class));
    }

    public EvaluacionterapeutaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public TerapeutaCriteria createTerapeutaCriteria() {
        return new TerapeutaCriteria(createCriteria("terapeuta"));
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public Evaluacionterapeuta uniqueEvaluacionterapeuta() {
        return (Evaluacionterapeuta) super.uniqueResult();
    }

    public Evaluacionterapeuta[] listEvaluacionterapeuta() {
        java.util.List list = super.list();
        return (Evaluacionterapeuta[]) list.toArray(new Evaluacionterapeuta[list.size()]);
    }
}
