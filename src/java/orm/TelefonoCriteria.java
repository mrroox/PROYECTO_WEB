/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TelefonoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression redfija;
    public final StringExpression celular;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final CollectionExpression persona;

    public TelefonoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        redfija = new StringExpression("redfija", this);
        celular = new StringExpression("celular", this);
        organizacionId = new IntegerExpression("organizacion.id", this);
        organizacion = new AssociationExpression("organizacion", this);
        persona = new CollectionExpression("ORM_Persona", this);
    }

    public TelefonoCriteria(PersistentSession session) {
        this(session.createCriteria(Telefono.class));
    }

    public TelefonoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public OrganizacionCriteria createOrganizacionCriteria() {
        return new OrganizacionCriteria(createCriteria("organizacion"));
    }

    public PersonaCriteria createPersonaCriteria() {
        return new PersonaCriteria(createCriteria("ORM_Persona"));
    }

    public Telefono uniqueTelefono() {
        return (Telefono) super.uniqueResult();
    }

    public Telefono[] listTelefono() {
        java.util.List list = super.list();
        return (Telefono[]) list.toArray(new Telefono[list.size()]);
    }
}
