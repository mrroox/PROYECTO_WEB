/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Detalleasignacion_asigdispcliente {

    public Detalleasignacion_asigdispcliente() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_DETALLEASIGNACION_ASIGDISPCLIENTE_DETALLEASIGNACION) {
            this.detalleasignacion = (orm.Detalleasignacion) owner;
        } else if (key == orm.ORMConstants.KEY_DETALLEASIGNACION_ASIGDISPCLIENTE_ASIGDISPCLIENTE) {
            this.asigdispcliente = (orm.Asigdispcliente) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Detalleasignacion detalleasignacion;

    private orm.Asigdispcliente asigdispcliente;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setDetalleasignacion(orm.Detalleasignacion value) {
        if (detalleasignacion != null) {
            detalleasignacion.detalleasignacion_asigdispcliente.remove(this);
        }
        if (value != null) {
            value.detalleasignacion_asigdispcliente.add(this);
        }
    }

    public orm.Detalleasignacion getDetalleasignacion() {
        return detalleasignacion;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Detalleasignacion(orm.Detalleasignacion value) {
        this.detalleasignacion = value;
    }

    private orm.Detalleasignacion getORM_Detalleasignacion() {
        return detalleasignacion;
    }

    public void setAsigdispcliente(orm.Asigdispcliente value) {
        if (asigdispcliente != null) {
            asigdispcliente.detalleasignacion_asigdispcliente.remove(this);
        }
        if (value != null) {
            value.detalleasignacion_asigdispcliente.add(this);
        }
    }

    public orm.Asigdispcliente getAsigdispcliente() {
        return asigdispcliente;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Asigdispcliente(orm.Asigdispcliente value) {
        this.asigdispcliente = value;
    }

    private orm.Asigdispcliente getORM_Asigdispcliente() {
        return asigdispcliente;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
