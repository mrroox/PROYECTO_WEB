/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Asigdisporganizacion_dispositivoDAO {

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacion_dispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo getAsigdisporganizacion_dispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdisporganizacion_dispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacion_dispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo getAsigdisporganizacion_dispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdisporganizacion_dispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdisporganizacion_dispositivo) session.load(orm.Asigdisporganizacion_dispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo getAsigdisporganizacion_dispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdisporganizacion_dispositivo) session.get(orm.Asigdisporganizacion_dispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdisporganizacion_dispositivo) session.load(orm.Asigdisporganizacion_dispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo getAsigdisporganizacion_dispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdisporganizacion_dispositivo) session.get(orm.Asigdisporganizacion_dispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion_dispositivo(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdisporganizacion_dispositivo(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion_dispositivo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdisporganizacion_dispositivo(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo[] listAsigdisporganizacion_dispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo[] listAsigdisporganizacion_dispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion_dispositivo(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion_dispositivo as Asigdisporganizacion_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion_dispositivo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion_dispositivo as Asigdisporganizacion_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdisporganizacion_dispositivo", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo[] listAsigdisporganizacion_dispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryAsigdisporganizacion_dispositivo(session, condition, orderBy);
            return (Asigdisporganizacion_dispositivo[]) list.toArray(new Asigdisporganizacion_dispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo[] listAsigdisporganizacion_dispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryAsigdisporganizacion_dispositivo(session, condition, orderBy, lockMode);
            return (Asigdisporganizacion_dispositivo[]) list.toArray(new Asigdisporganizacion_dispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Asigdisporganizacion_dispositivo[] asigdisporganizacion_dispositivos = listAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy);
        if (asigdisporganizacion_dispositivos != null && asigdisporganizacion_dispositivos.length > 0) {
            return asigdisporganizacion_dispositivos[0];
        } else {
            return null;
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Asigdisporganizacion_dispositivo[] asigdisporganizacion_dispositivos = listAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy, lockMode);
        if (asigdisporganizacion_dispositivos != null && asigdisporganizacion_dispositivos.length > 0) {
            return asigdisporganizacion_dispositivos[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacion_dispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacion_dispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdisporganizacion_dispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacion_dispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion_dispositivo as Asigdisporganizacion_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacion_dispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion_dispositivo as Asigdisporganizacion_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdisporganizacion_dispositivo", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo createAsigdisporganizacion_dispositivo() {
        return new orm.Asigdisporganizacion_dispositivo();
    }

    public static boolean save(orm.Asigdisporganizacion_dispositivo asigdisporganizacion_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(asigdisporganizacion_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Asigdisporganizacion_dispositivo asigdisporganizacion_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(asigdisporganizacion_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdisporganizacion_dispositivo asigdisporganizacion_dispositivo) throws PersistentException {
        try {
            if (asigdisporganizacion_dispositivo.getAsigdisporganizacion() != null) {
                asigdisporganizacion_dispositivo.getAsigdisporganizacion().asigdisporganizacion_dispositivo.remove(asigdisporganizacion_dispositivo);
            }

            if (asigdisporganizacion_dispositivo.getDispositivo() != null) {
                asigdisporganizacion_dispositivo.getDispositivo().asigdisporganizacion_dispositivo.remove(asigdisporganizacion_dispositivo);
            }

            return delete(asigdisporganizacion_dispositivo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdisporganizacion_dispositivo asigdisporganizacion_dispositivo, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (asigdisporganizacion_dispositivo.getAsigdisporganizacion() != null) {
                asigdisporganizacion_dispositivo.getAsigdisporganizacion().asigdisporganizacion_dispositivo.remove(asigdisporganizacion_dispositivo);
            }

            if (asigdisporganizacion_dispositivo.getDispositivo() != null) {
                asigdisporganizacion_dispositivo.getDispositivo().asigdisporganizacion_dispositivo.remove(asigdisporganizacion_dispositivo);
            }

            try {
                session.delete(asigdisporganizacion_dispositivo);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Asigdisporganizacion_dispositivo asigdisporganizacion_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(asigdisporganizacion_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Asigdisporganizacion_dispositivo asigdisporganizacion_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(asigdisporganizacion_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion_dispositivo loadAsigdisporganizacion_dispositivoByCriteria(Asigdisporganizacion_dispositivoCriteria asigdisporganizacion_dispositivoCriteria) {
        Asigdisporganizacion_dispositivo[] asigdisporganizacion_dispositivos = listAsigdisporganizacion_dispositivoByCriteria(asigdisporganizacion_dispositivoCriteria);
        if (asigdisporganizacion_dispositivos == null || asigdisporganizacion_dispositivos.length == 0) {
            return null;
        }
        return asigdisporganizacion_dispositivos[0];
    }

    public static Asigdisporganizacion_dispositivo[] listAsigdisporganizacion_dispositivoByCriteria(Asigdisporganizacion_dispositivoCriteria asigdisporganizacion_dispositivoCriteria) {
        return asigdisporganizacion_dispositivoCriteria.listAsigdisporganizacion_dispositivo();
    }
}
