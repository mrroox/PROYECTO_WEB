/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsigdispterapeutaCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_asignacion;
    public final StringExpression observaciones;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final CollectionExpression asigdispterapeuta_detasigterapeuta;

    public AsigdispterapeutaCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha_asignacion = new StringExpression("fecha_asignacion", this);
        observaciones = new StringExpression("observaciones", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
        terapeutaId = new IntegerExpression("terapeuta.id", this);
        terapeuta = new AssociationExpression("terapeuta", this);
        asigdispterapeuta_detasigterapeuta = new CollectionExpression("ORM_Asigdispterapeuta_detasigterapeuta", this);
    }

    public AsigdispterapeutaCriteria(PersistentSession session) {
        this(session.createCriteria(Asigdispterapeuta.class));
    }

    public AsigdispterapeutaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public TerapeutaCriteria createTerapeutaCriteria() {
        return new TerapeutaCriteria(createCriteria("terapeuta"));
    }

    public Asigdispterapeuta_detasigterapeutaCriteria createAsigdispterapeuta_detasigterapeutaCriteria() {
        return new Asigdispterapeuta_detasigterapeutaCriteria(createCriteria("ORM_Asigdispterapeuta_detasigterapeuta"));
    }

    public Asigdispterapeuta uniqueAsigdispterapeuta() {
        return (Asigdispterapeuta) super.uniqueResult();
    }

    public Asigdispterapeuta[] listAsigdispterapeuta() {
        java.util.List list = super.list();
        return (Asigdispterapeuta[]) list.toArray(new Asigdispterapeuta[list.size()]);
    }
}
