/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosnivelcompletadoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression n1;
    public final IntegerExpression n2;
    public final IntegerExpression n3;
    public final IntegerExpression n4;
    public final IntegerExpression n5;
    public final IntegerExpression n6;
    public final IntegerExpression n7;
    public final IntegerExpression n8;
    public final IntegerExpression n9;
    public final IntegerExpression n10;
    public final IntegerExpression n11;
    public final IntegerExpression n12;
    public final IntegerExpression n13;
    public final IntegerExpression n14;
    public final IntegerExpression n15;
    public final IntegerExpression datosdispositivoId;
    public final AssociationExpression datosdispositivo;

    public DatosnivelcompletadoDetachedCriteria() {
        super(orm.Datosnivelcompletado.class, orm.DatosnivelcompletadoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        n1 = new IntegerExpression("n1", this.getDetachedCriteria());
        n2 = new IntegerExpression("n2", this.getDetachedCriteria());
        n3 = new IntegerExpression("n3", this.getDetachedCriteria());
        n4 = new IntegerExpression("n4", this.getDetachedCriteria());
        n5 = new IntegerExpression("n5", this.getDetachedCriteria());
        n6 = new IntegerExpression("n6", this.getDetachedCriteria());
        n7 = new IntegerExpression("n7", this.getDetachedCriteria());
        n8 = new IntegerExpression("n8", this.getDetachedCriteria());
        n9 = new IntegerExpression("n9", this.getDetachedCriteria());
        n10 = new IntegerExpression("n10", this.getDetachedCriteria());
        n11 = new IntegerExpression("n11", this.getDetachedCriteria());
        n12 = new IntegerExpression("n12", this.getDetachedCriteria());
        n13 = new IntegerExpression("n13", this.getDetachedCriteria());
        n14 = new IntegerExpression("n14", this.getDetachedCriteria());
        n15 = new IntegerExpression("n15", this.getDetachedCriteria());
        datosdispositivoId = new IntegerExpression("datosdispositivo.id", this.getDetachedCriteria());
        datosdispositivo = new AssociationExpression("datosdispositivo", this.getDetachedCriteria());
    }

    public DatosnivelcompletadoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DatosnivelcompletadoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        n1 = new IntegerExpression("n1", this.getDetachedCriteria());
        n2 = new IntegerExpression("n2", this.getDetachedCriteria());
        n3 = new IntegerExpression("n3", this.getDetachedCriteria());
        n4 = new IntegerExpression("n4", this.getDetachedCriteria());
        n5 = new IntegerExpression("n5", this.getDetachedCriteria());
        n6 = new IntegerExpression("n6", this.getDetachedCriteria());
        n7 = new IntegerExpression("n7", this.getDetachedCriteria());
        n8 = new IntegerExpression("n8", this.getDetachedCriteria());
        n9 = new IntegerExpression("n9", this.getDetachedCriteria());
        n10 = new IntegerExpression("n10", this.getDetachedCriteria());
        n11 = new IntegerExpression("n11", this.getDetachedCriteria());
        n12 = new IntegerExpression("n12", this.getDetachedCriteria());
        n13 = new IntegerExpression("n13", this.getDetachedCriteria());
        n14 = new IntegerExpression("n14", this.getDetachedCriteria());
        n15 = new IntegerExpression("n15", this.getDetachedCriteria());
        datosdispositivoId = new IntegerExpression("datosdispositivo.id", this.getDetachedCriteria());
        datosdispositivo = new AssociationExpression("datosdispositivo", this.getDetachedCriteria());
    }

    public DatosdispositivoDetachedCriteria createDatosdispositivoCriteria() {
        return new DatosdispositivoDetachedCriteria(createCriteria("datosdispositivo"));
    }

    public Datosnivelcompletado uniqueDatosnivelcompletado(PersistentSession session) {
        return (Datosnivelcompletado) super.createExecutableCriteria(session).uniqueResult();
    }

    public Datosnivelcompletado[] listDatosnivelcompletado(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Datosnivelcompletado[]) list.toArray(new Datosnivelcompletado[list.size()]);
    }
}
