/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Datosdispocitivo_datosdeusodiarioDAO {

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_datosdeusodiarioByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario getDatosdispocitivo_datosdeusodiarioByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdispocitivo_datosdeusodiarioByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_datosdeusodiarioByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario getDatosdispocitivo_datosdeusodiarioByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdispocitivo_datosdeusodiarioByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdispocitivo_datosdeusodiario) session.load(orm.Datosdispocitivo_datosdeusodiario.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario getDatosdispocitivo_datosdeusodiarioByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdispocitivo_datosdeusodiario) session.get(orm.Datosdispocitivo_datosdeusodiario.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdispocitivo_datosdeusodiario) session.load(orm.Datosdispocitivo_datosdeusodiario.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario getDatosdispocitivo_datosdeusodiarioByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdispocitivo_datosdeusodiario) session.get(orm.Datosdispocitivo_datosdeusodiario.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_datosdeusodiario(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdispocitivo_datosdeusodiario(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_datosdeusodiario(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdispocitivo_datosdeusodiario(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario[] listDatosdispocitivo_datosdeusodiarioByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario[] listDatosdispocitivo_datosdeusodiarioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_datosdeusodiario(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_datosdeusodiario as Datosdispocitivo_datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispocitivo_datosdeusodiario(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_datosdeusodiario as Datosdispocitivo_datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdispocitivo_datosdeusodiario", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario[] listDatosdispocitivo_datosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDatosdispocitivo_datosdeusodiario(session, condition, orderBy);
            return (Datosdispocitivo_datosdeusodiario[]) list.toArray(new Datosdispocitivo_datosdeusodiario[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario[] listDatosdispocitivo_datosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDatosdispocitivo_datosdeusodiario(session, condition, orderBy, lockMode);
            return (Datosdispocitivo_datosdeusodiario[]) list.toArray(new Datosdispocitivo_datosdeusodiario[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Datosdispocitivo_datosdeusodiario[] datosdispocitivo_datosdeusodiarios = listDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy);
        if (datosdispocitivo_datosdeusodiarios != null && datosdispocitivo_datosdeusodiarios.length > 0) {
            return datosdispocitivo_datosdeusodiarios[0];
        } else {
            return null;
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Datosdispocitivo_datosdeusodiario[] datosdispocitivo_datosdeusodiarios = listDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        if (datosdispocitivo_datosdeusodiarios != null && datosdispocitivo_datosdeusodiarios.length > 0) {
            return datosdispocitivo_datosdeusodiarios[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_datosdeusodiarioByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_datosdeusodiarioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdispocitivo_datosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_datosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_datosdeusodiario as Datosdispocitivo_datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispocitivo_datosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispocitivo_datosdeusodiario as Datosdispocitivo_datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdispocitivo_datosdeusodiario", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario createDatosdispocitivo_datosdeusodiario() {
        return new orm.Datosdispocitivo_datosdeusodiario();
    }

    public static boolean save(orm.Datosdispocitivo_datosdeusodiario datosdispocitivo_datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(datosdispocitivo_datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Datosdispocitivo_datosdeusodiario datosdispocitivo_datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(datosdispocitivo_datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdispocitivo_datosdeusodiario datosdispocitivo_datosdeusodiario) throws PersistentException {
        try {
            if (datosdispocitivo_datosdeusodiario.getDatosdispocitivo() != null) {
                datosdispocitivo_datosdeusodiario.getDatosdispocitivo().datosdispocitivo_datosdeusodiario.remove(datosdispocitivo_datosdeusodiario);
            }

            if (datosdispocitivo_datosdeusodiario.getDatosdeusodiario() != null) {
                datosdispocitivo_datosdeusodiario.getDatosdeusodiario().datosdispocitivo_datosdeusodiario.remove(datosdispocitivo_datosdeusodiario);
            }

            return delete(datosdispocitivo_datosdeusodiario);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdispocitivo_datosdeusodiario datosdispocitivo_datosdeusodiario, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (datosdispocitivo_datosdeusodiario.getDatosdispocitivo() != null) {
                datosdispocitivo_datosdeusodiario.getDatosdispocitivo().datosdispocitivo_datosdeusodiario.remove(datosdispocitivo_datosdeusodiario);
            }

            if (datosdispocitivo_datosdeusodiario.getDatosdeusodiario() != null) {
                datosdispocitivo_datosdeusodiario.getDatosdeusodiario().datosdispocitivo_datosdeusodiario.remove(datosdispocitivo_datosdeusodiario);
            }

            try {
                session.delete(datosdispocitivo_datosdeusodiario);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Datosdispocitivo_datosdeusodiario datosdispocitivo_datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(datosdispocitivo_datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Datosdispocitivo_datosdeusodiario datosdispocitivo_datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(datosdispocitivo_datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispocitivo_datosdeusodiario loadDatosdispocitivo_datosdeusodiarioByCriteria(Datosdispocitivo_datosdeusodiarioCriteria datosdispocitivo_datosdeusodiarioCriteria) {
        Datosdispocitivo_datosdeusodiario[] datosdispocitivo_datosdeusodiarios = listDatosdispocitivo_datosdeusodiarioByCriteria(datosdispocitivo_datosdeusodiarioCriteria);
        if (datosdispocitivo_datosdeusodiarios == null || datosdispocitivo_datosdeusodiarios.length == 0) {
            return null;
        }
        return datosdispocitivo_datosdeusodiarios[0];
    }

    public static Datosdispocitivo_datosdeusodiario[] listDatosdispocitivo_datosdeusodiarioByCriteria(Datosdispocitivo_datosdeusodiarioCriteria datosdispocitivo_datosdeusodiarioCriteria) {
        return datosdispocitivo_datosdeusodiarioCriteria.listDatosdispocitivo_datosdeusodiario();
    }
}
