/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class AsigdispclienteDAO {

    public static Asigdispcliente loadAsigdispclienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispclienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente getAsigdispclienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdispclienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente loadAsigdispclienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispclienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente getAsigdispclienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdispclienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente loadAsigdispclienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdispcliente) session.load(orm.Asigdispcliente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente getAsigdispclienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdispcliente) session.get(orm.Asigdispcliente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente loadAsigdispclienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdispcliente) session.load(orm.Asigdispcliente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente getAsigdispclienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdispcliente) session.get(orm.Asigdispcliente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispcliente(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdispcliente(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispcliente(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdispcliente(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente[] listAsigdispclienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdispclienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente[] listAsigdispclienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdispclienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispcliente(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispcliente as Asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispcliente(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispcliente as Asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdispcliente", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente[] listAsigdispclienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryAsigdispcliente(session, condition, orderBy);
            return (Asigdispcliente[]) list.toArray(new Asigdispcliente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente[] listAsigdispclienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryAsigdispcliente(session, condition, orderBy, lockMode);
            return (Asigdispcliente[]) list.toArray(new Asigdispcliente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente loadAsigdispclienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispclienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente loadAsigdispclienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispclienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente loadAsigdispclienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Asigdispcliente[] asigdispclientes = listAsigdispclienteByQuery(session, condition, orderBy);
        if (asigdispclientes != null && asigdispclientes.length > 0) {
            return asigdispclientes[0];
        } else {
            return null;
        }
    }

    public static Asigdispcliente loadAsigdispclienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Asigdispcliente[] asigdispclientes = listAsigdispclienteByQuery(session, condition, orderBy, lockMode);
        if (asigdispclientes != null && asigdispclientes.length > 0) {
            return asigdispclientes[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateAsigdispclienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdispclienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispclienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdispclienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispclienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispcliente as Asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispclienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispcliente as Asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdispcliente", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente createAsigdispcliente() {
        return new orm.Asigdispcliente();
    }

    public static boolean save(orm.Asigdispcliente asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Asigdispcliente asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdispcliente asigdispcliente) throws PersistentException {
        try {
            if (asigdispcliente.getClienteterapeuta() != null) {
                asigdispcliente.getClienteterapeuta().setAsigdispcliente(null);
            }

            if (asigdispcliente.getPaciente() != null) {
                asigdispcliente.getPaciente().setAsigdispcliente(null);
            }

            orm.Detalleasignacion_asigdispcliente[] lDetalleasignacion_asigdispclientes = asigdispcliente.detalleasignacion_asigdispcliente.toArray();
            for (int i = 0; i < lDetalleasignacion_asigdispclientes.length; i++) {
                lDetalleasignacion_asigdispclientes[i].setAsigdispcliente(null);
            }
            return delete(asigdispcliente);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdispcliente asigdispcliente, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (asigdispcliente.getClienteterapeuta() != null) {
                asigdispcliente.getClienteterapeuta().setAsigdispcliente(null);
            }

            if (asigdispcliente.getPaciente() != null) {
                asigdispcliente.getPaciente().setAsigdispcliente(null);
            }

            orm.Detalleasignacion_asigdispcliente[] lDetalleasignacion_asigdispclientes = asigdispcliente.detalleasignacion_asigdispcliente.toArray();
            for (int i = 0; i < lDetalleasignacion_asigdispclientes.length; i++) {
                lDetalleasignacion_asigdispclientes[i].setAsigdispcliente(null);
            }
            try {
                session.delete(asigdispcliente);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Asigdispcliente asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Asigdispcliente asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispcliente loadAsigdispclienteByCriteria(AsigdispclienteCriteria asigdispclienteCriteria) {
        Asigdispcliente[] asigdispclientes = listAsigdispclienteByCriteria(asigdispclienteCriteria);
        if (asigdispclientes == null || asigdispclientes.length == 0) {
            return null;
        }
        return asigdispclientes[0];
    }

    public static Asigdispcliente[] listAsigdispclienteByCriteria(AsigdispclienteCriteria asigdispclienteCriteria) {
        return asigdispclienteCriteria.listAsigdispcliente();
    }
}
