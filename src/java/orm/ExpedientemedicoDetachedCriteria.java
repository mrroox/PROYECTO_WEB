/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ExpedientemedicoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression patologia;
    public final StringExpression observacion;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public ExpedientemedicoDetachedCriteria() {
        super(orm.Expedientemedico.class, orm.ExpedientemedicoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        patologia = new StringExpression("patologia", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public ExpedientemedicoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.ExpedientemedicoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        patologia = new StringExpression("patologia", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public Expedientemedico uniqueExpedientemedico(PersistentSession session) {
        return (Expedientemedico) super.createExecutableCriteria(session).uniqueResult();
    }

    public Expedientemedico[] listExpedientemedico(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Expedientemedico[]) list.toArray(new Expedientemedico[list.size()]);
    }
}
