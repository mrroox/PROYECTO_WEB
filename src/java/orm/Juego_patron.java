/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Juego_patron {

    public Juego_patron() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_JUEGO_PATRON_JUEGO) {
            this.juego = (orm.Juego) owner;
        } else if (key == orm.ORMConstants.KEY_JUEGO_PATRON_PATRON) {
            this.patron = (orm.Patron) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Juego juego;

    private orm.Patron patron;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setJuego(orm.Juego value) {
        if (juego != null) {
            juego.juego_patron.remove(this);
        }
        if (value != null) {
            value.juego_patron.add(this);
        }
    }

    public orm.Juego getJuego() {
        return juego;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Juego(orm.Juego value) {
        this.juego = value;
    }

    private orm.Juego getORM_Juego() {
        return juego;
    }

    public void setPatron(orm.Patron value) {
        if (patron != null) {
            patron.juego_patron.remove(this);
        }
        if (value != null) {
            value.juego_patron.add(this);
        }
    }

    public orm.Patron getPatron() {
        return patron;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Patron(orm.Patron value) {
        this.patron = value;
    }

    private orm.Patron getORM_Patron() {
        return patron;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
