/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Juego_patronDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression juegoId;
    public final AssociationExpression juego;
    public final IntegerExpression patronId;
    public final AssociationExpression patron;

    public Juego_patronDetachedCriteria() {
        super(orm.Juego_patron.class, orm.Juego_patronCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        juegoId = new IntegerExpression("juego.id", this.getDetachedCriteria());
        juego = new AssociationExpression("juego", this.getDetachedCriteria());
        patronId = new IntegerExpression("patron.id", this.getDetachedCriteria());
        patron = new AssociationExpression("patron", this.getDetachedCriteria());
    }

    public Juego_patronDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Juego_patronCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        juegoId = new IntegerExpression("juego.id", this.getDetachedCriteria());
        juego = new AssociationExpression("juego", this.getDetachedCriteria());
        patronId = new IntegerExpression("patron.id", this.getDetachedCriteria());
        patron = new AssociationExpression("patron", this.getDetachedCriteria());
    }

    public JuegoDetachedCriteria createJuegoCriteria() {
        return new JuegoDetachedCriteria(createCriteria("juego"));
    }

    public PatronDetachedCriteria createPatronCriteria() {
        return new PatronDetachedCriteria(createCriteria("patron"));
    }

    public Juego_patron uniqueJuego_patron(PersistentSession session) {
        return (Juego_patron) super.createExecutableCriteria(session).uniqueResult();
    }

    public Juego_patron[] listJuego_patron(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Juego_patron[]) list.toArray(new Juego_patron[list.size()]);
    }
}
