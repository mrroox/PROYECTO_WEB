/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Tipodispositivo {

    public Tipodispositivo() {
    }

    private int id;

    private String nombre;

    private orm.Dispositivo dispositivo;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setNombre(String value) {
        this.nombre = value;
    }

    public String getNombre() {
        return nombre;
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (this.dispositivo != value) {
            orm.Dispositivo ldispositivo = this.dispositivo;
            this.dispositivo = value;
            if (value != null) {
                dispositivo.setTipodispositivo(this);
            }
            if (ldispositivo != null && ldispositivo.getTipodispositivo() == this) {
                ldispositivo.setTipodispositivo(null);
            }
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
