/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DetasigterapeutaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression observaciones;
    public final StringExpression fecha_asignacion;
    public final StringExpression fecha_devolucion;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final CollectionExpression asigdispterapeuta_detasigterapeuta;

    public DetasigterapeutaDetachedCriteria() {
        super(orm.Detasigterapeuta.class, orm.DetasigterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        fecha_devolucion = new StringExpression("fecha_devolucion", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        asigdispterapeuta_detasigterapeuta = new CollectionExpression("ORM_Asigdispterapeuta_detasigterapeuta", this.getDetachedCriteria());
    }

    public DetasigterapeutaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DetasigterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        fecha_devolucion = new StringExpression("fecha_devolucion", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        asigdispterapeuta_detasigterapeuta = new CollectionExpression("ORM_Asigdispterapeuta_detasigterapeuta", this.getDetachedCriteria());
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public Asigdispterapeuta_detasigterapeutaDetachedCriteria createAsigdispterapeuta_detasigterapeutaCriteria() {
        return new Asigdispterapeuta_detasigterapeutaDetachedCriteria(createCriteria("ORM_Asigdispterapeuta_detasigterapeuta"));
    }

    public Detasigterapeuta uniqueDetasigterapeuta(PersistentSession session) {
        return (Detasigterapeuta) super.createExecutableCriteria(session).uniqueResult();
    }

    public Detasigterapeuta[] listDetasigterapeuta(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Detasigterapeuta[]) list.toArray(new Detasigterapeuta[list.size()]);
    }
}
