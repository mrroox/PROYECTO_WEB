/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsigdispterapeutaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_asignacion;
    public final StringExpression observaciones;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final CollectionExpression asigdispterapeuta_detasigterapeuta;

    public AsigdispterapeutaDetachedCriteria() {
        super(orm.Asigdispterapeuta.class, orm.AsigdispterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        asigdispterapeuta_detasigterapeuta = new CollectionExpression("ORM_Asigdispterapeuta_detasigterapeuta", this.getDetachedCriteria());
    }

    public AsigdispterapeutaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.AsigdispterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        asigdispterapeuta_detasigterapeuta = new CollectionExpression("ORM_Asigdispterapeuta_detasigterapeuta", this.getDetachedCriteria());
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public TerapeutaDetachedCriteria createTerapeutaCriteria() {
        return new TerapeutaDetachedCriteria(createCriteria("terapeuta"));
    }

    public Asigdispterapeuta_detasigterapeutaDetachedCriteria createAsigdispterapeuta_detasigterapeutaCriteria() {
        return new Asigdispterapeuta_detasigterapeutaDetachedCriteria(createCriteria("ORM_Asigdispterapeuta_detasigterapeuta"));
    }

    public Asigdispterapeuta uniqueAsigdispterapeuta(PersistentSession session) {
        return (Asigdispterapeuta) super.createExecutableCriteria(session).uniqueResult();
    }

    public Asigdispterapeuta[] listAsigdispterapeuta(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Asigdispterapeuta[]) list.toArray(new Asigdispterapeuta[list.size()]);
    }
}
