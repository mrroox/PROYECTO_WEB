/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class AsigdispterapeutaDAO {

    public static Asigdispterapeuta loadAsigdispterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta getAsigdispterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdispterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta getAsigdispterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdispterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdispterapeuta) session.load(orm.Asigdispterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta getAsigdispterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdispterapeuta) session.get(orm.Asigdispterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdispterapeuta) session.load(orm.Asigdispterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta getAsigdispterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdispterapeuta) session.get(orm.Asigdispterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdispterapeuta(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdispterapeuta(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta[] listAsigdispterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdispterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta[] listAsigdispterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdispterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta as Asigdispterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdispterapeuta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta as Asigdispterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdispterapeuta", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta[] listAsigdispterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryAsigdispterapeuta(session, condition, orderBy);
            return (Asigdispterapeuta[]) list.toArray(new Asigdispterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta[] listAsigdispterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryAsigdispterapeuta(session, condition, orderBy, lockMode);
            return (Asigdispterapeuta[]) list.toArray(new Asigdispterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdispterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Asigdispterapeuta[] asigdispterapeutas = listAsigdispterapeutaByQuery(session, condition, orderBy);
        if (asigdispterapeutas != null && asigdispterapeutas.length > 0) {
            return asigdispterapeutas[0];
        } else {
            return null;
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Asigdispterapeuta[] asigdispterapeutas = listAsigdispterapeutaByQuery(session, condition, orderBy, lockMode);
        if (asigdispterapeutas != null && asigdispterapeutas.length > 0) {
            return asigdispterapeutas[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateAsigdispterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdispterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdispterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta as Asigdispterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdispterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdispterapeuta as Asigdispterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdispterapeuta", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta createAsigdispterapeuta() {
        return new orm.Asigdispterapeuta();
    }

    public static boolean save(orm.Asigdispterapeuta asigdispterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(asigdispterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Asigdispterapeuta asigdispterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(asigdispterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdispterapeuta asigdispterapeuta) throws PersistentException {
        try {
            if (asigdispterapeuta.getPaciente() != null) {
                asigdispterapeuta.getPaciente().setAsigdispterapeuta(null);
            }

            if (asigdispterapeuta.getTerapeuta() != null) {
                asigdispterapeuta.getTerapeuta().setAsigdispterapeuta(null);
            }

            orm.Asigdispterapeuta_detasigterapeuta[] lAsigdispterapeuta_detasigterapeutas = asigdispterapeuta.asigdispterapeuta_detasigterapeuta.toArray();
            for (int i = 0; i < lAsigdispterapeuta_detasigterapeutas.length; i++) {
                lAsigdispterapeuta_detasigterapeutas[i].setAsigdispterapeuta(null);
            }
            return delete(asigdispterapeuta);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdispterapeuta asigdispterapeuta, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (asigdispterapeuta.getPaciente() != null) {
                asigdispterapeuta.getPaciente().setAsigdispterapeuta(null);
            }

            if (asigdispterapeuta.getTerapeuta() != null) {
                asigdispterapeuta.getTerapeuta().setAsigdispterapeuta(null);
            }

            orm.Asigdispterapeuta_detasigterapeuta[] lAsigdispterapeuta_detasigterapeutas = asigdispterapeuta.asigdispterapeuta_detasigterapeuta.toArray();
            for (int i = 0; i < lAsigdispterapeuta_detasigterapeutas.length; i++) {
                lAsigdispterapeuta_detasigterapeutas[i].setAsigdispterapeuta(null);
            }
            try {
                session.delete(asigdispterapeuta);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Asigdispterapeuta asigdispterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(asigdispterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Asigdispterapeuta asigdispterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(asigdispterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdispterapeuta loadAsigdispterapeutaByCriteria(AsigdispterapeutaCriteria asigdispterapeutaCriteria) {
        Asigdispterapeuta[] asigdispterapeutas = listAsigdispterapeutaByCriteria(asigdispterapeutaCriteria);
        if (asigdispterapeutas == null || asigdispterapeutas.length == 0) {
            return null;
        }
        return asigdispterapeutas[0];
    }

    public static Asigdispterapeuta[] listAsigdispterapeutaByCriteria(AsigdispterapeutaCriteria asigdispterapeutaCriteria) {
        return asigdispterapeutaCriteria.listAsigdispterapeuta();
    }
}
