/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Dispositivo {

    public Dispositivo() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_DISPOSITIVO_CLIENTETERAPEUTA_DISPOSITIVO) {
            return ORM_clienteterapeuta_dispositivo;
        } else if (key == orm.ORMConstants.KEY_DISPOSITIVO_DISPOSITIVO_ORGANIZACION) {
            return ORM_dispositivo_organizacion;
        } else if (key == orm.ORMConstants.KEY_DISPOSITIVO_DETALLEASIGNACION) {
            return ORM_detalleasignacion;
        } else if (key == orm.ORMConstants.KEY_DISPOSITIVO_ASIGDISPORGANIZACION_DISPOSITIVO) {
            return ORM_asigdisporganizacion_dispositivo;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String nombre;

    private String descripcion;

    private String numserie;

    private orm.Tipodispositivo tipodispositivo;

    private orm.Datosdispositivo dotosdispositivo;

    private java.util.Set ORM_clienteterapeuta_dispositivo = new java.util.HashSet();

    private java.util.Set ORM_dispositivo_organizacion = new java.util.HashSet();

    private orm.Asigdisporganizacion asigdisporganizacion;

    private java.util.Set ORM_detalleasignacion = new java.util.HashSet();

    private orm.Detasigterapeuta detasigterapeuta;

    private java.util.Set ORM_asigdisporganizacion_dispositivo = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setNombre(String value) {
        this.nombre = value;
    }

    public String getNombre() {
        return nombre;
    }

    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setNumserie(String value) {
        this.numserie = value;
    }

    public String getNumserie() {
        return numserie;
    }

    public void setTipodispositivo(orm.Tipodispositivo value) {
        if (this.tipodispositivo != value) {
            orm.Tipodispositivo ltipodispositivo = this.tipodispositivo;
            this.tipodispositivo = value;
            if (value != null) {
                tipodispositivo.setDispositivo(this);
            }
            if (ltipodispositivo != null && ltipodispositivo.getDispositivo() == this) {
                ltipodispositivo.setDispositivo(null);
            }
        }
    }

    public orm.Tipodispositivo getTipodispositivo() {
        return tipodispositivo;
    }

    public void setDotosdispositivo(orm.Datosdispositivo value) {
        if (this.dotosdispositivo != value) {
            orm.Datosdispositivo ldotosdispositivo = this.dotosdispositivo;
            this.dotosdispositivo = value;
            if (value != null) {
                dotosdispositivo.setDispositivo(this);
            }
            if (ldotosdispositivo != null && ldotosdispositivo.getDispositivo() == this) {
                ldotosdispositivo.setDispositivo(null);
            }
        }
    }

    public orm.Datosdispositivo getDotosdispositivo() {
        return dotosdispositivo;
    }

    private void setORM_Clienteterapeuta_dispositivo(java.util.Set value) {
        this.ORM_clienteterapeuta_dispositivo = value;
    }

    private java.util.Set getORM_Clienteterapeuta_dispositivo() {
        return ORM_clienteterapeuta_dispositivo;
    }

    public final orm.Clienteterapeuta_dispositivoSetCollection clienteterapeuta_dispositivo = new orm.Clienteterapeuta_dispositivoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DISPOSITIVO_CLIENTETERAPEUTA_DISPOSITIVO, orm.ORMConstants.KEY_CLIENTETERAPEUTA_DISPOSITIVO_DISPOSITIVO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    private void setORM_Dispositivo_organizacion(java.util.Set value) {
        this.ORM_dispositivo_organizacion = value;
    }

    private java.util.Set getORM_Dispositivo_organizacion() {
        return ORM_dispositivo_organizacion;
    }

    public final orm.Dispositivo_organizacionSetCollection dispositivo_organizacion = new orm.Dispositivo_organizacionSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DISPOSITIVO_DISPOSITIVO_ORGANIZACION, orm.ORMConstants.KEY_DISPOSITIVO_ORGANIZACION_DISPOSITIVO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setAsigdisporganizacion(orm.Asigdisporganizacion value) {
        if (this.asigdisporganizacion != value) {
            orm.Asigdisporganizacion lasigdisporganizacion = this.asigdisporganizacion;
            this.asigdisporganizacion = value;
            if (value != null) {
                asigdisporganizacion.setDispositivo(this);
            }
            if (lasigdisporganizacion != null && lasigdisporganizacion.getDispositivo() == this) {
                lasigdisporganizacion.setDispositivo(null);
            }
        }
    }

    public orm.Asigdisporganizacion getAsigdisporganizacion() {
        return asigdisporganizacion;
    }

    private void setORM_Detalleasignacion(java.util.Set value) {
        this.ORM_detalleasignacion = value;
    }

    private java.util.Set getORM_Detalleasignacion() {
        return ORM_detalleasignacion;
    }

    public final orm.DetalleasignacionSetCollection detalleasignacion = new orm.DetalleasignacionSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DISPOSITIVO_DETALLEASIGNACION, orm.ORMConstants.KEY_DETALLEASIGNACION_DISPOSITIVO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setDetasigterapeuta(orm.Detasigterapeuta value) {
        if (this.detasigterapeuta != value) {
            orm.Detasigterapeuta ldetasigterapeuta = this.detasigterapeuta;
            this.detasigterapeuta = value;
            if (value != null) {
                detasigterapeuta.setDispositivo(this);
            }
            if (ldetasigterapeuta != null && ldetasigterapeuta.getDispositivo() == this) {
                ldetasigterapeuta.setDispositivo(null);
            }
        }
    }

    public orm.Detasigterapeuta getDetasigterapeuta() {
        return detasigterapeuta;
    }

    private void setORM_Asigdisporganizacion_dispositivo(java.util.Set value) {
        this.ORM_asigdisporganizacion_dispositivo = value;
    }

    private java.util.Set getORM_Asigdisporganizacion_dispositivo() {
        return ORM_asigdisporganizacion_dispositivo;
    }

    public final orm.Asigdisporganizacion_dispositivoSetCollection asigdisporganizacion_dispositivo = new orm.Asigdisporganizacion_dispositivoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DISPOSITIVO_ASIGDISPORGANIZACION_DISPOSITIVO, orm.ORMConstants.KEY_ASIGDISPORGANIZACION_DISPOSITIVO_DISPOSITIVO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
