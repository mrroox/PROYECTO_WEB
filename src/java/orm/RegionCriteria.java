/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegionCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final CollectionExpression direccion;

    public RegionCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        nombre = new StringExpression("nombre", this);
        direccion = new CollectionExpression("ORM_Direccion", this);
    }

    public RegionCriteria(PersistentSession session) {
        this(session.createCriteria(Region.class));
    }

    public RegionCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DireccionCriteria createDireccionCriteria() {
        return new DireccionCriteria(createCriteria("ORM_Direccion"));
    }

    public Region uniqueRegion() {
        return (Region) super.uniqueResult();
    }

    public Region[] listRegion() {
        java.util.List list = super.list();
        return (Region[]) list.toArray(new Region[list.size()]);
    }
}
