/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class AsigdisporganizacionDAO {

    public static Asigdisporganizacion loadAsigdisporganizacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion getAsigdisporganizacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdisporganizacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion getAsigdisporganizacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getAsigdisporganizacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdisporganizacion) session.load(orm.Asigdisporganizacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion getAsigdisporganizacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Asigdisporganizacion) session.get(orm.Asigdisporganizacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdisporganizacion) session.load(orm.Asigdisporganizacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion getAsigdisporganizacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Asigdisporganizacion) session.get(orm.Asigdisporganizacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdisporganizacion(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryAsigdisporganizacion(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion[] listAsigdisporganizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdisporganizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion[] listAsigdisporganizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listAsigdisporganizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion as Asigdisporganizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryAsigdisporganizacion(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion as Asigdisporganizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdisporganizacion", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion[] listAsigdisporganizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryAsigdisporganizacion(session, condition, orderBy);
            return (Asigdisporganizacion[]) list.toArray(new Asigdisporganizacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion[] listAsigdisporganizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryAsigdisporganizacion(session, condition, orderBy, lockMode);
            return (Asigdisporganizacion[]) list.toArray(new Asigdisporganizacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadAsigdisporganizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Asigdisporganizacion[] asigdisporganizacions = listAsigdisporganizacionByQuery(session, condition, orderBy);
        if (asigdisporganizacions != null && asigdisporganizacions.length > 0) {
            return asigdisporganizacions[0];
        } else {
            return null;
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Asigdisporganizacion[] asigdisporganizacions = listAsigdisporganizacionByQuery(session, condition, orderBy, lockMode);
        if (asigdisporganizacions != null && asigdisporganizacions.length > 0) {
            return asigdisporganizacions[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdisporganizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateAsigdisporganizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion as Asigdisporganizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateAsigdisporganizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Asigdisporganizacion as Asigdisporganizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Asigdisporganizacion", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion createAsigdisporganizacion() {
        return new orm.Asigdisporganizacion();
    }

    public static boolean save(orm.Asigdisporganizacion asigdisporganizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(asigdisporganizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Asigdisporganizacion asigdisporganizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(asigdisporganizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdisporganizacion asigdisporganizacion) throws PersistentException {
        try {
            if (asigdisporganizacion.getOrganizacion() != null) {
                asigdisporganizacion.getOrganizacion().setAsigdisporganizacion(null);
            }

            if (asigdisporganizacion.getTerapeuta() != null) {
                asigdisporganizacion.getTerapeuta().setAsigdisporganizacion(null);
            }

            if (asigdisporganizacion.getDispositivo() != null) {
                asigdisporganizacion.getDispositivo().setAsigdisporganizacion(null);
            }

            orm.Asigdisporganizacion_dispositivo[] lAsigdisporganizacion_dispositivos = asigdisporganizacion.asigdisporganizacion_dispositivo.toArray();
            for (int i = 0; i < lAsigdisporganizacion_dispositivos.length; i++) {
                lAsigdisporganizacion_dispositivos[i].setAsigdisporganizacion(null);
            }
            return delete(asigdisporganizacion);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Asigdisporganizacion asigdisporganizacion, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (asigdisporganizacion.getOrganizacion() != null) {
                asigdisporganizacion.getOrganizacion().setAsigdisporganizacion(null);
            }

            if (asigdisporganizacion.getTerapeuta() != null) {
                asigdisporganizacion.getTerapeuta().setAsigdisporganizacion(null);
            }

            if (asigdisporganizacion.getDispositivo() != null) {
                asigdisporganizacion.getDispositivo().setAsigdisporganizacion(null);
            }

            orm.Asigdisporganizacion_dispositivo[] lAsigdisporganizacion_dispositivos = asigdisporganizacion.asigdisporganizacion_dispositivo.toArray();
            for (int i = 0; i < lAsigdisporganizacion_dispositivos.length; i++) {
                lAsigdisporganizacion_dispositivos[i].setAsigdisporganizacion(null);
            }
            try {
                session.delete(asigdisporganizacion);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Asigdisporganizacion asigdisporganizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(asigdisporganizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Asigdisporganizacion asigdisporganizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(asigdisporganizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Asigdisporganizacion loadAsigdisporganizacionByCriteria(AsigdisporganizacionCriteria asigdisporganizacionCriteria) {
        Asigdisporganizacion[] asigdisporganizacions = listAsigdisporganizacionByCriteria(asigdisporganizacionCriteria);
        if (asigdisporganizacions == null || asigdisporganizacions.length == 0) {
            return null;
        }
        return asigdisporganizacions[0];
    }

    public static Asigdisporganizacion[] listAsigdisporganizacionByCriteria(AsigdisporganizacionCriteria asigdisporganizacionCriteria) {
        return asigdisporganizacionCriteria.listAsigdisporganizacion();
    }
}
