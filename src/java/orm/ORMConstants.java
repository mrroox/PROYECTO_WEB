/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {

    final int KEY_APODERADO_APODERADO_PACIENTE = -149387303;

    final int KEY_APODERADO_PACIENTE_APODERADO = 657886097;

    final int KEY_APODERADO_PACIENTE_PACIENTE = 1151005243;

    final int KEY_APODERADO_PERSONA = 1739127912;

    final int KEY_ASIGDISPCLIENTE_CLIENTETERAPEUTA = -1504470358;

    final int KEY_ASIGDISPCLIENTE_DETALLEASIGNACION_ASIGDISPCLIENTE = 1177755185;

    final int KEY_ASIGDISPCLIENTE_PACIENTE = 933483176;

    final int KEY_ASIGDISPORGANIZACION_ASIGDISPORGANIZACION_DISPOSITIVO = 1467610321;

    final int KEY_ASIGDISPORGANIZACION_DISPOSITIVO = -254258104;

    final int KEY_ASIGDISPORGANIZACION_DISPOSITIVO_ASIGDISPORGANIZACION = 871945709;

    final int KEY_ASIGDISPORGANIZACION_DISPOSITIVO_DISPOSITIVO = 2105947930;

    final int KEY_ASIGDISPORGANIZACION_ORGANIZACION = 981403149;

    final int KEY_ASIGDISPORGANIZACION_TERAPEUTA = -599419740;

    final int KEY_ASIGDISPTERAPEUTA_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA = -1970993198;

    final int KEY_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA_ASIGDISPTERAPEUTA = -1789014774;

    final int KEY_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA_DETASIGTERAPEUTA = -1420721861;

    final int KEY_ASIGDISPTERAPEUTA_PACIENTE = -598526379;

    final int KEY_ASIGDISPTERAPEUTA_TERAPEUTA = -1424512823;

    final int KEY_CARGO_TERAPEUTA_CARGO = 1838840199;

    final int KEY_CLIENTETERAPEUTA_ASIGDISPCLIENTE = -738959972;

    final int KEY_CLIENTETERAPEUTA_CLIENTETERAPEUTA_DISPOSITIVO = -1907306767;

    final int KEY_CLIENTETERAPEUTA_CLIENTETERAPEUTA_PACIENTE = -1875406575;

    final int KEY_CLIENTETERAPEUTA_DISPOSITIVO_CLIENTETERAPEUTA = 1102092941;

    final int KEY_CLIENTETERAPEUTA_DISPOSITIVO_DISPOSITIVO = 1218113591;

    final int KEY_CLIENTETERAPEUTA_EVALUACION = 1021743641;

    final int KEY_CLIENTETERAPEUTA_PACIENTE_CLIENTETERAPEUTA = -618742059;

    final int KEY_CLIENTETERAPEUTA_PACIENTE_PACIENTE = -52473389;

    final int KEY_CLIENTETERAPEUTA_PERSONA = -2091156992;

    final int KEY_DATOSDEUSODIARIO_DATOSDISPOCITIVO_DATOSDEUSODIARIO = 24637020;

    final int KEY_DATOSDISPOCITIVO_DATOSDEUSODIARIO_DATOSDEUSODIARIO = -39161690;

    final int KEY_DATOSDISPOCITIVO_DATOSDEUSODIARIO_DATOSDISPOCITIVO = 1828158139;

    final int KEY_DATOSDISPOCITIVO_JUEGO_DATOSDISPOCITIVO = 1143840764;

    final int KEY_DATOSDISPOCITIVO_JUEGO_JUEGO = 469122220;

    final int KEY_DATOSDISPOSITIVO_DATOSDISPOCITIVO_DATOSDEUSODIARIO = -1230421727;

    final int KEY_DATOSDISPOSITIVO_DATOSDISPOCITIVO_JUEGO = 396906930;

    final int KEY_DATOSDISPOSITIVO_DATOSNIVELCOMPLETADO = 597425008;

    final int KEY_DATOSDISPOSITIVO_DATOSUSOGENERAL = -426398711;

    final int KEY_DATOSDISPOSITIVO_DATOSUSONIVEL = -1392733341;

    final int KEY_DATOSDISPOSITIVO_DISPOSITIVO = 1493301224;

    final int KEY_DATOSNIVELCOMPLETADO_DATOSDISPOSITIVO = -1829952978;

    final int KEY_DATOSUSOGENERAL_DATOSDISPOSITIVO = -1163805053;

    final int KEY_DATOSUSONIVEL_DATOSDISPOSITIVO = -1367207895;

    final int KEY_DETALLEASIGNACION_ASIGDISPCLIENTE_ASIGDISPCLIENTE = 230846065;

    final int KEY_DETALLEASIGNACION_ASIGDISPCLIENTE_DETALLEASIGNACION = -1678744728;

    final int KEY_DETALLEASIGNACION_DETALLEASIGNACION_ASIGDISPCLIENTE = -2102905944;

    final int KEY_DETALLEASIGNACION_DISPOSITIVO = -347333967;

    final int KEY_DETASIGTERAPEUTA_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA = -295382269;

    final int KEY_DETASIGTERAPEUTA_DISPOSITIVO = 2104273692;

    final int KEY_DIRECCION_ORGANIZACION = -1211635277;

    final int KEY_DIRECCION_PAIS = 422467754;

    final int KEY_DIRECCION_PERSONA = 1605620605;

    final int KEY_DIRECCION_REGION = -1969494781;

    final int KEY_DISPOSITIVO_ASIGDISPORGANIZACION = -849922716;

    final int KEY_DISPOSITIVO_ASIGDISPORGANIZACION_DISPOSITIVO = -871346634;

    final int KEY_DISPOSITIVO_CLIENTETERAPEUTA_DISPOSITIVO = 595210707;

    final int KEY_DISPOSITIVO_DETALLEASIGNACION = 2113587761;

    final int KEY_DISPOSITIVO_DETASIGTERAPEUTA = 351326520;

    final int KEY_DISPOSITIVO_DISPOSITIVO_ORGANIZACION = -2020471840;

    final int KEY_DISPOSITIVO_DOTOSDISPOSITIVO = 81379218;

    final int KEY_DISPOSITIVO_ORGANIZACION_DISPOSITIVO = -461113404;

    final int KEY_DISPOSITIVO_ORGANIZACION_ORGANIZACION = -1136143855;

    final int KEY_DISPOSITIVO_TIPODISPOSITIVO = 706658575;

    final int KEY_EVALUACION_CLIENTETERAPEUTA = 1984223077;

    final int KEY_EVALUACION_PACIENTE = -2018575773;

    final int KEY_EVALUACIONTERAPEUTA_PACIENTE = 543651856;

    final int KEY_EVALUACIONTERAPEUTA_TERAPEUTA = -376725906;

    final int KEY_EXPEDIENTEMEDICO_PACIENTE = -1039093792;

    final int KEY_JUEGO_DATOSDISPOCITIVO_JUEGO = -287168634;

    final int KEY_JUEGO_JUEGO_PATRON = -54461272;

    final int KEY_JUEGO_PATRON_JUEGO = 893745166;

    final int KEY_JUEGO_PATRON_PATRON = 2090058402;

    final int KEY_ORGANIZACION_ASIGDISPORGANIZACION = 460386481;

    final int KEY_ORGANIZACION_DIRECCION = 1102567861;

    final int KEY_ORGANIZACION_DISPOSITIVO_ORGANIZACION = 1888704685;

    final int KEY_ORGANIZACION_TELEFONO = 2031270351;

    final int KEY_ORGANIZACION_TERAPEUTA = -530646094;

    final int KEY_PACIENTE_APODERADO_PACIENTE = -950428285;

    final int KEY_PACIENTE_ASIGDISPCLIENTE = 1963578010;

    final int KEY_PACIENTE_ASIGDISPTERAPEUTA = 815600589;

    final int KEY_PACIENTE_CLIENTETERAPEUTA_PACIENTE = -151691121;

    final int KEY_PACIENTE_EVALUACION = -2007009701;

    final int KEY_PACIENTE_EVALUACIONTERAPEUTA = -2013270222;

    final int KEY_PACIENTE_EXPEDIENTEMEDICO = -1179288130;

    final int KEY_PACIENTE_PACIENTE_TERAPEUTA = 470318925;

    final int KEY_PACIENTE_PERSONA = 1308413182;

    final int KEY_PACIENTE_TERAPEUTA_PACIENTE = -1576242255;

    final int KEY_PACIENTE_TERAPEUTA_TERAPEUTA = -1668933907;

    final int KEY_PAIS_DIRECCION = 1835458796;

    final int KEY_PATRON_JUEGO_PATRON = -69810052;

    final int KEY_PERSONA_APODERADO = -1179323352;

    final int KEY_PERSONA_CLIENTETERAPEUTA = 1020626630;

    final int KEY_PERSONA_DIRECCION = 1067364413;

    final int KEY_PERSONA_PACIENTE = -1125016892;

    final int KEY_PERSONA_TELEFONO = 1060303431;

    final int KEY_PERSONA_TERAPEUTA = -565849542;

    final int KEY_REGION_DIRECCION = 644387269;

    final int KEY_TELEFONO_ORGANIZACION = 481685039;

    final int KEY_TELEFONO_PERSONA = 536605057;

    final int KEY_TERAPEUTA_ASIGDISPORGANIZACION = 720402504;

    final int KEY_TERAPEUTA_ASIGDISPTERAPEUTA = 505201737;

    final int KEY_TERAPEUTA_CARGO_CARGO = 290404167;

    final int KEY_TERAPEUTA_CARGO_TERAPEUTA = 1255473464;

    final int KEY_TERAPEUTA_EVALUACIONTERAPEUTA = 341143726;

    final int KEY_TERAPEUTA_ORGANIZACION = 799976150;

    final int KEY_TERAPEUTA_PACIENTE_TERAPEUTA = -562110895;

    final int KEY_TERAPEUTA_PERSONA = -1052006278;

    final int KEY_TERAPEUTA_TERAPEUTA_CARGO = 224071416;

    final int KEY_TIPODISPOSITIVO_DISPOSITIVO = -130963441;

}
