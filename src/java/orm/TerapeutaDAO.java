/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class TerapeutaDAO {

    public static Terapeuta loadTerapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta getTerapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getTerapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta loadTerapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta getTerapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getTerapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta loadTerapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Terapeuta) session.load(orm.Terapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta getTerapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Terapeuta) session.get(orm.Terapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta loadTerapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Terapeuta) session.load(orm.Terapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta getTerapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Terapeuta) session.get(orm.Terapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryTerapeuta(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryTerapeuta(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta[] listTerapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listTerapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta[] listTerapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listTerapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta as Terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta as Terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Terapeuta", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta[] listTerapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryTerapeuta(session, condition, orderBy);
            return (Terapeuta[]) list.toArray(new Terapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta[] listTerapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryTerapeuta(session, condition, orderBy, lockMode);
            return (Terapeuta[]) list.toArray(new Terapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta loadTerapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta loadTerapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta loadTerapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Terapeuta[] terapeutas = listTerapeutaByQuery(session, condition, orderBy);
        if (terapeutas != null && terapeutas.length > 0) {
            return terapeutas[0];
        } else {
            return null;
        }
    }

    public static Terapeuta loadTerapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Terapeuta[] terapeutas = listTerapeutaByQuery(session, condition, orderBy, lockMode);
        if (terapeutas != null && terapeutas.length > 0) {
            return terapeutas[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateTerapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateTerapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateTerapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateTerapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateTerapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta as Terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateTerapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta as Terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Terapeuta", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta createTerapeuta() {
        return new orm.Terapeuta();
    }

    public static boolean save(orm.Terapeuta terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Terapeuta terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Terapeuta terapeuta) throws PersistentException {
        try {
            if (terapeuta.getPersona() != null) {
                terapeuta.getPersona().setTerapeuta(null);
            }

            if (terapeuta.getOrganizacion() != null) {
                terapeuta.getOrganizacion().terapeuta.remove(terapeuta);
            }

            orm.Paciente_terapeuta[] lPaciente_terapeutas = terapeuta.paciente_terapeuta.toArray();
            for (int i = 0; i < lPaciente_terapeutas.length; i++) {
                lPaciente_terapeutas[i].setTerapeuta(null);
            }
            orm.Terapeuta_cargo[] lTerapeuta_cargos = terapeuta.terapeuta_cargo.toArray();
            for (int i = 0; i < lTerapeuta_cargos.length; i++) {
                lTerapeuta_cargos[i].setTerapeuta(null);
            }
            if (terapeuta.getAsigdisporganizacion() != null) {
                terapeuta.getAsigdisporganizacion().setTerapeuta(null);
            }

            if (terapeuta.getEvaluacionterapeuta() != null) {
                terapeuta.getEvaluacionterapeuta().setTerapeuta(null);
            }

            if (terapeuta.getAsigdispterapeuta() != null) {
                terapeuta.getAsigdispterapeuta().setTerapeuta(null);
            }

            return delete(terapeuta);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Terapeuta terapeuta, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (terapeuta.getPersona() != null) {
                terapeuta.getPersona().setTerapeuta(null);
            }

            if (terapeuta.getOrganizacion() != null) {
                terapeuta.getOrganizacion().terapeuta.remove(terapeuta);
            }

            orm.Paciente_terapeuta[] lPaciente_terapeutas = terapeuta.paciente_terapeuta.toArray();
            for (int i = 0; i < lPaciente_terapeutas.length; i++) {
                lPaciente_terapeutas[i].setTerapeuta(null);
            }
            orm.Terapeuta_cargo[] lTerapeuta_cargos = terapeuta.terapeuta_cargo.toArray();
            for (int i = 0; i < lTerapeuta_cargos.length; i++) {
                lTerapeuta_cargos[i].setTerapeuta(null);
            }
            if (terapeuta.getAsigdisporganizacion() != null) {
                terapeuta.getAsigdisporganizacion().setTerapeuta(null);
            }

            if (terapeuta.getEvaluacionterapeuta() != null) {
                terapeuta.getEvaluacionterapeuta().setTerapeuta(null);
            }

            if (terapeuta.getAsigdispterapeuta() != null) {
                terapeuta.getAsigdispterapeuta().setTerapeuta(null);
            }

            try {
                session.delete(terapeuta);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Terapeuta terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Terapeuta terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta loadTerapeutaByCriteria(TerapeutaCriteria terapeutaCriteria) {
        Terapeuta[] terapeutas = listTerapeutaByCriteria(terapeutaCriteria);
        if (terapeutas == null || terapeutas.length == 0) {
            return null;
        }
        return terapeutas[0];
    }

    public static Terapeuta[] listTerapeutaByCriteria(TerapeutaCriteria terapeutaCriteria) {
        return terapeutaCriteria.listTerapeuta();
    }
}
