/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DetalleasignacionCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression observaciones;
    public final StringExpression fecha_asignacion;
    public final StringExpression fecha_devolucion;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final CollectionExpression detalleasignacion_asigdispcliente;

    public DetalleasignacionCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        observaciones = new StringExpression("observaciones", this);
        fecha_asignacion = new StringExpression("fecha_asignacion", this);
        fecha_devolucion = new StringExpression("fecha_devolucion", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
        detalleasignacion_asigdispcliente = new CollectionExpression("ORM_Detalleasignacion_asigdispcliente", this);
    }

    public DetalleasignacionCriteria(PersistentSession session) {
        this(session.createCriteria(Detalleasignacion.class));
    }

    public DetalleasignacionCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public Detalleasignacion_asigdispclienteCriteria createDetalleasignacion_asigdispclienteCriteria() {
        return new Detalleasignacion_asigdispclienteCriteria(createCriteria("ORM_Detalleasignacion_asigdispcliente"));
    }

    public Detalleasignacion uniqueDetalleasignacion() {
        return (Detalleasignacion) super.uniqueResult();
    }

    public Detalleasignacion[] listDetalleasignacion() {
        java.util.List list = super.list();
        return (Detalleasignacion[]) list.toArray(new Detalleasignacion[list.size()]);
    }
}
