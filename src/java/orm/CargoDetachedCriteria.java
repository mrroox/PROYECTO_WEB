/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CargoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final CollectionExpression terapeuta_cargo;

    public CargoDetachedCriteria() {
        super(orm.Cargo.class, orm.CargoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        terapeuta_cargo = new CollectionExpression("ORM_Terapeuta_cargo", this.getDetachedCriteria());
    }

    public CargoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.CargoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        terapeuta_cargo = new CollectionExpression("ORM_Terapeuta_cargo", this.getDetachedCriteria());
    }

    public Terapeuta_cargoDetachedCriteria createTerapeuta_cargoCriteria() {
        return new Terapeuta_cargoDetachedCriteria(createCriteria("ORM_Terapeuta_cargo"));
    }

    public Cargo uniqueCargo(PersistentSession session) {
        return (Cargo) super.createExecutableCriteria(session).uniqueResult();
    }

    public Cargo[] listCargo(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Cargo[]) list.toArray(new Cargo[list.size()]);
    }
}
