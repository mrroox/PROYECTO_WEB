/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosusogeneralCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression tiempototaluso;
    public final IntegerExpression nivelmaximologrado;
    public final IntegerExpression totalpatronesrealizados;
    public final IntegerExpression datosdispositivoId;
    public final AssociationExpression datosdispositivo;

    public DatosusogeneralCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        tiempototaluso = new IntegerExpression("tiempototaluso", this);
        nivelmaximologrado = new IntegerExpression("nivelmaximologrado", this);
        totalpatronesrealizados = new IntegerExpression("totalpatronesrealizados", this);
        datosdispositivoId = new IntegerExpression("datosdispositivo.id", this);
        datosdispositivo = new AssociationExpression("datosdispositivo", this);
    }

    public DatosusogeneralCriteria(PersistentSession session) {
        this(session.createCriteria(Datosusogeneral.class));
    }

    public DatosusogeneralCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DatosdispositivoCriteria createDatosdispositivoCriteria() {
        return new DatosdispositivoCriteria(createCriteria("datosdispositivo"));
    }

    public Datosusogeneral uniqueDatosusogeneral() {
        return (Datosusogeneral) super.uniqueResult();
    }

    public Datosusogeneral[] listDatosusogeneral() {
        java.util.List list = super.list();
        return (Datosusogeneral[]) list.toArray(new Datosusogeneral[list.size()]);
    }
}
