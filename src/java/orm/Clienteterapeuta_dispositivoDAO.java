/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Clienteterapeuta_dispositivoDAO {

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_dispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo getClienteterapeuta_dispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getClienteterapeuta_dispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_dispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo getClienteterapeuta_dispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getClienteterapeuta_dispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Clienteterapeuta_dispositivo) session.load(orm.Clienteterapeuta_dispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo getClienteterapeuta_dispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Clienteterapeuta_dispositivo) session.get(orm.Clienteterapeuta_dispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Clienteterapeuta_dispositivo) session.load(orm.Clienteterapeuta_dispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo getClienteterapeuta_dispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Clienteterapeuta_dispositivo) session.get(orm.Clienteterapeuta_dispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_dispositivo(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryClienteterapeuta_dispositivo(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_dispositivo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryClienteterapeuta_dispositivo(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo[] listClienteterapeuta_dispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listClienteterapeuta_dispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo[] listClienteterapeuta_dispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listClienteterapeuta_dispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_dispositivo(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_dispositivo as Clienteterapeuta_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_dispositivo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_dispositivo as Clienteterapeuta_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Clienteterapeuta_dispositivo", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo[] listClienteterapeuta_dispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryClienteterapeuta_dispositivo(session, condition, orderBy);
            return (Clienteterapeuta_dispositivo[]) list.toArray(new Clienteterapeuta_dispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo[] listClienteterapeuta_dispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryClienteterapeuta_dispositivo(session, condition, orderBy, lockMode);
            return (Clienteterapeuta_dispositivo[]) list.toArray(new Clienteterapeuta_dispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_dispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_dispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Clienteterapeuta_dispositivo[] clienteterapeuta_dispositivos = listClienteterapeuta_dispositivoByQuery(session, condition, orderBy);
        if (clienteterapeuta_dispositivos != null && clienteterapeuta_dispositivos.length > 0) {
            return clienteterapeuta_dispositivos[0];
        } else {
            return null;
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Clienteterapeuta_dispositivo[] clienteterapeuta_dispositivos = listClienteterapeuta_dispositivoByQuery(session, condition, orderBy, lockMode);
        if (clienteterapeuta_dispositivos != null && clienteterapeuta_dispositivos.length > 0) {
            return clienteterapeuta_dispositivos[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_dispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateClienteterapeuta_dispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_dispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateClienteterapeuta_dispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_dispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_dispositivo as Clienteterapeuta_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_dispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_dispositivo as Clienteterapeuta_dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Clienteterapeuta_dispositivo", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo createClienteterapeuta_dispositivo() {
        return new orm.Clienteterapeuta_dispositivo();
    }

    public static boolean save(orm.Clienteterapeuta_dispositivo clienteterapeuta_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(clienteterapeuta_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Clienteterapeuta_dispositivo clienteterapeuta_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(clienteterapeuta_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Clienteterapeuta_dispositivo clienteterapeuta_dispositivo) throws PersistentException {
        try {
            if (clienteterapeuta_dispositivo.getClienteterapeuta() != null) {
                clienteterapeuta_dispositivo.getClienteterapeuta().clienteterapeuta_dispositivo.remove(clienteterapeuta_dispositivo);
            }

            if (clienteterapeuta_dispositivo.getDispositivo() != null) {
                clienteterapeuta_dispositivo.getDispositivo().clienteterapeuta_dispositivo.remove(clienteterapeuta_dispositivo);
            }

            return delete(clienteterapeuta_dispositivo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Clienteterapeuta_dispositivo clienteterapeuta_dispositivo, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (clienteterapeuta_dispositivo.getClienteterapeuta() != null) {
                clienteterapeuta_dispositivo.getClienteterapeuta().clienteterapeuta_dispositivo.remove(clienteterapeuta_dispositivo);
            }

            if (clienteterapeuta_dispositivo.getDispositivo() != null) {
                clienteterapeuta_dispositivo.getDispositivo().clienteterapeuta_dispositivo.remove(clienteterapeuta_dispositivo);
            }

            try {
                session.delete(clienteterapeuta_dispositivo);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Clienteterapeuta_dispositivo clienteterapeuta_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(clienteterapeuta_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Clienteterapeuta_dispositivo clienteterapeuta_dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(clienteterapeuta_dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_dispositivo loadClienteterapeuta_dispositivoByCriteria(Clienteterapeuta_dispositivoCriteria clienteterapeuta_dispositivoCriteria) {
        Clienteterapeuta_dispositivo[] clienteterapeuta_dispositivos = listClienteterapeuta_dispositivoByCriteria(clienteterapeuta_dispositivoCriteria);
        if (clienteterapeuta_dispositivos == null || clienteterapeuta_dispositivos.length == 0) {
            return null;
        }
        return clienteterapeuta_dispositivos[0];
    }

    public static Clienteterapeuta_dispositivo[] listClienteterapeuta_dispositivoByCriteria(Clienteterapeuta_dispositivoCriteria clienteterapeuta_dispositivoCriteria) {
        return clienteterapeuta_dispositivoCriteria.listClienteterapeuta_dispositivo();
    }
}
