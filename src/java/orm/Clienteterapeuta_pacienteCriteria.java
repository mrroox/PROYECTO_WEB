/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Clienteterapeuta_pacienteCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_ingreso;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public Clienteterapeuta_pacienteCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha_ingreso = new StringExpression("fecha_ingreso", this);
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this);
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
    }

    public Clienteterapeuta_pacienteCriteria(PersistentSession session) {
        this(session.createCriteria(Clienteterapeuta_paciente.class));
    }

    public Clienteterapeuta_pacienteCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public ClienteterapeutaCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaCriteria(createCriteria("clienteterapeuta"));
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public Clienteterapeuta_paciente uniqueClienteterapeuta_paciente() {
        return (Clienteterapeuta_paciente) super.uniqueResult();
    }

    public Clienteterapeuta_paciente[] listClienteterapeuta_paciente() {
        java.util.List list = super.list();
        return (Clienteterapeuta_paciente[]) list.toArray(new Clienteterapeuta_paciente[list.size()]);
    }
}
