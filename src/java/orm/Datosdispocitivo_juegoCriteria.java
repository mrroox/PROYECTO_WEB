/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Datosdispocitivo_juegoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression datosdispocitivoId;
    public final AssociationExpression datosdispocitivo;
    public final IntegerExpression juegoId;
    public final AssociationExpression juego;

    public Datosdispocitivo_juegoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        datosdispocitivoId = new IntegerExpression("datosdispocitivo.id", this);
        datosdispocitivo = new AssociationExpression("datosdispocitivo", this);
        juegoId = new IntegerExpression("juego.id", this);
        juego = new AssociationExpression("juego", this);
    }

    public Datosdispocitivo_juegoCriteria(PersistentSession session) {
        this(session.createCriteria(Datosdispocitivo_juego.class));
    }

    public Datosdispocitivo_juegoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DatosdispositivoCriteria createDatosdispocitivoCriteria() {
        return new DatosdispositivoCriteria(createCriteria("datosdispocitivo"));
    }

    public JuegoCriteria createJuegoCriteria() {
        return new JuegoCriteria(createCriteria("juego"));
    }

    public Datosdispocitivo_juego uniqueDatosdispocitivo_juego() {
        return (Datosdispocitivo_juego) super.uniqueResult();
    }

    public Datosdispocitivo_juego[] listDatosdispocitivo_juego() {
        java.util.List list = super.list();
        return (Datosdispocitivo_juego[]) list.toArray(new Datosdispocitivo_juego[list.size()]);
    }
}
