/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class EvaluacionterapeutaDAO {

    public static Evaluacionterapeuta loadEvaluacionterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadEvaluacionterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta getEvaluacionterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getEvaluacionterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadEvaluacionterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta getEvaluacionterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getEvaluacionterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Evaluacionterapeuta) session.load(orm.Evaluacionterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta getEvaluacionterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Evaluacionterapeuta) session.get(orm.Evaluacionterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Evaluacionterapeuta) session.load(orm.Evaluacionterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta getEvaluacionterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Evaluacionterapeuta) session.get(orm.Evaluacionterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryEvaluacionterapeuta(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryEvaluacionterapeuta(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryEvaluacionterapeuta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryEvaluacionterapeuta(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta[] listEvaluacionterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listEvaluacionterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta[] listEvaluacionterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listEvaluacionterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryEvaluacionterapeuta(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Evaluacionterapeuta as Evaluacionterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryEvaluacionterapeuta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Evaluacionterapeuta as Evaluacionterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Evaluacionterapeuta", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta[] listEvaluacionterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryEvaluacionterapeuta(session, condition, orderBy);
            return (Evaluacionterapeuta[]) list.toArray(new Evaluacionterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta[] listEvaluacionterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryEvaluacionterapeuta(session, condition, orderBy, lockMode);
            return (Evaluacionterapeuta[]) list.toArray(new Evaluacionterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadEvaluacionterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadEvaluacionterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Evaluacionterapeuta[] evaluacionterapeutas = listEvaluacionterapeutaByQuery(session, condition, orderBy);
        if (evaluacionterapeutas != null && evaluacionterapeutas.length > 0) {
            return evaluacionterapeutas[0];
        } else {
            return null;
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Evaluacionterapeuta[] evaluacionterapeutas = listEvaluacionterapeutaByQuery(session, condition, orderBy, lockMode);
        if (evaluacionterapeutas != null && evaluacionterapeutas.length > 0) {
            return evaluacionterapeutas[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateEvaluacionterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateEvaluacionterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateEvaluacionterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateEvaluacionterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateEvaluacionterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Evaluacionterapeuta as Evaluacionterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateEvaluacionterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Evaluacionterapeuta as Evaluacionterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Evaluacionterapeuta", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta createEvaluacionterapeuta() {
        return new orm.Evaluacionterapeuta();
    }

    public static boolean save(orm.Evaluacionterapeuta evaluacionterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(evaluacionterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Evaluacionterapeuta evaluacionterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(evaluacionterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Evaluacionterapeuta evaluacionterapeuta) throws PersistentException {
        try {
            if (evaluacionterapeuta.getTerapeuta() != null) {
                evaluacionterapeuta.getTerapeuta().setEvaluacionterapeuta(null);
            }

            if (evaluacionterapeuta.getPaciente() != null) {
                evaluacionterapeuta.getPaciente().setEvaluacionterapeuta(null);
            }

            return delete(evaluacionterapeuta);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Evaluacionterapeuta evaluacionterapeuta, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (evaluacionterapeuta.getTerapeuta() != null) {
                evaluacionterapeuta.getTerapeuta().setEvaluacionterapeuta(null);
            }

            if (evaluacionterapeuta.getPaciente() != null) {
                evaluacionterapeuta.getPaciente().setEvaluacionterapeuta(null);
            }

            try {
                session.delete(evaluacionterapeuta);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Evaluacionterapeuta evaluacionterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(evaluacionterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Evaluacionterapeuta evaluacionterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(evaluacionterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Evaluacionterapeuta loadEvaluacionterapeutaByCriteria(EvaluacionterapeutaCriteria evaluacionterapeutaCriteria) {
        Evaluacionterapeuta[] evaluacionterapeutas = listEvaluacionterapeutaByCriteria(evaluacionterapeutaCriteria);
        if (evaluacionterapeutas == null || evaluacionterapeutas.length == 0) {
            return null;
        }
        return evaluacionterapeutas[0];
    }

    public static Evaluacionterapeuta[] listEvaluacionterapeutaByCriteria(EvaluacionterapeutaCriteria evaluacionterapeutaCriteria) {
        return evaluacionterapeutaCriteria.listEvaluacionterapeuta();
    }
}
