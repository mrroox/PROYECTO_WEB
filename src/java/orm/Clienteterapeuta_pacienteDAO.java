/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Clienteterapeuta_pacienteDAO {

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_pacienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente getClienteterapeuta_pacienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getClienteterapeuta_pacienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_pacienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente getClienteterapeuta_pacienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getClienteterapeuta_pacienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Clienteterapeuta_paciente) session.load(orm.Clienteterapeuta_paciente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente getClienteterapeuta_pacienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Clienteterapeuta_paciente) session.get(orm.Clienteterapeuta_paciente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Clienteterapeuta_paciente) session.load(orm.Clienteterapeuta_paciente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente getClienteterapeuta_pacienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Clienteterapeuta_paciente) session.get(orm.Clienteterapeuta_paciente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_paciente(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryClienteterapeuta_paciente(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_paciente(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryClienteterapeuta_paciente(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente[] listClienteterapeuta_pacienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listClienteterapeuta_pacienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente[] listClienteterapeuta_pacienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listClienteterapeuta_pacienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_paciente(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_paciente as Clienteterapeuta_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta_paciente(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_paciente as Clienteterapeuta_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Clienteterapeuta_paciente", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente[] listClienteterapeuta_pacienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryClienteterapeuta_paciente(session, condition, orderBy);
            return (Clienteterapeuta_paciente[]) list.toArray(new Clienteterapeuta_paciente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente[] listClienteterapeuta_pacienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryClienteterapeuta_paciente(session, condition, orderBy, lockMode);
            return (Clienteterapeuta_paciente[]) list.toArray(new Clienteterapeuta_paciente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_pacienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeuta_pacienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Clienteterapeuta_paciente[] clienteterapeuta_pacientes = listClienteterapeuta_pacienteByQuery(session, condition, orderBy);
        if (clienteterapeuta_pacientes != null && clienteterapeuta_pacientes.length > 0) {
            return clienteterapeuta_pacientes[0];
        } else {
            return null;
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Clienteterapeuta_paciente[] clienteterapeuta_pacientes = listClienteterapeuta_pacienteByQuery(session, condition, orderBy, lockMode);
        if (clienteterapeuta_pacientes != null && clienteterapeuta_pacientes.length > 0) {
            return clienteterapeuta_pacientes[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_pacienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateClienteterapeuta_pacienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_pacienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateClienteterapeuta_pacienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_pacienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_paciente as Clienteterapeuta_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeuta_pacienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta_paciente as Clienteterapeuta_paciente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Clienteterapeuta_paciente", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente createClienteterapeuta_paciente() {
        return new orm.Clienteterapeuta_paciente();
    }

    public static boolean save(orm.Clienteterapeuta_paciente clienteterapeuta_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(clienteterapeuta_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Clienteterapeuta_paciente clienteterapeuta_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(clienteterapeuta_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Clienteterapeuta_paciente clienteterapeuta_paciente) throws PersistentException {
        try {
            if (clienteterapeuta_paciente.getClienteterapeuta() != null) {
                clienteterapeuta_paciente.getClienteterapeuta().clienteterapeuta_paciente.remove(clienteterapeuta_paciente);
            }

            if (clienteterapeuta_paciente.getPaciente() != null) {
                clienteterapeuta_paciente.getPaciente().clienteterapeuta_paciente.remove(clienteterapeuta_paciente);
            }

            return delete(clienteterapeuta_paciente);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Clienteterapeuta_paciente clienteterapeuta_paciente, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (clienteterapeuta_paciente.getClienteterapeuta() != null) {
                clienteterapeuta_paciente.getClienteterapeuta().clienteterapeuta_paciente.remove(clienteterapeuta_paciente);
            }

            if (clienteterapeuta_paciente.getPaciente() != null) {
                clienteterapeuta_paciente.getPaciente().clienteterapeuta_paciente.remove(clienteterapeuta_paciente);
            }

            try {
                session.delete(clienteterapeuta_paciente);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Clienteterapeuta_paciente clienteterapeuta_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(clienteterapeuta_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Clienteterapeuta_paciente clienteterapeuta_paciente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(clienteterapeuta_paciente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta_paciente loadClienteterapeuta_pacienteByCriteria(Clienteterapeuta_pacienteCriteria clienteterapeuta_pacienteCriteria) {
        Clienteterapeuta_paciente[] clienteterapeuta_pacientes = listClienteterapeuta_pacienteByCriteria(clienteterapeuta_pacienteCriteria);
        if (clienteterapeuta_pacientes == null || clienteterapeuta_pacientes.length == 0) {
            return null;
        }
        return clienteterapeuta_pacientes[0];
    }

    public static Clienteterapeuta_paciente[] listClienteterapeuta_pacienteByCriteria(Clienteterapeuta_pacienteCriteria clienteterapeuta_pacienteCriteria) {
        return clienteterapeuta_pacienteCriteria.listClienteterapeuta_paciente();
    }
}
