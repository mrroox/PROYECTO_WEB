/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Dispositivo_organizacionDAO {

    public static Dispositivo_organizacion loadDispositivo_organizacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivo_organizacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion getDispositivo_organizacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDispositivo_organizacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivo_organizacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion getDispositivo_organizacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDispositivo_organizacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Dispositivo_organizacion) session.load(orm.Dispositivo_organizacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion getDispositivo_organizacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Dispositivo_organizacion) session.get(orm.Dispositivo_organizacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Dispositivo_organizacion) session.load(orm.Dispositivo_organizacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion getDispositivo_organizacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Dispositivo_organizacion) session.get(orm.Dispositivo_organizacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo_organizacion(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDispositivo_organizacion(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo_organizacion(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDispositivo_organizacion(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion[] listDispositivo_organizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDispositivo_organizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion[] listDispositivo_organizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDispositivo_organizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo_organizacion(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo_organizacion as Dispositivo_organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo_organizacion(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo_organizacion as Dispositivo_organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Dispositivo_organizacion", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion[] listDispositivo_organizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDispositivo_organizacion(session, condition, orderBy);
            return (Dispositivo_organizacion[]) list.toArray(new Dispositivo_organizacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion[] listDispositivo_organizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDispositivo_organizacion(session, condition, orderBy, lockMode);
            return (Dispositivo_organizacion[]) list.toArray(new Dispositivo_organizacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivo_organizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivo_organizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Dispositivo_organizacion[] dispositivo_organizacions = listDispositivo_organizacionByQuery(session, condition, orderBy);
        if (dispositivo_organizacions != null && dispositivo_organizacions.length > 0) {
            return dispositivo_organizacions[0];
        } else {
            return null;
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Dispositivo_organizacion[] dispositivo_organizacions = listDispositivo_organizacionByQuery(session, condition, orderBy, lockMode);
        if (dispositivo_organizacions != null && dispositivo_organizacions.length > 0) {
            return dispositivo_organizacions[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDispositivo_organizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDispositivo_organizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDispositivo_organizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDispositivo_organizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDispositivo_organizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo_organizacion as Dispositivo_organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDispositivo_organizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo_organizacion as Dispositivo_organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Dispositivo_organizacion", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion createDispositivo_organizacion() {
        return new orm.Dispositivo_organizacion();
    }

    public static boolean save(orm.Dispositivo_organizacion dispositivo_organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(dispositivo_organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Dispositivo_organizacion dispositivo_organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(dispositivo_organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Dispositivo_organizacion dispositivo_organizacion) throws PersistentException {
        try {
            if (dispositivo_organizacion.getDispositivo() != null) {
                dispositivo_organizacion.getDispositivo().dispositivo_organizacion.remove(dispositivo_organizacion);
            }

            if (dispositivo_organizacion.getOrganizacion() != null) {
                dispositivo_organizacion.getOrganizacion().dispositivo_organizacion.remove(dispositivo_organizacion);
            }

            return delete(dispositivo_organizacion);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Dispositivo_organizacion dispositivo_organizacion, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (dispositivo_organizacion.getDispositivo() != null) {
                dispositivo_organizacion.getDispositivo().dispositivo_organizacion.remove(dispositivo_organizacion);
            }

            if (dispositivo_organizacion.getOrganizacion() != null) {
                dispositivo_organizacion.getOrganizacion().dispositivo_organizacion.remove(dispositivo_organizacion);
            }

            try {
                session.delete(dispositivo_organizacion);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Dispositivo_organizacion dispositivo_organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(dispositivo_organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Dispositivo_organizacion dispositivo_organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(dispositivo_organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo_organizacion loadDispositivo_organizacionByCriteria(Dispositivo_organizacionCriteria dispositivo_organizacionCriteria) {
        Dispositivo_organizacion[] dispositivo_organizacions = listDispositivo_organizacionByCriteria(dispositivo_organizacionCriteria);
        if (dispositivo_organizacions == null || dispositivo_organizacions.length == 0) {
            return null;
        }
        return dispositivo_organizacions[0];
    }

    public static Dispositivo_organizacion[] listDispositivo_organizacionByCriteria(Dispositivo_organizacionCriteria dispositivo_organizacionCriteria) {
        return dispositivo_organizacionCriteria.listDispositivo_organizacion();
    }
}
