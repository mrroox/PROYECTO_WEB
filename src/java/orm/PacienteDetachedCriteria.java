/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PacienteDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression expedientemedicoId;
    public final AssociationExpression expedientemedico;
    public final IntegerExpression personaId;
    public final AssociationExpression persona;
    public final IntegerExpression personadireccionid;
    public final CollectionExpression clienteterapeuta_paciente;
    public final IntegerExpression asigdispclienteId;
    public final AssociationExpression asigdispcliente;
    public final IntegerExpression evaluacionId;
    public final AssociationExpression evaluacion;
    public final CollectionExpression apoderado_paciente;
    public final CollectionExpression paciente_terapeuta;
    public final IntegerExpression evaluacionterapeutaId;
    public final AssociationExpression evaluacionterapeuta;
    public final IntegerExpression asigdispterapeutaId;
    public final AssociationExpression asigdispterapeuta;

    public PacienteDetachedCriteria() {
        super(orm.Paciente.class, orm.PacienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        expedientemedicoId = new IntegerExpression("expedientemedico.id", this.getDetachedCriteria());
        expedientemedico = new AssociationExpression("expedientemedico", this.getDetachedCriteria());
        personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
        persona = new AssociationExpression("persona", this.getDetachedCriteria());
        personadireccionid = new IntegerExpression("personadireccionid", this.getDetachedCriteria());
        clienteterapeuta_paciente = new CollectionExpression("ORM_Clienteterapeuta_paciente", this.getDetachedCriteria());
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this.getDetachedCriteria());
        asigdispcliente = new AssociationExpression("asigdispcliente", this.getDetachedCriteria());
        evaluacionId = new IntegerExpression("evaluacion.id", this.getDetachedCriteria());
        evaluacion = new AssociationExpression("evaluacion", this.getDetachedCriteria());
        apoderado_paciente = new CollectionExpression("ORM_Apoderado_paciente", this.getDetachedCriteria());
        paciente_terapeuta = new CollectionExpression("ORM_Paciente_terapeuta", this.getDetachedCriteria());
        evaluacionterapeutaId = new IntegerExpression("evaluacionterapeuta.id", this.getDetachedCriteria());
        evaluacionterapeuta = new AssociationExpression("evaluacionterapeuta", this.getDetachedCriteria());
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this.getDetachedCriteria());
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this.getDetachedCriteria());
    }

    public PacienteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.PacienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        expedientemedicoId = new IntegerExpression("expedientemedico.id", this.getDetachedCriteria());
        expedientemedico = new AssociationExpression("expedientemedico", this.getDetachedCriteria());
        personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
        persona = new AssociationExpression("persona", this.getDetachedCriteria());
        personadireccionid = new IntegerExpression("personadireccionid", this.getDetachedCriteria());
        clienteterapeuta_paciente = new CollectionExpression("ORM_Clienteterapeuta_paciente", this.getDetachedCriteria());
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this.getDetachedCriteria());
        asigdispcliente = new AssociationExpression("asigdispcliente", this.getDetachedCriteria());
        evaluacionId = new IntegerExpression("evaluacion.id", this.getDetachedCriteria());
        evaluacion = new AssociationExpression("evaluacion", this.getDetachedCriteria());
        apoderado_paciente = new CollectionExpression("ORM_Apoderado_paciente", this.getDetachedCriteria());
        paciente_terapeuta = new CollectionExpression("ORM_Paciente_terapeuta", this.getDetachedCriteria());
        evaluacionterapeutaId = new IntegerExpression("evaluacionterapeuta.id", this.getDetachedCriteria());
        evaluacionterapeuta = new AssociationExpression("evaluacionterapeuta", this.getDetachedCriteria());
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this.getDetachedCriteria());
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this.getDetachedCriteria());
    }

    public ExpedientemedicoDetachedCriteria createExpedientemedicoCriteria() {
        return new ExpedientemedicoDetachedCriteria(createCriteria("expedientemedico"));
    }

    public PersonaDetachedCriteria createPersonaCriteria() {
        return new PersonaDetachedCriteria(createCriteria("persona"));
    }

    public Clienteterapeuta_pacienteDetachedCriteria createClienteterapeuta_pacienteCriteria() {
        return new Clienteterapeuta_pacienteDetachedCriteria(createCriteria("ORM_Clienteterapeuta_paciente"));
    }

    public AsigdispclienteDetachedCriteria createAsigdispclienteCriteria() {
        return new AsigdispclienteDetachedCriteria(createCriteria("asigdispcliente"));
    }

    public EvaluacionDetachedCriteria createEvaluacionCriteria() {
        return new EvaluacionDetachedCriteria(createCriteria("evaluacion"));
    }

    public Apoderado_pacienteDetachedCriteria createApoderado_pacienteCriteria() {
        return new Apoderado_pacienteDetachedCriteria(createCriteria("ORM_Apoderado_paciente"));
    }

    public Paciente_terapeutaDetachedCriteria createPaciente_terapeutaCriteria() {
        return new Paciente_terapeutaDetachedCriteria(createCriteria("ORM_Paciente_terapeuta"));
    }

    public EvaluacionterapeutaDetachedCriteria createEvaluacionterapeutaCriteria() {
        return new EvaluacionterapeutaDetachedCriteria(createCriteria("evaluacionterapeuta"));
    }

    public AsigdispterapeutaDetachedCriteria createAsigdispterapeutaCriteria() {
        return new AsigdispterapeutaDetachedCriteria(createCriteria("asigdispterapeuta"));
    }

    public Paciente uniquePaciente(PersistentSession session) {
        return (Paciente) super.createExecutableCriteria(session).uniqueResult();
    }

    public Paciente[] listPaciente(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Paciente[]) list.toArray(new Paciente[list.size()]);
    }
}
