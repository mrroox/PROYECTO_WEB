/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Asigdispcliente {

    public Asigdispcliente() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_ASIGDISPCLIENTE_DETALLEASIGNACION_ASIGDISPCLIENTE) {
            return ORM_detalleasignacion_asigdispcliente;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String fecha_asignacion;

    private String observaciones;

    private orm.Clienteterapeuta clienteterapeuta;

    private orm.Paciente paciente;

    private java.util.Set ORM_detalleasignacion_asigdispcliente = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFecha_asignacion(String value) {
        this.fecha_asignacion = value;
    }

    public String getFecha_asignacion() {
        return fecha_asignacion;
    }

    public void setObservaciones(String value) {
        this.observaciones = value;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setClienteterapeuta(orm.Clienteterapeuta value) {
        if (this.clienteterapeuta != value) {
            orm.Clienteterapeuta lclienteterapeuta = this.clienteterapeuta;
            this.clienteterapeuta = value;
            if (value != null) {
                clienteterapeuta.setAsigdispcliente(this);
            }
            if (lclienteterapeuta != null && lclienteterapeuta.getAsigdispcliente() == this) {
                lclienteterapeuta.setAsigdispcliente(null);
            }
        }
    }

    public orm.Clienteterapeuta getClienteterapeuta() {
        return clienteterapeuta;
    }

    public void setPaciente(orm.Paciente value) {
        if (this.paciente != value) {
            orm.Paciente lpaciente = this.paciente;
            this.paciente = value;
            if (value != null) {
                paciente.setAsigdispcliente(this);
            }
            if (lpaciente != null && lpaciente.getAsigdispcliente() == this) {
                lpaciente.setAsigdispcliente(null);
            }
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    private void setORM_Detalleasignacion_asigdispcliente(java.util.Set value) {
        this.ORM_detalleasignacion_asigdispcliente = value;
    }

    private java.util.Set getORM_Detalleasignacion_asigdispcliente() {
        return ORM_detalleasignacion_asigdispcliente;
    }

    public final orm.Detalleasignacion_asigdispclienteSetCollection detalleasignacion_asigdispcliente = new orm.Detalleasignacion_asigdispclienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ASIGDISPCLIENTE_DETALLEASIGNACION_ASIGDISPCLIENTE, orm.ORMConstants.KEY_DETALLEASIGNACION_ASIGDISPCLIENTE_ASIGDISPCLIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
