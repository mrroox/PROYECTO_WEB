/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Terapeuta_cargo {

    public Terapeuta_cargo() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_TERAPEUTA_CARGO_TERAPEUTA) {
            this.terapeuta = (orm.Terapeuta) owner;
        } else if (key == orm.ORMConstants.KEY_TERAPEUTA_CARGO_CARGO) {
            this.cargo = (orm.Cargo) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Terapeuta terapeuta;

    private orm.Cargo cargo;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setTerapeuta(orm.Terapeuta value) {
        if (terapeuta != null) {
            terapeuta.terapeuta_cargo.remove(this);
        }
        if (value != null) {
            value.terapeuta_cargo.add(this);
        }
    }

    public orm.Terapeuta getTerapeuta() {
        return terapeuta;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Terapeuta(orm.Terapeuta value) {
        this.terapeuta = value;
    }

    private orm.Terapeuta getORM_Terapeuta() {
        return terapeuta;
    }

    public void setCargo(orm.Cargo value) {
        if (cargo != null) {
            cargo.terapeuta_cargo.remove(this);
        }
        if (value != null) {
            value.terapeuta_cargo.add(this);
        }
    }

    public orm.Cargo getCargo() {
        return cargo;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Cargo(orm.Cargo value) {
        this.cargo = value;
    }

    private orm.Cargo getORM_Cargo() {
        return cargo;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
