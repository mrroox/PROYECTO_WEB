/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Asigdispterapeuta_detasigterapeutaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression asigdispterapeutaId;
    public final AssociationExpression asigdispterapeuta;
    public final IntegerExpression detasigterapeutaId;
    public final AssociationExpression detasigterapeuta;

    public Asigdispterapeuta_detasigterapeutaDetachedCriteria() {
        super(orm.Asigdispterapeuta_detasigterapeuta.class, orm.Asigdispterapeuta_detasigterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this.getDetachedCriteria());
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this.getDetachedCriteria());
        detasigterapeutaId = new IntegerExpression("detasigterapeuta.id", this.getDetachedCriteria());
        detasigterapeuta = new AssociationExpression("detasigterapeuta", this.getDetachedCriteria());
    }

    public Asigdispterapeuta_detasigterapeutaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Asigdispterapeuta_detasigterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this.getDetachedCriteria());
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this.getDetachedCriteria());
        detasigterapeutaId = new IntegerExpression("detasigterapeuta.id", this.getDetachedCriteria());
        detasigterapeuta = new AssociationExpression("detasigterapeuta", this.getDetachedCriteria());
    }

    public AsigdispterapeutaDetachedCriteria createAsigdispterapeutaCriteria() {
        return new AsigdispterapeutaDetachedCriteria(createCriteria("asigdispterapeuta"));
    }

    public DetasigterapeutaDetachedCriteria createDetasigterapeutaCriteria() {
        return new DetasigterapeutaDetachedCriteria(createCriteria("detasigterapeuta"));
    }

    public Asigdispterapeuta_detasigterapeuta uniqueAsigdispterapeuta_detasigterapeuta(PersistentSession session) {
        return (Asigdispterapeuta_detasigterapeuta) super.createExecutableCriteria(session).uniqueResult();
    }

    public Asigdispterapeuta_detasigterapeuta[] listAsigdispterapeuta_detasigterapeuta(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Asigdispterapeuta_detasigterapeuta[]) list.toArray(new Asigdispterapeuta_detasigterapeuta[list.size()]);
    }
}
