/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TipodispositivoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;

    public TipodispositivoDetachedCriteria() {
        super(orm.Tipodispositivo.class, orm.TipodispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
    }

    public TipodispositivoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.TipodispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public Tipodispositivo uniqueTipodispositivo(PersistentSession session) {
        return (Tipodispositivo) super.createExecutableCriteria(session).uniqueResult();
    }

    public Tipodispositivo[] listTipodispositivo(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Tipodispositivo[]) list.toArray(new Tipodispositivo[list.size()]);
    }
}
