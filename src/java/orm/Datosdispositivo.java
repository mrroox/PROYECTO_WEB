/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Datosdispositivo {

    public Datosdispositivo() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_DATOSDISPOSITIVO_DATOSDISPOCITIVO_DATOSDEUSODIARIO) {
            return ORM_datosdispocitivo_datosdeusodiario;
        } else if (key == orm.ORMConstants.KEY_DATOSDISPOSITIVO_DATOSDISPOCITIVO_JUEGO) {
            return ORM_datosdispocitivo_juego;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String fechaperiodo;

    private orm.Datosusogeneral datosusogeneral;

    private orm.Datosusonivel datosusonivel;

    private orm.Datosnivelcompletado datosnivelcompletado;

    private orm.Dispositivo dispositivo;

    private java.util.Set ORM_datosdispocitivo_datosdeusodiario = new java.util.HashSet();

    private java.util.Set ORM_datosdispocitivo_juego = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFechaperiodo(String value) {
        this.fechaperiodo = value;
    }

    public String getFechaperiodo() {
        return fechaperiodo;
    }

    public void setDatosusogeneral(orm.Datosusogeneral value) {
        if (this.datosusogeneral != value) {
            orm.Datosusogeneral ldatosusogeneral = this.datosusogeneral;
            this.datosusogeneral = value;
            if (value != null) {
                datosusogeneral.setDatosdispositivo(this);
            }
            if (ldatosusogeneral != null && ldatosusogeneral.getDatosdispositivo() == this) {
                ldatosusogeneral.setDatosdispositivo(null);
            }
        }
    }

    public orm.Datosusogeneral getDatosusogeneral() {
        return datosusogeneral;
    }

    public void setDatosusonivel(orm.Datosusonivel value) {
        if (this.datosusonivel != value) {
            orm.Datosusonivel ldatosusonivel = this.datosusonivel;
            this.datosusonivel = value;
            if (value != null) {
                datosusonivel.setDatosdispositivo(this);
            }
            if (ldatosusonivel != null && ldatosusonivel.getDatosdispositivo() == this) {
                ldatosusonivel.setDatosdispositivo(null);
            }
        }
    }

    public orm.Datosusonivel getDatosusonivel() {
        return datosusonivel;
    }

    public void setDatosnivelcompletado(orm.Datosnivelcompletado value) {
        if (this.datosnivelcompletado != value) {
            orm.Datosnivelcompletado ldatosnivelcompletado = this.datosnivelcompletado;
            this.datosnivelcompletado = value;
            if (value != null) {
                datosnivelcompletado.setDatosdispositivo(this);
            }
            if (ldatosnivelcompletado != null && ldatosnivelcompletado.getDatosdispositivo() == this) {
                ldatosnivelcompletado.setDatosdispositivo(null);
            }
        }
    }

    public orm.Datosnivelcompletado getDatosnivelcompletado() {
        return datosnivelcompletado;
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (this.dispositivo != value) {
            orm.Dispositivo ldispositivo = this.dispositivo;
            this.dispositivo = value;
            if (value != null) {
                dispositivo.setDotosdispositivo(this);
            }
            if (ldispositivo != null && ldispositivo.getDotosdispositivo() == this) {
                ldispositivo.setDotosdispositivo(null);
            }
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    private void setORM_Datosdispocitivo_datosdeusodiario(java.util.Set value) {
        this.ORM_datosdispocitivo_datosdeusodiario = value;
    }

    private java.util.Set getORM_Datosdispocitivo_datosdeusodiario() {
        return ORM_datosdispocitivo_datosdeusodiario;
    }

    public final orm.Datosdispocitivo_datosdeusodiarioSetCollection datosdispocitivo_datosdeusodiario = new orm.Datosdispocitivo_datosdeusodiarioSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DATOSDISPOSITIVO_DATOSDISPOCITIVO_DATOSDEUSODIARIO, orm.ORMConstants.KEY_DATOSDISPOCITIVO_DATOSDEUSODIARIO_DATOSDISPOCITIVO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    private void setORM_Datosdispocitivo_juego(java.util.Set value) {
        this.ORM_datosdispocitivo_juego = value;
    }

    private java.util.Set getORM_Datosdispocitivo_juego() {
        return ORM_datosdispocitivo_juego;
    }

    public final orm.Datosdispocitivo_juegoSetCollection datosdispocitivo_juego = new orm.Datosdispocitivo_juegoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DATOSDISPOSITIVO_DATOSDISPOCITIVO_JUEGO, orm.ORMConstants.KEY_DATOSDISPOCITIVO_JUEGO_DATOSDISPOCITIVO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
