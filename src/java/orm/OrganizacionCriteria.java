/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class OrganizacionCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final StringExpression descripcion;
    public final StringExpression razon_social;
    public final StringExpression rut;
    public final StringExpression fax;
    public final StringExpression email;
    public final StringExpression web;
    public final StringExpression password;
    public final IntegerExpression telefonoId;
    public final AssociationExpression telefono;
    public final IntegerExpression direccionId;
    public final AssociationExpression direccion;
    public final CollectionExpression dispositivo_organizacion;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final CollectionExpression terapeuta;

    public OrganizacionCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        nombre = new StringExpression("nombre", this);
        descripcion = new StringExpression("descripcion", this);
        razon_social = new StringExpression("razon_social", this);
        rut = new StringExpression("rut", this);
        fax = new StringExpression("fax", this);
        email = new StringExpression("email", this);
        web = new StringExpression("web", this);
        password = new StringExpression("password", this);
        telefonoId = new IntegerExpression("telefono.id", this);
        telefono = new AssociationExpression("telefono", this);
        direccionId = new IntegerExpression("direccion.id", this);
        direccion = new AssociationExpression("direccion", this);
        dispositivo_organizacion = new CollectionExpression("ORM_Dispositivo_organizacion", this);
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this);
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this);
        terapeuta = new CollectionExpression("ORM_Terapeuta", this);
    }

    public OrganizacionCriteria(PersistentSession session) {
        this(session.createCriteria(Organizacion.class));
    }

    public OrganizacionCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public TelefonoCriteria createTelefonoCriteria() {
        return new TelefonoCriteria(createCriteria("telefono"));
    }

    public DireccionCriteria createDireccionCriteria() {
        return new DireccionCriteria(createCriteria("direccion"));
    }

    public Dispositivo_organizacionCriteria createDispositivo_organizacionCriteria() {
        return new Dispositivo_organizacionCriteria(createCriteria("ORM_Dispositivo_organizacion"));
    }

    public AsigdisporganizacionCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionCriteria(createCriteria("asigdisporganizacion"));
    }

    public TerapeutaCriteria createTerapeutaCriteria() {
        return new TerapeutaCriteria(createCriteria("ORM_Terapeuta"));
    }

    public Organizacion uniqueOrganizacion() {
        return (Organizacion) super.uniqueResult();
    }

    public Organizacion[] listOrganizacion() {
        java.util.List list = super.list();
        return (Organizacion[]) list.toArray(new Organizacion[list.size()]);
    }
}
