/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DetasigterapeutaDAO {

    public static Detasigterapeuta loadDetasigterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetasigterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta getDetasigterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDetasigterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetasigterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta getDetasigterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDetasigterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Detasigterapeuta) session.load(orm.Detasigterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta getDetasigterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Detasigterapeuta) session.get(orm.Detasigterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Detasigterapeuta) session.load(orm.Detasigterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta getDetasigterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Detasigterapeuta) session.get(orm.Detasigterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetasigterapeuta(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDetasigterapeuta(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetasigterapeuta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDetasigterapeuta(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta[] listDetasigterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDetasigterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta[] listDetasigterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDetasigterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetasigterapeuta(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detasigterapeuta as Detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetasigterapeuta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detasigterapeuta as Detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Detasigterapeuta", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta[] listDetasigterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDetasigterapeuta(session, condition, orderBy);
            return (Detasigterapeuta[]) list.toArray(new Detasigterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta[] listDetasigterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDetasigterapeuta(session, condition, orderBy, lockMode);
            return (Detasigterapeuta[]) list.toArray(new Detasigterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetasigterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetasigterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Detasigterapeuta[] detasigterapeutas = listDetasigterapeutaByQuery(session, condition, orderBy);
        if (detasigterapeutas != null && detasigterapeutas.length > 0) {
            return detasigterapeutas[0];
        } else {
            return null;
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Detasigterapeuta[] detasigterapeutas = listDetasigterapeutaByQuery(session, condition, orderBy, lockMode);
        if (detasigterapeutas != null && detasigterapeutas.length > 0) {
            return detasigterapeutas[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDetasigterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDetasigterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetasigterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDetasigterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetasigterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detasigterapeuta as Detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetasigterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detasigterapeuta as Detasigterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Detasigterapeuta", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta createDetasigterapeuta() {
        return new orm.Detasigterapeuta();
    }

    public static boolean save(orm.Detasigterapeuta detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Detasigterapeuta detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Detasigterapeuta detasigterapeuta) throws PersistentException {
        try {
            if (detasigterapeuta.getDispositivo() != null) {
                detasigterapeuta.getDispositivo().setDetasigterapeuta(null);
            }

            orm.Asigdispterapeuta_detasigterapeuta[] lAsigdispterapeuta_detasigterapeutas = detasigterapeuta.asigdispterapeuta_detasigterapeuta.toArray();
            for (int i = 0; i < lAsigdispterapeuta_detasigterapeutas.length; i++) {
                lAsigdispterapeuta_detasigterapeutas[i].setDetasigterapeuta(null);
            }
            return delete(detasigterapeuta);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Detasigterapeuta detasigterapeuta, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (detasigterapeuta.getDispositivo() != null) {
                detasigterapeuta.getDispositivo().setDetasigterapeuta(null);
            }

            orm.Asigdispterapeuta_detasigterapeuta[] lAsigdispterapeuta_detasigterapeutas = detasigterapeuta.asigdispterapeuta_detasigterapeuta.toArray();
            for (int i = 0; i < lAsigdispterapeuta_detasigterapeutas.length; i++) {
                lAsigdispterapeuta_detasigterapeutas[i].setDetasigterapeuta(null);
            }
            try {
                session.delete(detasigterapeuta);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Detasigterapeuta detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Detasigterapeuta detasigterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(detasigterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detasigterapeuta loadDetasigterapeutaByCriteria(DetasigterapeutaCriteria detasigterapeutaCriteria) {
        Detasigterapeuta[] detasigterapeutas = listDetasigterapeutaByCriteria(detasigterapeutaCriteria);
        if (detasigterapeutas == null || detasigterapeutas.length == 0) {
            return null;
        }
        return detasigterapeutas[0];
    }

    public static Detasigterapeuta[] listDetasigterapeutaByCriteria(DetasigterapeutaCriteria detasigterapeutaCriteria) {
        return detasigterapeutaCriteria.listDetasigterapeuta();
    }
}
