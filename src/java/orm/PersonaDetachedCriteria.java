/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PersonaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression run;
    public final StringExpression nombre;
    public final StringExpression apellido_paterno;
    public final StringExpression apellido_materno;
    public final StringExpression fecha_nacimiento;
    public final StringExpression email;
    public final StringExpression sexo;
    public final IntegerExpression direccionId;
    public final AssociationExpression direccion;
    public final IntegerExpression telefonoId;
    public final AssociationExpression telefono;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final CollectionExpression apoderado;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;

    public PersonaDetachedCriteria() {
        super(orm.Persona.class, orm.PersonaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        run = new StringExpression("run", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        apellido_paterno = new StringExpression("apellido_paterno", this.getDetachedCriteria());
        apellido_materno = new StringExpression("apellido_materno", this.getDetachedCriteria());
        fecha_nacimiento = new StringExpression("fecha_nacimiento", this.getDetachedCriteria());
        email = new StringExpression("email", this.getDetachedCriteria());
        sexo = new StringExpression("sexo", this.getDetachedCriteria());
        direccionId = new IntegerExpression("direccion.id", this.getDetachedCriteria());
        direccion = new AssociationExpression("direccion", this.getDetachedCriteria());
        telefonoId = new IntegerExpression("telefono.id", this.getDetachedCriteria());
        telefono = new AssociationExpression("telefono", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        apoderado = new CollectionExpression("ORM_Apoderado", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
    }

    public PersonaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.PersonaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        run = new StringExpression("run", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        apellido_paterno = new StringExpression("apellido_paterno", this.getDetachedCriteria());
        apellido_materno = new StringExpression("apellido_materno", this.getDetachedCriteria());
        fecha_nacimiento = new StringExpression("fecha_nacimiento", this.getDetachedCriteria());
        email = new StringExpression("email", this.getDetachedCriteria());
        sexo = new StringExpression("sexo", this.getDetachedCriteria());
        direccionId = new IntegerExpression("direccion.id", this.getDetachedCriteria());
        direccion = new AssociationExpression("direccion", this.getDetachedCriteria());
        telefonoId = new IntegerExpression("telefono.id", this.getDetachedCriteria());
        telefono = new AssociationExpression("telefono", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        apoderado = new CollectionExpression("ORM_Apoderado", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
    }

    public DireccionDetachedCriteria createDireccionCriteria() {
        return new DireccionDetachedCriteria(createCriteria("direccion"));
    }

    public TelefonoDetachedCriteria createTelefonoCriteria() {
        return new TelefonoDetachedCriteria(createCriteria("telefono"));
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public ClienteterapeutaDetachedCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaDetachedCriteria(createCriteria("clienteterapeuta"));
    }

    public ApoderadoDetachedCriteria createApoderadoCriteria() {
        return new ApoderadoDetachedCriteria(createCriteria("ORM_Apoderado"));
    }

    public TerapeutaDetachedCriteria createTerapeutaCriteria() {
        return new TerapeutaDetachedCriteria(createCriteria("terapeuta"));
    }

    public Persona uniquePersona(PersistentSession session) {
        return (Persona) super.createExecutableCriteria(session).uniqueResult();
    }

    public Persona[] listPersona(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Persona[]) list.toArray(new Persona[list.size()]);
    }
}
