/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Clienteterapeuta {

    public Clienteterapeuta() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_CLIENTETERAPEUTA_CLIENTETERAPEUTA_PACIENTE) {
            return ORM_clienteterapeuta_paciente;
        } else if (key == orm.ORMConstants.KEY_CLIENTETERAPEUTA_CLIENTETERAPEUTA_DISPOSITIVO) {
            return ORM_clienteterapeuta_dispositivo;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String password;

    private orm.Persona persona;

    private int personadireccionid;

    private orm.Evaluacion evaluacion;

    private java.util.Set ORM_clienteterapeuta_paciente = new java.util.HashSet();

    private orm.Asigdispcliente asigdispcliente;

    private java.util.Set ORM_clienteterapeuta_dispositivo = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return password;
    }

    public void setPersonadireccionid(int value) {
        this.personadireccionid = value;
    }

    public int getPersonadireccionid() {
        return personadireccionid;
    }

    public void setPersona(orm.Persona value) {
        if (this.persona != value) {
            orm.Persona lpersona = this.persona;
            this.persona = value;
            if (value != null) {
                persona.setClienteterapeuta(this);
            }
            if (lpersona != null && lpersona.getClienteterapeuta() == this) {
                lpersona.setClienteterapeuta(null);
            }
        }
    }

    public orm.Persona getPersona() {
        return persona;
    }

    public void setEvaluacion(orm.Evaluacion value) {
        if (this.evaluacion != value) {
            orm.Evaluacion levaluacion = this.evaluacion;
            this.evaluacion = value;
            if (value != null) {
                evaluacion.setClienteterapeuta(this);
            }
            if (levaluacion != null && levaluacion.getClienteterapeuta() == this) {
                levaluacion.setClienteterapeuta(null);
            }
        }
    }

    public orm.Evaluacion getEvaluacion() {
        return evaluacion;
    }

    private void setORM_Clienteterapeuta_paciente(java.util.Set value) {
        this.ORM_clienteterapeuta_paciente = value;
    }

    private java.util.Set getORM_Clienteterapeuta_paciente() {
        return ORM_clienteterapeuta_paciente;
    }

    public final orm.Clienteterapeuta_pacienteSetCollection clienteterapeuta_paciente = new orm.Clienteterapeuta_pacienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_CLIENTETERAPEUTA_CLIENTETERAPEUTA_PACIENTE, orm.ORMConstants.KEY_CLIENTETERAPEUTA_PACIENTE_CLIENTETERAPEUTA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setAsigdispcliente(orm.Asigdispcliente value) {
        if (this.asigdispcliente != value) {
            orm.Asigdispcliente lasigdispcliente = this.asigdispcliente;
            this.asigdispcliente = value;
            if (value != null) {
                asigdispcliente.setClienteterapeuta(this);
            }
            if (lasigdispcliente != null && lasigdispcliente.getClienteterapeuta() == this) {
                lasigdispcliente.setClienteterapeuta(null);
            }
        }
    }

    public orm.Asigdispcliente getAsigdispcliente() {
        return asigdispcliente;
    }

    private void setORM_Clienteterapeuta_dispositivo(java.util.Set value) {
        this.ORM_clienteterapeuta_dispositivo = value;
    }

    private java.util.Set getORM_Clienteterapeuta_dispositivo() {
        return ORM_clienteterapeuta_dispositivo;
    }

    public final orm.Clienteterapeuta_dispositivoSetCollection clienteterapeuta_dispositivo = new orm.Clienteterapeuta_dispositivoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_CLIENTETERAPEUTA_CLIENTETERAPEUTA_DISPOSITIVO, orm.ORMConstants.KEY_CLIENTETERAPEUTA_DISPOSITIVO_CLIENTETERAPEUTA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
