/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DatosdeusodiarioDAO {

    public static Datosdeusodiario loadDatosdeusodiarioByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdeusodiarioByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario getDatosdeusodiarioByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdeusodiarioByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdeusodiarioByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario getDatosdeusodiarioByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdeusodiarioByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdeusodiario) session.load(orm.Datosdeusodiario.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario getDatosdeusodiarioByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdeusodiario) session.get(orm.Datosdeusodiario.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdeusodiario) session.load(orm.Datosdeusodiario.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario getDatosdeusodiarioByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdeusodiario) session.get(orm.Datosdeusodiario.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdeusodiario(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdeusodiario(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdeusodiario(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdeusodiario(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario[] listDatosdeusodiarioByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdeusodiarioByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario[] listDatosdeusodiarioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdeusodiario(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdeusodiario as Datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdeusodiario(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdeusodiario as Datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdeusodiario", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario[] listDatosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDatosdeusodiario(session, condition, orderBy);
            return (Datosdeusodiario[]) list.toArray(new Datosdeusodiario[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario[] listDatosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDatosdeusodiario(session, condition, orderBy, lockMode);
            return (Datosdeusodiario[]) list.toArray(new Datosdeusodiario[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdeusodiarioByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Datosdeusodiario[] datosdeusodiarios = listDatosdeusodiarioByQuery(session, condition, orderBy);
        if (datosdeusodiarios != null && datosdeusodiarios.length > 0) {
            return datosdeusodiarios[0];
        } else {
            return null;
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Datosdeusodiario[] datosdeusodiarios = listDatosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        if (datosdeusodiarios != null && datosdeusodiarios.length > 0) {
            return datosdeusodiarios[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDatosdeusodiarioByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdeusodiarioByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdeusodiarioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdeusodiarioByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdeusodiario as Datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdeusodiarioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdeusodiario as Datosdeusodiario");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdeusodiario", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario createDatosdeusodiario() {
        return new orm.Datosdeusodiario();
    }

    public static boolean save(orm.Datosdeusodiario datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Datosdeusodiario datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdeusodiario datosdeusodiario) throws PersistentException {
        try {
            orm.Datosdispocitivo_datosdeusodiario[] lDatosdispocitivo_datosdeusodiarios = datosdeusodiario.datosdispocitivo_datosdeusodiario.toArray();
            for (int i = 0; i < lDatosdispocitivo_datosdeusodiarios.length; i++) {
                lDatosdispocitivo_datosdeusodiarios[i].setDatosdeusodiario(null);
            }
            return delete(datosdeusodiario);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdeusodiario datosdeusodiario, org.orm.PersistentSession session) throws PersistentException {
        try {
            orm.Datosdispocitivo_datosdeusodiario[] lDatosdispocitivo_datosdeusodiarios = datosdeusodiario.datosdispocitivo_datosdeusodiario.toArray();
            for (int i = 0; i < lDatosdispocitivo_datosdeusodiarios.length; i++) {
                lDatosdispocitivo_datosdeusodiarios[i].setDatosdeusodiario(null);
            }
            try {
                session.delete(datosdeusodiario);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Datosdeusodiario datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Datosdeusodiario datosdeusodiario) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(datosdeusodiario);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdeusodiario loadDatosdeusodiarioByCriteria(DatosdeusodiarioCriteria datosdeusodiarioCriteria) {
        Datosdeusodiario[] datosdeusodiarios = listDatosdeusodiarioByCriteria(datosdeusodiarioCriteria);
        if (datosdeusodiarios == null || datosdeusodiarios.length == 0) {
            return null;
        }
        return datosdeusodiarios[0];
    }

    public static Datosdeusodiario[] listDatosdeusodiarioByCriteria(DatosdeusodiarioCriteria datosdeusodiarioCriteria) {
        return datosdeusodiarioCriteria.listDatosdeusodiario();
    }
}
