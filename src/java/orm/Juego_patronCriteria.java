/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Juego_patronCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression juegoId;
    public final AssociationExpression juego;
    public final IntegerExpression patronId;
    public final AssociationExpression patron;

    public Juego_patronCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        juegoId = new IntegerExpression("juego.id", this);
        juego = new AssociationExpression("juego", this);
        patronId = new IntegerExpression("patron.id", this);
        patron = new AssociationExpression("patron", this);
    }

    public Juego_patronCriteria(PersistentSession session) {
        this(session.createCriteria(Juego_patron.class));
    }

    public Juego_patronCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public JuegoCriteria createJuegoCriteria() {
        return new JuegoCriteria(createCriteria("juego"));
    }

    public PatronCriteria createPatronCriteria() {
        return new PatronCriteria(createCriteria("patron"));
    }

    public Juego_patron uniqueJuego_patron() {
        return (Juego_patron) super.uniqueResult();
    }

    public Juego_patron[] listJuego_patron() {
        java.util.List list = super.list();
        return (Juego_patron[]) list.toArray(new Juego_patron[list.size()]);
    }
}
