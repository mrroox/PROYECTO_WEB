/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DireccionDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression calle;
    public final StringExpression numero;
    public final IntegerExpression paisId;
    public final AssociationExpression pais;
    public final IntegerExpression regionId;
    public final AssociationExpression region;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final CollectionExpression persona;

    public DireccionDetachedCriteria() {
        super(orm.Direccion.class, orm.DireccionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        calle = new StringExpression("calle", this.getDetachedCriteria());
        numero = new StringExpression("numero", this.getDetachedCriteria());
        paisId = new IntegerExpression("pais.id", this.getDetachedCriteria());
        pais = new AssociationExpression("pais", this.getDetachedCriteria());
        regionId = new IntegerExpression("region.id", this.getDetachedCriteria());
        region = new AssociationExpression("region", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        persona = new CollectionExpression("ORM_Persona", this.getDetachedCriteria());
    }

    public DireccionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DireccionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        calle = new StringExpression("calle", this.getDetachedCriteria());
        numero = new StringExpression("numero", this.getDetachedCriteria());
        paisId = new IntegerExpression("pais.id", this.getDetachedCriteria());
        pais = new AssociationExpression("pais", this.getDetachedCriteria());
        regionId = new IntegerExpression("region.id", this.getDetachedCriteria());
        region = new AssociationExpression("region", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        persona = new CollectionExpression("ORM_Persona", this.getDetachedCriteria());
    }

    public PaisDetachedCriteria createPaisCriteria() {
        return new PaisDetachedCriteria(createCriteria("pais"));
    }

    public RegionDetachedCriteria createRegionCriteria() {
        return new RegionDetachedCriteria(createCriteria("region"));
    }

    public OrganizacionDetachedCriteria createOrganizacionCriteria() {
        return new OrganizacionDetachedCriteria(createCriteria("organizacion"));
    }

    public PersonaDetachedCriteria createPersonaCriteria() {
        return new PersonaDetachedCriteria(createCriteria("ORM_Persona"));
    }

    public Direccion uniqueDireccion(PersistentSession session) {
        return (Direccion) super.createExecutableCriteria(session).uniqueResult();
    }

    public Direccion[] listDireccion(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Direccion[]) list.toArray(new Direccion[list.size()]);
    }
}
