/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DispositivoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final StringExpression descripcion;
    public final StringExpression numserie;
    public final IntegerExpression tipodispositivoId;
    public final AssociationExpression tipodispositivo;
    public final IntegerExpression dotosdispositivoId;
    public final AssociationExpression dotosdispositivo;
    public final CollectionExpression clienteterapeuta_dispositivo;
    public final CollectionExpression dispositivo_organizacion;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final CollectionExpression detalleasignacion;
    public final IntegerExpression detasigterapeutaId;
    public final AssociationExpression detasigterapeuta;
    public final CollectionExpression asigdisporganizacion_dispositivo;

    public DispositivoDetachedCriteria() {
        super(orm.Dispositivo.class, orm.DispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
        numserie = new StringExpression("numserie", this.getDetachedCriteria());
        tipodispositivoId = new IntegerExpression("tipodispositivo.id", this.getDetachedCriteria());
        tipodispositivo = new AssociationExpression("tipodispositivo", this.getDetachedCriteria());
        dotosdispositivoId = new IntegerExpression("dotosdispositivo.id", this.getDetachedCriteria());
        dotosdispositivo = new AssociationExpression("dotosdispositivo", this.getDetachedCriteria());
        clienteterapeuta_dispositivo = new CollectionExpression("ORM_Clienteterapeuta_dispositivo", this.getDetachedCriteria());
        dispositivo_organizacion = new CollectionExpression("ORM_Dispositivo_organizacion", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        detalleasignacion = new CollectionExpression("ORM_Detalleasignacion", this.getDetachedCriteria());
        detasigterapeutaId = new IntegerExpression("detasigterapeuta.id", this.getDetachedCriteria());
        detasigterapeuta = new AssociationExpression("detasigterapeuta", this.getDetachedCriteria());
        asigdisporganizacion_dispositivo = new CollectionExpression("ORM_Asigdisporganizacion_dispositivo", this.getDetachedCriteria());
    }

    public DispositivoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
        numserie = new StringExpression("numserie", this.getDetachedCriteria());
        tipodispositivoId = new IntegerExpression("tipodispositivo.id", this.getDetachedCriteria());
        tipodispositivo = new AssociationExpression("tipodispositivo", this.getDetachedCriteria());
        dotosdispositivoId = new IntegerExpression("dotosdispositivo.id", this.getDetachedCriteria());
        dotosdispositivo = new AssociationExpression("dotosdispositivo", this.getDetachedCriteria());
        clienteterapeuta_dispositivo = new CollectionExpression("ORM_Clienteterapeuta_dispositivo", this.getDetachedCriteria());
        dispositivo_organizacion = new CollectionExpression("ORM_Dispositivo_organizacion", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        detalleasignacion = new CollectionExpression("ORM_Detalleasignacion", this.getDetachedCriteria());
        detasigterapeutaId = new IntegerExpression("detasigterapeuta.id", this.getDetachedCriteria());
        detasigterapeuta = new AssociationExpression("detasigterapeuta", this.getDetachedCriteria());
        asigdisporganizacion_dispositivo = new CollectionExpression("ORM_Asigdisporganizacion_dispositivo", this.getDetachedCriteria());
    }

    public TipodispositivoDetachedCriteria createTipodispositivoCriteria() {
        return new TipodispositivoDetachedCriteria(createCriteria("tipodispositivo"));
    }

    public DatosdispositivoDetachedCriteria createDotosdispositivoCriteria() {
        return new DatosdispositivoDetachedCriteria(createCriteria("dotosdispositivo"));
    }

    public Clienteterapeuta_dispositivoDetachedCriteria createClienteterapeuta_dispositivoCriteria() {
        return new Clienteterapeuta_dispositivoDetachedCriteria(createCriteria("ORM_Clienteterapeuta_dispositivo"));
    }

    public Dispositivo_organizacionDetachedCriteria createDispositivo_organizacionCriteria() {
        return new Dispositivo_organizacionDetachedCriteria(createCriteria("ORM_Dispositivo_organizacion"));
    }

    public AsigdisporganizacionDetachedCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionDetachedCriteria(createCriteria("asigdisporganizacion"));
    }

    public DetalleasignacionDetachedCriteria createDetalleasignacionCriteria() {
        return new DetalleasignacionDetachedCriteria(createCriteria("ORM_Detalleasignacion"));
    }

    public DetasigterapeutaDetachedCriteria createDetasigterapeutaCriteria() {
        return new DetasigterapeutaDetachedCriteria(createCriteria("detasigterapeuta"));
    }

    public Asigdisporganizacion_dispositivoDetachedCriteria createAsigdisporganizacion_dispositivoCriteria() {
        return new Asigdisporganizacion_dispositivoDetachedCriteria(createCriteria("ORM_Asigdisporganizacion_dispositivo"));
    }

    public Dispositivo uniqueDispositivo(PersistentSession session) {
        return (Dispositivo) super.createExecutableCriteria(session).uniqueResult();
    }

    public Dispositivo[] listDispositivo(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Dispositivo[]) list.toArray(new Dispositivo[list.size()]);
    }
}
