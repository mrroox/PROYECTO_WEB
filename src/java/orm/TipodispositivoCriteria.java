/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TipodispositivoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;

    public TipodispositivoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        nombre = new StringExpression("nombre", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
    }

    public TipodispositivoCriteria(PersistentSession session) {
        this(session.createCriteria(Tipodispositivo.class));
    }

    public TipodispositivoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public Tipodispositivo uniqueTipodispositivo() {
        return (Tipodispositivo) super.uniqueResult();
    }

    public Tipodispositivo[] listTipodispositivo() {
        java.util.List list = super.list();
        return (Tipodispositivo[]) list.toArray(new Tipodispositivo[list.size()]);
    }
}
