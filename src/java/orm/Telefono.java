/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Telefono {

    public Telefono() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_TELEFONO_PERSONA) {
            return ORM_persona;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String redfija;

    private String celular;

    private orm.Organizacion organizacion;

    private java.util.Set ORM_persona = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setRedfija(String value) {
        this.redfija = value;
    }

    public String getRedfija() {
        return redfija;
    }

    public void setCelular(String value) {
        this.celular = value;
    }

    public String getCelular() {
        return celular;
    }

    public void setOrganizacion(orm.Organizacion value) {
        if (this.organizacion != value) {
            orm.Organizacion lorganizacion = this.organizacion;
            this.organizacion = value;
            if (value != null) {
                organizacion.setTelefono(this);
            }
            if (lorganizacion != null && lorganizacion.getTelefono() == this) {
                lorganizacion.setTelefono(null);
            }
        }
    }

    public orm.Organizacion getOrganizacion() {
        return organizacion;
    }

    private void setORM_Persona(java.util.Set value) {
        this.ORM_persona = value;
    }

    private java.util.Set getORM_Persona() {
        return ORM_persona;
    }

    public final orm.PersonaSetCollection persona = new orm.PersonaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TELEFONO_PERSONA, orm.ORMConstants.KEY_PERSONA_TELEFONO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
