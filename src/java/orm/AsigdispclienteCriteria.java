/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsigdispclienteCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_asignacion;
    public final StringExpression observaciones;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final CollectionExpression detalleasignacion_asigdispcliente;

    public AsigdispclienteCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha_asignacion = new StringExpression("fecha_asignacion", this);
        observaciones = new StringExpression("observaciones", this);
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this);
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
        detalleasignacion_asigdispcliente = new CollectionExpression("ORM_Detalleasignacion_asigdispcliente", this);
    }

    public AsigdispclienteCriteria(PersistentSession session) {
        this(session.createCriteria(Asigdispcliente.class));
    }

    public AsigdispclienteCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public ClienteterapeutaCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaCriteria(createCriteria("clienteterapeuta"));
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public Detalleasignacion_asigdispclienteCriteria createDetalleasignacion_asigdispclienteCriteria() {
        return new Detalleasignacion_asigdispclienteCriteria(createCriteria("ORM_Detalleasignacion_asigdispcliente"));
    }

    public Asigdispcliente uniqueAsigdispcliente() {
        return (Asigdispcliente) super.uniqueResult();
    }

    public Asigdispcliente[] listAsigdispcliente() {
        java.util.List list = super.list();
        return (Asigdispcliente[]) list.toArray(new Asigdispcliente[list.size()]);
    }
}
