/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Asigdispterapeuta_detasigterapeuta {

    public Asigdispterapeuta_detasigterapeuta() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA_ASIGDISPTERAPEUTA) {
            this.asigdispterapeuta = (orm.Asigdispterapeuta) owner;
        } else if (key == orm.ORMConstants.KEY_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA_DETASIGTERAPEUTA) {
            this.detasigterapeuta = (orm.Detasigterapeuta) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Asigdispterapeuta asigdispterapeuta;

    private orm.Detasigterapeuta detasigterapeuta;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setAsigdispterapeuta(orm.Asigdispterapeuta value) {
        if (asigdispterapeuta != null) {
            asigdispterapeuta.asigdispterapeuta_detasigterapeuta.remove(this);
        }
        if (value != null) {
            value.asigdispterapeuta_detasigterapeuta.add(this);
        }
    }

    public orm.Asigdispterapeuta getAsigdispterapeuta() {
        return asigdispterapeuta;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Asigdispterapeuta(orm.Asigdispterapeuta value) {
        this.asigdispterapeuta = value;
    }

    private orm.Asigdispterapeuta getORM_Asigdispterapeuta() {
        return asigdispterapeuta;
    }

    public void setDetasigterapeuta(orm.Detasigterapeuta value) {
        if (detasigterapeuta != null) {
            detasigterapeuta.asigdispterapeuta_detasigterapeuta.remove(this);
        }
        if (value != null) {
            value.asigdispterapeuta_detasigterapeuta.add(this);
        }
    }

    public orm.Detasigterapeuta getDetasigterapeuta() {
        return detasigterapeuta;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Detasigterapeuta(orm.Detasigterapeuta value) {
        this.detasigterapeuta = value;
    }

    private orm.Detasigterapeuta getORM_Detasigterapeuta() {
        return detasigterapeuta;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
