/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Datosdispocitivo_juego {

    public Datosdispocitivo_juego() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_DATOSDISPOCITIVO_JUEGO_DATOSDISPOCITIVO) {
            this.datosdispocitivo = (orm.Datosdispositivo) owner;
        } else if (key == orm.ORMConstants.KEY_DATOSDISPOCITIVO_JUEGO_JUEGO) {
            this.juego = (orm.Juego) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Datosdispositivo datosdispocitivo;

    private orm.Juego juego;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setDatosdispocitivo(orm.Datosdispositivo value) {
        if (datosdispocitivo != null) {
            datosdispocitivo.datosdispocitivo_juego.remove(this);
        }
        if (value != null) {
            value.datosdispocitivo_juego.add(this);
        }
    }

    public orm.Datosdispositivo getDatosdispocitivo() {
        return datosdispocitivo;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Datosdispocitivo(orm.Datosdispositivo value) {
        this.datosdispocitivo = value;
    }

    private orm.Datosdispositivo getORM_Datosdispocitivo() {
        return datosdispocitivo;
    }

    public void setJuego(orm.Juego value) {
        if (juego != null) {
            juego.datosdispocitivo_juego.remove(this);
        }
        if (value != null) {
            value.datosdispocitivo_juego.add(this);
        }
    }

    public orm.Juego getJuego() {
        return juego;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Juego(orm.Juego value) {
        this.juego = value;
    }

    private orm.Juego getORM_Juego() {
        return juego;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
