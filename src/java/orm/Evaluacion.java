/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Evaluacion {

    public Evaluacion() {
    }

    private int id;

    private String fecha_evaluacion;

    private String observacion;

    private orm.Clienteterapeuta clienteterapeuta;

    private orm.Paciente paciente;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFecha_evaluacion(String value) {
        this.fecha_evaluacion = value;
    }

    public String getFecha_evaluacion() {
        return fecha_evaluacion;
    }

    public void setObservacion(String value) {
        this.observacion = value;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setClienteterapeuta(orm.Clienteterapeuta value) {
        if (this.clienteterapeuta != value) {
            orm.Clienteterapeuta lclienteterapeuta = this.clienteterapeuta;
            this.clienteterapeuta = value;
            if (value != null) {
                clienteterapeuta.setEvaluacion(this);
            }
            if (lclienteterapeuta != null && lclienteterapeuta.getEvaluacion() == this) {
                lclienteterapeuta.setEvaluacion(null);
            }
        }
    }

    public orm.Clienteterapeuta getClienteterapeuta() {
        return clienteterapeuta;
    }

    public void setPaciente(orm.Paciente value) {
        if (this.paciente != value) {
            orm.Paciente lpaciente = this.paciente;
            this.paciente = value;
            if (value != null) {
                paciente.setEvaluacion(this);
            }
            if (lpaciente != null && lpaciente.getEvaluacion() == this) {
                lpaciente.setEvaluacion(null);
            }
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
