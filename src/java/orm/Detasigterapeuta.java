/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Detasigterapeuta {

    public Detasigterapeuta() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_DETASIGTERAPEUTA_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA) {
            return ORM_asigdispterapeuta_detasigterapeuta;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String observaciones;

    private String fecha_asignacion;

    private String fecha_devolucion;

    private orm.Dispositivo dispositivo;

    private java.util.Set ORM_asigdispterapeuta_detasigterapeuta = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setObservaciones(String value) {
        this.observaciones = value;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setFecha_asignacion(String value) {
        this.fecha_asignacion = value;
    }

    public String getFecha_asignacion() {
        return fecha_asignacion;
    }

    public void setFecha_devolucion(String value) {
        this.fecha_devolucion = value;
    }

    public String getFecha_devolucion() {
        return fecha_devolucion;
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (this.dispositivo != value) {
            orm.Dispositivo ldispositivo = this.dispositivo;
            this.dispositivo = value;
            if (value != null) {
                dispositivo.setDetasigterapeuta(this);
            }
            if (ldispositivo != null && ldispositivo.getDetasigterapeuta() == this) {
                ldispositivo.setDetasigterapeuta(null);
            }
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    private void setORM_Asigdispterapeuta_detasigterapeuta(java.util.Set value) {
        this.ORM_asigdispterapeuta_detasigterapeuta = value;
    }

    private java.util.Set getORM_Asigdispterapeuta_detasigterapeuta() {
        return ORM_asigdispterapeuta_detasigterapeuta;
    }

    public final orm.Asigdispterapeuta_detasigterapeutaSetCollection asigdispterapeuta_detasigterapeuta = new orm.Asigdispterapeuta_detasigterapeutaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DETASIGTERAPEUTA_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA, orm.ORMConstants.KEY_ASIGDISPTERAPEUTA_DETASIGTERAPEUTA_DETASIGTERAPEUTA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
