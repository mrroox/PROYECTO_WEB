/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Region {

    public Region() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_REGION_DIRECCION) {
            return ORM_direccion;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String nombre;

    private java.util.Set ORM_direccion = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setNombre(String value) {
        this.nombre = value;
    }

    public String getNombre() {
        return nombre;
    }

    private void setORM_Direccion(java.util.Set value) {
        this.ORM_direccion = value;
    }

    private java.util.Set getORM_Direccion() {
        return ORM_direccion;
    }

    public final orm.DireccionSetCollection direccion = new orm.DireccionSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_REGION_DIRECCION, orm.ORMConstants.KEY_DIRECCION_REGION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
