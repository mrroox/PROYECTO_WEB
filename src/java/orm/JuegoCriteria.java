/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class JuegoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fechajuego;
    public final StringExpression horajuego;
    public final CollectionExpression juego_patron;
    public final CollectionExpression datosdispocitivo_juego;

    public JuegoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fechajuego = new StringExpression("fechajuego", this);
        horajuego = new StringExpression("horajuego", this);
        juego_patron = new CollectionExpression("ORM_Juego_patron", this);
        datosdispocitivo_juego = new CollectionExpression("ORM_Datosdispocitivo_juego", this);
    }

    public JuegoCriteria(PersistentSession session) {
        this(session.createCriteria(Juego.class));
    }

    public JuegoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public Juego_patronCriteria createJuego_patronCriteria() {
        return new Juego_patronCriteria(createCriteria("ORM_Juego_patron"));
    }

    public Datosdispocitivo_juegoCriteria createDatosdispocitivo_juegoCriteria() {
        return new Datosdispocitivo_juegoCriteria(createCriteria("ORM_Datosdispocitivo_juego"));
    }

    public Juego uniqueJuego() {
        return (Juego) super.uniqueResult();
    }

    public Juego[] listJuego() {
        java.util.List list = super.list();
        return (Juego[]) list.toArray(new Juego[list.size()]);
    }
}
