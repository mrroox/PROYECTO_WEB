/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TerapeutaCriteria extends AbstractORMCriteria {

    public final IntegerExpression personaId;
    public final AssociationExpression persona;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final IntegerExpression id;
    public final StringExpression password;
    public final IntegerExpression personadireccionid;
    public final CollectionExpression paciente_terapeuta;
    public final CollectionExpression terapeuta_cargo;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final IntegerExpression evaluacionterapeutaId;
    public final AssociationExpression evaluacionterapeuta;
    public final IntegerExpression asigdispterapeutaId;
    public final AssociationExpression asigdispterapeuta;

    public TerapeutaCriteria(Criteria criteria) {
        super(criteria);
        personaId = new IntegerExpression("persona.id", this);
        persona = new AssociationExpression("persona", this);
        organizacionId = new IntegerExpression("organizacion.id", this);
        organizacion = new AssociationExpression("organizacion", this);
        id = new IntegerExpression("id", this);
        password = new StringExpression("password", this);
        personadireccionid = new IntegerExpression("personadireccionid", this);
        paciente_terapeuta = new CollectionExpression("ORM_Paciente_terapeuta", this);
        terapeuta_cargo = new CollectionExpression("ORM_Terapeuta_cargo", this);
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this);
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this);
        evaluacionterapeutaId = new IntegerExpression("evaluacionterapeuta.id", this);
        evaluacionterapeuta = new AssociationExpression("evaluacionterapeuta", this);
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this);
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this);
    }

    public TerapeutaCriteria(PersistentSession session) {
        this(session.createCriteria(Terapeuta.class));
    }

    public TerapeutaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public PersonaCriteria createPersonaCriteria() {
        return new PersonaCriteria(createCriteria("persona"));
    }

    public OrganizacionCriteria createOrganizacionCriteria() {
        return new OrganizacionCriteria(createCriteria("organizacion"));
    }

    public Paciente_terapeutaCriteria createPaciente_terapeutaCriteria() {
        return new Paciente_terapeutaCriteria(createCriteria("ORM_Paciente_terapeuta"));
    }

    public Terapeuta_cargoCriteria createTerapeuta_cargoCriteria() {
        return new Terapeuta_cargoCriteria(createCriteria("ORM_Terapeuta_cargo"));
    }

    public AsigdisporganizacionCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionCriteria(createCriteria("asigdisporganizacion"));
    }

    public EvaluacionterapeutaCriteria createEvaluacionterapeutaCriteria() {
        return new EvaluacionterapeutaCriteria(createCriteria("evaluacionterapeuta"));
    }

    public AsigdispterapeutaCriteria createAsigdispterapeutaCriteria() {
        return new AsigdispterapeutaCriteria(createCriteria("asigdispterapeuta"));
    }

    public Terapeuta uniqueTerapeuta() {
        return (Terapeuta) super.uniqueResult();
    }

    public Terapeuta[] listTerapeuta() {
        java.util.List list = super.list();
        return (Terapeuta[]) list.toArray(new Terapeuta[list.size()]);
    }
}
