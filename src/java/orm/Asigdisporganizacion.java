/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Asigdisporganizacion {

    public Asigdisporganizacion() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_ASIGDISPORGANIZACION_ASIGDISPORGANIZACION_DISPOSITIVO) {
            return ORM_asigdisporganizacion_dispositivo;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String fecha_asignacion;

    private orm.Organizacion organizacion;

    private orm.Terapeuta terapeuta;

    private orm.Dispositivo dispositivo;

    private Integer cantidad;

    private String observacion;

    private java.util.Set ORM_asigdisporganizacion_dispositivo = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFecha_asignacion(String value) {
        this.fecha_asignacion = value;
    }

    public String getFecha_asignacion() {
        return fecha_asignacion;
    }

    public void setCantidad(int value) {
        setCantidad(new Integer(value));
    }

    public void setCantidad(Integer value) {
        this.cantidad = value;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setObservacion(String value) {
        this.observacion = value;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setOrganizacion(orm.Organizacion value) {
        if (this.organizacion != value) {
            orm.Organizacion lorganizacion = this.organizacion;
            this.organizacion = value;
            if (value != null) {
                organizacion.setAsigdisporganizacion(this);
            }
            if (lorganizacion != null && lorganizacion.getAsigdisporganizacion() == this) {
                lorganizacion.setAsigdisporganizacion(null);
            }
        }
    }

    public orm.Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setTerapeuta(orm.Terapeuta value) {
        if (this.terapeuta != value) {
            orm.Terapeuta lterapeuta = this.terapeuta;
            this.terapeuta = value;
            if (value != null) {
                terapeuta.setAsigdisporganizacion(this);
            }
            if (lterapeuta != null && lterapeuta.getAsigdisporganizacion() == this) {
                lterapeuta.setAsigdisporganizacion(null);
            }
        }
    }

    public orm.Terapeuta getTerapeuta() {
        return terapeuta;
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (this.dispositivo != value) {
            orm.Dispositivo ldispositivo = this.dispositivo;
            this.dispositivo = value;
            if (value != null) {
                dispositivo.setAsigdisporganizacion(this);
            }
            if (ldispositivo != null && ldispositivo.getAsigdisporganizacion() == this) {
                ldispositivo.setAsigdisporganizacion(null);
            }
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    private void setORM_Asigdisporganizacion_dispositivo(java.util.Set value) {
        this.ORM_asigdisporganizacion_dispositivo = value;
    }

    private java.util.Set getORM_Asigdisporganizacion_dispositivo() {
        return ORM_asigdisporganizacion_dispositivo;
    }

    public final orm.Asigdisporganizacion_dispositivoSetCollection asigdisporganizacion_dispositivo = new orm.Asigdisporganizacion_dispositivoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ASIGDISPORGANIZACION_ASIGDISPORGANIZACION_DISPOSITIVO, orm.ORMConstants.KEY_ASIGDISPORGANIZACION_DISPOSITIVO_ASIGDISPORGANIZACION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
