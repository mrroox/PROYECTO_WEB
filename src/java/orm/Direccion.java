/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Direccion {

    public Direccion() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_DIRECCION_PERSONA) {
            return ORM_persona;
        }

        return null;
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_DIRECCION_PAIS) {
            this.pais = (orm.Pais) owner;
        } else if (key == orm.ORMConstants.KEY_DIRECCION_REGION) {
            this.region = (orm.Region) owner;
        } else if (key == orm.ORMConstants.KEY_DIRECCION_ORGANIZACION) {
            this.organizacion = (orm.Organizacion) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private String calle;

    private String numero;

    private orm.Pais pais;

    private orm.Region region;

    private orm.Organizacion organizacion;

    private java.util.Set ORM_persona = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setCalle(String value) {
        this.calle = value;
    }

    public String getCalle() {
        return calle;
    }

    public void setNumero(String value) {
        this.numero = value;
    }

    public String getNumero() {
        return numero;
    }

    public void setPais(orm.Pais value) {
        if (pais != null) {
            pais.direccion.remove(this);
        }
        if (value != null) {
            value.direccion.add(this);
        }
    }

    public orm.Pais getPais() {
        return pais;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Pais(orm.Pais value) {
        this.pais = value;
    }

    private orm.Pais getORM_Pais() {
        return pais;
    }

    public void setRegion(orm.Region value) {
        if (region != null) {
            region.direccion.remove(this);
        }
        if (value != null) {
            value.direccion.add(this);
        }
    }

    public orm.Region getRegion() {
        return region;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Region(orm.Region value) {
        this.region = value;
    }

    private orm.Region getORM_Region() {
        return region;
    }

    public void setOrganizacion(orm.Organizacion value) {
        if (this.organizacion != value) {
            orm.Organizacion lorganizacion = this.organizacion;
            this.organizacion = value;
            if (value != null) {
                organizacion.setDireccion(this);
            }
            if (lorganizacion != null && lorganizacion.getDireccion() == this) {
                lorganizacion.setDireccion(null);
            }
        }
    }

    public orm.Organizacion getOrganizacion() {
        return organizacion;
    }

    private void setORM_Persona(java.util.Set value) {
        this.ORM_persona = value;
    }

    private java.util.Set getORM_Persona() {
        return ORM_persona;
    }

    public final orm.PersonaSetCollection persona = new orm.PersonaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DIRECCION_PERSONA, orm.ORMConstants.KEY_PERSONA_DIRECCION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
