/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EvaluacionDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_evaluacion;
    public final StringExpression observacion;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public EvaluacionDetachedCriteria() {
        super(orm.Evaluacion.class, orm.EvaluacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_evaluacion = new StringExpression("fecha_evaluacion", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public EvaluacionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.EvaluacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_evaluacion = new StringExpression("fecha_evaluacion", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public ClienteterapeutaDetachedCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaDetachedCriteria(createCriteria("clienteterapeuta"));
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public Evaluacion uniqueEvaluacion(PersistentSession session) {
        return (Evaluacion) super.createExecutableCriteria(session).uniqueResult();
    }

    public Evaluacion[] listEvaluacion(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Evaluacion[]) list.toArray(new Evaluacion[list.size()]);
    }
}
