/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Juego {

    public Juego() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_JUEGO_JUEGO_PATRON) {
            return ORM_juego_patron;
        } else if (key == orm.ORMConstants.KEY_JUEGO_DATOSDISPOCITIVO_JUEGO) {
            return ORM_datosdispocitivo_juego;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String fechajuego;

    private String horajuego;

    private java.util.Set ORM_juego_patron = new java.util.HashSet();

    private java.util.Set ORM_datosdispocitivo_juego = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFechajuego(String value) {
        this.fechajuego = value;
    }

    public String getFechajuego() {
        return fechajuego;
    }

    public void setHorajuego(String value) {
        this.horajuego = value;
    }

    public String getHorajuego() {
        return horajuego;
    }

    private void setORM_Juego_patron(java.util.Set value) {
        this.ORM_juego_patron = value;
    }

    private java.util.Set getORM_Juego_patron() {
        return ORM_juego_patron;
    }

    public final orm.Juego_patronSetCollection juego_patron = new orm.Juego_patronSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_JUEGO_JUEGO_PATRON, orm.ORMConstants.KEY_JUEGO_PATRON_JUEGO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    private void setORM_Datosdispocitivo_juego(java.util.Set value) {
        this.ORM_datosdispocitivo_juego = value;
    }

    private java.util.Set getORM_Datosdispocitivo_juego() {
        return ORM_datosdispocitivo_juego;
    }

    public final orm.Datosdispocitivo_juegoSetCollection datosdispocitivo_juego = new orm.Datosdispocitivo_juegoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_JUEGO_DATOSDISPOCITIVO_JUEGO, orm.ORMConstants.KEY_DATOSDISPOCITIVO_JUEGO_JUEGO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
