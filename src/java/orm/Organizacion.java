/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Organizacion {

    public Organizacion() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_ORGANIZACION_DISPOSITIVO_ORGANIZACION) {
            return ORM_dispositivo_organizacion;
        } else if (key == orm.ORMConstants.KEY_ORGANIZACION_TERAPEUTA) {
            return ORM_terapeuta;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String nombre;

    private String descripcion;

    private String razon_social;

    private String rut;

    private String fax;

    private String email;

    private String web;

    private String password;

    private orm.Telefono telefono;

    private orm.Direccion direccion;

    private java.util.Set ORM_dispositivo_organizacion = new java.util.HashSet();

    private orm.Asigdisporganizacion asigdisporganizacion;

    private java.util.Set ORM_terapeuta = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setNombre(String value) {
        this.nombre = value;
    }

    public String getNombre() {
        return nombre;
    }

    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setRazon_social(String value) {
        this.razon_social = value;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRut(String value) {
        this.rut = value;
    }

    public String getRut() {
        return rut;
    }

    public void setFax(String value) {
        this.fax = value;
    }

    public String getFax() {
        return fax;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getEmail() {
        return email;
    }

    public void setWeb(String value) {
        this.web = value;
    }

    public String getWeb() {
        return web;
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return password;
    }

    public void setTelefono(orm.Telefono value) {
        if (this.telefono != value) {
            orm.Telefono ltelefono = this.telefono;
            this.telefono = value;
            if (value != null) {
                telefono.setOrganizacion(this);
            }
            if (ltelefono != null && ltelefono.getOrganizacion() == this) {
                ltelefono.setOrganizacion(null);
            }
        }
    }

    public orm.Telefono getTelefono() {
        return telefono;
    }

    public void setDireccion(orm.Direccion value) {
        if (this.direccion != value) {
            orm.Direccion ldireccion = this.direccion;
            this.direccion = value;
            if (value != null) {
                direccion.setOrganizacion(this);
            }
            if (ldireccion != null && ldireccion.getOrganizacion() == this) {
                ldireccion.setOrganizacion(null);
            }
        }
    }

    public orm.Direccion getDireccion() {
        return direccion;
    }

    private void setORM_Dispositivo_organizacion(java.util.Set value) {
        this.ORM_dispositivo_organizacion = value;
    }

    private java.util.Set getORM_Dispositivo_organizacion() {
        return ORM_dispositivo_organizacion;
    }

    public final orm.Dispositivo_organizacionSetCollection dispositivo_organizacion = new orm.Dispositivo_organizacionSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ORGANIZACION_DISPOSITIVO_ORGANIZACION, orm.ORMConstants.KEY_DISPOSITIVO_ORGANIZACION_ORGANIZACION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setAsigdisporganizacion(orm.Asigdisporganizacion value) {
        if (this.asigdisporganizacion != value) {
            orm.Asigdisporganizacion lasigdisporganizacion = this.asigdisporganizacion;
            this.asigdisporganizacion = value;
            if (value != null) {
                asigdisporganizacion.setOrganizacion(this);
            }
            if (lasigdisporganizacion != null && lasigdisporganizacion.getOrganizacion() == this) {
                lasigdisporganizacion.setOrganizacion(null);
            }
        }
    }

    public orm.Asigdisporganizacion getAsigdisporganizacion() {
        return asigdisporganizacion;
    }

    private void setORM_Terapeuta(java.util.Set value) {
        this.ORM_terapeuta = value;
    }

    private java.util.Set getORM_Terapeuta() {
        return ORM_terapeuta;
    }

    public final orm.TerapeutaSetCollection terapeuta = new orm.TerapeutaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ORGANIZACION_TERAPEUTA, orm.ORMConstants.KEY_TERAPEUTA_ORGANIZACION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
