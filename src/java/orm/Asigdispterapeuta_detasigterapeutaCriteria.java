/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Asigdispterapeuta_detasigterapeutaCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression asigdispterapeutaId;
    public final AssociationExpression asigdispterapeuta;
    public final IntegerExpression detasigterapeutaId;
    public final AssociationExpression detasigterapeuta;

    public Asigdispterapeuta_detasigterapeutaCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this);
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this);
        detasigterapeutaId = new IntegerExpression("detasigterapeuta.id", this);
        detasigterapeuta = new AssociationExpression("detasigterapeuta", this);
    }

    public Asigdispterapeuta_detasigterapeutaCriteria(PersistentSession session) {
        this(session.createCriteria(Asigdispterapeuta_detasigterapeuta.class));
    }

    public Asigdispterapeuta_detasigterapeutaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public AsigdispterapeutaCriteria createAsigdispterapeutaCriteria() {
        return new AsigdispterapeutaCriteria(createCriteria("asigdispterapeuta"));
    }

    public DetasigterapeutaCriteria createDetasigterapeutaCriteria() {
        return new DetasigterapeutaCriteria(createCriteria("detasigterapeuta"));
    }

    public Asigdispterapeuta_detasigterapeuta uniqueAsigdispterapeuta_detasigterapeuta() {
        return (Asigdispterapeuta_detasigterapeuta) super.uniqueResult();
    }

    public Asigdispterapeuta_detasigterapeuta[] listAsigdispterapeuta_detasigterapeuta() {
        java.util.List list = super.list();
        return (Asigdispterapeuta_detasigterapeuta[]) list.toArray(new Asigdispterapeuta_detasigterapeuta[list.size()]);
    }
}
