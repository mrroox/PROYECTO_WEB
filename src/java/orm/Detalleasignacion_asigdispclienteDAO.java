/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Detalleasignacion_asigdispclienteDAO {

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacion_asigdispclienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente getDetalleasignacion_asigdispclienteByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDetalleasignacion_asigdispclienteByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacion_asigdispclienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente getDetalleasignacion_asigdispclienteByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDetalleasignacion_asigdispclienteByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Detalleasignacion_asigdispcliente) session.load(orm.Detalleasignacion_asigdispcliente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente getDetalleasignacion_asigdispclienteByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Detalleasignacion_asigdispcliente) session.get(orm.Detalleasignacion_asigdispcliente.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Detalleasignacion_asigdispcliente) session.load(orm.Detalleasignacion_asigdispcliente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente getDetalleasignacion_asigdispclienteByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Detalleasignacion_asigdispcliente) session.get(orm.Detalleasignacion_asigdispcliente.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion_asigdispcliente(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDetalleasignacion_asigdispcliente(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion_asigdispcliente(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDetalleasignacion_asigdispcliente(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente[] listDetalleasignacion_asigdispclienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente[] listDetalleasignacion_asigdispclienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion_asigdispcliente(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion_asigdispcliente as Detalleasignacion_asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDetalleasignacion_asigdispcliente(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion_asigdispcliente as Detalleasignacion_asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Detalleasignacion_asigdispcliente", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente[] listDetalleasignacion_asigdispclienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDetalleasignacion_asigdispcliente(session, condition, orderBy);
            return (Detalleasignacion_asigdispcliente[]) list.toArray(new Detalleasignacion_asigdispcliente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente[] listDetalleasignacion_asigdispclienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDetalleasignacion_asigdispcliente(session, condition, orderBy, lockMode);
            return (Detalleasignacion_asigdispcliente[]) list.toArray(new Detalleasignacion_asigdispcliente[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Detalleasignacion_asigdispcliente[] detalleasignacion_asigdispclientes = listDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy);
        if (detalleasignacion_asigdispclientes != null && detalleasignacion_asigdispclientes.length > 0) {
            return detalleasignacion_asigdispclientes[0];
        } else {
            return null;
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Detalleasignacion_asigdispcliente[] detalleasignacion_asigdispclientes = listDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy, lockMode);
        if (detalleasignacion_asigdispclientes != null && detalleasignacion_asigdispclientes.length > 0) {
            return detalleasignacion_asigdispclientes[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDetalleasignacion_asigdispclienteByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetalleasignacion_asigdispclienteByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDetalleasignacion_asigdispclienteByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetalleasignacion_asigdispclienteByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion_asigdispcliente as Detalleasignacion_asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDetalleasignacion_asigdispclienteByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Detalleasignacion_asigdispcliente as Detalleasignacion_asigdispcliente");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Detalleasignacion_asigdispcliente", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente createDetalleasignacion_asigdispcliente() {
        return new orm.Detalleasignacion_asigdispcliente();
    }

    public static boolean save(orm.Detalleasignacion_asigdispcliente detalleasignacion_asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(detalleasignacion_asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Detalleasignacion_asigdispcliente detalleasignacion_asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(detalleasignacion_asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Detalleasignacion_asigdispcliente detalleasignacion_asigdispcliente) throws PersistentException {
        try {
            if (detalleasignacion_asigdispcliente.getDetalleasignacion() != null) {
                detalleasignacion_asigdispcliente.getDetalleasignacion().detalleasignacion_asigdispcliente.remove(detalleasignacion_asigdispcliente);
            }

            if (detalleasignacion_asigdispcliente.getAsigdispcliente() != null) {
                detalleasignacion_asigdispcliente.getAsigdispcliente().detalleasignacion_asigdispcliente.remove(detalleasignacion_asigdispcliente);
            }

            return delete(detalleasignacion_asigdispcliente);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Detalleasignacion_asigdispcliente detalleasignacion_asigdispcliente, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (detalleasignacion_asigdispcliente.getDetalleasignacion() != null) {
                detalleasignacion_asigdispcliente.getDetalleasignacion().detalleasignacion_asigdispcliente.remove(detalleasignacion_asigdispcliente);
            }

            if (detalleasignacion_asigdispcliente.getAsigdispcliente() != null) {
                detalleasignacion_asigdispcliente.getAsigdispcliente().detalleasignacion_asigdispcliente.remove(detalleasignacion_asigdispcliente);
            }

            try {
                session.delete(detalleasignacion_asigdispcliente);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Detalleasignacion_asigdispcliente detalleasignacion_asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(detalleasignacion_asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Detalleasignacion_asigdispcliente detalleasignacion_asigdispcliente) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(detalleasignacion_asigdispcliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Detalleasignacion_asigdispcliente loadDetalleasignacion_asigdispclienteByCriteria(Detalleasignacion_asigdispclienteCriteria detalleasignacion_asigdispclienteCriteria) {
        Detalleasignacion_asigdispcliente[] detalleasignacion_asigdispclientes = listDetalleasignacion_asigdispclienteByCriteria(detalleasignacion_asigdispclienteCriteria);
        if (detalleasignacion_asigdispclientes == null || detalleasignacion_asigdispclientes.length == 0) {
            return null;
        }
        return detalleasignacion_asigdispclientes[0];
    }

    public static Detalleasignacion_asigdispcliente[] listDetalleasignacion_asigdispclienteByCriteria(Detalleasignacion_asigdispclienteCriteria detalleasignacion_asigdispclienteCriteria) {
        return detalleasignacion_asigdispclienteCriteria.listDetalleasignacion_asigdispcliente();
    }
}
