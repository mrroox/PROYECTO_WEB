/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Asigdisporganizacion_dispositivoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;

    public Asigdisporganizacion_dispositivoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this);
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
    }

    public Asigdisporganizacion_dispositivoCriteria(PersistentSession session) {
        this(session.createCriteria(Asigdisporganizacion_dispositivo.class));
    }

    public Asigdisporganizacion_dispositivoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public AsigdisporganizacionCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionCriteria(createCriteria("asigdisporganizacion"));
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public Asigdisporganizacion_dispositivo uniqueAsigdisporganizacion_dispositivo() {
        return (Asigdisporganizacion_dispositivo) super.uniqueResult();
    }

    public Asigdisporganizacion_dispositivo[] listAsigdisporganizacion_dispositivo() {
        java.util.List list = super.list();
        return (Asigdisporganizacion_dispositivo[]) list.toArray(new Asigdisporganizacion_dispositivo[list.size()]);
    }
}
