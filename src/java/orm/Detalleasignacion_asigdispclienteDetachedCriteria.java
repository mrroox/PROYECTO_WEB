/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Detalleasignacion_asigdispclienteDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression detalleasignacionId;
    public final AssociationExpression detalleasignacion;
    public final IntegerExpression asigdispclienteId;
    public final AssociationExpression asigdispcliente;

    public Detalleasignacion_asigdispclienteDetachedCriteria() {
        super(orm.Detalleasignacion_asigdispcliente.class, orm.Detalleasignacion_asigdispclienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        detalleasignacionId = new IntegerExpression("detalleasignacion.id", this.getDetachedCriteria());
        detalleasignacion = new AssociationExpression("detalleasignacion", this.getDetachedCriteria());
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this.getDetachedCriteria());
        asigdispcliente = new AssociationExpression("asigdispcliente", this.getDetachedCriteria());
    }

    public Detalleasignacion_asigdispclienteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Detalleasignacion_asigdispclienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        detalleasignacionId = new IntegerExpression("detalleasignacion.id", this.getDetachedCriteria());
        detalleasignacion = new AssociationExpression("detalleasignacion", this.getDetachedCriteria());
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this.getDetachedCriteria());
        asigdispcliente = new AssociationExpression("asigdispcliente", this.getDetachedCriteria());
    }

    public DetalleasignacionDetachedCriteria createDetalleasignacionCriteria() {
        return new DetalleasignacionDetachedCriteria(createCriteria("detalleasignacion"));
    }

    public AsigdispclienteDetachedCriteria createAsigdispclienteCriteria() {
        return new AsigdispclienteDetachedCriteria(createCriteria("asigdispcliente"));
    }

    public Detalleasignacion_asigdispcliente uniqueDetalleasignacion_asigdispcliente(PersistentSession session) {
        return (Detalleasignacion_asigdispcliente) super.createExecutableCriteria(session).uniqueResult();
    }

    public Detalleasignacion_asigdispcliente[] listDetalleasignacion_asigdispcliente(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Detalleasignacion_asigdispcliente[]) list.toArray(new Detalleasignacion_asigdispcliente[list.size()]);
    }
}
