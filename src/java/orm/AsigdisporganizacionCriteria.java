/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsigdisporganizacionCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_asignacion;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final IntegerExpression cantidad;
    public final StringExpression observacion;
    public final CollectionExpression asigdisporganizacion_dispositivo;

    public AsigdisporganizacionCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha_asignacion = new StringExpression("fecha_asignacion", this);
        organizacionId = new IntegerExpression("organizacion.id", this);
        organizacion = new AssociationExpression("organizacion", this);
        terapeutaId = new IntegerExpression("terapeuta.id", this);
        terapeuta = new AssociationExpression("terapeuta", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
        cantidad = new IntegerExpression("cantidad", this);
        observacion = new StringExpression("observacion", this);
        asigdisporganizacion_dispositivo = new CollectionExpression("ORM_Asigdisporganizacion_dispositivo", this);
    }

    public AsigdisporganizacionCriteria(PersistentSession session) {
        this(session.createCriteria(Asigdisporganizacion.class));
    }

    public AsigdisporganizacionCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public OrganizacionCriteria createOrganizacionCriteria() {
        return new OrganizacionCriteria(createCriteria("organizacion"));
    }

    public TerapeutaCriteria createTerapeutaCriteria() {
        return new TerapeutaCriteria(createCriteria("terapeuta"));
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public Asigdisporganizacion_dispositivoCriteria createAsigdisporganizacion_dispositivoCriteria() {
        return new Asigdisporganizacion_dispositivoCriteria(createCriteria("ORM_Asigdisporganizacion_dispositivo"));
    }

    public Asigdisporganizacion uniqueAsigdisporganizacion() {
        return (Asigdisporganizacion) super.uniqueResult();
    }

    public Asigdisporganizacion[] listAsigdisporganizacion() {
        java.util.List list = super.list();
        return (Asigdisporganizacion[]) list.toArray(new Asigdisporganizacion[list.size()]);
    }
}
