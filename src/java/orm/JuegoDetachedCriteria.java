/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class JuegoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fechajuego;
    public final StringExpression horajuego;
    public final CollectionExpression juego_patron;
    public final CollectionExpression datosdispocitivo_juego;

    public JuegoDetachedCriteria() {
        super(orm.Juego.class, orm.JuegoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fechajuego = new StringExpression("fechajuego", this.getDetachedCriteria());
        horajuego = new StringExpression("horajuego", this.getDetachedCriteria());
        juego_patron = new CollectionExpression("ORM_Juego_patron", this.getDetachedCriteria());
        datosdispocitivo_juego = new CollectionExpression("ORM_Datosdispocitivo_juego", this.getDetachedCriteria());
    }

    public JuegoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.JuegoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fechajuego = new StringExpression("fechajuego", this.getDetachedCriteria());
        horajuego = new StringExpression("horajuego", this.getDetachedCriteria());
        juego_patron = new CollectionExpression("ORM_Juego_patron", this.getDetachedCriteria());
        datosdispocitivo_juego = new CollectionExpression("ORM_Datosdispocitivo_juego", this.getDetachedCriteria());
    }

    public Juego_patronDetachedCriteria createJuego_patronCriteria() {
        return new Juego_patronDetachedCriteria(createCriteria("ORM_Juego_patron"));
    }

    public Datosdispocitivo_juegoDetachedCriteria createDatosdispocitivo_juegoCriteria() {
        return new Datosdispocitivo_juegoDetachedCriteria(createCriteria("ORM_Datosdispocitivo_juego"));
    }

    public Juego uniqueJuego(PersistentSession session) {
        return (Juego) super.createExecutableCriteria(session).uniqueResult();
    }

    public Juego[] listJuego(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Juego[]) list.toArray(new Juego[list.size()]);
    }
}
