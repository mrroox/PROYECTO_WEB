/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ClienteterapeutaCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression password;
    public final IntegerExpression personaId;
    public final AssociationExpression persona;
    public final IntegerExpression personadireccionid;
    public final IntegerExpression evaluacionId;
    public final AssociationExpression evaluacion;
    public final CollectionExpression clienteterapeuta_paciente;
    public final IntegerExpression asigdispclienteId;
    public final AssociationExpression asigdispcliente;
    public final CollectionExpression clienteterapeuta_dispositivo;

    public ClienteterapeutaCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        password = new StringExpression("password", this);
        personaId = new IntegerExpression("persona.id", this);
        persona = new AssociationExpression("persona", this);
        personadireccionid = new IntegerExpression("personadireccionid", this);
        evaluacionId = new IntegerExpression("evaluacion.id", this);
        evaluacion = new AssociationExpression("evaluacion", this);
        clienteterapeuta_paciente = new CollectionExpression("ORM_Clienteterapeuta_paciente", this);
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this);
        asigdispcliente = new AssociationExpression("asigdispcliente", this);
        clienteterapeuta_dispositivo = new CollectionExpression("ORM_Clienteterapeuta_dispositivo", this);
    }

    public ClienteterapeutaCriteria(PersistentSession session) {
        this(session.createCriteria(Clienteterapeuta.class));
    }

    public ClienteterapeutaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public PersonaCriteria createPersonaCriteria() {
        return new PersonaCriteria(createCriteria("persona"));
    }

    public EvaluacionCriteria createEvaluacionCriteria() {
        return new EvaluacionCriteria(createCriteria("evaluacion"));
    }

    public Clienteterapeuta_pacienteCriteria createClienteterapeuta_pacienteCriteria() {
        return new Clienteterapeuta_pacienteCriteria(createCriteria("ORM_Clienteterapeuta_paciente"));
    }

    public AsigdispclienteCriteria createAsigdispclienteCriteria() {
        return new AsigdispclienteCriteria(createCriteria("asigdispcliente"));
    }

    public Clienteterapeuta_dispositivoCriteria createClienteterapeuta_dispositivoCriteria() {
        return new Clienteterapeuta_dispositivoCriteria(createCriteria("ORM_Clienteterapeuta_dispositivo"));
    }

    public Clienteterapeuta uniqueClienteterapeuta() {
        return (Clienteterapeuta) super.uniqueResult();
    }

    public Clienteterapeuta[] listClienteterapeuta() {
        java.util.List list = super.list();
        return (Clienteterapeuta[]) list.toArray(new Clienteterapeuta[list.size()]);
    }
}
