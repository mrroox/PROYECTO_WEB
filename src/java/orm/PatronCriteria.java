/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PatronCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression nivel;
    public final IntegerExpression nivelmaximo;
    public final IntegerExpression tiemporespuesta;
    public final CollectionExpression juego_patron;

    public PatronCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        nivel = new IntegerExpression("nivel", this);
        nivelmaximo = new IntegerExpression("nivelmaximo", this);
        tiemporespuesta = new IntegerExpression("tiemporespuesta", this);
        juego_patron = new CollectionExpression("ORM_Juego_patron", this);
    }

    public PatronCriteria(PersistentSession session) {
        this(session.createCriteria(Patron.class));
    }

    public PatronCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public Juego_patronCriteria createJuego_patronCriteria() {
        return new Juego_patronCriteria(createCriteria("ORM_Juego_patron"));
    }

    public Patron uniquePatron() {
        return (Patron) super.uniqueResult();
    }

    public Patron[] listPatron() {
        java.util.List list = super.list();
        return (Patron[]) list.toArray(new Patron[list.size()]);
    }
}
