/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DetalleasignacionDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression observaciones;
    public final StringExpression fecha_asignacion;
    public final StringExpression fecha_devolucion;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final CollectionExpression detalleasignacion_asigdispcliente;

    public DetalleasignacionDetachedCriteria() {
        super(orm.Detalleasignacion.class, orm.DetalleasignacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        fecha_devolucion = new StringExpression("fecha_devolucion", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        detalleasignacion_asigdispcliente = new CollectionExpression("ORM_Detalleasignacion_asigdispcliente", this.getDetachedCriteria());
    }

    public DetalleasignacionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DetalleasignacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        fecha_devolucion = new StringExpression("fecha_devolucion", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        detalleasignacion_asigdispcliente = new CollectionExpression("ORM_Detalleasignacion_asigdispcliente", this.getDetachedCriteria());
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public Detalleasignacion_asigdispclienteDetachedCriteria createDetalleasignacion_asigdispclienteCriteria() {
        return new Detalleasignacion_asigdispclienteDetachedCriteria(createCriteria("ORM_Detalleasignacion_asigdispcliente"));
    }

    public Detalleasignacion uniqueDetalleasignacion(PersistentSession session) {
        return (Detalleasignacion) super.createExecutableCriteria(session).uniqueResult();
    }

    public Detalleasignacion[] listDetalleasignacion(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Detalleasignacion[]) list.toArray(new Detalleasignacion[list.size()]);
    }
}
