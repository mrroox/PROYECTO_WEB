/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosusogeneralDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression tiempototaluso;
    public final IntegerExpression nivelmaximologrado;
    public final IntegerExpression totalpatronesrealizados;
    public final IntegerExpression datosdispositivoId;
    public final AssociationExpression datosdispositivo;

    public DatosusogeneralDetachedCriteria() {
        super(orm.Datosusogeneral.class, orm.DatosusogeneralCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        tiempototaluso = new IntegerExpression("tiempototaluso", this.getDetachedCriteria());
        nivelmaximologrado = new IntegerExpression("nivelmaximologrado", this.getDetachedCriteria());
        totalpatronesrealizados = new IntegerExpression("totalpatronesrealizados", this.getDetachedCriteria());
        datosdispositivoId = new IntegerExpression("datosdispositivo.id", this.getDetachedCriteria());
        datosdispositivo = new AssociationExpression("datosdispositivo", this.getDetachedCriteria());
    }

    public DatosusogeneralDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DatosusogeneralCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        tiempototaluso = new IntegerExpression("tiempototaluso", this.getDetachedCriteria());
        nivelmaximologrado = new IntegerExpression("nivelmaximologrado", this.getDetachedCriteria());
        totalpatronesrealizados = new IntegerExpression("totalpatronesrealizados", this.getDetachedCriteria());
        datosdispositivoId = new IntegerExpression("datosdispositivo.id", this.getDetachedCriteria());
        datosdispositivo = new AssociationExpression("datosdispositivo", this.getDetachedCriteria());
    }

    public DatosdispositivoDetachedCriteria createDatosdispositivoCriteria() {
        return new DatosdispositivoDetachedCriteria(createCriteria("datosdispositivo"));
    }

    public Datosusogeneral uniqueDatosusogeneral(PersistentSession session) {
        return (Datosusogeneral) super.createExecutableCriteria(session).uniqueResult();
    }

    public Datosusogeneral[] listDatosusogeneral(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Datosusogeneral[]) list.toArray(new Datosusogeneral[list.size()]);
    }
}
