/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Paciente_terapeuta {

    public Paciente_terapeuta() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_PACIENTE_TERAPEUTA_PACIENTE) {
            this.paciente = (orm.Paciente) owner;
        } else if (key == orm.ORMConstants.KEY_PACIENTE_TERAPEUTA_TERAPEUTA) {
            this.terapeuta = (orm.Terapeuta) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private String fecha_ingreso;

    private orm.Paciente paciente;

    private orm.Terapeuta terapeuta;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFecha_ingreso(String value) {
        this.fecha_ingreso = value;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setPaciente(orm.Paciente value) {
        if (paciente != null) {
            paciente.paciente_terapeuta.remove(this);
        }
        if (value != null) {
            value.paciente_terapeuta.add(this);
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Paciente(orm.Paciente value) {
        this.paciente = value;
    }

    private orm.Paciente getORM_Paciente() {
        return paciente;
    }

    public void setTerapeuta(orm.Terapeuta value) {
        if (terapeuta != null) {
            terapeuta.paciente_terapeuta.remove(this);
        }
        if (value != null) {
            value.paciente_terapeuta.add(this);
        }
    }

    public orm.Terapeuta getTerapeuta() {
        return terapeuta;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Terapeuta(orm.Terapeuta value) {
        this.terapeuta = value;
    }

    private orm.Terapeuta getORM_Terapeuta() {
        return terapeuta;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
