/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Apoderado_paciente {

    public Apoderado_paciente() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_APODERADO_PACIENTE_APODERADO) {
            this.apoderado = (orm.Apoderado) owner;
        } else if (key == orm.ORMConstants.KEY_APODERADO_PACIENTE_PACIENTE) {
            this.paciente = (orm.Paciente) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Apoderado apoderado;

    private orm.Paciente paciente;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setApoderado(orm.Apoderado value) {
        if (apoderado != null) {
            apoderado.apoderado_paciente.remove(this);
        }
        if (value != null) {
            value.apoderado_paciente.add(this);
        }
    }

    public orm.Apoderado getApoderado() {
        return apoderado;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Apoderado(orm.Apoderado value) {
        this.apoderado = value;
    }

    private orm.Apoderado getORM_Apoderado() {
        return apoderado;
    }

    public void setPaciente(orm.Paciente value) {
        if (paciente != null) {
            paciente.apoderado_paciente.remove(this);
        }
        if (value != null) {
            value.apoderado_paciente.add(this);
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Paciente(orm.Paciente value) {
        this.paciente = value;
    }

    private orm.Paciente getORM_Paciente() {
        return paciente;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
