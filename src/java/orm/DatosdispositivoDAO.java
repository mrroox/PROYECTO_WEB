/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DatosdispositivoDAO {

    public static Datosdispositivo loadDatosdispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo getDatosdispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo loadDatosdispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo getDatosdispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDatosdispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo loadDatosdispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdispositivo) session.load(orm.Datosdispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo getDatosdispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Datosdispositivo) session.get(orm.Datosdispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo loadDatosdispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdispositivo) session.load(orm.Datosdispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo getDatosdispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Datosdispositivo) session.get(orm.Datosdispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispositivo(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdispositivo(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispositivo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDatosdispositivo(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo[] listDatosdispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo[] listDatosdispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDatosdispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispositivo(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispositivo as Datosdispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDatosdispositivo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispositivo as Datosdispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdispositivo", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo[] listDatosdispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDatosdispositivo(session, condition, orderBy);
            return (Datosdispositivo[]) list.toArray(new Datosdispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo[] listDatosdispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDatosdispositivo(session, condition, orderBy, lockMode);
            return (Datosdispositivo[]) list.toArray(new Datosdispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo loadDatosdispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo loadDatosdispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDatosdispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo loadDatosdispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Datosdispositivo[] datosdispositivos = listDatosdispositivoByQuery(session, condition, orderBy);
        if (datosdispositivos != null && datosdispositivos.length > 0) {
            return datosdispositivos[0];
        } else {
            return null;
        }
    }

    public static Datosdispositivo loadDatosdispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Datosdispositivo[] datosdispositivos = listDatosdispositivoByQuery(session, condition, orderBy, lockMode);
        if (datosdispositivos != null && datosdispositivos.length > 0) {
            return datosdispositivos[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDatosdispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDatosdispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispositivo as Datosdispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDatosdispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Datosdispositivo as Datosdispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Datosdispositivo", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo createDatosdispositivo() {
        return new orm.Datosdispositivo();
    }

    public static boolean save(orm.Datosdispositivo datosdispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(datosdispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Datosdispositivo datosdispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(datosdispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdispositivo datosdispositivo) throws PersistentException {
        try {
            if (datosdispositivo.getDatosusogeneral() != null) {
                datosdispositivo.getDatosusogeneral().setDatosdispositivo(null);
            }

            if (datosdispositivo.getDatosusonivel() != null) {
                datosdispositivo.getDatosusonivel().setDatosdispositivo(null);
            }

            if (datosdispositivo.getDatosnivelcompletado() != null) {
                datosdispositivo.getDatosnivelcompletado().setDatosdispositivo(null);
            }

            if (datosdispositivo.getDispositivo() != null) {
                datosdispositivo.getDispositivo().setDotosdispositivo(null);
            }

            orm.Datosdispocitivo_datosdeusodiario[] lDatosdispocitivo_datosdeusodiarios = datosdispositivo.datosdispocitivo_datosdeusodiario.toArray();
            for (int i = 0; i < lDatosdispocitivo_datosdeusodiarios.length; i++) {
                lDatosdispocitivo_datosdeusodiarios[i].setDatosdispocitivo(null);
            }
            orm.Datosdispocitivo_juego[] lDatosdispocitivo_juegos = datosdispositivo.datosdispocitivo_juego.toArray();
            for (int i = 0; i < lDatosdispocitivo_juegos.length; i++) {
                lDatosdispocitivo_juegos[i].setDatosdispocitivo(null);
            }
            return delete(datosdispositivo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Datosdispositivo datosdispositivo, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (datosdispositivo.getDatosusogeneral() != null) {
                datosdispositivo.getDatosusogeneral().setDatosdispositivo(null);
            }

            if (datosdispositivo.getDatosusonivel() != null) {
                datosdispositivo.getDatosusonivel().setDatosdispositivo(null);
            }

            if (datosdispositivo.getDatosnivelcompletado() != null) {
                datosdispositivo.getDatosnivelcompletado().setDatosdispositivo(null);
            }

            if (datosdispositivo.getDispositivo() != null) {
                datosdispositivo.getDispositivo().setDotosdispositivo(null);
            }

            orm.Datosdispocitivo_datosdeusodiario[] lDatosdispocitivo_datosdeusodiarios = datosdispositivo.datosdispocitivo_datosdeusodiario.toArray();
            for (int i = 0; i < lDatosdispocitivo_datosdeusodiarios.length; i++) {
                lDatosdispocitivo_datosdeusodiarios[i].setDatosdispocitivo(null);
            }
            orm.Datosdispocitivo_juego[] lDatosdispocitivo_juegos = datosdispositivo.datosdispocitivo_juego.toArray();
            for (int i = 0; i < lDatosdispocitivo_juegos.length; i++) {
                lDatosdispocitivo_juegos[i].setDatosdispocitivo(null);
            }
            try {
                session.delete(datosdispositivo);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Datosdispositivo datosdispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(datosdispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Datosdispositivo datosdispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(datosdispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Datosdispositivo loadDatosdispositivoByCriteria(DatosdispositivoCriteria datosdispositivoCriteria) {
        Datosdispositivo[] datosdispositivos = listDatosdispositivoByCriteria(datosdispositivoCriteria);
        if (datosdispositivos == null || datosdispositivos.length == 0) {
            return null;
        }
        return datosdispositivos[0];
    }

    public static Datosdispositivo[] listDatosdispositivoByCriteria(DatosdispositivoCriteria datosdispositivoCriteria) {
        return datosdispositivoCriteria.listDatosdispositivo();
    }
}
