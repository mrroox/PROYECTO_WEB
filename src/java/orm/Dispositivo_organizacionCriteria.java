/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Dispositivo_organizacionCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;

    public Dispositivo_organizacionCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
        organizacionId = new IntegerExpression("organizacion.id", this);
        organizacion = new AssociationExpression("organizacion", this);
    }

    public Dispositivo_organizacionCriteria(PersistentSession session) {
        this(session.createCriteria(Dispositivo_organizacion.class));
    }

    public Dispositivo_organizacionCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public OrganizacionCriteria createOrganizacionCriteria() {
        return new OrganizacionCriteria(createCriteria("organizacion"));
    }

    public Dispositivo_organizacion uniqueDispositivo_organizacion() {
        return (Dispositivo_organizacion) super.uniqueResult();
    }

    public Dispositivo_organizacion[] listDispositivo_organizacion() {
        java.util.List list = super.list();
        return (Dispositivo_organizacion[]) list.toArray(new Dispositivo_organizacion[list.size()]);
    }
}
