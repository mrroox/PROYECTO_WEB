/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Clienteterapeuta_dispositivoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;

    public Clienteterapeuta_dispositivoDetachedCriteria() {
        super(orm.Clienteterapeuta_dispositivo.class, orm.Clienteterapeuta_dispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
    }

    public Clienteterapeuta_dispositivoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Clienteterapeuta_dispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this.getDetachedCriteria());
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
    }

    public ClienteterapeutaDetachedCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaDetachedCriteria(createCriteria("clienteterapeuta"));
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public Clienteterapeuta_dispositivo uniqueClienteterapeuta_dispositivo(PersistentSession session) {
        return (Clienteterapeuta_dispositivo) super.createExecutableCriteria(session).uniqueResult();
    }

    public Clienteterapeuta_dispositivo[] listClienteterapeuta_dispositivo(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Clienteterapeuta_dispositivo[]) list.toArray(new Clienteterapeuta_dispositivo[list.size()]);
    }
}
