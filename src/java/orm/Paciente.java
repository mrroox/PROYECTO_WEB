/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Paciente {

    public Paciente() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_PACIENTE_CLIENTETERAPEUTA_PACIENTE) {
            return ORM_clienteterapeuta_paciente;
        } else if (key == orm.ORMConstants.KEY_PACIENTE_APODERADO_PACIENTE) {
            return ORM_apoderado_paciente;
        } else if (key == orm.ORMConstants.KEY_PACIENTE_PACIENTE_TERAPEUTA) {
            return ORM_paciente_terapeuta;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private orm.Expedientemedico expedientemedico;

    private orm.Persona persona;

    private int personadireccionid;

    private java.util.Set ORM_clienteterapeuta_paciente = new java.util.HashSet();

    private orm.Asigdispcliente asigdispcliente;

    private orm.Evaluacion evaluacion;

    private java.util.Set ORM_apoderado_paciente = new java.util.HashSet();

    private java.util.Set ORM_paciente_terapeuta = new java.util.HashSet();

    private orm.Evaluacionterapeuta evaluacionterapeuta;

    private orm.Asigdispterapeuta asigdispterapeuta;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setPersonadireccionid(int value) {
        this.personadireccionid = value;
    }

    public int getPersonadireccionid() {
        return personadireccionid;
    }

    public void setExpedientemedico(orm.Expedientemedico value) {
        if (this.expedientemedico != value) {
            orm.Expedientemedico lexpedientemedico = this.expedientemedico;
            this.expedientemedico = value;
            if (value != null) {
                expedientemedico.setPaciente(this);
            }
            if (lexpedientemedico != null && lexpedientemedico.getPaciente() == this) {
                lexpedientemedico.setPaciente(null);
            }
        }
    }

    public orm.Expedientemedico getExpedientemedico() {
        return expedientemedico;
    }

    public void setPersona(orm.Persona value) {
        if (this.persona != value) {
            orm.Persona lpersona = this.persona;
            this.persona = value;
            if (value != null) {
                persona.setPaciente(this);
            }
            if (lpersona != null && lpersona.getPaciente() == this) {
                lpersona.setPaciente(null);
            }
        }
    }

    public orm.Persona getPersona() {
        return persona;
    }

    private void setORM_Clienteterapeuta_paciente(java.util.Set value) {
        this.ORM_clienteterapeuta_paciente = value;
    }

    private java.util.Set getORM_Clienteterapeuta_paciente() {
        return ORM_clienteterapeuta_paciente;
    }

    public final orm.Clienteterapeuta_pacienteSetCollection clienteterapeuta_paciente = new orm.Clienteterapeuta_pacienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PACIENTE_CLIENTETERAPEUTA_PACIENTE, orm.ORMConstants.KEY_CLIENTETERAPEUTA_PACIENTE_PACIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setAsigdispcliente(orm.Asigdispcliente value) {
        if (this.asigdispcliente != value) {
            orm.Asigdispcliente lasigdispcliente = this.asigdispcliente;
            this.asigdispcliente = value;
            if (value != null) {
                asigdispcliente.setPaciente(this);
            }
            if (lasigdispcliente != null && lasigdispcliente.getPaciente() == this) {
                lasigdispcliente.setPaciente(null);
            }
        }
    }

    public orm.Asigdispcliente getAsigdispcliente() {
        return asigdispcliente;
    }

    public void setEvaluacion(orm.Evaluacion value) {
        if (this.evaluacion != value) {
            orm.Evaluacion levaluacion = this.evaluacion;
            this.evaluacion = value;
            if (value != null) {
                evaluacion.setPaciente(this);
            }
            if (levaluacion != null && levaluacion.getPaciente() == this) {
                levaluacion.setPaciente(null);
            }
        }
    }

    public orm.Evaluacion getEvaluacion() {
        return evaluacion;
    }

    private void setORM_Apoderado_paciente(java.util.Set value) {
        this.ORM_apoderado_paciente = value;
    }

    private java.util.Set getORM_Apoderado_paciente() {
        return ORM_apoderado_paciente;
    }

    public final orm.Apoderado_pacienteSetCollection apoderado_paciente = new orm.Apoderado_pacienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PACIENTE_APODERADO_PACIENTE, orm.ORMConstants.KEY_APODERADO_PACIENTE_PACIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    private void setORM_Paciente_terapeuta(java.util.Set value) {
        this.ORM_paciente_terapeuta = value;
    }

    private java.util.Set getORM_Paciente_terapeuta() {
        return ORM_paciente_terapeuta;
    }

    public final orm.Paciente_terapeutaSetCollection paciente_terapeuta = new orm.Paciente_terapeutaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PACIENTE_PACIENTE_TERAPEUTA, orm.ORMConstants.KEY_PACIENTE_TERAPEUTA_PACIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setEvaluacionterapeuta(orm.Evaluacionterapeuta value) {
        if (this.evaluacionterapeuta != value) {
            orm.Evaluacionterapeuta levaluacionterapeuta = this.evaluacionterapeuta;
            this.evaluacionterapeuta = value;
            if (value != null) {
                evaluacionterapeuta.setPaciente(this);
            }
            if (levaluacionterapeuta != null && levaluacionterapeuta.getPaciente() == this) {
                levaluacionterapeuta.setPaciente(null);
            }
        }
    }

    public orm.Evaluacionterapeuta getEvaluacionterapeuta() {
        return evaluacionterapeuta;
    }

    public void setAsigdispterapeuta(orm.Asigdispterapeuta value) {
        if (this.asigdispterapeuta != value) {
            orm.Asigdispterapeuta lasigdispterapeuta = this.asigdispterapeuta;
            this.asigdispterapeuta = value;
            if (value != null) {
                asigdispterapeuta.setPaciente(this);
            }
            if (lasigdispterapeuta != null && lasigdispterapeuta.getPaciente() == this) {
                lasigdispterapeuta.setPaciente(null);
            }
        }
    }

    public orm.Asigdispterapeuta getAsigdispterapeuta() {
        return asigdispterapeuta;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
