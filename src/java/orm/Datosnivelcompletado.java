/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Datosnivelcompletado {

    public Datosnivelcompletado() {
    }

    private int id;

    private Integer n1;

    private Integer n2;

    private Integer n3;

    private Integer n4;

    private Integer n5;

    private Integer n6;

    private Integer n7;

    private Integer n8;

    private Integer n9;

    private Integer n10;

    private Integer n11;

    private Integer n12;

    private Integer n13;

    private Integer n14;

    private Integer n15;

    private orm.Datosdispositivo datosdispositivo;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setN1(int value) {
        setN1(new Integer(value));
    }

    public void setN1(Integer value) {
        this.n1 = value;
    }

    public Integer getN1() {
        return n1;
    }

    public void setN2(int value) {
        setN2(new Integer(value));
    }

    public void setN2(Integer value) {
        this.n2 = value;
    }

    public Integer getN2() {
        return n2;
    }

    public void setN3(int value) {
        setN3(new Integer(value));
    }

    public void setN3(Integer value) {
        this.n3 = value;
    }

    public Integer getN3() {
        return n3;
    }

    public void setN4(int value) {
        setN4(new Integer(value));
    }

    public void setN4(Integer value) {
        this.n4 = value;
    }

    public Integer getN4() {
        return n4;
    }

    public void setN5(int value) {
        setN5(new Integer(value));
    }

    public void setN5(Integer value) {
        this.n5 = value;
    }

    public Integer getN5() {
        return n5;
    }

    public void setN6(int value) {
        setN6(new Integer(value));
    }

    public void setN6(Integer value) {
        this.n6 = value;
    }

    public Integer getN6() {
        return n6;
    }

    public void setN7(int value) {
        setN7(new Integer(value));
    }

    public void setN7(Integer value) {
        this.n7 = value;
    }

    public Integer getN7() {
        return n7;
    }

    public void setN8(int value) {
        setN8(new Integer(value));
    }

    public void setN8(Integer value) {
        this.n8 = value;
    }

    public Integer getN8() {
        return n8;
    }

    public void setN9(int value) {
        setN9(new Integer(value));
    }

    public void setN9(Integer value) {
        this.n9 = value;
    }

    public Integer getN9() {
        return n9;
    }

    public void setN10(int value) {
        setN10(new Integer(value));
    }

    public void setN10(Integer value) {
        this.n10 = value;
    }

    public Integer getN10() {
        return n10;
    }

    public void setN11(int value) {
        setN11(new Integer(value));
    }

    public void setN11(Integer value) {
        this.n11 = value;
    }

    public Integer getN11() {
        return n11;
    }

    public void setN12(int value) {
        setN12(new Integer(value));
    }

    public void setN12(Integer value) {
        this.n12 = value;
    }

    public Integer getN12() {
        return n12;
    }

    public void setN13(int value) {
        setN13(new Integer(value));
    }

    public void setN13(Integer value) {
        this.n13 = value;
    }

    public Integer getN13() {
        return n13;
    }

    public void setN14(int value) {
        setN14(new Integer(value));
    }

    public void setN14(Integer value) {
        this.n14 = value;
    }

    public Integer getN14() {
        return n14;
    }

    public void setN15(int value) {
        setN15(new Integer(value));
    }

    public void setN15(Integer value) {
        this.n15 = value;
    }

    public Integer getN15() {
        return n15;
    }

    public void setDatosdispositivo(orm.Datosdispositivo value) {
        if (this.datosdispositivo != value) {
            orm.Datosdispositivo ldatosdispositivo = this.datosdispositivo;
            this.datosdispositivo = value;
            if (value != null) {
                datosdispositivo.setDatosnivelcompletado(this);
            }
            if (ldatosdispositivo != null && ldatosdispositivo.getDatosnivelcompletado() == this) {
                ldatosdispositivo.setDatosnivelcompletado(null);
            }
        }
    }

    public orm.Datosdispositivo getDatosdispositivo() {
        return datosdispositivo;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
