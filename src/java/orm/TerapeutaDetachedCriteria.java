/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TerapeutaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression personaId;
    public final AssociationExpression persona;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final IntegerExpression id;
    public final StringExpression password;
    public final IntegerExpression personadireccionid;
    public final CollectionExpression paciente_terapeuta;
    public final CollectionExpression terapeuta_cargo;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final IntegerExpression evaluacionterapeutaId;
    public final AssociationExpression evaluacionterapeuta;
    public final IntegerExpression asigdispterapeutaId;
    public final AssociationExpression asigdispterapeuta;

    public TerapeutaDetachedCriteria() {
        super(orm.Terapeuta.class, orm.TerapeutaCriteria.class);
        personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
        persona = new AssociationExpression("persona", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        id = new IntegerExpression("id", this.getDetachedCriteria());
        password = new StringExpression("password", this.getDetachedCriteria());
        personadireccionid = new IntegerExpression("personadireccionid", this.getDetachedCriteria());
        paciente_terapeuta = new CollectionExpression("ORM_Paciente_terapeuta", this.getDetachedCriteria());
        terapeuta_cargo = new CollectionExpression("ORM_Terapeuta_cargo", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        evaluacionterapeutaId = new IntegerExpression("evaluacionterapeuta.id", this.getDetachedCriteria());
        evaluacionterapeuta = new AssociationExpression("evaluacionterapeuta", this.getDetachedCriteria());
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this.getDetachedCriteria());
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this.getDetachedCriteria());
    }

    public TerapeutaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.TerapeutaCriteria.class);
        personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
        persona = new AssociationExpression("persona", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        id = new IntegerExpression("id", this.getDetachedCriteria());
        password = new StringExpression("password", this.getDetachedCriteria());
        personadireccionid = new IntegerExpression("personadireccionid", this.getDetachedCriteria());
        paciente_terapeuta = new CollectionExpression("ORM_Paciente_terapeuta", this.getDetachedCriteria());
        terapeuta_cargo = new CollectionExpression("ORM_Terapeuta_cargo", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        evaluacionterapeutaId = new IntegerExpression("evaluacionterapeuta.id", this.getDetachedCriteria());
        evaluacionterapeuta = new AssociationExpression("evaluacionterapeuta", this.getDetachedCriteria());
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this.getDetachedCriteria());
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this.getDetachedCriteria());
    }

    public PersonaDetachedCriteria createPersonaCriteria() {
        return new PersonaDetachedCriteria(createCriteria("persona"));
    }

    public OrganizacionDetachedCriteria createOrganizacionCriteria() {
        return new OrganizacionDetachedCriteria(createCriteria("organizacion"));
    }

    public Paciente_terapeutaDetachedCriteria createPaciente_terapeutaCriteria() {
        return new Paciente_terapeutaDetachedCriteria(createCriteria("ORM_Paciente_terapeuta"));
    }

    public Terapeuta_cargoDetachedCriteria createTerapeuta_cargoCriteria() {
        return new Terapeuta_cargoDetachedCriteria(createCriteria("ORM_Terapeuta_cargo"));
    }

    public AsigdisporganizacionDetachedCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionDetachedCriteria(createCriteria("asigdisporganizacion"));
    }

    public EvaluacionterapeutaDetachedCriteria createEvaluacionterapeutaCriteria() {
        return new EvaluacionterapeutaDetachedCriteria(createCriteria("evaluacionterapeuta"));
    }

    public AsigdispterapeutaDetachedCriteria createAsigdispterapeutaCriteria() {
        return new AsigdispterapeutaDetachedCriteria(createCriteria("asigdispterapeuta"));
    }

    public Terapeuta uniqueTerapeuta(PersistentSession session) {
        return (Terapeuta) super.createExecutableCriteria(session).uniqueResult();
    }

    public Terapeuta[] listTerapeuta(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Terapeuta[]) list.toArray(new Terapeuta[list.size()]);
    }
}
