/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class OrganizacionDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression nombre;
    public final StringExpression descripcion;
    public final StringExpression razon_social;
    public final StringExpression rut;
    public final StringExpression fax;
    public final StringExpression email;
    public final StringExpression web;
    public final StringExpression password;
    public final IntegerExpression telefonoId;
    public final AssociationExpression telefono;
    public final IntegerExpression direccionId;
    public final AssociationExpression direccion;
    public final CollectionExpression dispositivo_organizacion;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final CollectionExpression terapeuta;

    public OrganizacionDetachedCriteria() {
        super(orm.Organizacion.class, orm.OrganizacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
        razon_social = new StringExpression("razon_social", this.getDetachedCriteria());
        rut = new StringExpression("rut", this.getDetachedCriteria());
        fax = new StringExpression("fax", this.getDetachedCriteria());
        email = new StringExpression("email", this.getDetachedCriteria());
        web = new StringExpression("web", this.getDetachedCriteria());
        password = new StringExpression("password", this.getDetachedCriteria());
        telefonoId = new IntegerExpression("telefono.id", this.getDetachedCriteria());
        telefono = new AssociationExpression("telefono", this.getDetachedCriteria());
        direccionId = new IntegerExpression("direccion.id", this.getDetachedCriteria());
        direccion = new AssociationExpression("direccion", this.getDetachedCriteria());
        dispositivo_organizacion = new CollectionExpression("ORM_Dispositivo_organizacion", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        terapeuta = new CollectionExpression("ORM_Terapeuta", this.getDetachedCriteria());
    }

    public OrganizacionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.OrganizacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nombre = new StringExpression("nombre", this.getDetachedCriteria());
        descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
        razon_social = new StringExpression("razon_social", this.getDetachedCriteria());
        rut = new StringExpression("rut", this.getDetachedCriteria());
        fax = new StringExpression("fax", this.getDetachedCriteria());
        email = new StringExpression("email", this.getDetachedCriteria());
        web = new StringExpression("web", this.getDetachedCriteria());
        password = new StringExpression("password", this.getDetachedCriteria());
        telefonoId = new IntegerExpression("telefono.id", this.getDetachedCriteria());
        telefono = new AssociationExpression("telefono", this.getDetachedCriteria());
        direccionId = new IntegerExpression("direccion.id", this.getDetachedCriteria());
        direccion = new AssociationExpression("direccion", this.getDetachedCriteria());
        dispositivo_organizacion = new CollectionExpression("ORM_Dispositivo_organizacion", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        terapeuta = new CollectionExpression("ORM_Terapeuta", this.getDetachedCriteria());
    }

    public TelefonoDetachedCriteria createTelefonoCriteria() {
        return new TelefonoDetachedCriteria(createCriteria("telefono"));
    }

    public DireccionDetachedCriteria createDireccionCriteria() {
        return new DireccionDetachedCriteria(createCriteria("direccion"));
    }

    public Dispositivo_organizacionDetachedCriteria createDispositivo_organizacionCriteria() {
        return new Dispositivo_organizacionDetachedCriteria(createCriteria("ORM_Dispositivo_organizacion"));
    }

    public AsigdisporganizacionDetachedCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionDetachedCriteria(createCriteria("asigdisporganizacion"));
    }

    public TerapeutaDetachedCriteria createTerapeutaCriteria() {
        return new TerapeutaDetachedCriteria(createCriteria("ORM_Terapeuta"));
    }

    public Organizacion uniqueOrganizacion(PersistentSession session) {
        return (Organizacion) super.createExecutableCriteria(session).uniqueResult();
    }

    public Organizacion[] listOrganizacion(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Organizacion[]) list.toArray(new Organizacion[list.size()]);
    }
}
