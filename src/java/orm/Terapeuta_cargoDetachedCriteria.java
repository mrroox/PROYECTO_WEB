/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Terapeuta_cargoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final IntegerExpression cargoId;
    public final AssociationExpression cargo;

    public Terapeuta_cargoDetachedCriteria() {
        super(orm.Terapeuta_cargo.class, orm.Terapeuta_cargoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        cargoId = new IntegerExpression("cargo.id", this.getDetachedCriteria());
        cargo = new AssociationExpression("cargo", this.getDetachedCriteria());
    }

    public Terapeuta_cargoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Terapeuta_cargoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        cargoId = new IntegerExpression("cargo.id", this.getDetachedCriteria());
        cargo = new AssociationExpression("cargo", this.getDetachedCriteria());
    }

    public TerapeutaDetachedCriteria createTerapeutaCriteria() {
        return new TerapeutaDetachedCriteria(createCriteria("terapeuta"));
    }

    public CargoDetachedCriteria createCargoCriteria() {
        return new CargoDetachedCriteria(createCriteria("cargo"));
    }

    public Terapeuta_cargo uniqueTerapeuta_cargo(PersistentSession session) {
        return (Terapeuta_cargo) super.createExecutableCriteria(session).uniqueResult();
    }

    public Terapeuta_cargo[] listTerapeuta_cargo(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Terapeuta_cargo[]) list.toArray(new Terapeuta_cargo[list.size()]);
    }
}
