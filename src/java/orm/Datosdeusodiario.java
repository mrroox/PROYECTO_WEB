/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Datosdeusodiario {

    public Datosdeusodiario() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_DATOSDEUSODIARIO_DATOSDISPOCITIVO_DATOSDEUSODIARIO) {
            return ORM_datosdispocitivo_datosdeusodiario;
        }

        return null;
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

    };

    private int id;

    private String fecha;

    private Double tiempototaluso;

    private String nivelmaximo;

    private Integer totalpatronesrealizados;

    private java.util.Set ORM_datosdispocitivo_datosdeusodiario = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFecha(String value) {
        this.fecha = value;
    }

    public String getFecha() {
        return fecha;
    }

    public void setTiempototaluso(double value) {
        setTiempototaluso(new Double(value));
    }

    public void setTiempototaluso(Double value) {
        this.tiempototaluso = value;
    }

    public Double getTiempototaluso() {
        return tiempototaluso;
    }

    public void setNivelmaximo(String value) {
        this.nivelmaximo = value;
    }

    public String getNivelmaximo() {
        return nivelmaximo;
    }

    public void setTotalpatronesrealizados(int value) {
        setTotalpatronesrealizados(new Integer(value));
    }

    public void setTotalpatronesrealizados(Integer value) {
        this.totalpatronesrealizados = value;
    }

    public Integer getTotalpatronesrealizados() {
        return totalpatronesrealizados;
    }

    private void setORM_Datosdispocitivo_datosdeusodiario(java.util.Set value) {
        this.ORM_datosdispocitivo_datosdeusodiario = value;
    }

    private java.util.Set getORM_Datosdispocitivo_datosdeusodiario() {
        return ORM_datosdispocitivo_datosdeusodiario;
    }

    public final orm.Datosdispocitivo_datosdeusodiarioSetCollection datosdispocitivo_datosdeusodiario = new orm.Datosdispocitivo_datosdeusodiarioSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DATOSDEUSODIARIO_DATOSDISPOCITIVO_DATOSDEUSODIARIO, orm.ORMConstants.KEY_DATOSDISPOCITIVO_DATOSDEUSODIARIO_DATOSDEUSODIARIO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
