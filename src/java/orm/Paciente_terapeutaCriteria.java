/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Paciente_terapeutaCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_ingreso;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;

    public Paciente_terapeutaCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha_ingreso = new StringExpression("fecha_ingreso", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
        terapeutaId = new IntegerExpression("terapeuta.id", this);
        terapeuta = new AssociationExpression("terapeuta", this);
    }

    public Paciente_terapeutaCriteria(PersistentSession session) {
        this(session.createCriteria(Paciente_terapeuta.class));
    }

    public Paciente_terapeutaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public TerapeutaCriteria createTerapeutaCriteria() {
        return new TerapeutaCriteria(createCriteria("terapeuta"));
    }

    public Paciente_terapeuta uniquePaciente_terapeuta() {
        return (Paciente_terapeuta) super.uniqueResult();
    }

    public Paciente_terapeuta[] listPaciente_terapeuta() {
        java.util.List list = super.list();
        return (Paciente_terapeuta[]) list.toArray(new Paciente_terapeuta[list.size()]);
    }
}
