/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Detalleasignacion {

    public Detalleasignacion() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_DETALLEASIGNACION_DETALLEASIGNACION_ASIGDISPCLIENTE) {
            return ORM_detalleasignacion_asigdispcliente;
        }

        return null;
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_DETALLEASIGNACION_DISPOSITIVO) {
            this.dispositivo = (orm.Dispositivo) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private String observaciones;

    private String fecha_asignacion;

    private String fecha_devolucion;

    private orm.Dispositivo dispositivo;

    private java.util.Set ORM_detalleasignacion_asigdispcliente = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setObservaciones(String value) {
        this.observaciones = value;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setFecha_asignacion(String value) {
        this.fecha_asignacion = value;
    }

    public String getFecha_asignacion() {
        return fecha_asignacion;
    }

    public void setFecha_devolucion(String value) {
        this.fecha_devolucion = value;
    }

    public String getFecha_devolucion() {
        return fecha_devolucion;
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (dispositivo != null) {
            dispositivo.detalleasignacion.remove(this);
        }
        if (value != null) {
            value.detalleasignacion.add(this);
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Dispositivo(orm.Dispositivo value) {
        this.dispositivo = value;
    }

    private orm.Dispositivo getORM_Dispositivo() {
        return dispositivo;
    }

    private void setORM_Detalleasignacion_asigdispcliente(java.util.Set value) {
        this.ORM_detalleasignacion_asigdispcliente = value;
    }

    private java.util.Set getORM_Detalleasignacion_asigdispcliente() {
        return ORM_detalleasignacion_asigdispcliente;
    }

    public final orm.Detalleasignacion_asigdispclienteSetCollection detalleasignacion_asigdispcliente = new orm.Detalleasignacion_asigdispclienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DETALLEASIGNACION_DETALLEASIGNACION_ASIGDISPCLIENTE, orm.ORMConstants.KEY_DETALLEASIGNACION_ASIGDISPCLIENTE_DETALLEASIGNACION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
