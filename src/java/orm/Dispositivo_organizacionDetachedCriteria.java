/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Dispositivo_organizacionDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;

    public Dispositivo_organizacionDetachedCriteria() {
        super(orm.Dispositivo_organizacion.class, orm.Dispositivo_organizacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
    }

    public Dispositivo_organizacionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Dispositivo_organizacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public OrganizacionDetachedCriteria createOrganizacionCriteria() {
        return new OrganizacionDetachedCriteria(createCriteria("organizacion"));
    }

    public Dispositivo_organizacion uniqueDispositivo_organizacion(PersistentSession session) {
        return (Dispositivo_organizacion) super.createExecutableCriteria(session).uniqueResult();
    }

    public Dispositivo_organizacion[] listDispositivo_organizacion(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Dispositivo_organizacion[]) list.toArray(new Dispositivo_organizacion[list.size()]);
    }
}
