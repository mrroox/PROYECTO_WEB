/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class OrganizacionDAO {

    public static Organizacion loadOrganizacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadOrganizacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion getOrganizacionByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getOrganizacionByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion loadOrganizacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadOrganizacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion getOrganizacionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getOrganizacionByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion loadOrganizacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Organizacion) session.load(orm.Organizacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion getOrganizacionByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Organizacion) session.get(orm.Organizacion.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion loadOrganizacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Organizacion) session.load(orm.Organizacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion getOrganizacionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Organizacion) session.get(orm.Organizacion.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryOrganizacion(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryOrganizacion(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryOrganizacion(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryOrganizacion(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion[] listOrganizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listOrganizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion[] listOrganizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listOrganizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryOrganizacion(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Organizacion as Organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryOrganizacion(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Organizacion as Organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Organizacion", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion[] listOrganizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryOrganizacion(session, condition, orderBy);
            return (Organizacion[]) list.toArray(new Organizacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion[] listOrganizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryOrganizacion(session, condition, orderBy, lockMode);
            return (Organizacion[]) list.toArray(new Organizacion[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion loadOrganizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadOrganizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion loadOrganizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadOrganizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion loadOrganizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Organizacion[] organizacions = listOrganizacionByQuery(session, condition, orderBy);
        if (organizacions != null && organizacions.length > 0) {
            return organizacions[0];
        } else {
            return null;
        }
    }

    public static Organizacion loadOrganizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Organizacion[] organizacions = listOrganizacionByQuery(session, condition, orderBy, lockMode);
        if (organizacions != null && organizacions.length > 0) {
            return organizacions[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateOrganizacionByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateOrganizacionByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateOrganizacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateOrganizacionByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateOrganizacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Organizacion as Organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateOrganizacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Organizacion as Organizacion");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Organizacion", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion createOrganizacion() {
        return new orm.Organizacion();
    }

    public static boolean save(orm.Organizacion organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Organizacion organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Organizacion organizacion) throws PersistentException {
        try {
            if (organizacion.getTelefono() != null) {
                organizacion.getTelefono().setOrganizacion(null);
            }

            if (organizacion.getDireccion() != null) {
                organizacion.getDireccion().setOrganizacion(null);
            }

            orm.Dispositivo_organizacion[] lDispositivo_organizacions = organizacion.dispositivo_organizacion.toArray();
            for (int i = 0; i < lDispositivo_organizacions.length; i++) {
                lDispositivo_organizacions[i].setOrganizacion(null);
            }
            if (organizacion.getAsigdisporganizacion() != null) {
                organizacion.getAsigdisporganizacion().setOrganizacion(null);
            }

            orm.Terapeuta[] lTerapeutas = organizacion.terapeuta.toArray();
            for (int i = 0; i < lTerapeutas.length; i++) {
                lTerapeutas[i].setOrganizacion(null);
            }
            return delete(organizacion);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Organizacion organizacion, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (organizacion.getTelefono() != null) {
                organizacion.getTelefono().setOrganizacion(null);
            }

            if (organizacion.getDireccion() != null) {
                organizacion.getDireccion().setOrganizacion(null);
            }

            orm.Dispositivo_organizacion[] lDispositivo_organizacions = organizacion.dispositivo_organizacion.toArray();
            for (int i = 0; i < lDispositivo_organizacions.length; i++) {
                lDispositivo_organizacions[i].setOrganizacion(null);
            }
            if (organizacion.getAsigdisporganizacion() != null) {
                organizacion.getAsigdisporganizacion().setOrganizacion(null);
            }

            orm.Terapeuta[] lTerapeutas = organizacion.terapeuta.toArray();
            for (int i = 0; i < lTerapeutas.length; i++) {
                lTerapeutas[i].setOrganizacion(null);
            }
            try {
                session.delete(organizacion);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Organizacion organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Organizacion organizacion) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(organizacion);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Organizacion loadOrganizacionByCriteria(OrganizacionCriteria organizacionCriteria) {
        Organizacion[] organizacions = listOrganizacionByCriteria(organizacionCriteria);
        if (organizacions == null || organizacions.length == 0) {
            return null;
        }
        return organizacions[0];
    }

    public static Organizacion[] listOrganizacionByCriteria(OrganizacionCriteria organizacionCriteria) {
        return organizacionCriteria.listOrganizacion();
    }
}
