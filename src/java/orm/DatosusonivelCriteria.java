/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosusonivelCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression n1;
    public final IntegerExpression n2;
    public final IntegerExpression n3;
    public final IntegerExpression n4;
    public final IntegerExpression n5;
    public final IntegerExpression n6;
    public final IntegerExpression n7;
    public final IntegerExpression n8;
    public final IntegerExpression n9;
    public final IntegerExpression n10;
    public final IntegerExpression n11;
    public final IntegerExpression n12;
    public final IntegerExpression n13;
    public final IntegerExpression n14;
    public final IntegerExpression n15;
    public final IntegerExpression datosdispositivoId;
    public final AssociationExpression datosdispositivo;

    public DatosusonivelCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        n1 = new IntegerExpression("n1", this);
        n2 = new IntegerExpression("n2", this);
        n3 = new IntegerExpression("n3", this);
        n4 = new IntegerExpression("n4", this);
        n5 = new IntegerExpression("n5", this);
        n6 = new IntegerExpression("n6", this);
        n7 = new IntegerExpression("n7", this);
        n8 = new IntegerExpression("n8", this);
        n9 = new IntegerExpression("n9", this);
        n10 = new IntegerExpression("n10", this);
        n11 = new IntegerExpression("n11", this);
        n12 = new IntegerExpression("n12", this);
        n13 = new IntegerExpression("n13", this);
        n14 = new IntegerExpression("n14", this);
        n15 = new IntegerExpression("n15", this);
        datosdispositivoId = new IntegerExpression("datosdispositivo.id", this);
        datosdispositivo = new AssociationExpression("datosdispositivo", this);
    }

    public DatosusonivelCriteria(PersistentSession session) {
        this(session.createCriteria(Datosusonivel.class));
    }

    public DatosusonivelCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DatosdispositivoCriteria createDatosdispositivoCriteria() {
        return new DatosdispositivoCriteria(createCriteria("datosdispositivo"));
    }

    public Datosusonivel uniqueDatosusonivel() {
        return (Datosusonivel) super.uniqueResult();
    }

    public Datosusonivel[] listDatosusonivel() {
        java.util.List list = super.list();
        return (Datosusonivel[]) list.toArray(new Datosusonivel[list.size()]);
    }
}
