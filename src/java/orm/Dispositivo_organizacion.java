/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Dispositivo_organizacion {

    public Dispositivo_organizacion() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_DISPOSITIVO_ORGANIZACION_DISPOSITIVO) {
            this.dispositivo = (orm.Dispositivo) owner;
        } else if (key == orm.ORMConstants.KEY_DISPOSITIVO_ORGANIZACION_ORGANIZACION) {
            this.organizacion = (orm.Organizacion) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Dispositivo dispositivo;

    private orm.Organizacion organizacion;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setDispositivo(orm.Dispositivo value) {
        if (dispositivo != null) {
            dispositivo.dispositivo_organizacion.remove(this);
        }
        if (value != null) {
            value.dispositivo_organizacion.add(this);
        }
    }

    public orm.Dispositivo getDispositivo() {
        return dispositivo;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Dispositivo(orm.Dispositivo value) {
        this.dispositivo = value;
    }

    private orm.Dispositivo getORM_Dispositivo() {
        return dispositivo;
    }

    public void setOrganizacion(orm.Organizacion value) {
        if (organizacion != null) {
            organizacion.dispositivo_organizacion.remove(this);
        }
        if (value != null) {
            value.dispositivo_organizacion.add(this);
        }
    }

    public orm.Organizacion getOrganizacion() {
        return organizacion;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Organizacion(orm.Organizacion value) {
        this.organizacion = value;
    }

    private orm.Organizacion getORM_Organizacion() {
        return organizacion;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
