/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Apoderado {

    public Apoderado() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_APODERADO_APODERADO_PACIENTE) {
            return ORM_apoderado_paciente;
        }

        return null;
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_APODERADO_PERSONA) {
            this.persona = (orm.Persona) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Persona persona;

    private int personadireccionid;

    private java.util.Set ORM_apoderado_paciente = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setPersonadireccionid(int value) {
        this.personadireccionid = value;
    }

    public int getPersonadireccionid() {
        return personadireccionid;
    }

    public void setPersona(orm.Persona value) {
        if (persona != null) {
            persona.apoderado.remove(this);
        }
        if (value != null) {
            value.apoderado.add(this);
        }
    }

    public orm.Persona getPersona() {
        return persona;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Persona(orm.Persona value) {
        this.persona = value;
    }

    private orm.Persona getORM_Persona() {
        return persona;
    }

    private void setORM_Apoderado_paciente(java.util.Set value) {
        this.ORM_apoderado_paciente = value;
    }

    private java.util.Set getORM_Apoderado_paciente() {
        return ORM_apoderado_paciente;
    }

    public final orm.Apoderado_pacienteSetCollection apoderado_paciente = new orm.Apoderado_pacienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_APODERADO_APODERADO_PACIENTE, orm.ORMConstants.KEY_APODERADO_PACIENTE_APODERADO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
