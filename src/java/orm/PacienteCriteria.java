/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PacienteCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression expedientemedicoId;
    public final AssociationExpression expedientemedico;
    public final IntegerExpression personaId;
    public final AssociationExpression persona;
    public final IntegerExpression personadireccionid;
    public final CollectionExpression clienteterapeuta_paciente;
    public final IntegerExpression asigdispclienteId;
    public final AssociationExpression asigdispcliente;
    public final IntegerExpression evaluacionId;
    public final AssociationExpression evaluacion;
    public final CollectionExpression apoderado_paciente;
    public final CollectionExpression paciente_terapeuta;
    public final IntegerExpression evaluacionterapeutaId;
    public final AssociationExpression evaluacionterapeuta;
    public final IntegerExpression asigdispterapeutaId;
    public final AssociationExpression asigdispterapeuta;

    public PacienteCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        expedientemedicoId = new IntegerExpression("expedientemedico.id", this);
        expedientemedico = new AssociationExpression("expedientemedico", this);
        personaId = new IntegerExpression("persona.id", this);
        persona = new AssociationExpression("persona", this);
        personadireccionid = new IntegerExpression("personadireccionid", this);
        clienteterapeuta_paciente = new CollectionExpression("ORM_Clienteterapeuta_paciente", this);
        asigdispclienteId = new IntegerExpression("asigdispcliente.id", this);
        asigdispcliente = new AssociationExpression("asigdispcliente", this);
        evaluacionId = new IntegerExpression("evaluacion.id", this);
        evaluacion = new AssociationExpression("evaluacion", this);
        apoderado_paciente = new CollectionExpression("ORM_Apoderado_paciente", this);
        paciente_terapeuta = new CollectionExpression("ORM_Paciente_terapeuta", this);
        evaluacionterapeutaId = new IntegerExpression("evaluacionterapeuta.id", this);
        evaluacionterapeuta = new AssociationExpression("evaluacionterapeuta", this);
        asigdispterapeutaId = new IntegerExpression("asigdispterapeuta.id", this);
        asigdispterapeuta = new AssociationExpression("asigdispterapeuta", this);
    }

    public PacienteCriteria(PersistentSession session) {
        this(session.createCriteria(Paciente.class));
    }

    public PacienteCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public ExpedientemedicoCriteria createExpedientemedicoCriteria() {
        return new ExpedientemedicoCriteria(createCriteria("expedientemedico"));
    }

    public PersonaCriteria createPersonaCriteria() {
        return new PersonaCriteria(createCriteria("persona"));
    }

    public Clienteterapeuta_pacienteCriteria createClienteterapeuta_pacienteCriteria() {
        return new Clienteterapeuta_pacienteCriteria(createCriteria("ORM_Clienteterapeuta_paciente"));
    }

    public AsigdispclienteCriteria createAsigdispclienteCriteria() {
        return new AsigdispclienteCriteria(createCriteria("asigdispcliente"));
    }

    public EvaluacionCriteria createEvaluacionCriteria() {
        return new EvaluacionCriteria(createCriteria("evaluacion"));
    }

    public Apoderado_pacienteCriteria createApoderado_pacienteCriteria() {
        return new Apoderado_pacienteCriteria(createCriteria("ORM_Apoderado_paciente"));
    }

    public Paciente_terapeutaCriteria createPaciente_terapeutaCriteria() {
        return new Paciente_terapeutaCriteria(createCriteria("ORM_Paciente_terapeuta"));
    }

    public EvaluacionterapeutaCriteria createEvaluacionterapeutaCriteria() {
        return new EvaluacionterapeutaCriteria(createCriteria("evaluacionterapeuta"));
    }

    public AsigdispterapeutaCriteria createAsigdispterapeutaCriteria() {
        return new AsigdispterapeutaCriteria(createCriteria("asigdispterapeuta"));
    }

    public Paciente uniquePaciente() {
        return (Paciente) super.uniqueResult();
    }

    public Paciente[] listPaciente() {
        java.util.List list = super.list();
        return (Paciente[]) list.toArray(new Paciente[list.size()]);
    }
}
