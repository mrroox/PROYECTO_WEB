/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Terapeuta {

    public Terapeuta() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_TERAPEUTA_PACIENTE_TERAPEUTA) {
            return ORM_paciente_terapeuta;
        } else if (key == orm.ORMConstants.KEY_TERAPEUTA_TERAPEUTA_CARGO) {
            return ORM_terapeuta_cargo;
        }

        return null;
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_TERAPEUTA_PERSONA) {
            this.persona = (orm.Persona) owner;
        } else if (key == orm.ORMConstants.KEY_TERAPEUTA_ORGANIZACION) {
            this.organizacion = (orm.Organizacion) owner;
        } else if (key == orm.ORMConstants.KEY_TERAPEUTA_ASIGDISPORGANIZACION) {
            this.asigdisporganizacion = (orm.Asigdisporganizacion) owner;
        } else if (key == orm.ORMConstants.KEY_TERAPEUTA_EVALUACIONTERAPEUTA) {
            this.evaluacionterapeuta = (orm.Evaluacionterapeuta) owner;
        } else if (key == orm.ORMConstants.KEY_TERAPEUTA_ASIGDISPTERAPEUTA) {
            this.asigdispterapeuta = (orm.Asigdispterapeuta) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private orm.Persona persona;

    private orm.Organizacion organizacion;

    private int id;

    private String password;

    private int personadireccionid;

    private java.util.Set ORM_paciente_terapeuta = new java.util.HashSet();

    private java.util.Set ORM_terapeuta_cargo = new java.util.HashSet();

    private orm.Asigdisporganizacion asigdisporganizacion;

    private orm.Evaluacionterapeuta evaluacionterapeuta;

    private orm.Asigdispterapeuta asigdispterapeuta;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return password;
    }

    public void setPersonadireccionid(int value) {
        this.personadireccionid = value;
    }

    public int getPersonadireccionid() {
        return personadireccionid;
    }

    public void setPersona(orm.Persona value) {
        if (this.persona != value) {
            orm.Persona lpersona = this.persona;
            this.persona = value;
            if (value != null) {
                persona.setTerapeuta(this);
            }
            if (lpersona != null && lpersona.getTerapeuta() == this) {
                lpersona.setTerapeuta(null);
            }
        }
    }

    public orm.Persona getPersona() {
        return persona;
    }

    public void setOrganizacion(orm.Organizacion value) {
        if (organizacion != null) {
            organizacion.terapeuta.remove(this);
        }
        if (value != null) {
            value.terapeuta.add(this);
        }
    }

    public orm.Organizacion getOrganizacion() {
        return organizacion;
    }

    /**
     * This method is for internal use only.
     */
    public void setORM_Organizacion(orm.Organizacion value) {
        this.organizacion = value;
    }

    private orm.Organizacion getORM_Organizacion() {
        return organizacion;
    }

    private void setORM_Paciente_terapeuta(java.util.Set value) {
        this.ORM_paciente_terapeuta = value;
    }

    private java.util.Set getORM_Paciente_terapeuta() {
        return ORM_paciente_terapeuta;
    }

    public final orm.Paciente_terapeutaSetCollection paciente_terapeuta = new orm.Paciente_terapeutaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TERAPEUTA_PACIENTE_TERAPEUTA, orm.ORMConstants.KEY_PACIENTE_TERAPEUTA_TERAPEUTA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    private void setORM_Terapeuta_cargo(java.util.Set value) {
        this.ORM_terapeuta_cargo = value;
    }

    private java.util.Set getORM_Terapeuta_cargo() {
        return ORM_terapeuta_cargo;
    }

    public final orm.Terapeuta_cargoSetCollection terapeuta_cargo = new orm.Terapeuta_cargoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TERAPEUTA_TERAPEUTA_CARGO, orm.ORMConstants.KEY_TERAPEUTA_CARGO_TERAPEUTA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public void setAsigdisporganizacion(orm.Asigdisporganizacion value) {
        if (this.asigdisporganizacion != value) {
            orm.Asigdisporganizacion lasigdisporganizacion = this.asigdisporganizacion;
            this.asigdisporganizacion = value;
            if (value != null) {
                asigdisporganizacion.setTerapeuta(this);
            }
            if (lasigdisporganizacion != null && lasigdisporganizacion.getTerapeuta() == this) {
                lasigdisporganizacion.setTerapeuta(null);
            }
        }
    }

    public orm.Asigdisporganizacion getAsigdisporganizacion() {
        return asigdisporganizacion;
    }

    public void setEvaluacionterapeuta(orm.Evaluacionterapeuta value) {
        if (this.evaluacionterapeuta != value) {
            orm.Evaluacionterapeuta levaluacionterapeuta = this.evaluacionterapeuta;
            this.evaluacionterapeuta = value;
            if (value != null) {
                evaluacionterapeuta.setTerapeuta(this);
            }
            if (levaluacionterapeuta != null && levaluacionterapeuta.getTerapeuta() == this) {
                levaluacionterapeuta.setTerapeuta(null);
            }
        }
    }

    public orm.Evaluacionterapeuta getEvaluacionterapeuta() {
        return evaluacionterapeuta;
    }

    public void setAsigdispterapeuta(orm.Asigdispterapeuta value) {
        if (this.asigdispterapeuta != value) {
            orm.Asigdispterapeuta lasigdispterapeuta = this.asigdispterapeuta;
            this.asigdispterapeuta = value;
            if (value != null) {
                asigdispterapeuta.setTerapeuta(this);
            }
            if (lasigdispterapeuta != null && lasigdispterapeuta.getTerapeuta() == this) {
                lasigdispterapeuta.setTerapeuta(null);
            }
        }
    }

    public orm.Asigdispterapeuta getAsigdispterapeuta() {
        return asigdispterapeuta;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
