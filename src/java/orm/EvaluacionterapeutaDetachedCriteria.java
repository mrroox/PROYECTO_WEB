/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EvaluacionterapeutaDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_evaluacion;
    public final StringExpression observacion;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public EvaluacionterapeutaDetachedCriteria() {
        super(orm.Evaluacionterapeuta.class, orm.EvaluacionterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_evaluacion = new StringExpression("fecha_evaluacion", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public EvaluacionterapeutaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.EvaluacionterapeutaCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_evaluacion = new StringExpression("fecha_evaluacion", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public TerapeutaDetachedCriteria createTerapeutaCriteria() {
        return new TerapeutaDetachedCriteria(createCriteria("terapeuta"));
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public Evaluacionterapeuta uniqueEvaluacionterapeuta(PersistentSession session) {
        return (Evaluacionterapeuta) super.createExecutableCriteria(session).uniqueResult();
    }

    public Evaluacionterapeuta[] listEvaluacionterapeuta(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Evaluacionterapeuta[]) list.toArray(new Evaluacionterapeuta[list.size()]);
    }
}
