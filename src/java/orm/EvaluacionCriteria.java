/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EvaluacionCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_evaluacion;
    public final StringExpression observacion;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public EvaluacionCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha_evaluacion = new StringExpression("fecha_evaluacion", this);
        observacion = new StringExpression("observacion", this);
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this);
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
    }

    public EvaluacionCriteria(PersistentSession session) {
        this(session.createCriteria(Evaluacion.class));
    }

    public EvaluacionCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public ClienteterapeutaCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaCriteria(createCriteria("clienteterapeuta"));
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public Evaluacion uniqueEvaluacion() {
        return (Evaluacion) super.uniqueResult();
    }

    public Evaluacion[] listEvaluacion() {
        java.util.List list = super.list();
        return (Evaluacion[]) list.toArray(new Evaluacion[list.size()]);
    }
}
