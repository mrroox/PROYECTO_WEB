/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PersonaCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression run;
    public final StringExpression nombre;
    public final StringExpression apellido_paterno;
    public final StringExpression apellido_materno;
    public final StringExpression fecha_nacimiento;
    public final StringExpression email;
    public final StringExpression sexo;
    public final IntegerExpression direccionId;
    public final AssociationExpression direccion;
    public final IntegerExpression telefonoId;
    public final AssociationExpression telefono;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;
    public final IntegerExpression clienteterapeutaId;
    public final AssociationExpression clienteterapeuta;
    public final CollectionExpression apoderado;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;

    public PersonaCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        run = new StringExpression("run", this);
        nombre = new StringExpression("nombre", this);
        apellido_paterno = new StringExpression("apellido_paterno", this);
        apellido_materno = new StringExpression("apellido_materno", this);
        fecha_nacimiento = new StringExpression("fecha_nacimiento", this);
        email = new StringExpression("email", this);
        sexo = new StringExpression("sexo", this);
        direccionId = new IntegerExpression("direccion.id", this);
        direccion = new AssociationExpression("direccion", this);
        telefonoId = new IntegerExpression("telefono.id", this);
        telefono = new AssociationExpression("telefono", this);
        pacienteId = new IntegerExpression("paciente.id", this);
        paciente = new AssociationExpression("paciente", this);
        clienteterapeutaId = new IntegerExpression("clienteterapeuta.id", this);
        clienteterapeuta = new AssociationExpression("clienteterapeuta", this);
        apoderado = new CollectionExpression("ORM_Apoderado", this);
        terapeutaId = new IntegerExpression("terapeuta.id", this);
        terapeuta = new AssociationExpression("terapeuta", this);
    }

    public PersonaCriteria(PersistentSession session) {
        this(session.createCriteria(Persona.class));
    }

    public PersonaCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DireccionCriteria createDireccionCriteria() {
        return new DireccionCriteria(createCriteria("direccion"));
    }

    public TelefonoCriteria createTelefonoCriteria() {
        return new TelefonoCriteria(createCriteria("telefono"));
    }

    public PacienteCriteria createPacienteCriteria() {
        return new PacienteCriteria(createCriteria("paciente"));
    }

    public ClienteterapeutaCriteria createClienteterapeutaCriteria() {
        return new ClienteterapeutaCriteria(createCriteria("clienteterapeuta"));
    }

    public ApoderadoCriteria createApoderadoCriteria() {
        return new ApoderadoCriteria(createCriteria("ORM_Apoderado"));
    }

    public TerapeutaCriteria createTerapeutaCriteria() {
        return new TerapeutaCriteria(createCriteria("terapeuta"));
    }

    public Persona uniquePersona() {
        return (Persona) super.uniqueResult();
    }

    public Persona[] listPersona() {
        java.util.List list = super.list();
        return (Persona[]) list.toArray(new Persona[list.size()]);
    }
}
