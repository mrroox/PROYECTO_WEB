/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsigdisporganizacionDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha_asignacion;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final IntegerExpression terapeutaId;
    public final AssociationExpression terapeuta;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final IntegerExpression cantidad;
    public final StringExpression observacion;
    public final CollectionExpression asigdisporganizacion_dispositivo;

    public AsigdisporganizacionDetachedCriteria() {
        super(orm.Asigdisporganizacion.class, orm.AsigdisporganizacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        cantidad = new IntegerExpression("cantidad", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        asigdisporganizacion_dispositivo = new CollectionExpression("ORM_Asigdisporganizacion_dispositivo", this.getDetachedCriteria());
    }

    public AsigdisporganizacionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.AsigdisporganizacionCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fecha_asignacion = new StringExpression("fecha_asignacion", this.getDetachedCriteria());
        organizacionId = new IntegerExpression("organizacion.id", this.getDetachedCriteria());
        organizacion = new AssociationExpression("organizacion", this.getDetachedCriteria());
        terapeutaId = new IntegerExpression("terapeuta.id", this.getDetachedCriteria());
        terapeuta = new AssociationExpression("terapeuta", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        cantidad = new IntegerExpression("cantidad", this.getDetachedCriteria());
        observacion = new StringExpression("observacion", this.getDetachedCriteria());
        asigdisporganizacion_dispositivo = new CollectionExpression("ORM_Asigdisporganizacion_dispositivo", this.getDetachedCriteria());
    }

    public OrganizacionDetachedCriteria createOrganizacionCriteria() {
        return new OrganizacionDetachedCriteria(createCriteria("organizacion"));
    }

    public TerapeutaDetachedCriteria createTerapeutaCriteria() {
        return new TerapeutaDetachedCriteria(createCriteria("terapeuta"));
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public Asigdisporganizacion_dispositivoDetachedCriteria createAsigdisporganizacion_dispositivoCriteria() {
        return new Asigdisporganizacion_dispositivoDetachedCriteria(createCriteria("ORM_Asigdisporganizacion_dispositivo"));
    }

    public Asigdisporganizacion uniqueAsigdisporganizacion(PersistentSession session) {
        return (Asigdisporganizacion) super.createExecutableCriteria(session).uniqueResult();
    }

    public Asigdisporganizacion[] listAsigdisporganizacion(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Asigdisporganizacion[]) list.toArray(new Asigdisporganizacion[list.size()]);
    }
}
