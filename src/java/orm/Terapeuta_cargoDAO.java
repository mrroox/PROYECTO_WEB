/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Terapeuta_cargoDAO {

    public static Terapeuta_cargo loadTerapeuta_cargoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeuta_cargoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo getTerapeuta_cargoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getTerapeuta_cargoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeuta_cargoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo getTerapeuta_cargoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getTerapeuta_cargoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Terapeuta_cargo) session.load(orm.Terapeuta_cargo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo getTerapeuta_cargoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Terapeuta_cargo) session.get(orm.Terapeuta_cargo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Terapeuta_cargo) session.load(orm.Terapeuta_cargo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo getTerapeuta_cargoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Terapeuta_cargo) session.get(orm.Terapeuta_cargo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta_cargo(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryTerapeuta_cargo(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta_cargo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryTerapeuta_cargo(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo[] listTerapeuta_cargoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listTerapeuta_cargoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo[] listTerapeuta_cargoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listTerapeuta_cargoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta_cargo(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta_cargo as Terapeuta_cargo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryTerapeuta_cargo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta_cargo as Terapeuta_cargo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Terapeuta_cargo", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo[] listTerapeuta_cargoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryTerapeuta_cargo(session, condition, orderBy);
            return (Terapeuta_cargo[]) list.toArray(new Terapeuta_cargo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo[] listTerapeuta_cargoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryTerapeuta_cargo(session, condition, orderBy, lockMode);
            return (Terapeuta_cargo[]) list.toArray(new Terapeuta_cargo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeuta_cargoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadTerapeuta_cargoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Terapeuta_cargo[] terapeuta_cargos = listTerapeuta_cargoByQuery(session, condition, orderBy);
        if (terapeuta_cargos != null && terapeuta_cargos.length > 0) {
            return terapeuta_cargos[0];
        } else {
            return null;
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Terapeuta_cargo[] terapeuta_cargos = listTerapeuta_cargoByQuery(session, condition, orderBy, lockMode);
        if (terapeuta_cargos != null && terapeuta_cargos.length > 0) {
            return terapeuta_cargos[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateTerapeuta_cargoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateTerapeuta_cargoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateTerapeuta_cargoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateTerapeuta_cargoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateTerapeuta_cargoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta_cargo as Terapeuta_cargo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateTerapeuta_cargoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Terapeuta_cargo as Terapeuta_cargo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Terapeuta_cargo", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo createTerapeuta_cargo() {
        return new orm.Terapeuta_cargo();
    }

    public static boolean save(orm.Terapeuta_cargo terapeuta_cargo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(terapeuta_cargo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Terapeuta_cargo terapeuta_cargo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(terapeuta_cargo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Terapeuta_cargo terapeuta_cargo) throws PersistentException {
        try {
            if (terapeuta_cargo.getTerapeuta() != null) {
                terapeuta_cargo.getTerapeuta().terapeuta_cargo.remove(terapeuta_cargo);
            }

            if (terapeuta_cargo.getCargo() != null) {
                terapeuta_cargo.getCargo().terapeuta_cargo.remove(terapeuta_cargo);
            }

            return delete(terapeuta_cargo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Terapeuta_cargo terapeuta_cargo, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (terapeuta_cargo.getTerapeuta() != null) {
                terapeuta_cargo.getTerapeuta().terapeuta_cargo.remove(terapeuta_cargo);
            }

            if (terapeuta_cargo.getCargo() != null) {
                terapeuta_cargo.getCargo().terapeuta_cargo.remove(terapeuta_cargo);
            }

            try {
                session.delete(terapeuta_cargo);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Terapeuta_cargo terapeuta_cargo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(terapeuta_cargo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Terapeuta_cargo terapeuta_cargo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(terapeuta_cargo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Terapeuta_cargo loadTerapeuta_cargoByCriteria(Terapeuta_cargoCriteria terapeuta_cargoCriteria) {
        Terapeuta_cargo[] terapeuta_cargos = listTerapeuta_cargoByCriteria(terapeuta_cargoCriteria);
        if (terapeuta_cargos == null || terapeuta_cargos.length == 0) {
            return null;
        }
        return terapeuta_cargos[0];
    }

    public static Terapeuta_cargo[] listTerapeuta_cargoByCriteria(Terapeuta_cargoCriteria terapeuta_cargoCriteria) {
        return terapeuta_cargoCriteria.listTerapeuta_cargo();
    }
}
