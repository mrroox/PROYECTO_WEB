/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosdispositivoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final StringExpression fechaperiodo;
    public final IntegerExpression datosusogeneralId;
    public final AssociationExpression datosusogeneral;
    public final IntegerExpression datosusonivelId;
    public final AssociationExpression datosusonivel;
    public final IntegerExpression datosnivelcompletadoId;
    public final AssociationExpression datosnivelcompletado;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final CollectionExpression datosdispocitivo_datosdeusodiario;
    public final CollectionExpression datosdispocitivo_juego;

    public DatosdispositivoDetachedCriteria() {
        super(orm.Datosdispositivo.class, orm.DatosdispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fechaperiodo = new StringExpression("fechaperiodo", this.getDetachedCriteria());
        datosusogeneralId = new IntegerExpression("datosusogeneral.id", this.getDetachedCriteria());
        datosusogeneral = new AssociationExpression("datosusogeneral", this.getDetachedCriteria());
        datosusonivelId = new IntegerExpression("datosusonivel.id", this.getDetachedCriteria());
        datosusonivel = new AssociationExpression("datosusonivel", this.getDetachedCriteria());
        datosnivelcompletadoId = new IntegerExpression("datosnivelcompletado.id", this.getDetachedCriteria());
        datosnivelcompletado = new AssociationExpression("datosnivelcompletado", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        datosdispocitivo_datosdeusodiario = new CollectionExpression("ORM_Datosdispocitivo_datosdeusodiario", this.getDetachedCriteria());
        datosdispocitivo_juego = new CollectionExpression("ORM_Datosdispocitivo_juego", this.getDetachedCriteria());
    }

    public DatosdispositivoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.DatosdispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        fechaperiodo = new StringExpression("fechaperiodo", this.getDetachedCriteria());
        datosusogeneralId = new IntegerExpression("datosusogeneral.id", this.getDetachedCriteria());
        datosusogeneral = new AssociationExpression("datosusogeneral", this.getDetachedCriteria());
        datosusonivelId = new IntegerExpression("datosusonivel.id", this.getDetachedCriteria());
        datosusonivel = new AssociationExpression("datosusonivel", this.getDetachedCriteria());
        datosnivelcompletadoId = new IntegerExpression("datosnivelcompletado.id", this.getDetachedCriteria());
        datosnivelcompletado = new AssociationExpression("datosnivelcompletado", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
        datosdispocitivo_datosdeusodiario = new CollectionExpression("ORM_Datosdispocitivo_datosdeusodiario", this.getDetachedCriteria());
        datosdispocitivo_juego = new CollectionExpression("ORM_Datosdispocitivo_juego", this.getDetachedCriteria());
    }

    public DatosusogeneralDetachedCriteria createDatosusogeneralCriteria() {
        return new DatosusogeneralDetachedCriteria(createCriteria("datosusogeneral"));
    }

    public DatosusonivelDetachedCriteria createDatosusonivelCriteria() {
        return new DatosusonivelDetachedCriteria(createCriteria("datosusonivel"));
    }

    public DatosnivelcompletadoDetachedCriteria createDatosnivelcompletadoCriteria() {
        return new DatosnivelcompletadoDetachedCriteria(createCriteria("datosnivelcompletado"));
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public Datosdispocitivo_datosdeusodiarioDetachedCriteria createDatosdispocitivo_datosdeusodiarioCriteria() {
        return new Datosdispocitivo_datosdeusodiarioDetachedCriteria(createCriteria("ORM_Datosdispocitivo_datosdeusodiario"));
    }

    public Datosdispocitivo_juegoDetachedCriteria createDatosdispocitivo_juegoCriteria() {
        return new Datosdispocitivo_juegoDetachedCriteria(createCriteria("ORM_Datosdispocitivo_juego"));
    }

    public Datosdispositivo uniqueDatosdispositivo(PersistentSession session) {
        return (Datosdispositivo) super.createExecutableCriteria(session).uniqueResult();
    }

    public Datosdispositivo[] listDatosdispositivo(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Datosdispositivo[]) list.toArray(new Datosdispositivo[list.size()]);
    }
}
