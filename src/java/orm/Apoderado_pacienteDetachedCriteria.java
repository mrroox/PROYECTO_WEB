/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Apoderado_pacienteDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression apoderadoId;
    public final AssociationExpression apoderado;
    public final IntegerExpression pacienteId;
    public final AssociationExpression paciente;

    public Apoderado_pacienteDetachedCriteria() {
        super(orm.Apoderado_paciente.class, orm.Apoderado_pacienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        apoderadoId = new IntegerExpression("apoderado.id", this.getDetachedCriteria());
        apoderado = new AssociationExpression("apoderado", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public Apoderado_pacienteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Apoderado_pacienteCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        apoderadoId = new IntegerExpression("apoderado.id", this.getDetachedCriteria());
        apoderado = new AssociationExpression("apoderado", this.getDetachedCriteria());
        pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
        paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
    }

    public ApoderadoDetachedCriteria createApoderadoCriteria() {
        return new ApoderadoDetachedCriteria(createCriteria("apoderado"));
    }

    public PacienteDetachedCriteria createPacienteCriteria() {
        return new PacienteDetachedCriteria(createCriteria("paciente"));
    }

    public Apoderado_paciente uniqueApoderado_paciente(PersistentSession session) {
        return (Apoderado_paciente) super.createExecutableCriteria(session).uniqueResult();
    }

    public Apoderado_paciente[] listApoderado_paciente(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Apoderado_paciente[]) list.toArray(new Apoderado_paciente[list.size()]);
    }
}
