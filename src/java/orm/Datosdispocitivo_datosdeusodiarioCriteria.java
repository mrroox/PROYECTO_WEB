/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Datosdispocitivo_datosdeusodiarioCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final IntegerExpression datosdispocitivoId;
    public final AssociationExpression datosdispocitivo;
    public final IntegerExpression datosdeusodiarioId;
    public final AssociationExpression datosdeusodiario;

    public Datosdispocitivo_datosdeusodiarioCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        datosdispocitivoId = new IntegerExpression("datosdispocitivo.id", this);
        datosdispocitivo = new AssociationExpression("datosdispocitivo", this);
        datosdeusodiarioId = new IntegerExpression("datosdeusodiario.id", this);
        datosdeusodiario = new AssociationExpression("datosdeusodiario", this);
    }

    public Datosdispocitivo_datosdeusodiarioCriteria(PersistentSession session) {
        this(session.createCriteria(Datosdispocitivo_datosdeusodiario.class));
    }

    public Datosdispocitivo_datosdeusodiarioCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DatosdispositivoCriteria createDatosdispocitivoCriteria() {
        return new DatosdispositivoCriteria(createCriteria("datosdispocitivo"));
    }

    public DatosdeusodiarioCriteria createDatosdeusodiarioCriteria() {
        return new DatosdeusodiarioCriteria(createCriteria("datosdeusodiario"));
    }

    public Datosdispocitivo_datosdeusodiario uniqueDatosdispocitivo_datosdeusodiario() {
        return (Datosdispocitivo_datosdeusodiario) super.uniqueResult();
    }

    public Datosdispocitivo_datosdeusodiario[] listDatosdispocitivo_datosdeusodiario() {
        java.util.List list = super.list();
        return (Datosdispocitivo_datosdeusodiario[]) list.toArray(new Datosdispocitivo_datosdeusodiario[list.size()]);
    }
}
