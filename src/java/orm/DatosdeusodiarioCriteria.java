/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosdeusodiarioCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fecha;
    public final DoubleExpression tiempototaluso;
    public final StringExpression nivelmaximo;
    public final IntegerExpression totalpatronesrealizados;
    public final CollectionExpression datosdispocitivo_datosdeusodiario;

    public DatosdeusodiarioCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fecha = new StringExpression("fecha", this);
        tiempototaluso = new DoubleExpression("tiempototaluso", this);
        nivelmaximo = new StringExpression("nivelmaximo", this);
        totalpatronesrealizados = new IntegerExpression("totalpatronesrealizados", this);
        datosdispocitivo_datosdeusodiario = new CollectionExpression("ORM_Datosdispocitivo_datosdeusodiario", this);
    }

    public DatosdeusodiarioCriteria(PersistentSession session) {
        this(session.createCriteria(Datosdeusodiario.class));
    }

    public DatosdeusodiarioCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public Datosdispocitivo_datosdeusodiarioCriteria createDatosdispocitivo_datosdeusodiarioCriteria() {
        return new Datosdispocitivo_datosdeusodiarioCriteria(createCriteria("ORM_Datosdispocitivo_datosdeusodiario"));
    }

    public Datosdeusodiario uniqueDatosdeusodiario() {
        return (Datosdeusodiario) super.uniqueResult();
    }

    public Datosdeusodiario[] listDatosdeusodiario() {
        java.util.List list = super.list();
        return (Datosdeusodiario[]) list.toArray(new Datosdeusodiario[list.size()]);
    }
}
