/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DireccionCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression calle;
    public final StringExpression numero;
    public final IntegerExpression paisId;
    public final AssociationExpression pais;
    public final IntegerExpression regionId;
    public final AssociationExpression region;
    public final IntegerExpression organizacionId;
    public final AssociationExpression organizacion;
    public final CollectionExpression persona;

    public DireccionCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        calle = new StringExpression("calle", this);
        numero = new StringExpression("numero", this);
        paisId = new IntegerExpression("pais.id", this);
        pais = new AssociationExpression("pais", this);
        regionId = new IntegerExpression("region.id", this);
        region = new AssociationExpression("region", this);
        organizacionId = new IntegerExpression("organizacion.id", this);
        organizacion = new AssociationExpression("organizacion", this);
        persona = new CollectionExpression("ORM_Persona", this);
    }

    public DireccionCriteria(PersistentSession session) {
        this(session.createCriteria(Direccion.class));
    }

    public DireccionCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public PaisCriteria createPaisCriteria() {
        return new PaisCriteria(createCriteria("pais"));
    }

    public RegionCriteria createRegionCriteria() {
        return new RegionCriteria(createCriteria("region"));
    }

    public OrganizacionCriteria createOrganizacionCriteria() {
        return new OrganizacionCriteria(createCriteria("organizacion"));
    }

    public PersonaCriteria createPersonaCriteria() {
        return new PersonaCriteria(createCriteria("ORM_Persona"));
    }

    public Direccion uniqueDireccion() {
        return (Direccion) super.uniqueResult();
    }

    public Direccion[] listDireccion() {
        java.util.List list = super.list();
        return (Direccion[]) list.toArray(new Direccion[list.size()]);
    }
}
