/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Evaluacionterapeuta {

    public Evaluacionterapeuta() {
    }

    private int id;

    private String fecha_evaluacion;

    private String observacion;

    private orm.Terapeuta terapeuta;

    private orm.Paciente paciente;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setFecha_evaluacion(String value) {
        this.fecha_evaluacion = value;
    }

    public String getFecha_evaluacion() {
        return fecha_evaluacion;
    }

    public void setObservacion(String value) {
        this.observacion = value;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setTerapeuta(orm.Terapeuta value) {
        if (this.terapeuta != value) {
            orm.Terapeuta lterapeuta = this.terapeuta;
            this.terapeuta = value;
            if (value != null) {
                terapeuta.setEvaluacionterapeuta(this);
            }
            if (lterapeuta != null && lterapeuta.getEvaluacionterapeuta() == this) {
                lterapeuta.setEvaluacionterapeuta(null);
            }
        }
    }

    public orm.Terapeuta getTerapeuta() {
        return terapeuta;
    }

    public void setPaciente(orm.Paciente value) {
        if (this.paciente != value) {
            orm.Paciente lpaciente = this.paciente;
            this.paciente = value;
            if (value != null) {
                paciente.setEvaluacionterapeuta(this);
            }
            if (lpaciente != null && lpaciente.getEvaluacionterapeuta() == this) {
                lpaciente.setEvaluacionterapeuta(null);
            }
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
