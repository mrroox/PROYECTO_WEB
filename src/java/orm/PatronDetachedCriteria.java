/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PatronDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression nivel;
    public final IntegerExpression nivelmaximo;
    public final IntegerExpression tiemporespuesta;
    public final CollectionExpression juego_patron;

    public PatronDetachedCriteria() {
        super(orm.Patron.class, orm.PatronCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nivel = new IntegerExpression("nivel", this.getDetachedCriteria());
        nivelmaximo = new IntegerExpression("nivelmaximo", this.getDetachedCriteria());
        tiemporespuesta = new IntegerExpression("tiemporespuesta", this.getDetachedCriteria());
        juego_patron = new CollectionExpression("ORM_Juego_patron", this.getDetachedCriteria());
    }

    public PatronDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.PatronCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        nivel = new IntegerExpression("nivel", this.getDetachedCriteria());
        nivelmaximo = new IntegerExpression("nivelmaximo", this.getDetachedCriteria());
        tiemporespuesta = new IntegerExpression("tiemporespuesta", this.getDetachedCriteria());
        juego_patron = new CollectionExpression("ORM_Juego_patron", this.getDetachedCriteria());
    }

    public Juego_patronDetachedCriteria createJuego_patronCriteria() {
        return new Juego_patronDetachedCriteria(createCriteria("ORM_Juego_patron"));
    }

    public Patron uniquePatron(PersistentSession session) {
        return (Patron) super.createExecutableCriteria(session).uniqueResult();
    }

    public Patron[] listPatron(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Patron[]) list.toArray(new Patron[list.size()]);
    }
}
