/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DispositivoDAO {

    public static Dispositivo loadDispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo getDispositivoByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDispositivoByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo loadDispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo getDispositivoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getDispositivoByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo loadDispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Dispositivo) session.load(orm.Dispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo getDispositivoByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Dispositivo) session.get(orm.Dispositivo.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo loadDispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Dispositivo) session.load(orm.Dispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo getDispositivoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Dispositivo) session.get(orm.Dispositivo.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDispositivo(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryDispositivo(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo[] listDispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo[] listDispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listDispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo as Dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryDispositivo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo as Dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Dispositivo", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo[] listDispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryDispositivo(session, condition, orderBy);
            return (Dispositivo[]) list.toArray(new Dispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo[] listDispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryDispositivo(session, condition, orderBy, lockMode);
            return (Dispositivo[]) list.toArray(new Dispositivo[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo loadDispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo loadDispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadDispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo loadDispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Dispositivo[] dispositivos = listDispositivoByQuery(session, condition, orderBy);
        if (dispositivos != null && dispositivos.length > 0) {
            return dispositivos[0];
        } else {
            return null;
        }
    }

    public static Dispositivo loadDispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Dispositivo[] dispositivos = listDispositivoByQuery(session, condition, orderBy, lockMode);
        if (dispositivos != null && dispositivos.length > 0) {
            return dispositivos[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateDispositivoByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDispositivoByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDispositivoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateDispositivoByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDispositivoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo as Dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateDispositivoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Dispositivo as Dispositivo");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Dispositivo", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo createDispositivo() {
        return new orm.Dispositivo();
    }

    public static boolean save(orm.Dispositivo dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Dispositivo dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Dispositivo dispositivo) throws PersistentException {
        try {
            if (dispositivo.getTipodispositivo() != null) {
                dispositivo.getTipodispositivo().setDispositivo(null);
            }

            if (dispositivo.getDotosdispositivo() != null) {
                dispositivo.getDotosdispositivo().setDispositivo(null);
            }

            orm.Clienteterapeuta_dispositivo[] lClienteterapeuta_dispositivos = dispositivo.clienteterapeuta_dispositivo.toArray();
            for (int i = 0; i < lClienteterapeuta_dispositivos.length; i++) {
                lClienteterapeuta_dispositivos[i].setDispositivo(null);
            }
            orm.Dispositivo_organizacion[] lDispositivo_organizacions = dispositivo.dispositivo_organizacion.toArray();
            for (int i = 0; i < lDispositivo_organizacions.length; i++) {
                lDispositivo_organizacions[i].setDispositivo(null);
            }
            if (dispositivo.getAsigdisporganizacion() != null) {
                dispositivo.getAsigdisporganizacion().setDispositivo(null);
            }

            orm.Detalleasignacion[] lDetalleasignacions = dispositivo.detalleasignacion.toArray();
            for (int i = 0; i < lDetalleasignacions.length; i++) {
                lDetalleasignacions[i].setDispositivo(null);
            }
            if (dispositivo.getDetasigterapeuta() != null) {
                dispositivo.getDetasigterapeuta().setDispositivo(null);
            }

            orm.Asigdisporganizacion_dispositivo[] lAsigdisporganizacion_dispositivos = dispositivo.asigdisporganizacion_dispositivo.toArray();
            for (int i = 0; i < lAsigdisporganizacion_dispositivos.length; i++) {
                lAsigdisporganizacion_dispositivos[i].setDispositivo(null);
            }
            return delete(dispositivo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Dispositivo dispositivo, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (dispositivo.getTipodispositivo() != null) {
                dispositivo.getTipodispositivo().setDispositivo(null);
            }

            if (dispositivo.getDotosdispositivo() != null) {
                dispositivo.getDotosdispositivo().setDispositivo(null);
            }

            orm.Clienteterapeuta_dispositivo[] lClienteterapeuta_dispositivos = dispositivo.clienteterapeuta_dispositivo.toArray();
            for (int i = 0; i < lClienteterapeuta_dispositivos.length; i++) {
                lClienteterapeuta_dispositivos[i].setDispositivo(null);
            }
            orm.Dispositivo_organizacion[] lDispositivo_organizacions = dispositivo.dispositivo_organizacion.toArray();
            for (int i = 0; i < lDispositivo_organizacions.length; i++) {
                lDispositivo_organizacions[i].setDispositivo(null);
            }
            if (dispositivo.getAsigdisporganizacion() != null) {
                dispositivo.getAsigdisporganizacion().setDispositivo(null);
            }

            orm.Detalleasignacion[] lDetalleasignacions = dispositivo.detalleasignacion.toArray();
            for (int i = 0; i < lDetalleasignacions.length; i++) {
                lDetalleasignacions[i].setDispositivo(null);
            }
            if (dispositivo.getDetasigterapeuta() != null) {
                dispositivo.getDetasigterapeuta().setDispositivo(null);
            }

            orm.Asigdisporganizacion_dispositivo[] lAsigdisporganizacion_dispositivos = dispositivo.asigdisporganizacion_dispositivo.toArray();
            for (int i = 0; i < lAsigdisporganizacion_dispositivos.length; i++) {
                lAsigdisporganizacion_dispositivos[i].setDispositivo(null);
            }
            try {
                session.delete(dispositivo);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Dispositivo dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Dispositivo dispositivo) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(dispositivo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Dispositivo loadDispositivoByCriteria(DispositivoCriteria dispositivoCriteria) {
        Dispositivo[] dispositivos = listDispositivoByCriteria(dispositivoCriteria);
        if (dispositivos == null || dispositivos.length == 0) {
            return null;
        }
        return dispositivos[0];
    }

    public static Dispositivo[] listDispositivoByCriteria(DispositivoCriteria dispositivoCriteria) {
        return dispositivoCriteria.listDispositivo();
    }
}
