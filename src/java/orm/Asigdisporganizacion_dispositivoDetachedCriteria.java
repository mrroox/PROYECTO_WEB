/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Asigdisporganizacion_dispositivoDetachedCriteria extends AbstractORMDetachedCriteria {

    public final IntegerExpression id;
    public final IntegerExpression asigdisporganizacionId;
    public final AssociationExpression asigdisporganizacion;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;

    public Asigdisporganizacion_dispositivoDetachedCriteria() {
        super(orm.Asigdisporganizacion_dispositivo.class, orm.Asigdisporganizacion_dispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
    }

    public Asigdisporganizacion_dispositivoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
        super(aDetachedCriteria, orm.Asigdisporganizacion_dispositivoCriteria.class);
        id = new IntegerExpression("id", this.getDetachedCriteria());
        asigdisporganizacionId = new IntegerExpression("asigdisporganizacion.id", this.getDetachedCriteria());
        asigdisporganizacion = new AssociationExpression("asigdisporganizacion", this.getDetachedCriteria());
        dispositivoId = new IntegerExpression("dispositivo.id", this.getDetachedCriteria());
        dispositivo = new AssociationExpression("dispositivo", this.getDetachedCriteria());
    }

    public AsigdisporganizacionDetachedCriteria createAsigdisporganizacionCriteria() {
        return new AsigdisporganizacionDetachedCriteria(createCriteria("asigdisporganizacion"));
    }

    public DispositivoDetachedCriteria createDispositivoCriteria() {
        return new DispositivoDetachedCriteria(createCriteria("dispositivo"));
    }

    public Asigdisporganizacion_dispositivo uniqueAsigdisporganizacion_dispositivo(PersistentSession session) {
        return (Asigdisporganizacion_dispositivo) super.createExecutableCriteria(session).uniqueResult();
    }

    public Asigdisporganizacion_dispositivo[] listAsigdisporganizacion_dispositivo(PersistentSession session) {
        List list = super.createExecutableCriteria(session).list();
        return (Asigdisporganizacion_dispositivo[]) list.toArray(new Asigdisporganizacion_dispositivo[list.size()]);
    }
}
