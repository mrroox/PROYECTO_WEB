/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class ClienteterapeutaDAO {

    public static Clienteterapeuta loadClienteterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta getClienteterapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getClienteterapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta getClienteterapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getClienteterapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Clienteterapeuta) session.load(orm.Clienteterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta getClienteterapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Clienteterapeuta) session.get(orm.Clienteterapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Clienteterapeuta) session.load(orm.Clienteterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta getClienteterapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Clienteterapeuta) session.get(orm.Clienteterapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryClienteterapeuta(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryClienteterapeuta(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta[] listClienteterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listClienteterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta[] listClienteterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listClienteterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta as Clienteterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryClienteterapeuta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta as Clienteterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Clienteterapeuta", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta[] listClienteterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryClienteterapeuta(session, condition, orderBy);
            return (Clienteterapeuta[]) list.toArray(new Clienteterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta[] listClienteterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryClienteterapeuta(session, condition, orderBy, lockMode);
            return (Clienteterapeuta[]) list.toArray(new Clienteterapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadClienteterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Clienteterapeuta[] clienteterapeutas = listClienteterapeutaByQuery(session, condition, orderBy);
        if (clienteterapeutas != null && clienteterapeutas.length > 0) {
            return clienteterapeutas[0];
        } else {
            return null;
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Clienteterapeuta[] clienteterapeutas = listClienteterapeutaByQuery(session, condition, orderBy, lockMode);
        if (clienteterapeutas != null && clienteterapeutas.length > 0) {
            return clienteterapeutas[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iterateClienteterapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateClienteterapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iterateClienteterapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta as Clienteterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iterateClienteterapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Clienteterapeuta as Clienteterapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Clienteterapeuta", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta createClienteterapeuta() {
        return new orm.Clienteterapeuta();
    }

    public static boolean save(orm.Clienteterapeuta clienteterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(clienteterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Clienteterapeuta clienteterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(clienteterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Clienteterapeuta clienteterapeuta) throws PersistentException {
        try {
            if (clienteterapeuta.getPersona() != null) {
                clienteterapeuta.getPersona().setClienteterapeuta(null);
            }

            if (clienteterapeuta.getEvaluacion() != null) {
                clienteterapeuta.getEvaluacion().setClienteterapeuta(null);
            }

            orm.Clienteterapeuta_paciente[] lClienteterapeuta_pacientes = clienteterapeuta.clienteterapeuta_paciente.toArray();
            for (int i = 0; i < lClienteterapeuta_pacientes.length; i++) {
                lClienteterapeuta_pacientes[i].setClienteterapeuta(null);
            }
            if (clienteterapeuta.getAsigdispcliente() != null) {
                clienteterapeuta.getAsigdispcliente().setClienteterapeuta(null);
            }

            orm.Clienteterapeuta_dispositivo[] lClienteterapeuta_dispositivos = clienteterapeuta.clienteterapeuta_dispositivo.toArray();
            for (int i = 0; i < lClienteterapeuta_dispositivos.length; i++) {
                lClienteterapeuta_dispositivos[i].setClienteterapeuta(null);
            }
            return delete(clienteterapeuta);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Clienteterapeuta clienteterapeuta, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (clienteterapeuta.getPersona() != null) {
                clienteterapeuta.getPersona().setClienteterapeuta(null);
            }

            if (clienteterapeuta.getEvaluacion() != null) {
                clienteterapeuta.getEvaluacion().setClienteterapeuta(null);
            }

            orm.Clienteterapeuta_paciente[] lClienteterapeuta_pacientes = clienteterapeuta.clienteterapeuta_paciente.toArray();
            for (int i = 0; i < lClienteterapeuta_pacientes.length; i++) {
                lClienteterapeuta_pacientes[i].setClienteterapeuta(null);
            }
            if (clienteterapeuta.getAsigdispcliente() != null) {
                clienteterapeuta.getAsigdispcliente().setClienteterapeuta(null);
            }

            orm.Clienteterapeuta_dispositivo[] lClienteterapeuta_dispositivos = clienteterapeuta.clienteterapeuta_dispositivo.toArray();
            for (int i = 0; i < lClienteterapeuta_dispositivos.length; i++) {
                lClienteterapeuta_dispositivos[i].setClienteterapeuta(null);
            }
            try {
                session.delete(clienteterapeuta);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Clienteterapeuta clienteterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(clienteterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Clienteterapeuta clienteterapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(clienteterapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Clienteterapeuta loadClienteterapeutaByCriteria(ClienteterapeutaCriteria clienteterapeutaCriteria) {
        Clienteterapeuta[] clienteterapeutas = listClienteterapeutaByCriteria(clienteterapeutaCriteria);
        if (clienteterapeutas == null || clienteterapeutas.length == 0) {
            return null;
        }
        return clienteterapeutas[0];
    }

    public static Clienteterapeuta[] listClienteterapeutaByCriteria(ClienteterapeutaCriteria clienteterapeutaCriteria) {
        return clienteterapeutaCriteria.listClienteterapeuta();
    }
}
