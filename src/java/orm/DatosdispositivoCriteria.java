/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosdispositivoCriteria extends AbstractORMCriteria {

    public final IntegerExpression id;
    public final StringExpression fechaperiodo;
    public final IntegerExpression datosusogeneralId;
    public final AssociationExpression datosusogeneral;
    public final IntegerExpression datosusonivelId;
    public final AssociationExpression datosusonivel;
    public final IntegerExpression datosnivelcompletadoId;
    public final AssociationExpression datosnivelcompletado;
    public final IntegerExpression dispositivoId;
    public final AssociationExpression dispositivo;
    public final CollectionExpression datosdispocitivo_datosdeusodiario;
    public final CollectionExpression datosdispocitivo_juego;

    public DatosdispositivoCriteria(Criteria criteria) {
        super(criteria);
        id = new IntegerExpression("id", this);
        fechaperiodo = new StringExpression("fechaperiodo", this);
        datosusogeneralId = new IntegerExpression("datosusogeneral.id", this);
        datosusogeneral = new AssociationExpression("datosusogeneral", this);
        datosusonivelId = new IntegerExpression("datosusonivel.id", this);
        datosusonivel = new AssociationExpression("datosusonivel", this);
        datosnivelcompletadoId = new IntegerExpression("datosnivelcompletado.id", this);
        datosnivelcompletado = new AssociationExpression("datosnivelcompletado", this);
        dispositivoId = new IntegerExpression("dispositivo.id", this);
        dispositivo = new AssociationExpression("dispositivo", this);
        datosdispocitivo_datosdeusodiario = new CollectionExpression("ORM_Datosdispocitivo_datosdeusodiario", this);
        datosdispocitivo_juego = new CollectionExpression("ORM_Datosdispocitivo_juego", this);
    }

    public DatosdispositivoCriteria(PersistentSession session) {
        this(session.createCriteria(Datosdispositivo.class));
    }

    public DatosdispositivoCriteria() throws PersistentException {
        this(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession());
    }

    public DatosusogeneralCriteria createDatosusogeneralCriteria() {
        return new DatosusogeneralCriteria(createCriteria("datosusogeneral"));
    }

    public DatosusonivelCriteria createDatosusonivelCriteria() {
        return new DatosusonivelCriteria(createCriteria("datosusonivel"));
    }

    public DatosnivelcompletadoCriteria createDatosnivelcompletadoCriteria() {
        return new DatosnivelcompletadoCriteria(createCriteria("datosnivelcompletado"));
    }

    public DispositivoCriteria createDispositivoCriteria() {
        return new DispositivoCriteria(createCriteria("dispositivo"));
    }

    public Datosdispocitivo_datosdeusodiarioCriteria createDatosdispocitivo_datosdeusodiarioCriteria() {
        return new Datosdispocitivo_datosdeusodiarioCriteria(createCriteria("ORM_Datosdispocitivo_datosdeusodiario"));
    }

    public Datosdispocitivo_juegoCriteria createDatosdispocitivo_juegoCriteria() {
        return new Datosdispocitivo_juegoCriteria(createCriteria("ORM_Datosdispocitivo_juego"));
    }

    public Datosdispositivo uniqueDatosdispositivo() {
        return (Datosdispositivo) super.uniqueResult();
    }

    public Datosdispositivo[] listDatosdispositivo() {
        java.util.List list = super.list();
        return (Datosdispositivo[]) list.toArray(new Datosdispositivo[list.size()]);
    }
}
