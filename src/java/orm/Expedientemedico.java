/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Expedientemedico {

    public Expedientemedico() {
    }

    private int id;

    private String patologia;

    private String observacion;

    private orm.Paciente paciente;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    public void setPatologia(String value) {
        this.patologia = value;
    }

    public String getPatologia() {
        return patologia;
    }

    public void setObservacion(String value) {
        this.observacion = value;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setPaciente(orm.Paciente value) {
        if (this.paciente != value) {
            orm.Paciente lpaciente = this.paciente;
            this.paciente = value;
            if (value != null) {
                paciente.setExpedientemedico(this);
            }
            if (lpaciente != null && lpaciente.getExpedientemedico() == this) {
                lpaciente.setExpedientemedico(null);
            }
        }
    }

    public orm.Paciente getPaciente() {
        return paciente;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
