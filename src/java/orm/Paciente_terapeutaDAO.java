/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Paciente_terapeutaDAO {

    public static Paciente_terapeuta loadPaciente_terapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadPaciente_terapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta getPaciente_terapeutaByORMID(int id) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getPaciente_terapeutaByORMID(session, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadPaciente_terapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta getPaciente_terapeutaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return getPaciente_terapeutaByORMID(session, id, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Paciente_terapeuta) session.load(orm.Paciente_terapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta getPaciente_terapeutaByORMID(PersistentSession session, int id) throws PersistentException {
        try {
            return (Paciente_terapeuta) session.get(orm.Paciente_terapeuta.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Paciente_terapeuta) session.load(orm.Paciente_terapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta getPaciente_terapeutaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            return (Paciente_terapeuta) session.get(orm.Paciente_terapeuta.class, new Integer(id), lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryPaciente_terapeuta(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryPaciente_terapeuta(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryPaciente_terapeuta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return queryPaciente_terapeuta(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta[] listPaciente_terapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listPaciente_terapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta[] listPaciente_terapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return listPaciente_terapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryPaciente_terapeuta(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Paciente_terapeuta as Paciente_terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static List queryPaciente_terapeuta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Paciente_terapeuta as Paciente_terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Paciente_terapeuta", lockMode);
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta[] listPaciente_terapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        try {
            List list = queryPaciente_terapeuta(session, condition, orderBy);
            return (Paciente_terapeuta[]) list.toArray(new Paciente_terapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta[] listPaciente_terapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            List list = queryPaciente_terapeuta(session, condition, orderBy, lockMode);
            return (Paciente_terapeuta[]) list.toArray(new Paciente_terapeuta[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadPaciente_terapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return loadPaciente_terapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        Paciente_terapeuta[] paciente_terapeutas = listPaciente_terapeutaByQuery(session, condition, orderBy);
        if (paciente_terapeutas != null && paciente_terapeutas.length > 0) {
            return paciente_terapeutas[0];
        } else {
            return null;
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        Paciente_terapeuta[] paciente_terapeutas = listPaciente_terapeutaByQuery(session, condition, orderBy, lockMode);
        if (paciente_terapeutas != null && paciente_terapeutas.length > 0) {
            return paciente_terapeutas[0];
        } else {
            return null;
        }
    }

    public static java.util.Iterator iteratePaciente_terapeutaByQuery(String condition, String orderBy) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iteratePaciente_terapeutaByQuery(session, condition, orderBy);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iteratePaciente_terapeutaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        try {
            PersistentSession session = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession();
            return iteratePaciente_terapeutaByQuery(session, condition, orderBy, lockMode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iteratePaciente_terapeutaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Paciente_terapeuta as Paciente_terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static java.util.Iterator iteratePaciente_terapeutaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
        StringBuffer sb = new StringBuffer("From orm.Paciente_terapeuta as Paciente_terapeuta");
        if (condition != null) {
            sb.append(" Where ").append(condition);
        }
        if (orderBy != null) {
            sb.append(" Order By ").append(orderBy);
        }
        try {
            Query query = session.createQuery(sb.toString());
            query.setLockMode("Paciente_terapeuta", lockMode);
            return query.iterate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta createPaciente_terapeuta() {
        return new orm.Paciente_terapeuta();
    }

    public static boolean save(orm.Paciente_terapeuta paciente_terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().saveObject(paciente_terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean delete(orm.Paciente_terapeuta paciente_terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().deleteObject(paciente_terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Paciente_terapeuta paciente_terapeuta) throws PersistentException {
        try {
            if (paciente_terapeuta.getPaciente() != null) {
                paciente_terapeuta.getPaciente().paciente_terapeuta.remove(paciente_terapeuta);
            }

            if (paciente_terapeuta.getTerapeuta() != null) {
                paciente_terapeuta.getTerapeuta().paciente_terapeuta.remove(paciente_terapeuta);
            }

            return delete(paciente_terapeuta);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean deleteAndDissociate(orm.Paciente_terapeuta paciente_terapeuta, org.orm.PersistentSession session) throws PersistentException {
        try {
            if (paciente_terapeuta.getPaciente() != null) {
                paciente_terapeuta.getPaciente().paciente_terapeuta.remove(paciente_terapeuta);
            }

            if (paciente_terapeuta.getTerapeuta() != null) {
                paciente_terapeuta.getTerapeuta().paciente_terapeuta.remove(paciente_terapeuta);
            }

            try {
                session.delete(paciente_terapeuta);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean refresh(orm.Paciente_terapeuta paciente_terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().refresh(paciente_terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static boolean evict(orm.Paciente_terapeuta paciente_terapeuta) throws PersistentException {
        try {
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().evict(paciente_terapeuta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistentException(e);
        }
    }

    public static Paciente_terapeuta loadPaciente_terapeutaByCriteria(Paciente_terapeutaCriteria paciente_terapeutaCriteria) {
        Paciente_terapeuta[] paciente_terapeutas = listPaciente_terapeutaByCriteria(paciente_terapeutaCriteria);
        if (paciente_terapeutas == null || paciente_terapeutas.length == 0) {
            return null;
        }
        return paciente_terapeutas[0];
    }

    public static Paciente_terapeuta[] listPaciente_terapeutaByCriteria(Paciente_terapeutaCriteria paciente_terapeutaCriteria) {
        return paciente_terapeutaCriteria.listPaciente_terapeuta();
    }
}
