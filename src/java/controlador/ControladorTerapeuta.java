package controlador;

import business.Deleted;
import business.ExNihilo;
import business.Search;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import orm.Clienteterapeuta;
import orm.Paciente;

/**
 * Version 0.0.1.
 *
 * @author unknown
 */
public class ControladorTerapeuta extends HttpServlet {

    /**
     * Procesa las peticiones por GET y Por POST
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = request.getServletPath();
        String destino = "/WEB-INF/vistas/";
        String run;
        Paciente P;
        RequestDispatcher rd;
        
        switch (ruta) {
            case "/intranet":
                HttpSession sesion = request.getSession();
                destino += "intranet_terapeuta.jsp";
                this.reloadListaPacientes(request);

                rd = request.getRequestDispatcher(destino);
                rd.forward(request, response);
                break;
            case "/intranet/u":
                destino += "intranet_terapeuta.jsp";
//                response.sendRedirect(destino);
                rd = request.getRequestDispatcher(destino);
                rd.forward(request, response);
                break;
            case "/ver_paciente":
                run = request.getParameter("id");
                P = Search.obtenerPaciente(run);
                request.setAttribute("paciente", P);
                destino += "vista_paciente.jsp";
                rd = request.getRequestDispatcher(destino);
                rd.forward(request, response);
                break;
            case "/borrar_paciente":
                run = request.getParameter("id");
                P = Search.obtenerPaciente(run);
                Deleted.eliminarPaciente(P);

                destino = "intranet";
                rd = request.getRequestDispatcher(destino);
                rd.forward(request, response);
                break;

            case "/intranet/u/out":
                if (this.logOut(request)) {

                    response.sendRedirect("/PROYECTO_WEB");
                }
                destino = "usuarioListar";
                break;
            default:
                break;
        }

    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.processRequest(request, response);
    }

    private boolean logOut(HttpServletRequest request) {
        boolean estado = false;

        HttpSession sesion = request.getSession();
        if (sesion.getAttribute("terapeuta") != null) {
            sesion.invalidate();
            estado = true;

        } else {
            estado = false;
        }

        return estado;
    }

    private void reloadListaPacientes(HttpServletRequest request) {
        ArrayList<Paciente> listaPacientes = new ArrayList<>();
        HttpSession sesion = request.getSession();

        Clienteterapeuta CT = (Clienteterapeuta) sesion.getAttribute("terapeuta");
        listaPacientes = Search.obtenerListaPacienteCLienteTerapeuta(CT);

        request.setAttribute("pacientes", listaPacientes);

    }
    
    private void borrarPaciente(){
    
    }

}
