package controlador;

import business.Search;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import orm.Clienteterapeuta;
import orm.Organizacion;
import orm.Paciente;

/**
 *
 * @author Felipe Quezada
 */
@WebServlet(name = "ControladorVistas", urlPatterns = {"/ControladorVistas"})
public class ControladorVistas extends HttpServlet {

    /**
     * Procesa las peticiones por GET y Por POST
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = request.getServletPath();
        String destino = "/WEB-INF/vistas/";

        if (ruta.equals("/")) {
            destino = "PROYECTO_WEB/index.jsp";

        } else if (ruta.equals("/login")) {

            destino += "login.jsp";
        } else if (ruta.equals("/registro")) {
            destino += "register_terapeuta.jsp";
        }
        else if (ruta.equals("/contacto")) {

            destino += "contacto.jsp";

        } 
        else if (ruta.equals("/loginuser")) {
            if (this.loginUser(request)) {
                destino = "intranet";
            } else {
                destino += "mensaje.jsp";
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(destino);
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.processRequest(request, response);

    }

    private boolean loginUser(HttpServletRequest request) throws ServletException, IOException {

        String user = request.getParameter("user");
        String pass = request.getParameter("password");
        String msg = "Error 404";

        Clienteterapeuta CliTerapeuta = new Clienteterapeuta();
        Organizacion Org = new Organizacion();

        ArrayList<Paciente> listaPacientes = new ArrayList<>();

        //Validacion de los parametros de entrada.
        CliTerapeuta = Search.buscarClienteTerapeuta(user);

        HttpSession sesion = request.getSession();

        boolean respuesta = false;

        if (sesion.getAttribute("terapeuta") == null) {
            if (CliTerapeuta != null && CliTerapeuta.getPassword().equals(pass)) {
                listaPacientes = Search.obtenerListaPacienteCLienteTerapeuta(CliTerapeuta);

//            request.setAttribute("terapeuta", CliTerapeuta);
                request.setAttribute("pacientes", listaPacientes);
                sesion.setAttribute("terapeuta", CliTerapeuta);

                respuesta = true;
            } else if (Search.buscarOrganizacion(user) != null
                    && Search.buscarOrganizacion(user).getPassword().equals(pass)) {

                request.setAttribute("organizacion", Search.buscarOrganizacion(user));
                //reenviamos el objeto al jsp
                respuesta = true;
            } else {
                String msj = "Usuario No Registrado";
//
                request.setAttribute("mensaje", msj);

                respuesta = false;

            }

        } else {

            respuesta = true;
        }

        return respuesta;

    }
}
