package webservice;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reportes.ReportesClienteTerapeuta;

/**
 * Servicio REST implementando un Servlet Version 0.0.1.
 *
 * @author Felipe Quezada
 */
@WebServlet(name = "ClienteTerapeutaJson", urlPatterns = {"/ClienteTerapeutaJson"})
public class ClienteTerapeutaJson extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);

        /*
         Consulta = http://localhost:8080/PROYECTO_WEB/ClienteTerapeutaJson?run=201385962
         */
        String run = request.getParameter("run");
        String jsonCT = "Cliente Terapeuta no encontrado.";

        if (run != null) {
            ReportesClienteTerapeuta RCL = new ReportesClienteTerapeuta();
            jsonCT = RCL.reporteClienteTerapeutaJSONv2(run);
        }

        PrintWriter salida = response.getWriter();
        salida.println(jsonCT);
    }

}
