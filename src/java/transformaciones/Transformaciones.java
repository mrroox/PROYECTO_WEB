package transformaciones;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Version 0.0.1 .
 *
 * @author Felipe Quezada
 */
public class Transformaciones {

    /**
     * Metodo para generar el arhivo XML con los datos de registros
     *
     * @param datos String con datos a generar
     * @param nombre String nombre del archivo a generar
     */
    public static void generarArchivo(String datos, String nombre) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        String ruta = "src/java/transformaciones/reportes/" + nombre;
        try {
            fichero = new FileWriter(ruta);
            pw = new PrintWriter(fichero);
            pw.println(datos);

        } catch (IOException e) {

        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (IOException e2) {

            }
        }

    }

}
