<?xml version='1.0' encoding='UTF-8' ?> 
<!-- was: <?xml version="1.0" encoding="iso-8859-1"?> -->
<xsl:stylesheet xmlns:xsl= "http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table {
                    font-family: arial, 'sans serif';
                    margin-left: 15pt;
                    }
                    th,td {
                    font-size: 80%;
                    }
                    th {
                    background-color:#FAEBD7
                    }
                </style>
            </head>
            <body>
                <h1>Lista de Clientes Terapeutas Registrados en el Sistema</h1>
                <table border="1">
                    <xsl:apply-templates/>
                </table>
            </body>
        </html>
    </xsl:template>
  
    <xsl:template match="orm.Clienteterapeuta-array">

        <xsl:for-each select="orm.Clienteterapeuta">
            <tr>
                <th>Id Terapeuta</th>

                <th>Password Terapeuta</th>
            </tr>
            <tr>
                <td>
                    <xsl:value-of select="id"/>
                </td>
                <td>
                    <xsl:value-of select="password"/>
                </td>
                
                <xsl:for-each select="persona">
                    <tr>
                        <th>Id Persona</th>
                        <th>Run</th>
                        <th>Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Sexo</th>
                        <th>Fecha Nacimiento</th>
                        <th>email</th>
    
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="id"/>
                        </td>
                        <td>
                            <xsl:value-of select="run"/>
                        </td>
                        <td>
                            <xsl:value-of select="nombre"/>
                        </td>
                        <td>
                            <xsl:value-of select="apellido__paterno"/>
                        </td>
                        <td>
                            <xsl:value-of select="apellido__materno"/>
                        </td>
                        <td>
                            <xsl:value-of select="sexo"/>
                        </td>
                        
                        <td>
                            <xsl:value-of select="fecha__nacimiento"/>
                        </td>
                        <td>
                            <xsl:value-of select="email"/>
                        </td>

                    </tr>
                </xsl:for-each>
                
                
            </tr>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>