<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
            <w:body>
                <xsl:for-each select="orm.Clienteterapeuta-array"> 
                    <w:p>
                        <w:r>
                            <w:t>Lista de Terapeutas Registrados :</w:t> 
                        </w:r>
                    </w:p> 
          
                    <xsl:for-each select="orm.Clienteterapeuta">
                        <w:p>
                            <w:r>
                                <w:t>--------------------------------------------</w:t>                                
                            </w:r>
                        </w:p> 
                        <w:p>
                            <w:r>                               
                                <w:t>Id Terapeuta : </w:t> 
                                <xsl:value-of select="id"/>                                 
                            </w:r>
                        </w:p> 
                    
                        <xsl:for-each select="persona">
                            <w:p>
                                <w:r>
                                    <w:t>
                                        <w:t>Id Persona : </w:t> 
                                        <xsl:value-of select="id"/> 
                                    </w:t>
                                </w:r>
                            </w:p>
                            <w:p>
                                <w:r>
                                    <w:t> Nombre : </w:t> 
                                    <xsl:value-of select="nombre"/>                                    
                                </w:r>
                            </w:p> 
                            <w:p>
                                <w:r>
                                    <w:t> Apellido Paterno : </w:t> 
                                    <xsl:value-of select="apellido__paterno"/>
                                   
                                </w:r>
                            </w:p> 
                            <w:p>
                                <w:r>
                                    <w:t> Apellido Materno : </w:t>
                                    <xsl:value-of select="apellido__materno"/>                             
                                    
                                </w:r>
                            </w:p> 
                            <w:p>
                                <w:r>
                                    <w:t> Fecha Nacimiento : </w:t>
                                    <xsl:value-of select="fecha__nacimiento"/>                             
                                    
                                </w:r>
                            </w:p> 
                            <w:p>
                                <w:r>
                                    <w:t> Email : </w:t>
                                    <xsl:value-of select="email"/>                             
                                </w:r>
                            </w:p> 
                            
                            <w:p>
                                <w:r>
                                    <w:t>--------------------------------------------</w:t> 
                                </w:r>
                            </w:p> 
                            
                        </xsl:for-each>
                        
                    </xsl:for-each>
                </xsl:for-each>
            </w:body>
        </w:wordDocument>
    </xsl:template>
</xsl:stylesheet>

