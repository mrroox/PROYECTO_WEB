package transformaciones;

/**
 * Clase que sirve para almacenar temporalmente los datos de un Cliente
 * Terapeuta para hacer una transformacion a JSON
 *
 * Version 0.0.1.
 *
 * @author Felipe Quezada
 */
public class DataClassClienteTerapeutaJson {

    private String idCliTerapeuta;
    private String idPersona;
    private String run;
    private String nombre;
    private String a_paterno;
    private String a_materno;
    private String sexo;
    private String fechaN;
    private String email;

    public DataClassClienteTerapeutaJson(String idCT, String idP, String r, String n, String a_p, String a_m, String s, String fn, String e) {
        this.idCliTerapeuta = idCT;
        this.idPersona = idP;
        this.run = r;
        this.nombre = n;
        this.a_paterno = a_p;
        this.a_materno = a_m;
        this.sexo = s;
        this.fechaN = fn;
        this.email = e;
    }

}
