package transformaciones;

import java.io.File;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Result;
import javax.xml.transform.sax.SAXResult;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

/**
 * Transformador de xml con xsl generalizado el cual utiliza un objeto
 * Transformer.
 * Version 0.0.1.
 * @author unknown
 */
public class XslTransformer {

//    public XslTransformer() {
//    }
    /**
     * Realiza las pruebas correspondientes a las transformaciones xslt,
     * HTML,JSON,EXCEL,DOC y PDF.
     *
     * @param args Parametro para metodo main de pruebas.
     */
    public static void main(String[] args) {
        TransformarXMLtoHTML();
        TransformarXMLtoJSON();
        TransformarXMLtoExcel();
        TransformarXMLtoDOC();
        TransformtoPDF();

    }

    /**
     * Transforma un XML a un archivo Excel.
     *
     */
    public static void TransformtoPDF() {
        String xslPdf = "src/java/transformaciones/toPDFReporte.xsl";
        String xmlDoc = "src/java/transformaciones/listaClientes.xml";
        String outputDocS = "src/java/transformaciones/reportes/PdfClientes.pdf";

        try {

            // the XSL FO file
            File xsltFile = new File(xslPdf);
            // the XML file which provides the input
            StreamSource xmlSource = new StreamSource(new File(xmlDoc));
            // create an instance of fop factory
            FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());

            // a user agent is needed for transformation
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            // Setup output
            OutputStream out;
            out = new java.io.FileOutputStream(outputDocS);

            try {
                // Construct fop with desired output format
                Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

                // Setup XSLT
                TransformerFactory factory = TransformerFactory.newInstance();
                Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

                // Resulting SAX events (the generated FO) must be piped through to
                // FOP
                Result res = new SAXResult(fop.getDefaultHandler());

                // Start XSLT transformation and FOP processing
                // That's where the XML is first transformed to XSL-FO and then
                // PDF is created
                transformer.transform(xmlSource, res);
            } finally {
                out.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(XslTransformer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FOPException ex) {
            Logger.getLogger(XslTransformer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(XslTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Transforma un xml a un archivo Doc.
     */
    public static void TransformarXMLtoDOC() {
        XslTransformer xslT = new XslTransformer();
        String xslExcel = "src/java/transformaciones/toWordReporteClientes.xsl";
        String xmlDoc = "src/java/transformaciones/listaClientes.xml";
        String outputDocS = "src/java/transformaciones/reportes/WordClientes.doc";

        xslT.transform(xslExcel, xmlDoc, outputDocS);

    }

    /**
     * Transform un xml a un arhivo Excel.
     */
    public static void TransformarXMLtoExcel() {
        XslTransformer xslT = new XslTransformer();
        String xslExcel = "src/java/transformaciones/toExcelReporteClientes.xsl";
        String xmlDoc = "src/java/transformaciones/listaClientes.xml";
        String outputDocS = "src/java/transformaciones/reportes/ExcelClientes.xlsx";

        xslT.transform(xslExcel, xmlDoc, outputDocS);

    }

    /**
     * Transforma un xml a un archivo json.
     */
    public static void TransformarXMLtoJSON() {
        //Ejemplo para carteleraCine.xml ---> CarteleraCine.xsl ----> CarteleraCine.html
        XslTransformer xslT = new XslTransformer();
        String xslDoc = "src/java/transformaciones/xsltojson.xsl";
        String xmlDoc = "src/java/transformaciones/listaClientes.xml";
        String outputDocS = "src/java/transformaciones/reportes/listaClientes.json";

        xslT.transform(xslDoc, xmlDoc, outputDocS);

    }

    /**
     * Transforma un xml en un archivo html.
     */
    public static void TransformarXMLtoHTML() {
        //Ejemplo para carteleraCine.xml ---> CarteleraCine.xsl ----> CarteleraCine.html
        XslTransformer xslT = new XslTransformer();
        String xslDoc = "src/java/transformaciones/listaClientes.xsl";
        String xmlDoc = "src/java/transformaciones/listaClientes.xml";
        String outputDocS = "src/java/transformaciones/reportes/HtmlClientes.html";

        xslT.transform(xslDoc, xmlDoc, outputDocS);

    }

    /**
     * Transforma un xml a un archivo deseado con un xsl creado con anterioridad
     *
     * @param xslDocS Ruta del archivo xsl que realizara la transformacion
     * @param xmlDocS Ruta al archivo xml al que se le aplicara la
     * transformacion
     * @param outputDocS Ruta en la cual se guardara el archivo resultante
     *
     */
    public void transform(String xslDocS, String xmlDocS, String outputDocS) {
        try {
            //Se crea transformer factory el cual creara el objeto transformer
            TransformerFactory tFactory = TransformerFactory.newInstance();
            //Fuente del archivo xsl
            Source xslDoc = new StreamSource(xslDocS);
            //Fuente archivo xml
            Source xmlDoc = new StreamSource(xmlDocS);
            //Path del archivo de salida
            String outputFileName = outputDocS;
            //Se crea OutputStream con direccion al path de archivo de salida
            OutputStream htmlFile;
            try {
                htmlFile = new FileOutputStream(outputFileName);
                //Se crea el transformer respecto al archivo xsl
                Transformer trasform = tFactory.newTransformer(xslDoc);

                //Se transforma el documento xsl y se envia al documento de salida
                trasform.transform(xmlDoc, new StreamResult(htmlFile));

            } catch (FileNotFoundException ex) {
                Logger.getLogger(XslTransformer.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (TransformerException ex) {
            Logger.getLogger(XslTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
