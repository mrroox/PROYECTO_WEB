<xsl:stylesheet version="1.0" xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
    <xsl:template match="/">
        <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">
            <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
                <Author>ed woychowsky</Author>
                <LastAuthor>Edmond Woychowsky</LastAuthor>
                <Created>2007-01-26T16:54:15Z</Created>
                <LastSaved>2007-01-27T05:18:54Z</LastSaved>
                <Company>None</Company>
                <Version>10.3501</Version>
            </DocumentProperties>
            <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
                <DownloadComponents/>
                <LocationOfComponents HRef="file:///D:\" />
            </OfficeDocumentSettings>
            <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
                <WindowHeight>8700</WindowHeight>
                <WindowWidth>11355</WindowWidth>
                <WindowTopX>480</WindowTopX>
                <WindowTopY>120</WindowTopY>
                <ProtectStructure>False</ProtectStructure>
                <ProtectWindows>False</ProtectWindows>
            </ExcelWorkbook>
            <Styles>
                <Style ss:ID="Default" ss:Name="Normal">
                    <Alignment ss:Vertical="Bottom"/>
                    <Borders/>
                    <Font/>
                    <Interior/>
                    <NumberFormat/>
                    <Protection/>

                </Style>
            </Styles>
            <Worksheet ss:Name="Reporte Cliente Terapeuta">
                <Table>
                    <xsl:for-each select="orm.Clienteterapeuta-array">
                        
                        <Row>
                            <Cell>
                                <Data ss:Type="String">Id Terapeuta</Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">Id Persona</Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">Nombre</Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">Apellido Paterno</Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">Apellido Materno</Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">Sexo</Data>
                            </Cell>
                            <Cell>
                                <Data ss:Type="String">Fecha Nacimiento</Data>
                            </Cell>
                            
                        </Row>
                        <xsl:for-each select="orm.Clienteterapeuta">
                            <Row>
                                <Cell>
                                    <Data ss:Type="String">
                                        <xsl:value-of select="id"/>
                                    </Data>
                                </Cell>
                                <xsl:for-each select="persona">
                                    <Cell>
                                        <Data ss:Type="String"> 
                                            <xsl:value-of select="id"/>
                                        </Data>
                                    </Cell>
                                    <Cell>
                                        <Data ss:Type="String"> 
                                            <xsl:value-of select="nombre"/>
                                        </Data>
                                    </Cell>
                                    <Cell>
                                        <Data ss:Type="String"> 
                                            <xsl:value-of select="apellido__paterno"/>
                                        </Data>
                                    </Cell>
                                    <Cell>
                                        <Data ss:Type="String"> 
                                            <xsl:value-of select="apellido__materno"/>
                                        </Data>
                                    </Cell>
                                    <Cell>
                                        <Data ss:Type="String"> 
                                            <xsl:value-of select="sexo"/>
                                        </Data>
                                    </Cell>
                                    <Cell>
                                        <Data ss:Type="String"> 
                                            <xsl:value-of select="fecha__nacimiento"/>
                                        </Data>
                                    </Cell>
                                </xsl:for-each>
                            </Row>
                        </xsl:for-each>
                    </xsl:for-each>
                </Table>
                <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
                    <Print>
                        <ValidPrinterInfo/>
                        <HorizontalResolution>600</HorizontalResolution>
                        <VerticalResolution>0</VerticalResolution>
                    </Print>
                    <Selected/>
                    <Panes>
                        <Pane>
                            <Number>3</Number>
                            <ActiveRow>2</ActiveRow>
                        </Pane>
                    </Panes>
                    <ProtectObjects>False</ProtectObjects>
                    <ProtectScenarios>False</ProtectScenarios>
                </WorksheetOptions>
            </Worksheet>
        </Workbook>
    </xsl:template>
</xsl:stylesheet>

