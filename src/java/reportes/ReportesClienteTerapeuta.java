package reportes;

import business.Search;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;
import orm.Clienteterapeuta;
import transformaciones.DataClassClienteTerapeutaJson;
import static transformaciones.Transformaciones.generarArchivo;
import transformaciones.XslTransformer;

/**
 * Clase que se utiliza para generar los diferentes reportes correspondientes a
 * una Cliente Terapeuta.
 *
 * Version 0.0.1.
 *
 * @author Felipe Quezada
 */
public class ReportesClienteTerapeuta {

    public ReportesClienteTerapeuta() {
    }

    public static void main(String[] args) {
//reporteListaClienteTerapeutaXML();
        reporteListaClienteTerapeutaHTML();
        reporteListaClienteTerapeutaWORD();
        reporteListaClienteTerapeutaEXCEL();
        reporteListaClienteTerapeutaJSON();
        reporteListaClienteTerapeutaJSONv2();
        reporteListaClienteTerapeutaPDF();
    }

    /**
     * Genera un archivos XML de todos los Clientes Terapeutas registrados en el
     * Sistema.
     *
     * @return String del XML de la consulta.
     */
    public static String reporteListaClienteTerapeutaXML() {

        System.out.println("Listando Pacientes por Terapeuta...");
        Clienteterapeuta[] terapeuta = Search.buscarListaTotalClienteTerapeuta();
        String xml = new XStream(new DomDriver()).toXML(terapeuta);
        System.out.println("XML => " + xml);

        //Generamos el Archivo XML.
        generarArchivo(xml, "ListaClientesTerapeutas.xml");
        return xml;

    }

    /**
     * Genera un archivo HTML de todos los Clientes Terapeutas utilizando un
     * archivo de transformacion xsl a partir de un archivo XML.
     *
     */
    public static void reporteListaClienteTerapeutaHTML() {

        XslTransformer xslT = new XslTransformer();
        String xslDoc = "src/java/transformaciones/ListaClientesTerapeutas-HTML.xsl";
        String xmlDoc = "src/java/transformaciones/reportes/ListaClientesTerapeutas.xml";
        String outputDocS = "src/java/transformaciones/reportes/HTMLListaClientesTerapeutas.html";

        xslT.transform(xslDoc, xmlDoc, outputDocS);

    }

    /**
     * Genera un archivo DOC de todos los Clientes Terapeutas utilizando un
     * archivo de transformacion xsl a partir de una archivo xml.
     */
    public static void reporteListaClienteTerapeutaWORD() {

        XslTransformer xslT = new XslTransformer();
        String xslDoc = "src/java/transformaciones/ListaClientesTerapeutas-DOC.xsl";
        String xmlDoc = "src/java/transformaciones/reportes/ListaClientesTerapeutas.xml";
        String outputDocS = "src/java/transformaciones/reportes/DOC-ListaClientesTerapeutas.doc";

        xslT.transform(xslDoc, xmlDoc, outputDocS);

    }

    /**
     * Genera un archivo EXCEL de todos los Clientes Terapeutas utilizando un
     * archivo de transformacion xsl a partir de una archivo xml.
     */
    public static void reporteListaClienteTerapeutaEXCEL() {
        XslTransformer xslT = new XslTransformer();
        String xslDoc = "src/java/transformaciones/ListaClientesTerapeutas-EXCEL.xsl";
        String xmlDoc = "src/java/transformaciones/reportes/ListaClientesTerapeutas.xml";
        String outputDocS = "src/java/transformaciones/reportes/EXCEL-ListaClientesTerapeutas.xls";

        xslT.transform(xslDoc, xmlDoc, outputDocS);

    }

    /**
     * Genera un archivo JSON de todos los Clientes Terapeutas utilizando un
     * archivo de transformacion xsl a partir de una archivo xml.
     */
    public static void reporteListaClienteTerapeutaJSON() {
        XslTransformer xslT = new XslTransformer();
        String xslDoc = "src/java/transformaciones/ListaClientesTerapeutas-JSON.xsl";
        String xmlDoc = "src/java/transformaciones/reportes/ListaClientesTerapeutas.xml";
        String outputDocS = "src/java/transformaciones/reportes/JSON-ListaClientesTerapeutas.json";

        xslT.transform(xslDoc, xmlDoc, outputDocS);

    }

    /**
     * Genera un archivo JSON de todos los Clientes Terapeutas una Data Class de
     * ClienteTerapeuta.
     */
    public static void reporteListaClienteTerapeutaJSONv2() {
        ArrayList<DataClassClienteTerapeutaJson> ArregloClienteTerapeuta = new ArrayList<>();
        System.out.println("Listando Clientes Terapeutas...");
        Clienteterapeuta[] terapeuta = Search.buscarListaTotalClienteTerapeuta();
        for (int i = 0; i < terapeuta.length; i++) {
            ArregloClienteTerapeuta.add(new DataClassClienteTerapeutaJson("" + terapeuta[i].getId(),
                    "" + terapeuta[i].getPersona().getId(), terapeuta[i].getPersona().getRun(),
                    terapeuta[i].getPersona().getNombre(), terapeuta[i].getPersona().getApellido_paterno(),
                    terapeuta[i].getPersona().getApellido_materno(), terapeuta[i].getPersona().getSexo(),
                    terapeuta[i].getPersona().getFecha_nacimiento(), terapeuta[i].getPersona().getEmail()));
        }

        String json = new Gson().toJson(ArregloClienteTerapeuta);
        generarArchivo(json, "JSON-ListaClientesTerapeutasv2.json");
    }

    /**
     * Genera un archivo PDF de todos los Clientes Terapeutas utilizando un
     * archivo de transformacion xsl a partir de una archivo xml.
     */
    public static void reporteListaClienteTerapeutaPDF() {
        XslTransformer xslT = new XslTransformer();
        String xslDoc = "src/java/transformaciones/ListaClientesTerapeutas-PDF.xsl";
        String xmlDoc = "src/java/transformaciones/reportes/ListaClientesTerapeutas.xml";
        String outputDocS = "src/java/transformaciones/reportes/PDF-ListaClientesTerapeutas.pdf";

        xslT.transform(xslDoc, xmlDoc, outputDocS);

    }

    /**
     * Genera un archivo XML de un ClienteTerapeuta en especifico, buscado de
     * acuerdo a su run.
     *
     * @param run
     */
    public static void reporteClienteTerapeutaXML(String run) {

        System.out.println("Consultando por Terapeuta...");
        Clienteterapeuta terapeuta = Search.buscarClienteTerapeuta(run);
        String xml = new XStream(new DomDriver()).toXML(terapeuta);
        System.out.println("XML => " + xml);

        //Generamos el Archivo XML.
        generarArchivo(xml, "ClientesTerapeuta-" + run + ".xml");

    }

    public static void reporteClienteTerapeutaHTML() {
    }

    public static void reporteClienteTerapeutaWORD() {
    }

    public static void reporteClienteTerapeutaEXCEL() {
    }

    public static void reporteClienteTerapeutaJSON() {

    }

    /**
     * Genera un archivo JSON de un Cliente Terapeuta
     *
     * @param run del Cliente Terapeuta a consultar.
     * @return String del JSON generado.
     */
    public static String reporteClienteTerapeutaJSONv2(String run) {
        String json = "";

        System.out.println("Consultando por Cliente Terapeuta run => " + run + "...");
        Clienteterapeuta terapeuta = Search.buscarClienteTerapeuta(run);

        if (terapeuta != null) {
            DataClassClienteTerapeutaJson ClienteTerapeuta = new DataClassClienteTerapeutaJson("" + terapeuta.getId(),
                    "" + terapeuta.getPersona().getId(), terapeuta.getPersona().getRun(),
                    terapeuta.getPersona().getNombre(), terapeuta.getPersona().getApellido_paterno(),
                    terapeuta.getPersona().getApellido_materno(), terapeuta.getPersona().getSexo(),
                    terapeuta.getPersona().getFecha_nacimiento(), terapeuta.getPersona().getEmail());

            json = new Gson().toJson(ClienteTerapeuta);
            generarArchivo(json, "JSON-ClienteTerapeutasv2-" + run + ".json");
        }
        return json;
    }

    public static void reporteListaPacientesClienteTerapeuta() {

    }

}
