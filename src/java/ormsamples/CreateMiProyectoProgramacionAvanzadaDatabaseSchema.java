/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;

public class CreateMiProyectoProgramacionAvanzadaDatabaseSchema {

    public static void main(String[] args) {
        try {
            ORMDatabaseInitiator.createSchema(orm.MiProyectoProgramacionAvanzadaPersistentManager.instance());
            orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().disposePersistentManager();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
