/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;

public class CreateMiProyectoProgramacionAvanzadaData {

    public void createTestData() throws PersistentException {
        PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();
        try {
            orm.Datosnivelcompletado lormDatosnivelcompletado = orm.DatosnivelcompletadoDAO.createDatosnivelcompletado();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : datosdispositivo
            orm.DatosnivelcompletadoDAO.save(lormDatosnivelcompletado);
            orm.Datosdeusodiario lormDatosdeusodiario = orm.DatosdeusodiarioDAO.createDatosdeusodiario();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : datosdispocitivo_datosdeusodiario
            orm.DatosdeusodiarioDAO.save(lormDatosdeusodiario);
            orm.Datosusonivel lormDatosusonivel = orm.DatosusonivelDAO.createDatosusonivel();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : datosdispositivo
            orm.DatosusonivelDAO.save(lormDatosusonivel);
            orm.Clienteterapeuta lormClienteterapeuta = orm.ClienteterapeutaDAO.createClienteterapeuta();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : clienteterapeuta_dispositivo, asigdispcliente, clienteterapeuta_paciente, evaluacion, personadireccionid, persona
            orm.ClienteterapeutaDAO.save(lormClienteterapeuta);
            orm.Datosusogeneral lormDatosusogeneral = orm.DatosusogeneralDAO.createDatosusogeneral();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : datosdispositivo
            orm.DatosusogeneralDAO.save(lormDatosusogeneral);
            orm.Evaluacion lormEvaluacion = orm.EvaluacionDAO.createEvaluacion();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : paciente, clienteterapeuta
            orm.EvaluacionDAO.save(lormEvaluacion);
            orm.Paciente lormPaciente = orm.PacienteDAO.createPaciente();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asigdispterapeuta, evaluacionterapeuta, paciente_terapeuta, apoderado_paciente, evaluacion, asigdispcliente, clienteterapeuta_paciente, personadireccionid, persona, expedientemedico
            orm.PacienteDAO.save(lormPaciente);
            orm.Patron lormPatron = orm.PatronDAO.createPatron();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : juego_patron
            orm.PatronDAO.save(lormPatron);
            orm.Juego lormJuego = orm.JuegoDAO.createJuego();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : datosdispocitivo_juego, juego_patron
            orm.JuegoDAO.save(lormJuego);
            orm.Apoderado lormApoderado = orm.ApoderadoDAO.createApoderado();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : apoderado_paciente, personadireccionid, persona
            orm.ApoderadoDAO.save(lormApoderado);
            orm.Organizacion lormOrganizacion = orm.OrganizacionDAO.createOrganizacion();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : terapeuta, asigdisporganizacion, dispositivo_organizacion, direccion, telefono
            orm.OrganizacionDAO.save(lormOrganizacion);
            orm.Pais lormPais = orm.PaisDAO.createPais();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : direccion
            orm.PaisDAO.save(lormPais);
            orm.Region lormRegion = orm.RegionDAO.createRegion();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : direccion
            orm.RegionDAO.save(lormRegion);
            orm.Telefono lormTelefono = orm.TelefonoDAO.createTelefono();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : organizacion
            orm.TelefonoDAO.save(lormTelefono);
            orm.Direccion lormDireccion = orm.DireccionDAO.createDireccion();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : organizacion, region, pais
            orm.DireccionDAO.save(lormDireccion);
            orm.Expedientemedico lormExpedientemedico = orm.ExpedientemedicoDAO.createExpedientemedico();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : paciente
            orm.ExpedientemedicoDAO.save(lormExpedientemedico);
            orm.Clienteterapeuta_paciente lormClienteterapeuta_paciente = orm.Clienteterapeuta_pacienteDAO.createClienteterapeuta_paciente();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : paciente, clienteterapeuta
            orm.Clienteterapeuta_pacienteDAO.save(lormClienteterapeuta_paciente);
            orm.Apoderado_paciente lormApoderado_paciente = orm.Apoderado_pacienteDAO.createApoderado_paciente();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : paciente, apoderado
            orm.Apoderado_pacienteDAO.save(lormApoderado_paciente);
            orm.Asigdispcliente lormAsigdispcliente = orm.AsigdispclienteDAO.createAsigdispcliente();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : detalleasignacion_asigdispcliente, paciente, clienteterapeuta
            orm.AsigdispclienteDAO.save(lormAsigdispcliente);
            orm.Clienteterapeuta_dispositivo lormClienteterapeuta_dispositivo = orm.Clienteterapeuta_dispositivoDAO.createClienteterapeuta_dispositivo();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : dispositivo, clienteterapeuta
            orm.Clienteterapeuta_dispositivoDAO.save(lormClienteterapeuta_dispositivo);
            orm.Dispositivo lormDispositivo = orm.DispositivoDAO.createDispositivo();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asigdisporganizacion_dispositivo, detasigterapeuta, detalleasignacion, asigdisporganizacion, dispositivo_organizacion, clienteterapeuta_dispositivo, dotosdispositivo, tipodispositivo
            orm.DispositivoDAO.save(lormDispositivo);
            orm.Tipodispositivo lormTipodispositivo = orm.TipodispositivoDAO.createTipodispositivo();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : dispositivo
            orm.TipodispositivoDAO.save(lormTipodispositivo);
            orm.Datosdispositivo lormDatosdispositivo = orm.DatosdispositivoDAO.createDatosdispositivo();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : datosdispocitivo_juego, datosdispocitivo_datosdeusodiario, dispositivo, datosnivelcompletado, datosusonivel, datosusogeneral
            orm.DatosdispositivoDAO.save(lormDatosdispositivo);
            orm.Juego_patron lormJuego_patron = orm.Juego_patronDAO.createJuego_patron();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : patron, juego
            orm.Juego_patronDAO.save(lormJuego_patron);
            orm.Terapeuta lormTerapeuta = orm.TerapeutaDAO.createTerapeuta();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asigdispterapeuta, evaluacionterapeuta, asigdisporganizacion, terapeuta_cargo, paciente_terapeuta, personadireccionid, organizacion, persona
            orm.TerapeutaDAO.save(lormTerapeuta);
            orm.Cargo lormCargo = orm.CargoDAO.createCargo();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : terapeuta_cargo
            orm.CargoDAO.save(lormCargo);
            orm.Paciente_terapeuta lormPaciente_terapeuta = orm.Paciente_terapeutaDAO.createPaciente_terapeuta();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : terapeuta, paciente
            orm.Paciente_terapeutaDAO.save(lormPaciente_terapeuta);
            orm.Dispositivo_organizacion lormDispositivo_organizacion = orm.Dispositivo_organizacionDAO.createDispositivo_organizacion();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : organizacion, dispositivo
            orm.Dispositivo_organizacionDAO.save(lormDispositivo_organizacion);
            orm.Terapeuta_cargo lormTerapeuta_cargo = orm.Terapeuta_cargoDAO.createTerapeuta_cargo();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : cargo, terapeuta
            orm.Terapeuta_cargoDAO.save(lormTerapeuta_cargo);
            orm.Asigdisporganizacion lormAsigdisporganizacion = orm.AsigdisporganizacionDAO.createAsigdisporganizacion();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asigdisporganizacion_dispositivo, dispositivo, terapeuta, organizacion
            orm.AsigdisporganizacionDAO.save(lormAsigdisporganizacion);
            orm.Detalleasignacion lormDetalleasignacion = orm.DetalleasignacionDAO.createDetalleasignacion();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : detalleasignacion_asigdispcliente, dispositivo
            orm.DetalleasignacionDAO.save(lormDetalleasignacion);
            orm.Evaluacionterapeuta lormEvaluacionterapeuta = orm.EvaluacionterapeutaDAO.createEvaluacionterapeuta();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : paciente, terapeuta
            orm.EvaluacionterapeutaDAO.save(lormEvaluacionterapeuta);
            orm.Asigdispterapeuta lormAsigdispterapeuta = orm.AsigdispterapeutaDAO.createAsigdispterapeuta();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asigdispterapeuta_detasigterapeuta, terapeuta, paciente
            orm.AsigdispterapeutaDAO.save(lormAsigdispterapeuta);
            orm.Detasigterapeuta lormDetasigterapeuta = orm.DetasigterapeutaDAO.createDetasigterapeuta();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asigdispterapeuta_detasigterapeuta, dispositivo
            orm.DetasigterapeutaDAO.save(lormDetasigterapeuta);
            orm.Asigdispterapeuta_detasigterapeuta lormAsigdispterapeuta_detasigterapeuta = orm.Asigdispterapeuta_detasigterapeutaDAO.createAsigdispterapeuta_detasigterapeuta();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : detasigterapeuta, asigdispterapeuta
            orm.Asigdispterapeuta_detasigterapeutaDAO.save(lormAsigdispterapeuta_detasigterapeuta);
            orm.Detalleasignacion_asigdispcliente lormDetalleasignacion_asigdispcliente = orm.Detalleasignacion_asigdispclienteDAO.createDetalleasignacion_asigdispcliente();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asigdispcliente, detalleasignacion
            orm.Detalleasignacion_asigdispclienteDAO.save(lormDetalleasignacion_asigdispcliente);
            orm.Datosdispocitivo_datosdeusodiario lormDatosdispocitivo_datosdeusodiario = orm.Datosdispocitivo_datosdeusodiarioDAO.createDatosdispocitivo_datosdeusodiario();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : datosdeusodiario, datosdispocitivo
            orm.Datosdispocitivo_datosdeusodiarioDAO.save(lormDatosdispocitivo_datosdeusodiario);
            orm.Datosdispocitivo_juego lormDatosdispocitivo_juego = orm.Datosdispocitivo_juegoDAO.createDatosdispocitivo_juego();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : juego, datosdispocitivo
            orm.Datosdispocitivo_juegoDAO.save(lormDatosdispocitivo_juego);
            orm.Asigdisporganizacion_dispositivo lormAsigdisporganizacion_dispositivo = orm.Asigdisporganizacion_dispositivoDAO.createAsigdisporganizacion_dispositivo();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : dispositivo, asigdisporganizacion
            orm.Asigdisporganizacion_dispositivoDAO.save(lormAsigdisporganizacion_dispositivo);
            orm.Persona lormPersona = orm.PersonaDAO.createPersona();
            // TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : terapeuta, apoderado, clienteterapeuta, paciente
            orm.PersonaDAO.save(lormPersona);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }

    }

    public static void main(String[] args) {
        try {
            CreateMiProyectoProgramacionAvanzadaData createMiProyectoProgramacionAvanzadaData = new CreateMiProyectoProgramacionAvanzadaData();
            try {
                createMiProyectoProgramacionAvanzadaData.createTestData();
            } finally {
                orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().disposePersistentManager();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
