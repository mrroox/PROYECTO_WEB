/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;

public class ListMiProyectoProgramacionAvanzadaData {

    private static final int ROW_COUNT = 100;

    public void listTestData() throws PersistentException {
        System.out.println("Listing Datosnivelcompletado...");
        orm.Datosnivelcompletado[] ormDatosnivelcompletados = orm.DatosnivelcompletadoDAO.listDatosnivelcompletadoByQuery(null, null);
        int length = Math.min(ormDatosnivelcompletados.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosnivelcompletados[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Datosdeusodiario...");
        orm.Datosdeusodiario[] ormDatosdeusodiarios = orm.DatosdeusodiarioDAO.listDatosdeusodiarioByQuery(null, null);
        length = Math.min(ormDatosdeusodiarios.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdeusodiarios[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Datosusonivel...");
        orm.Datosusonivel[] ormDatosusonivels = orm.DatosusonivelDAO.listDatosusonivelByQuery(null, null);
        length = Math.min(ormDatosusonivels.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosusonivels[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Clienteterapeuta...");
        orm.Clienteterapeuta[] ormClienteterapeutas = orm.ClienteterapeutaDAO.listClienteterapeutaByQuery(null, null);
        length = Math.min(ormClienteterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormClienteterapeutas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Datosusogeneral...");
        orm.Datosusogeneral[] ormDatosusogenerals = orm.DatosusogeneralDAO.listDatosusogeneralByQuery(null, null);
        length = Math.min(ormDatosusogenerals.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosusogenerals[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Evaluacion...");
        orm.Evaluacion[] ormEvaluacions = orm.EvaluacionDAO.listEvaluacionByQuery(null, null);
        length = Math.min(ormEvaluacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormEvaluacions[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Paciente...");
        orm.Paciente[] ormPacientes = orm.PacienteDAO.listPacienteByQuery(null, null);
        length = Math.min(ormPacientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPacientes[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Patron...");
        orm.Patron[] ormPatrons = orm.PatronDAO.listPatronByQuery(null, null);
        length = Math.min(ormPatrons.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPatrons[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Juego...");
        orm.Juego[] ormJuegos = orm.JuegoDAO.listJuegoByQuery(null, null);
        length = Math.min(ormJuegos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormJuegos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Apoderado...");
        orm.Apoderado[] ormApoderados = orm.ApoderadoDAO.listApoderadoByQuery(null, null);
        length = Math.min(ormApoderados.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormApoderados[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Organizacion...");
        orm.Organizacion[] ormOrganizacions = orm.OrganizacionDAO.listOrganizacionByQuery(null, null);
        length = Math.min(ormOrganizacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormOrganizacions[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Pais...");
        orm.Pais[] ormPaises = orm.PaisDAO.listPaisByQuery(null, null);
        length = Math.min(ormPaises.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPaises[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Region...");
        orm.Region[] ormRegions = orm.RegionDAO.listRegionByQuery(null, null);
        length = Math.min(ormRegions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormRegions[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Telefono...");
        orm.Telefono[] ormTelefonos = orm.TelefonoDAO.listTelefonoByQuery(null, null);
        length = Math.min(ormTelefonos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTelefonos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Direccion...");
        orm.Direccion[] ormDireccions = orm.DireccionDAO.listDireccionByQuery(null, null);
        length = Math.min(ormDireccions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDireccions[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Expedientemedico...");
        orm.Expedientemedico[] ormExpedientemedicos = orm.ExpedientemedicoDAO.listExpedientemedicoByQuery(null, null);
        length = Math.min(ormExpedientemedicos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormExpedientemedicos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Clienteterapeuta_paciente...");
        orm.Clienteterapeuta_paciente[] ormClienteterapeuta_pacientes = orm.Clienteterapeuta_pacienteDAO.listClienteterapeuta_pacienteByQuery(null, null);
        length = Math.min(ormClienteterapeuta_pacientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormClienteterapeuta_pacientes[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Apoderado_paciente...");
        orm.Apoderado_paciente[] ormApoderado_pacientes = orm.Apoderado_pacienteDAO.listApoderado_pacienteByQuery(null, null);
        length = Math.min(ormApoderado_pacientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormApoderado_pacientes[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Asigdispcliente...");
        orm.Asigdispcliente[] ormAsigdispclientes = orm.AsigdispclienteDAO.listAsigdispclienteByQuery(null, null);
        length = Math.min(ormAsigdispclientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdispclientes[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Clienteterapeuta_dispositivo...");
        orm.Clienteterapeuta_dispositivo[] ormClienteterapeuta_dispositivos = orm.Clienteterapeuta_dispositivoDAO.listClienteterapeuta_dispositivoByQuery(null, null);
        length = Math.min(ormClienteterapeuta_dispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormClienteterapeuta_dispositivos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Dispositivo...");
        orm.Dispositivo[] ormDispositivos = orm.DispositivoDAO.listDispositivoByQuery(null, null);
        length = Math.min(ormDispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDispositivos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Tipodispositivo...");
        orm.Tipodispositivo[] ormTipodispositivos = orm.TipodispositivoDAO.listTipodispositivoByQuery(null, null);
        length = Math.min(ormTipodispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTipodispositivos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Datosdispositivo...");
        orm.Datosdispositivo[] ormDatosdispositivos = orm.DatosdispositivoDAO.listDatosdispositivoByQuery(null, null);
        length = Math.min(ormDatosdispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdispositivos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Juego_patron...");
        orm.Juego_patron[] ormJuego_patrons = orm.Juego_patronDAO.listJuego_patronByQuery(null, null);
        length = Math.min(ormJuego_patrons.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormJuego_patrons[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Terapeuta...");
        orm.Terapeuta[] ormTerapeutas = orm.TerapeutaDAO.listTerapeutaByQuery(null, null);
        length = Math.min(ormTerapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTerapeutas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Cargo...");
        orm.Cargo[] ormCargos = orm.CargoDAO.listCargoByQuery(null, null);
        length = Math.min(ormCargos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormCargos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Paciente_terapeuta...");
        orm.Paciente_terapeuta[] ormPaciente_terapeutas = orm.Paciente_terapeutaDAO.listPaciente_terapeutaByQuery(null, null);
        length = Math.min(ormPaciente_terapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPaciente_terapeutas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Dispositivo_organizacion...");
        orm.Dispositivo_organizacion[] ormDispositivo_organizacions = orm.Dispositivo_organizacionDAO.listDispositivo_organizacionByQuery(null, null);
        length = Math.min(ormDispositivo_organizacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDispositivo_organizacions[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Terapeuta_cargo...");
        orm.Terapeuta_cargo[] ormTerapeuta_cargos = orm.Terapeuta_cargoDAO.listTerapeuta_cargoByQuery(null, null);
        length = Math.min(ormTerapeuta_cargos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTerapeuta_cargos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Asigdisporganizacion...");
        orm.Asigdisporganizacion[] ormAsigdisporganizacions = orm.AsigdisporganizacionDAO.listAsigdisporganizacionByQuery(null, null);
        length = Math.min(ormAsigdisporganizacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdisporganizacions[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Detalleasignacion...");
        orm.Detalleasignacion[] ormDetalleasignacions = orm.DetalleasignacionDAO.listDetalleasignacionByQuery(null, null);
        length = Math.min(ormDetalleasignacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDetalleasignacions[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Evaluacionterapeuta...");
        orm.Evaluacionterapeuta[] ormEvaluacionterapeutas = orm.EvaluacionterapeutaDAO.listEvaluacionterapeutaByQuery(null, null);
        length = Math.min(ormEvaluacionterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormEvaluacionterapeutas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Asigdispterapeuta...");
        orm.Asigdispterapeuta[] ormAsigdispterapeutas = orm.AsigdispterapeutaDAO.listAsigdispterapeutaByQuery(null, null);
        length = Math.min(ormAsigdispterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdispterapeutas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Detasigterapeuta...");
        orm.Detasigterapeuta[] ormDetasigterapeutas = orm.DetasigterapeutaDAO.listDetasigterapeutaByQuery(null, null);
        length = Math.min(ormDetasigterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDetasigterapeutas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Asigdispterapeuta_detasigterapeuta...");
        orm.Asigdispterapeuta_detasigterapeuta[] ormAsigdispterapeuta_detasigterapeutas = orm.Asigdispterapeuta_detasigterapeutaDAO.listAsigdispterapeuta_detasigterapeutaByQuery(null, null);
        length = Math.min(ormAsigdispterapeuta_detasigterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdispterapeuta_detasigterapeutas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Detalleasignacion_asigdispcliente...");
        orm.Detalleasignacion_asigdispcliente[] ormDetalleasignacion_asigdispclientes = orm.Detalleasignacion_asigdispclienteDAO.listDetalleasignacion_asigdispclienteByQuery(null, null);
        length = Math.min(ormDetalleasignacion_asigdispclientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDetalleasignacion_asigdispclientes[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Datosdispocitivo_datosdeusodiario...");
        orm.Datosdispocitivo_datosdeusodiario[] ormDatosdispocitivo_datosdeusodiarios = orm.Datosdispocitivo_datosdeusodiarioDAO.listDatosdispocitivo_datosdeusodiarioByQuery(null, null);
        length = Math.min(ormDatosdispocitivo_datosdeusodiarios.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdispocitivo_datosdeusodiarios[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Datosdispocitivo_juego...");
        orm.Datosdispocitivo_juego[] ormDatosdispocitivo_juegos = orm.Datosdispocitivo_juegoDAO.listDatosdispocitivo_juegoByQuery(null, null);
        length = Math.min(ormDatosdispocitivo_juegos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdispocitivo_juegos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Asigdisporganizacion_dispositivo...");
        orm.Asigdisporganizacion_dispositivo[] ormAsigdisporganizacion_dispositivos = orm.Asigdisporganizacion_dispositivoDAO.listAsigdisporganizacion_dispositivoByQuery(null, null);
        length = Math.min(ormAsigdisporganizacion_dispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdisporganizacion_dispositivos[i]);
        }
        System.out.println(length + " record(s) retrieved.");

        System.out.println("Listing Persona...");
        orm.Persona[] ormPersonas = orm.PersonaDAO.listPersonaByQuery(null, null);
        length = Math.min(ormPersonas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPersonas[i]);
        }
        System.out.println(length + " record(s) retrieved.");

    }

    public void listByCriteria() throws PersistentException {
        System.out.println("Listing Datosnivelcompletado by Criteria...");
        orm.DatosnivelcompletadoCriteria lormDatosnivelcompletadoCriteria = new orm.DatosnivelcompletadoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDatosnivelcompletadoCriteria.id.eq();
        lormDatosnivelcompletadoCriteria.setMaxResults(ROW_COUNT);
        orm.Datosnivelcompletado[] ormDatosnivelcompletados = lormDatosnivelcompletadoCriteria.listDatosnivelcompletado();
        int length = ormDatosnivelcompletados == null ? 0 : Math.min(ormDatosnivelcompletados.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosnivelcompletados[i]);
        }
        System.out.println(length + " Datosnivelcompletado record(s) retrieved.");

        System.out.println("Listing Datosdeusodiario by Criteria...");
        orm.DatosdeusodiarioCriteria lormDatosdeusodiarioCriteria = new orm.DatosdeusodiarioCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDatosdeusodiarioCriteria.id.eq();
        lormDatosdeusodiarioCriteria.setMaxResults(ROW_COUNT);
        orm.Datosdeusodiario[] ormDatosdeusodiarios = lormDatosdeusodiarioCriteria.listDatosdeusodiario();
        length = ormDatosdeusodiarios == null ? 0 : Math.min(ormDatosdeusodiarios.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdeusodiarios[i]);
        }
        System.out.println(length + " Datosdeusodiario record(s) retrieved.");

        System.out.println("Listing Datosusonivel by Criteria...");
        orm.DatosusonivelCriteria lormDatosusonivelCriteria = new orm.DatosusonivelCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDatosusonivelCriteria.id.eq();
        lormDatosusonivelCriteria.setMaxResults(ROW_COUNT);
        orm.Datosusonivel[] ormDatosusonivels = lormDatosusonivelCriteria.listDatosusonivel();
        length = ormDatosusonivels == null ? 0 : Math.min(ormDatosusonivels.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosusonivels[i]);
        }
        System.out.println(length + " Datosusonivel record(s) retrieved.");

        System.out.println("Listing Clienteterapeuta by Criteria...");
        orm.ClienteterapeutaCriteria lormClienteterapeutaCriteria = new orm.ClienteterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormClienteterapeutaCriteria.id.eq();
        lormClienteterapeutaCriteria.setMaxResults(ROW_COUNT);
        orm.Clienteterapeuta[] ormClienteterapeutas = lormClienteterapeutaCriteria.listClienteterapeuta();
        length = ormClienteterapeutas == null ? 0 : Math.min(ormClienteterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormClienteterapeutas[i]);
        }
        System.out.println(length + " Clienteterapeuta record(s) retrieved.");

        System.out.println("Listing Datosusogeneral by Criteria...");
        orm.DatosusogeneralCriteria lormDatosusogeneralCriteria = new orm.DatosusogeneralCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDatosusogeneralCriteria.id.eq();
        lormDatosusogeneralCriteria.setMaxResults(ROW_COUNT);
        orm.Datosusogeneral[] ormDatosusogenerals = lormDatosusogeneralCriteria.listDatosusogeneral();
        length = ormDatosusogenerals == null ? 0 : Math.min(ormDatosusogenerals.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosusogenerals[i]);
        }
        System.out.println(length + " Datosusogeneral record(s) retrieved.");

        System.out.println("Listing Evaluacion by Criteria...");
        orm.EvaluacionCriteria lormEvaluacionCriteria = new orm.EvaluacionCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormEvaluacionCriteria.id.eq();
        lormEvaluacionCriteria.setMaxResults(ROW_COUNT);
        orm.Evaluacion[] ormEvaluacions = lormEvaluacionCriteria.listEvaluacion();
        length = ormEvaluacions == null ? 0 : Math.min(ormEvaluacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormEvaluacions[i]);
        }
        System.out.println(length + " Evaluacion record(s) retrieved.");

        System.out.println("Listing Paciente by Criteria...");
        orm.PacienteCriteria lormPacienteCriteria = new orm.PacienteCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormPacienteCriteria.id.eq();
        lormPacienteCriteria.setMaxResults(ROW_COUNT);
        orm.Paciente[] ormPacientes = lormPacienteCriteria.listPaciente();
        length = ormPacientes == null ? 0 : Math.min(ormPacientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPacientes[i]);
        }
        System.out.println(length + " Paciente record(s) retrieved.");

        System.out.println("Listing Patron by Criteria...");
        orm.PatronCriteria lormPatronCriteria = new orm.PatronCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormPatronCriteria.id.eq();
        lormPatronCriteria.setMaxResults(ROW_COUNT);
        orm.Patron[] ormPatrons = lormPatronCriteria.listPatron();
        length = ormPatrons == null ? 0 : Math.min(ormPatrons.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPatrons[i]);
        }
        System.out.println(length + " Patron record(s) retrieved.");

        System.out.println("Listing Juego by Criteria...");
        orm.JuegoCriteria lormJuegoCriteria = new orm.JuegoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormJuegoCriteria.id.eq();
        lormJuegoCriteria.setMaxResults(ROW_COUNT);
        orm.Juego[] ormJuegos = lormJuegoCriteria.listJuego();
        length = ormJuegos == null ? 0 : Math.min(ormJuegos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormJuegos[i]);
        }
        System.out.println(length + " Juego record(s) retrieved.");

        System.out.println("Listing Apoderado by Criteria...");
        orm.ApoderadoCriteria lormApoderadoCriteria = new orm.ApoderadoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormApoderadoCriteria.id.eq();
        lormApoderadoCriteria.setMaxResults(ROW_COUNT);
        orm.Apoderado[] ormApoderados = lormApoderadoCriteria.listApoderado();
        length = ormApoderados == null ? 0 : Math.min(ormApoderados.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormApoderados[i]);
        }
        System.out.println(length + " Apoderado record(s) retrieved.");

        System.out.println("Listing Organizacion by Criteria...");
        orm.OrganizacionCriteria lormOrganizacionCriteria = new orm.OrganizacionCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormOrganizacionCriteria.id.eq();
        lormOrganizacionCriteria.setMaxResults(ROW_COUNT);
        orm.Organizacion[] ormOrganizacions = lormOrganizacionCriteria.listOrganizacion();
        length = ormOrganizacions == null ? 0 : Math.min(ormOrganizacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormOrganizacions[i]);
        }
        System.out.println(length + " Organizacion record(s) retrieved.");

        System.out.println("Listing Pais by Criteria...");
        orm.PaisCriteria lormPaisCriteria = new orm.PaisCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormPaisCriteria.id.eq();
        lormPaisCriteria.setMaxResults(ROW_COUNT);
        orm.Pais[] ormPaises = lormPaisCriteria.listPais();
        length = ormPaises == null ? 0 : Math.min(ormPaises.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPaises[i]);
        }
        System.out.println(length + " Pais record(s) retrieved.");

        System.out.println("Listing Region by Criteria...");
        orm.RegionCriteria lormRegionCriteria = new orm.RegionCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormRegionCriteria.id.eq();
        lormRegionCriteria.setMaxResults(ROW_COUNT);
        orm.Region[] ormRegions = lormRegionCriteria.listRegion();
        length = ormRegions == null ? 0 : Math.min(ormRegions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormRegions[i]);
        }
        System.out.println(length + " Region record(s) retrieved.");

        System.out.println("Listing Telefono by Criteria...");
        orm.TelefonoCriteria lormTelefonoCriteria = new orm.TelefonoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormTelefonoCriteria.id.eq();
        lormTelefonoCriteria.setMaxResults(ROW_COUNT);
        orm.Telefono[] ormTelefonos = lormTelefonoCriteria.listTelefono();
        length = ormTelefonos == null ? 0 : Math.min(ormTelefonos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTelefonos[i]);
        }
        System.out.println(length + " Telefono record(s) retrieved.");

        System.out.println("Listing Direccion by Criteria...");
        orm.DireccionCriteria lormDireccionCriteria = new orm.DireccionCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDireccionCriteria.id.eq();
        lormDireccionCriteria.setMaxResults(ROW_COUNT);
        orm.Direccion[] ormDireccions = lormDireccionCriteria.listDireccion();
        length = ormDireccions == null ? 0 : Math.min(ormDireccions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDireccions[i]);
        }
        System.out.println(length + " Direccion record(s) retrieved.");

        System.out.println("Listing Expedientemedico by Criteria...");
        orm.ExpedientemedicoCriteria lormExpedientemedicoCriteria = new orm.ExpedientemedicoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormExpedientemedicoCriteria.id.eq();
        lormExpedientemedicoCriteria.setMaxResults(ROW_COUNT);
        orm.Expedientemedico[] ormExpedientemedicos = lormExpedientemedicoCriteria.listExpedientemedico();
        length = ormExpedientemedicos == null ? 0 : Math.min(ormExpedientemedicos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormExpedientemedicos[i]);
        }
        System.out.println(length + " Expedientemedico record(s) retrieved.");

        System.out.println("Listing Clienteterapeuta_paciente by Criteria...");
        orm.Clienteterapeuta_pacienteCriteria lormClienteterapeuta_pacienteCriteria = new orm.Clienteterapeuta_pacienteCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormClienteterapeuta_pacienteCriteria.id.eq();
        lormClienteterapeuta_pacienteCriteria.setMaxResults(ROW_COUNT);
        orm.Clienteterapeuta_paciente[] ormClienteterapeuta_pacientes = lormClienteterapeuta_pacienteCriteria.listClienteterapeuta_paciente();
        length = ormClienteterapeuta_pacientes == null ? 0 : Math.min(ormClienteterapeuta_pacientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormClienteterapeuta_pacientes[i]);
        }
        System.out.println(length + " Clienteterapeuta_paciente record(s) retrieved.");

        System.out.println("Listing Apoderado_paciente by Criteria...");
        orm.Apoderado_pacienteCriteria lormApoderado_pacienteCriteria = new orm.Apoderado_pacienteCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormApoderado_pacienteCriteria.id.eq();
        lormApoderado_pacienteCriteria.setMaxResults(ROW_COUNT);
        orm.Apoderado_paciente[] ormApoderado_pacientes = lormApoderado_pacienteCriteria.listApoderado_paciente();
        length = ormApoderado_pacientes == null ? 0 : Math.min(ormApoderado_pacientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormApoderado_pacientes[i]);
        }
        System.out.println(length + " Apoderado_paciente record(s) retrieved.");

        System.out.println("Listing Asigdispcliente by Criteria...");
        orm.AsigdispclienteCriteria lormAsigdispclienteCriteria = new orm.AsigdispclienteCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormAsigdispclienteCriteria.id.eq();
        lormAsigdispclienteCriteria.setMaxResults(ROW_COUNT);
        orm.Asigdispcliente[] ormAsigdispclientes = lormAsigdispclienteCriteria.listAsigdispcliente();
        length = ormAsigdispclientes == null ? 0 : Math.min(ormAsigdispclientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdispclientes[i]);
        }
        System.out.println(length + " Asigdispcliente record(s) retrieved.");

        System.out.println("Listing Clienteterapeuta_dispositivo by Criteria...");
        orm.Clienteterapeuta_dispositivoCriteria lormClienteterapeuta_dispositivoCriteria = new orm.Clienteterapeuta_dispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormClienteterapeuta_dispositivoCriteria.id.eq();
        lormClienteterapeuta_dispositivoCriteria.setMaxResults(ROW_COUNT);
        orm.Clienteterapeuta_dispositivo[] ormClienteterapeuta_dispositivos = lormClienteterapeuta_dispositivoCriteria.listClienteterapeuta_dispositivo();
        length = ormClienteterapeuta_dispositivos == null ? 0 : Math.min(ormClienteterapeuta_dispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormClienteterapeuta_dispositivos[i]);
        }
        System.out.println(length + " Clienteterapeuta_dispositivo record(s) retrieved.");

        System.out.println("Listing Dispositivo by Criteria...");
        orm.DispositivoCriteria lormDispositivoCriteria = new orm.DispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDispositivoCriteria.id.eq();
        lormDispositivoCriteria.setMaxResults(ROW_COUNT);
        orm.Dispositivo[] ormDispositivos = lormDispositivoCriteria.listDispositivo();
        length = ormDispositivos == null ? 0 : Math.min(ormDispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDispositivos[i]);
        }
        System.out.println(length + " Dispositivo record(s) retrieved.");

        System.out.println("Listing Tipodispositivo by Criteria...");
        orm.TipodispositivoCriteria lormTipodispositivoCriteria = new orm.TipodispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormTipodispositivoCriteria.id.eq();
        lormTipodispositivoCriteria.setMaxResults(ROW_COUNT);
        orm.Tipodispositivo[] ormTipodispositivos = lormTipodispositivoCriteria.listTipodispositivo();
        length = ormTipodispositivos == null ? 0 : Math.min(ormTipodispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTipodispositivos[i]);
        }
        System.out.println(length + " Tipodispositivo record(s) retrieved.");

        System.out.println("Listing Datosdispositivo by Criteria...");
        orm.DatosdispositivoCriteria lormDatosdispositivoCriteria = new orm.DatosdispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDatosdispositivoCriteria.id.eq();
        lormDatosdispositivoCriteria.setMaxResults(ROW_COUNT);
        orm.Datosdispositivo[] ormDatosdispositivos = lormDatosdispositivoCriteria.listDatosdispositivo();
        length = ormDatosdispositivos == null ? 0 : Math.min(ormDatosdispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdispositivos[i]);
        }
        System.out.println(length + " Datosdispositivo record(s) retrieved.");

        System.out.println("Listing Juego_patron by Criteria...");
        orm.Juego_patronCriteria lormJuego_patronCriteria = new orm.Juego_patronCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormJuego_patronCriteria.id.eq();
        lormJuego_patronCriteria.setMaxResults(ROW_COUNT);
        orm.Juego_patron[] ormJuego_patrons = lormJuego_patronCriteria.listJuego_patron();
        length = ormJuego_patrons == null ? 0 : Math.min(ormJuego_patrons.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormJuego_patrons[i]);
        }
        System.out.println(length + " Juego_patron record(s) retrieved.");

        System.out.println("Listing Terapeuta by Criteria...");
        orm.TerapeutaCriteria lormTerapeutaCriteria = new orm.TerapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormTerapeutaCriteria.id.eq();
        lormTerapeutaCriteria.setMaxResults(ROW_COUNT);
        orm.Terapeuta[] ormTerapeutas = lormTerapeutaCriteria.listTerapeuta();
        length = ormTerapeutas == null ? 0 : Math.min(ormTerapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTerapeutas[i]);
        }
        System.out.println(length + " Terapeuta record(s) retrieved.");

        System.out.println("Listing Cargo by Criteria...");
        orm.CargoCriteria lormCargoCriteria = new orm.CargoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormCargoCriteria.id.eq();
        lormCargoCriteria.setMaxResults(ROW_COUNT);
        orm.Cargo[] ormCargos = lormCargoCriteria.listCargo();
        length = ormCargos == null ? 0 : Math.min(ormCargos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormCargos[i]);
        }
        System.out.println(length + " Cargo record(s) retrieved.");

        System.out.println("Listing Paciente_terapeuta by Criteria...");
        orm.Paciente_terapeutaCriteria lormPaciente_terapeutaCriteria = new orm.Paciente_terapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormPaciente_terapeutaCriteria.id.eq();
        lormPaciente_terapeutaCriteria.setMaxResults(ROW_COUNT);
        orm.Paciente_terapeuta[] ormPaciente_terapeutas = lormPaciente_terapeutaCriteria.listPaciente_terapeuta();
        length = ormPaciente_terapeutas == null ? 0 : Math.min(ormPaciente_terapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPaciente_terapeutas[i]);
        }
        System.out.println(length + " Paciente_terapeuta record(s) retrieved.");

        System.out.println("Listing Dispositivo_organizacion by Criteria...");
        orm.Dispositivo_organizacionCriteria lormDispositivo_organizacionCriteria = new orm.Dispositivo_organizacionCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDispositivo_organizacionCriteria.id.eq();
        lormDispositivo_organizacionCriteria.setMaxResults(ROW_COUNT);
        orm.Dispositivo_organizacion[] ormDispositivo_organizacions = lormDispositivo_organizacionCriteria.listDispositivo_organizacion();
        length = ormDispositivo_organizacions == null ? 0 : Math.min(ormDispositivo_organizacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDispositivo_organizacions[i]);
        }
        System.out.println(length + " Dispositivo_organizacion record(s) retrieved.");

        System.out.println("Listing Terapeuta_cargo by Criteria...");
        orm.Terapeuta_cargoCriteria lormTerapeuta_cargoCriteria = new orm.Terapeuta_cargoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormTerapeuta_cargoCriteria.id.eq();
        lormTerapeuta_cargoCriteria.setMaxResults(ROW_COUNT);
        orm.Terapeuta_cargo[] ormTerapeuta_cargos = lormTerapeuta_cargoCriteria.listTerapeuta_cargo();
        length = ormTerapeuta_cargos == null ? 0 : Math.min(ormTerapeuta_cargos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormTerapeuta_cargos[i]);
        }
        System.out.println(length + " Terapeuta_cargo record(s) retrieved.");

        System.out.println("Listing Asigdisporganizacion by Criteria...");
        orm.AsigdisporganizacionCriteria lormAsigdisporganizacionCriteria = new orm.AsigdisporganizacionCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormAsigdisporganizacionCriteria.id.eq();
        lormAsigdisporganizacionCriteria.setMaxResults(ROW_COUNT);
        orm.Asigdisporganizacion[] ormAsigdisporganizacions = lormAsigdisporganizacionCriteria.listAsigdisporganizacion();
        length = ormAsigdisporganizacions == null ? 0 : Math.min(ormAsigdisporganizacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdisporganizacions[i]);
        }
        System.out.println(length + " Asigdisporganizacion record(s) retrieved.");

        System.out.println("Listing Detalleasignacion by Criteria...");
        orm.DetalleasignacionCriteria lormDetalleasignacionCriteria = new orm.DetalleasignacionCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDetalleasignacionCriteria.id.eq();
        lormDetalleasignacionCriteria.setMaxResults(ROW_COUNT);
        orm.Detalleasignacion[] ormDetalleasignacions = lormDetalleasignacionCriteria.listDetalleasignacion();
        length = ormDetalleasignacions == null ? 0 : Math.min(ormDetalleasignacions.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDetalleasignacions[i]);
        }
        System.out.println(length + " Detalleasignacion record(s) retrieved.");

        System.out.println("Listing Evaluacionterapeuta by Criteria...");
        orm.EvaluacionterapeutaCriteria lormEvaluacionterapeutaCriteria = new orm.EvaluacionterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormEvaluacionterapeutaCriteria.id.eq();
        lormEvaluacionterapeutaCriteria.setMaxResults(ROW_COUNT);
        orm.Evaluacionterapeuta[] ormEvaluacionterapeutas = lormEvaluacionterapeutaCriteria.listEvaluacionterapeuta();
        length = ormEvaluacionterapeutas == null ? 0 : Math.min(ormEvaluacionterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormEvaluacionterapeutas[i]);
        }
        System.out.println(length + " Evaluacionterapeuta record(s) retrieved.");

        System.out.println("Listing Asigdispterapeuta by Criteria...");
        orm.AsigdispterapeutaCriteria lormAsigdispterapeutaCriteria = new orm.AsigdispterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormAsigdispterapeutaCriteria.id.eq();
        lormAsigdispterapeutaCriteria.setMaxResults(ROW_COUNT);
        orm.Asigdispterapeuta[] ormAsigdispterapeutas = lormAsigdispterapeutaCriteria.listAsigdispterapeuta();
        length = ormAsigdispterapeutas == null ? 0 : Math.min(ormAsigdispterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdispterapeutas[i]);
        }
        System.out.println(length + " Asigdispterapeuta record(s) retrieved.");

        System.out.println("Listing Detasigterapeuta by Criteria...");
        orm.DetasigterapeutaCriteria lormDetasigterapeutaCriteria = new orm.DetasigterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDetasigterapeutaCriteria.id.eq();
        lormDetasigterapeutaCriteria.setMaxResults(ROW_COUNT);
        orm.Detasigterapeuta[] ormDetasigterapeutas = lormDetasigterapeutaCriteria.listDetasigterapeuta();
        length = ormDetasigterapeutas == null ? 0 : Math.min(ormDetasigterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDetasigterapeutas[i]);
        }
        System.out.println(length + " Detasigterapeuta record(s) retrieved.");

        System.out.println("Listing Asigdispterapeuta_detasigterapeuta by Criteria...");
        orm.Asigdispterapeuta_detasigterapeutaCriteria lormAsigdispterapeuta_detasigterapeutaCriteria = new orm.Asigdispterapeuta_detasigterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormAsigdispterapeuta_detasigterapeutaCriteria.id.eq();
        lormAsigdispterapeuta_detasigterapeutaCriteria.setMaxResults(ROW_COUNT);
        orm.Asigdispterapeuta_detasigterapeuta[] ormAsigdispterapeuta_detasigterapeutas = lormAsigdispterapeuta_detasigterapeutaCriteria.listAsigdispterapeuta_detasigterapeuta();
        length = ormAsigdispterapeuta_detasigterapeutas == null ? 0 : Math.min(ormAsigdispterapeuta_detasigterapeutas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdispterapeuta_detasigterapeutas[i]);
        }
        System.out.println(length + " Asigdispterapeuta_detasigterapeuta record(s) retrieved.");

        System.out.println("Listing Detalleasignacion_asigdispcliente by Criteria...");
        orm.Detalleasignacion_asigdispclienteCriteria lormDetalleasignacion_asigdispclienteCriteria = new orm.Detalleasignacion_asigdispclienteCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDetalleasignacion_asigdispclienteCriteria.id.eq();
        lormDetalleasignacion_asigdispclienteCriteria.setMaxResults(ROW_COUNT);
        orm.Detalleasignacion_asigdispcliente[] ormDetalleasignacion_asigdispclientes = lormDetalleasignacion_asigdispclienteCriteria.listDetalleasignacion_asigdispcliente();
        length = ormDetalleasignacion_asigdispclientes == null ? 0 : Math.min(ormDetalleasignacion_asigdispclientes.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDetalleasignacion_asigdispclientes[i]);
        }
        System.out.println(length + " Detalleasignacion_asigdispcliente record(s) retrieved.");

        System.out.println("Listing Datosdispocitivo_datosdeusodiario by Criteria...");
        orm.Datosdispocitivo_datosdeusodiarioCriteria lormDatosdispocitivo_datosdeusodiarioCriteria = new orm.Datosdispocitivo_datosdeusodiarioCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDatosdispocitivo_datosdeusodiarioCriteria.id.eq();
        lormDatosdispocitivo_datosdeusodiarioCriteria.setMaxResults(ROW_COUNT);
        orm.Datosdispocitivo_datosdeusodiario[] ormDatosdispocitivo_datosdeusodiarios = lormDatosdispocitivo_datosdeusodiarioCriteria.listDatosdispocitivo_datosdeusodiario();
        length = ormDatosdispocitivo_datosdeusodiarios == null ? 0 : Math.min(ormDatosdispocitivo_datosdeusodiarios.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdispocitivo_datosdeusodiarios[i]);
        }
        System.out.println(length + " Datosdispocitivo_datosdeusodiario record(s) retrieved.");

        System.out.println("Listing Datosdispocitivo_juego by Criteria...");
        orm.Datosdispocitivo_juegoCriteria lormDatosdispocitivo_juegoCriteria = new orm.Datosdispocitivo_juegoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormDatosdispocitivo_juegoCriteria.id.eq();
        lormDatosdispocitivo_juegoCriteria.setMaxResults(ROW_COUNT);
        orm.Datosdispocitivo_juego[] ormDatosdispocitivo_juegos = lormDatosdispocitivo_juegoCriteria.listDatosdispocitivo_juego();
        length = ormDatosdispocitivo_juegos == null ? 0 : Math.min(ormDatosdispocitivo_juegos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormDatosdispocitivo_juegos[i]);
        }
        System.out.println(length + " Datosdispocitivo_juego record(s) retrieved.");

        System.out.println("Listing Asigdisporganizacion_dispositivo by Criteria...");
        orm.Asigdisporganizacion_dispositivoCriteria lormAsigdisporganizacion_dispositivoCriteria = new orm.Asigdisporganizacion_dispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormAsigdisporganizacion_dispositivoCriteria.id.eq();
        lormAsigdisporganizacion_dispositivoCriteria.setMaxResults(ROW_COUNT);
        orm.Asigdisporganizacion_dispositivo[] ormAsigdisporganizacion_dispositivos = lormAsigdisporganizacion_dispositivoCriteria.listAsigdisporganizacion_dispositivo();
        length = ormAsigdisporganizacion_dispositivos == null ? 0 : Math.min(ormAsigdisporganizacion_dispositivos.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormAsigdisporganizacion_dispositivos[i]);
        }
        System.out.println(length + " Asigdisporganizacion_dispositivo record(s) retrieved.");

        System.out.println("Listing Persona by Criteria...");
        orm.PersonaCriteria lormPersonaCriteria = new orm.PersonaCriteria();
        // Please uncomment the follow line and fill in parameter(s) 
        //lormPersonaCriteria.id.eq();
        lormPersonaCriteria.setMaxResults(ROW_COUNT);
        orm.Persona[] ormPersonas = lormPersonaCriteria.listPersona();
        length = ormPersonas == null ? 0 : Math.min(ormPersonas.length, ROW_COUNT);
        for (int i = 0; i < length; i++) {
            System.out.println(ormPersonas[i]);
        }
        System.out.println(length + " Persona record(s) retrieved.");

    }

    public static void main(String[] args) {
        try {
            ListMiProyectoProgramacionAvanzadaData listMiProyectoProgramacionAvanzadaData = new ListMiProyectoProgramacionAvanzadaData();
            try {
                listMiProyectoProgramacionAvanzadaData.listTestData();
                //listMiProyectoProgramacionAvanzadaData.listByCriteria();
            } finally {
                orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().disposePersistentManager();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
