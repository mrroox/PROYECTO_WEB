/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;

public class DeleteMiProyectoProgramacionAvanzadaData {

    public void deleteTestData() throws PersistentException {
        PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();
        try {
            orm.Datosnivelcompletado lormDatosnivelcompletado = orm.DatosnivelcompletadoDAO.loadDatosnivelcompletadoByQuery(null, null);
            // Delete the persistent object
            orm.DatosnivelcompletadoDAO.delete(lormDatosnivelcompletado);
            orm.Datosdeusodiario lormDatosdeusodiario = orm.DatosdeusodiarioDAO.loadDatosdeusodiarioByQuery(null, null);
            // Delete the persistent object
            orm.DatosdeusodiarioDAO.delete(lormDatosdeusodiario);
            orm.Datosusonivel lormDatosusonivel = orm.DatosusonivelDAO.loadDatosusonivelByQuery(null, null);
            // Delete the persistent object
            orm.DatosusonivelDAO.delete(lormDatosusonivel);
            orm.Clienteterapeuta lormClienteterapeuta = orm.ClienteterapeutaDAO.loadClienteterapeutaByQuery(null, null);
            // Delete the persistent object
            orm.ClienteterapeutaDAO.delete(lormClienteterapeuta);
            orm.Datosusogeneral lormDatosusogeneral = orm.DatosusogeneralDAO.loadDatosusogeneralByQuery(null, null);
            // Delete the persistent object
            orm.DatosusogeneralDAO.delete(lormDatosusogeneral);
            orm.Evaluacion lormEvaluacion = orm.EvaluacionDAO.loadEvaluacionByQuery(null, null);
            // Delete the persistent object
            orm.EvaluacionDAO.delete(lormEvaluacion);
            orm.Paciente lormPaciente = orm.PacienteDAO.loadPacienteByQuery(null, null);
            // Delete the persistent object
            orm.PacienteDAO.delete(lormPaciente);
            orm.Patron lormPatron = orm.PatronDAO.loadPatronByQuery(null, null);
            // Delete the persistent object
            orm.PatronDAO.delete(lormPatron);
            orm.Juego lormJuego = orm.JuegoDAO.loadJuegoByQuery(null, null);
            // Delete the persistent object
            orm.JuegoDAO.delete(lormJuego);
            orm.Apoderado lormApoderado = orm.ApoderadoDAO.loadApoderadoByQuery(null, null);
            // Delete the persistent object
            orm.ApoderadoDAO.delete(lormApoderado);
            orm.Organizacion lormOrganizacion = orm.OrganizacionDAO.loadOrganizacionByQuery(null, null);
            // Delete the persistent object
            orm.OrganizacionDAO.delete(lormOrganizacion);
            orm.Pais lormPais = orm.PaisDAO.loadPaisByQuery(null, null);
            // Delete the persistent object
            orm.PaisDAO.delete(lormPais);
            orm.Region lormRegion = orm.RegionDAO.loadRegionByQuery(null, null);
            // Delete the persistent object
            orm.RegionDAO.delete(lormRegion);
            orm.Telefono lormTelefono = orm.TelefonoDAO.loadTelefonoByQuery(null, null);
            // Delete the persistent object
            orm.TelefonoDAO.delete(lormTelefono);
            orm.Direccion lormDireccion = orm.DireccionDAO.loadDireccionByQuery(null, null);
            // Delete the persistent object
            orm.DireccionDAO.delete(lormDireccion);
            orm.Expedientemedico lormExpedientemedico = orm.ExpedientemedicoDAO.loadExpedientemedicoByQuery(null, null);
            // Delete the persistent object
            orm.ExpedientemedicoDAO.delete(lormExpedientemedico);
            orm.Clienteterapeuta_paciente lormClienteterapeuta_paciente = orm.Clienteterapeuta_pacienteDAO.loadClienteterapeuta_pacienteByQuery(null, null);
            // Delete the persistent object
            orm.Clienteterapeuta_pacienteDAO.delete(lormClienteterapeuta_paciente);
            orm.Apoderado_paciente lormApoderado_paciente = orm.Apoderado_pacienteDAO.loadApoderado_pacienteByQuery(null, null);
            // Delete the persistent object
            orm.Apoderado_pacienteDAO.delete(lormApoderado_paciente);
            orm.Asigdispcliente lormAsigdispcliente = orm.AsigdispclienteDAO.loadAsigdispclienteByQuery(null, null);
            // Delete the persistent object
            orm.AsigdispclienteDAO.delete(lormAsigdispcliente);
            orm.Clienteterapeuta_dispositivo lormClienteterapeuta_dispositivo = orm.Clienteterapeuta_dispositivoDAO.loadClienteterapeuta_dispositivoByQuery(null, null);
            // Delete the persistent object
            orm.Clienteterapeuta_dispositivoDAO.delete(lormClienteterapeuta_dispositivo);
            orm.Dispositivo lormDispositivo = orm.DispositivoDAO.loadDispositivoByQuery(null, null);
            // Delete the persistent object
            orm.DispositivoDAO.delete(lormDispositivo);
            orm.Tipodispositivo lormTipodispositivo = orm.TipodispositivoDAO.loadTipodispositivoByQuery(null, null);
            // Delete the persistent object
            orm.TipodispositivoDAO.delete(lormTipodispositivo);
            orm.Datosdispositivo lormDatosdispositivo = orm.DatosdispositivoDAO.loadDatosdispositivoByQuery(null, null);
            // Delete the persistent object
            orm.DatosdispositivoDAO.delete(lormDatosdispositivo);
            orm.Juego_patron lormJuego_patron = orm.Juego_patronDAO.loadJuego_patronByQuery(null, null);
            // Delete the persistent object
            orm.Juego_patronDAO.delete(lormJuego_patron);
            orm.Terapeuta lormTerapeuta = orm.TerapeutaDAO.loadTerapeutaByQuery(null, null);
            // Delete the persistent object
            orm.TerapeutaDAO.delete(lormTerapeuta);
            orm.Cargo lormCargo = orm.CargoDAO.loadCargoByQuery(null, null);
            // Delete the persistent object
            orm.CargoDAO.delete(lormCargo);
            orm.Paciente_terapeuta lormPaciente_terapeuta = orm.Paciente_terapeutaDAO.loadPaciente_terapeutaByQuery(null, null);
            // Delete the persistent object
            orm.Paciente_terapeutaDAO.delete(lormPaciente_terapeuta);
            orm.Dispositivo_organizacion lormDispositivo_organizacion = orm.Dispositivo_organizacionDAO.loadDispositivo_organizacionByQuery(null, null);
            // Delete the persistent object
            orm.Dispositivo_organizacionDAO.delete(lormDispositivo_organizacion);
            orm.Terapeuta_cargo lormTerapeuta_cargo = orm.Terapeuta_cargoDAO.loadTerapeuta_cargoByQuery(null, null);
            // Delete the persistent object
            orm.Terapeuta_cargoDAO.delete(lormTerapeuta_cargo);
            orm.Asigdisporganizacion lormAsigdisporganizacion = orm.AsigdisporganizacionDAO.loadAsigdisporganizacionByQuery(null, null);
            // Delete the persistent object
            orm.AsigdisporganizacionDAO.delete(lormAsigdisporganizacion);
            orm.Detalleasignacion lormDetalleasignacion = orm.DetalleasignacionDAO.loadDetalleasignacionByQuery(null, null);
            // Delete the persistent object
            orm.DetalleasignacionDAO.delete(lormDetalleasignacion);
            orm.Evaluacionterapeuta lormEvaluacionterapeuta = orm.EvaluacionterapeutaDAO.loadEvaluacionterapeutaByQuery(null, null);
            // Delete the persistent object
            orm.EvaluacionterapeutaDAO.delete(lormEvaluacionterapeuta);
            orm.Asigdispterapeuta lormAsigdispterapeuta = orm.AsigdispterapeutaDAO.loadAsigdispterapeutaByQuery(null, null);
            // Delete the persistent object
            orm.AsigdispterapeutaDAO.delete(lormAsigdispterapeuta);
            orm.Detasigterapeuta lormDetasigterapeuta = orm.DetasigterapeutaDAO.loadDetasigterapeutaByQuery(null, null);
            // Delete the persistent object
            orm.DetasigterapeutaDAO.delete(lormDetasigterapeuta);
            orm.Asigdispterapeuta_detasigterapeuta lormAsigdispterapeuta_detasigterapeuta = orm.Asigdispterapeuta_detasigterapeutaDAO.loadAsigdispterapeuta_detasigterapeutaByQuery(null, null);
            // Delete the persistent object
            orm.Asigdispterapeuta_detasigterapeutaDAO.delete(lormAsigdispterapeuta_detasigterapeuta);
            orm.Detalleasignacion_asigdispcliente lormDetalleasignacion_asigdispcliente = orm.Detalleasignacion_asigdispclienteDAO.loadDetalleasignacion_asigdispclienteByQuery(null, null);
            // Delete the persistent object
            orm.Detalleasignacion_asigdispclienteDAO.delete(lormDetalleasignacion_asigdispcliente);
            orm.Datosdispocitivo_datosdeusodiario lormDatosdispocitivo_datosdeusodiario = orm.Datosdispocitivo_datosdeusodiarioDAO.loadDatosdispocitivo_datosdeusodiarioByQuery(null, null);
            // Delete the persistent object
            orm.Datosdispocitivo_datosdeusodiarioDAO.delete(lormDatosdispocitivo_datosdeusodiario);
            orm.Datosdispocitivo_juego lormDatosdispocitivo_juego = orm.Datosdispocitivo_juegoDAO.loadDatosdispocitivo_juegoByQuery(null, null);
            // Delete the persistent object
            orm.Datosdispocitivo_juegoDAO.delete(lormDatosdispocitivo_juego);
            orm.Asigdisporganizacion_dispositivo lormAsigdisporganizacion_dispositivo = orm.Asigdisporganizacion_dispositivoDAO.loadAsigdisporganizacion_dispositivoByQuery(null, null);
            // Delete the persistent object
            orm.Asigdisporganizacion_dispositivoDAO.delete(lormAsigdisporganizacion_dispositivo);
            orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
            // Delete the persistent object
            orm.PersonaDAO.delete(lormPersona);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }

    }

    public static void main(String[] args) {
        try {
            DeleteMiProyectoProgramacionAvanzadaData deleteMiProyectoProgramacionAvanzadaData = new DeleteMiProyectoProgramacionAvanzadaData();
            try {
                deleteMiProyectoProgramacionAvanzadaData.deleteTestData();
            } finally {
                orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().disposePersistentManager();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
