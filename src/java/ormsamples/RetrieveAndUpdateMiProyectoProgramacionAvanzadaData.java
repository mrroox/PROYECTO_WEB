/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;

public class RetrieveAndUpdateMiProyectoProgramacionAvanzadaData {

    public void retrieveAndUpdateTestData() throws PersistentException {
        PersistentTransaction t = orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().getSession().beginTransaction();
        try {
            orm.Datosnivelcompletado lormDatosnivelcompletado = orm.DatosnivelcompletadoDAO.loadDatosnivelcompletadoByQuery(null, null);
            // Update the properties of the persistent object
            orm.DatosnivelcompletadoDAO.save(lormDatosnivelcompletado);
            orm.Datosdeusodiario lormDatosdeusodiario = orm.DatosdeusodiarioDAO.loadDatosdeusodiarioByQuery(null, null);
            // Update the properties of the persistent object
            orm.DatosdeusodiarioDAO.save(lormDatosdeusodiario);
            orm.Datosusonivel lormDatosusonivel = orm.DatosusonivelDAO.loadDatosusonivelByQuery(null, null);
            // Update the properties of the persistent object
            orm.DatosusonivelDAO.save(lormDatosusonivel);
            orm.Clienteterapeuta lormClienteterapeuta = orm.ClienteterapeutaDAO.loadClienteterapeutaByQuery(null, null);
            // Update the properties of the persistent object
            orm.ClienteterapeutaDAO.save(lormClienteterapeuta);
            orm.Datosusogeneral lormDatosusogeneral = orm.DatosusogeneralDAO.loadDatosusogeneralByQuery(null, null);
            // Update the properties of the persistent object
            orm.DatosusogeneralDAO.save(lormDatosusogeneral);
            orm.Evaluacion lormEvaluacion = orm.EvaluacionDAO.loadEvaluacionByQuery(null, null);
            // Update the properties of the persistent object
            orm.EvaluacionDAO.save(lormEvaluacion);
            orm.Paciente lormPaciente = orm.PacienteDAO.loadPacienteByQuery(null, null);
            // Update the properties of the persistent object
            orm.PacienteDAO.save(lormPaciente);
            orm.Patron lormPatron = orm.PatronDAO.loadPatronByQuery(null, null);
            // Update the properties of the persistent object
            orm.PatronDAO.save(lormPatron);
            orm.Juego lormJuego = orm.JuegoDAO.loadJuegoByQuery(null, null);
            // Update the properties of the persistent object
            orm.JuegoDAO.save(lormJuego);
            orm.Apoderado lormApoderado = orm.ApoderadoDAO.loadApoderadoByQuery(null, null);
            // Update the properties of the persistent object
            orm.ApoderadoDAO.save(lormApoderado);
            orm.Organizacion lormOrganizacion = orm.OrganizacionDAO.loadOrganizacionByQuery(null, null);
            // Update the properties of the persistent object
            orm.OrganizacionDAO.save(lormOrganizacion);
            orm.Pais lormPais = orm.PaisDAO.loadPaisByQuery(null, null);
            // Update the properties of the persistent object
            orm.PaisDAO.save(lormPais);
            orm.Region lormRegion = orm.RegionDAO.loadRegionByQuery(null, null);
            // Update the properties of the persistent object
            orm.RegionDAO.save(lormRegion);
            orm.Telefono lormTelefono = orm.TelefonoDAO.loadTelefonoByQuery(null, null);
            // Update the properties of the persistent object
            orm.TelefonoDAO.save(lormTelefono);
            orm.Direccion lormDireccion = orm.DireccionDAO.loadDireccionByQuery(null, null);
            // Update the properties of the persistent object
            orm.DireccionDAO.save(lormDireccion);
            orm.Expedientemedico lormExpedientemedico = orm.ExpedientemedicoDAO.loadExpedientemedicoByQuery(null, null);
            // Update the properties of the persistent object
            orm.ExpedientemedicoDAO.save(lormExpedientemedico);
            orm.Clienteterapeuta_paciente lormClienteterapeuta_paciente = orm.Clienteterapeuta_pacienteDAO.loadClienteterapeuta_pacienteByQuery(null, null);
            // Update the properties of the persistent object
            orm.Clienteterapeuta_pacienteDAO.save(lormClienteterapeuta_paciente);
            orm.Apoderado_paciente lormApoderado_paciente = orm.Apoderado_pacienteDAO.loadApoderado_pacienteByQuery(null, null);
            // Update the properties of the persistent object
            orm.Apoderado_pacienteDAO.save(lormApoderado_paciente);
            orm.Asigdispcliente lormAsigdispcliente = orm.AsigdispclienteDAO.loadAsigdispclienteByQuery(null, null);
            // Update the properties of the persistent object
            orm.AsigdispclienteDAO.save(lormAsigdispcliente);
            orm.Clienteterapeuta_dispositivo lormClienteterapeuta_dispositivo = orm.Clienteterapeuta_dispositivoDAO.loadClienteterapeuta_dispositivoByQuery(null, null);
            // Update the properties of the persistent object
            orm.Clienteterapeuta_dispositivoDAO.save(lormClienteterapeuta_dispositivo);
            orm.Dispositivo lormDispositivo = orm.DispositivoDAO.loadDispositivoByQuery(null, null);
            // Update the properties of the persistent object
            orm.DispositivoDAO.save(lormDispositivo);
            orm.Tipodispositivo lormTipodispositivo = orm.TipodispositivoDAO.loadTipodispositivoByQuery(null, null);
            // Update the properties of the persistent object
            orm.TipodispositivoDAO.save(lormTipodispositivo);
            orm.Datosdispositivo lormDatosdispositivo = orm.DatosdispositivoDAO.loadDatosdispositivoByQuery(null, null);
            // Update the properties of the persistent object
            orm.DatosdispositivoDAO.save(lormDatosdispositivo);
            orm.Juego_patron lormJuego_patron = orm.Juego_patronDAO.loadJuego_patronByQuery(null, null);
            // Update the properties of the persistent object
            orm.Juego_patronDAO.save(lormJuego_patron);
            orm.Terapeuta lormTerapeuta = orm.TerapeutaDAO.loadTerapeutaByQuery(null, null);
            // Update the properties of the persistent object
            orm.TerapeutaDAO.save(lormTerapeuta);
            orm.Cargo lormCargo = orm.CargoDAO.loadCargoByQuery(null, null);
            // Update the properties of the persistent object
            orm.CargoDAO.save(lormCargo);
            orm.Paciente_terapeuta lormPaciente_terapeuta = orm.Paciente_terapeutaDAO.loadPaciente_terapeutaByQuery(null, null);
            // Update the properties of the persistent object
            orm.Paciente_terapeutaDAO.save(lormPaciente_terapeuta);
            orm.Dispositivo_organizacion lormDispositivo_organizacion = orm.Dispositivo_organizacionDAO.loadDispositivo_organizacionByQuery(null, null);
            // Update the properties of the persistent object
            orm.Dispositivo_organizacionDAO.save(lormDispositivo_organizacion);
            orm.Terapeuta_cargo lormTerapeuta_cargo = orm.Terapeuta_cargoDAO.loadTerapeuta_cargoByQuery(null, null);
            // Update the properties of the persistent object
            orm.Terapeuta_cargoDAO.save(lormTerapeuta_cargo);
            orm.Asigdisporganizacion lormAsigdisporganizacion = orm.AsigdisporganizacionDAO.loadAsigdisporganizacionByQuery(null, null);
            // Update the properties of the persistent object
            orm.AsigdisporganizacionDAO.save(lormAsigdisporganizacion);
            orm.Detalleasignacion lormDetalleasignacion = orm.DetalleasignacionDAO.loadDetalleasignacionByQuery(null, null);
            // Update the properties of the persistent object
            orm.DetalleasignacionDAO.save(lormDetalleasignacion);
            orm.Evaluacionterapeuta lormEvaluacionterapeuta = orm.EvaluacionterapeutaDAO.loadEvaluacionterapeutaByQuery(null, null);
            // Update the properties of the persistent object
            orm.EvaluacionterapeutaDAO.save(lormEvaluacionterapeuta);
            orm.Asigdispterapeuta lormAsigdispterapeuta = orm.AsigdispterapeutaDAO.loadAsigdispterapeutaByQuery(null, null);
            // Update the properties of the persistent object
            orm.AsigdispterapeutaDAO.save(lormAsigdispterapeuta);
            orm.Detasigterapeuta lormDetasigterapeuta = orm.DetasigterapeutaDAO.loadDetasigterapeutaByQuery(null, null);
            // Update the properties of the persistent object
            orm.DetasigterapeutaDAO.save(lormDetasigterapeuta);
            orm.Asigdispterapeuta_detasigterapeuta lormAsigdispterapeuta_detasigterapeuta = orm.Asigdispterapeuta_detasigterapeutaDAO.loadAsigdispterapeuta_detasigterapeutaByQuery(null, null);
            // Update the properties of the persistent object
            orm.Asigdispterapeuta_detasigterapeutaDAO.save(lormAsigdispterapeuta_detasigterapeuta);
            orm.Detalleasignacion_asigdispcliente lormDetalleasignacion_asigdispcliente = orm.Detalleasignacion_asigdispclienteDAO.loadDetalleasignacion_asigdispclienteByQuery(null, null);
            // Update the properties of the persistent object
            orm.Detalleasignacion_asigdispclienteDAO.save(lormDetalleasignacion_asigdispcliente);
            orm.Datosdispocitivo_datosdeusodiario lormDatosdispocitivo_datosdeusodiario = orm.Datosdispocitivo_datosdeusodiarioDAO.loadDatosdispocitivo_datosdeusodiarioByQuery(null, null);
            // Update the properties of the persistent object
            orm.Datosdispocitivo_datosdeusodiarioDAO.save(lormDatosdispocitivo_datosdeusodiario);
            orm.Datosdispocitivo_juego lormDatosdispocitivo_juego = orm.Datosdispocitivo_juegoDAO.loadDatosdispocitivo_juegoByQuery(null, null);
            // Update the properties of the persistent object
            orm.Datosdispocitivo_juegoDAO.save(lormDatosdispocitivo_juego);
            orm.Asigdisporganizacion_dispositivo lormAsigdisporganizacion_dispositivo = orm.Asigdisporganizacion_dispositivoDAO.loadAsigdisporganizacion_dispositivoByQuery(null, null);
            // Update the properties of the persistent object
            orm.Asigdisporganizacion_dispositivoDAO.save(lormAsigdisporganizacion_dispositivo);
            orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
            // Update the properties of the persistent object
            orm.PersonaDAO.save(lormPersona);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }

    }

    public void retrieveByCriteria() throws PersistentException {
        System.out.println("Retrieving Datosnivelcompletado by DatosnivelcompletadoCriteria");
        orm.DatosnivelcompletadoCriteria lormDatosnivelcompletadoCriteria = new orm.DatosnivelcompletadoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDatosnivelcompletadoCriteria.id.eq();
        System.out.println(lormDatosnivelcompletadoCriteria.uniqueDatosnivelcompletado());

        System.out.println("Retrieving Datosdeusodiario by DatosdeusodiarioCriteria");
        orm.DatosdeusodiarioCriteria lormDatosdeusodiarioCriteria = new orm.DatosdeusodiarioCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDatosdeusodiarioCriteria.id.eq();
        System.out.println(lormDatosdeusodiarioCriteria.uniqueDatosdeusodiario());

        System.out.println("Retrieving Datosusonivel by DatosusonivelCriteria");
        orm.DatosusonivelCriteria lormDatosusonivelCriteria = new orm.DatosusonivelCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDatosusonivelCriteria.id.eq();
        System.out.println(lormDatosusonivelCriteria.uniqueDatosusonivel());

        System.out.println("Retrieving Clienteterapeuta by ClienteterapeutaCriteria");
        orm.ClienteterapeutaCriteria lormClienteterapeutaCriteria = new orm.ClienteterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormClienteterapeutaCriteria.id.eq();
        System.out.println(lormClienteterapeutaCriteria.uniqueClienteterapeuta());

        System.out.println("Retrieving Datosusogeneral by DatosusogeneralCriteria");
        orm.DatosusogeneralCriteria lormDatosusogeneralCriteria = new orm.DatosusogeneralCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDatosusogeneralCriteria.id.eq();
        System.out.println(lormDatosusogeneralCriteria.uniqueDatosusogeneral());

        System.out.println("Retrieving Evaluacion by EvaluacionCriteria");
        orm.EvaluacionCriteria lormEvaluacionCriteria = new orm.EvaluacionCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormEvaluacionCriteria.id.eq();
        System.out.println(lormEvaluacionCriteria.uniqueEvaluacion());

        System.out.println("Retrieving Paciente by PacienteCriteria");
        orm.PacienteCriteria lormPacienteCriteria = new orm.PacienteCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormPacienteCriteria.id.eq();
        System.out.println(lormPacienteCriteria.uniquePaciente());

        System.out.println("Retrieving Patron by PatronCriteria");
        orm.PatronCriteria lormPatronCriteria = new orm.PatronCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormPatronCriteria.id.eq();
        System.out.println(lormPatronCriteria.uniquePatron());

        System.out.println("Retrieving Juego by JuegoCriteria");
        orm.JuegoCriteria lormJuegoCriteria = new orm.JuegoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormJuegoCriteria.id.eq();
        System.out.println(lormJuegoCriteria.uniqueJuego());

        System.out.println("Retrieving Apoderado by ApoderadoCriteria");
        orm.ApoderadoCriteria lormApoderadoCriteria = new orm.ApoderadoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormApoderadoCriteria.id.eq();
        System.out.println(lormApoderadoCriteria.uniqueApoderado());

        System.out.println("Retrieving Organizacion by OrganizacionCriteria");
        orm.OrganizacionCriteria lormOrganizacionCriteria = new orm.OrganizacionCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormOrganizacionCriteria.id.eq();
        System.out.println(lormOrganizacionCriteria.uniqueOrganizacion());

        System.out.println("Retrieving Pais by PaisCriteria");
        orm.PaisCriteria lormPaisCriteria = new orm.PaisCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormPaisCriteria.id.eq();
        System.out.println(lormPaisCriteria.uniquePais());

        System.out.println("Retrieving Region by RegionCriteria");
        orm.RegionCriteria lormRegionCriteria = new orm.RegionCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormRegionCriteria.id.eq();
        System.out.println(lormRegionCriteria.uniqueRegion());

        System.out.println("Retrieving Telefono by TelefonoCriteria");
        orm.TelefonoCriteria lormTelefonoCriteria = new orm.TelefonoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormTelefonoCriteria.id.eq();
        System.out.println(lormTelefonoCriteria.uniqueTelefono());

        System.out.println("Retrieving Direccion by DireccionCriteria");
        orm.DireccionCriteria lormDireccionCriteria = new orm.DireccionCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDireccionCriteria.id.eq();
        System.out.println(lormDireccionCriteria.uniqueDireccion());

        System.out.println("Retrieving Expedientemedico by ExpedientemedicoCriteria");
        orm.ExpedientemedicoCriteria lormExpedientemedicoCriteria = new orm.ExpedientemedicoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormExpedientemedicoCriteria.id.eq();
        System.out.println(lormExpedientemedicoCriteria.uniqueExpedientemedico());

        System.out.println("Retrieving Clienteterapeuta_paciente by Clienteterapeuta_pacienteCriteria");
        orm.Clienteterapeuta_pacienteCriteria lormClienteterapeuta_pacienteCriteria = new orm.Clienteterapeuta_pacienteCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormClienteterapeuta_pacienteCriteria.id.eq();
        System.out.println(lormClienteterapeuta_pacienteCriteria.uniqueClienteterapeuta_paciente());

        System.out.println("Retrieving Apoderado_paciente by Apoderado_pacienteCriteria");
        orm.Apoderado_pacienteCriteria lormApoderado_pacienteCriteria = new orm.Apoderado_pacienteCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormApoderado_pacienteCriteria.id.eq();
        System.out.println(lormApoderado_pacienteCriteria.uniqueApoderado_paciente());

        System.out.println("Retrieving Asigdispcliente by AsigdispclienteCriteria");
        orm.AsigdispclienteCriteria lormAsigdispclienteCriteria = new orm.AsigdispclienteCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormAsigdispclienteCriteria.id.eq();
        System.out.println(lormAsigdispclienteCriteria.uniqueAsigdispcliente());

        System.out.println("Retrieving Clienteterapeuta_dispositivo by Clienteterapeuta_dispositivoCriteria");
        orm.Clienteterapeuta_dispositivoCriteria lormClienteterapeuta_dispositivoCriteria = new orm.Clienteterapeuta_dispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormClienteterapeuta_dispositivoCriteria.id.eq();
        System.out.println(lormClienteterapeuta_dispositivoCriteria.uniqueClienteterapeuta_dispositivo());

        System.out.println("Retrieving Dispositivo by DispositivoCriteria");
        orm.DispositivoCriteria lormDispositivoCriteria = new orm.DispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDispositivoCriteria.id.eq();
        System.out.println(lormDispositivoCriteria.uniqueDispositivo());

        System.out.println("Retrieving Tipodispositivo by TipodispositivoCriteria");
        orm.TipodispositivoCriteria lormTipodispositivoCriteria = new orm.TipodispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormTipodispositivoCriteria.id.eq();
        System.out.println(lormTipodispositivoCriteria.uniqueTipodispositivo());

        System.out.println("Retrieving Datosdispositivo by DatosdispositivoCriteria");
        orm.DatosdispositivoCriteria lormDatosdispositivoCriteria = new orm.DatosdispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDatosdispositivoCriteria.id.eq();
        System.out.println(lormDatosdispositivoCriteria.uniqueDatosdispositivo());

        System.out.println("Retrieving Juego_patron by Juego_patronCriteria");
        orm.Juego_patronCriteria lormJuego_patronCriteria = new orm.Juego_patronCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormJuego_patronCriteria.id.eq();
        System.out.println(lormJuego_patronCriteria.uniqueJuego_patron());

        System.out.println("Retrieving Terapeuta by TerapeutaCriteria");
        orm.TerapeutaCriteria lormTerapeutaCriteria = new orm.TerapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormTerapeutaCriteria.id.eq();
        System.out.println(lormTerapeutaCriteria.uniqueTerapeuta());

        System.out.println("Retrieving Cargo by CargoCriteria");
        orm.CargoCriteria lormCargoCriteria = new orm.CargoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormCargoCriteria.id.eq();
        System.out.println(lormCargoCriteria.uniqueCargo());

        System.out.println("Retrieving Paciente_terapeuta by Paciente_terapeutaCriteria");
        orm.Paciente_terapeutaCriteria lormPaciente_terapeutaCriteria = new orm.Paciente_terapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormPaciente_terapeutaCriteria.id.eq();
        System.out.println(lormPaciente_terapeutaCriteria.uniquePaciente_terapeuta());

        System.out.println("Retrieving Dispositivo_organizacion by Dispositivo_organizacionCriteria");
        orm.Dispositivo_organizacionCriteria lormDispositivo_organizacionCriteria = new orm.Dispositivo_organizacionCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDispositivo_organizacionCriteria.id.eq();
        System.out.println(lormDispositivo_organizacionCriteria.uniqueDispositivo_organizacion());

        System.out.println("Retrieving Terapeuta_cargo by Terapeuta_cargoCriteria");
        orm.Terapeuta_cargoCriteria lormTerapeuta_cargoCriteria = new orm.Terapeuta_cargoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormTerapeuta_cargoCriteria.id.eq();
        System.out.println(lormTerapeuta_cargoCriteria.uniqueTerapeuta_cargo());

        System.out.println("Retrieving Asigdisporganizacion by AsigdisporganizacionCriteria");
        orm.AsigdisporganizacionCriteria lormAsigdisporganizacionCriteria = new orm.AsigdisporganizacionCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormAsigdisporganizacionCriteria.id.eq();
        System.out.println(lormAsigdisporganizacionCriteria.uniqueAsigdisporganizacion());

        System.out.println("Retrieving Detalleasignacion by DetalleasignacionCriteria");
        orm.DetalleasignacionCriteria lormDetalleasignacionCriteria = new orm.DetalleasignacionCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDetalleasignacionCriteria.id.eq();
        System.out.println(lormDetalleasignacionCriteria.uniqueDetalleasignacion());

        System.out.println("Retrieving Evaluacionterapeuta by EvaluacionterapeutaCriteria");
        orm.EvaluacionterapeutaCriteria lormEvaluacionterapeutaCriteria = new orm.EvaluacionterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormEvaluacionterapeutaCriteria.id.eq();
        System.out.println(lormEvaluacionterapeutaCriteria.uniqueEvaluacionterapeuta());

        System.out.println("Retrieving Asigdispterapeuta by AsigdispterapeutaCriteria");
        orm.AsigdispterapeutaCriteria lormAsigdispterapeutaCriteria = new orm.AsigdispterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormAsigdispterapeutaCriteria.id.eq();
        System.out.println(lormAsigdispterapeutaCriteria.uniqueAsigdispterapeuta());

        System.out.println("Retrieving Detasigterapeuta by DetasigterapeutaCriteria");
        orm.DetasigterapeutaCriteria lormDetasigterapeutaCriteria = new orm.DetasigterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDetasigterapeutaCriteria.id.eq();
        System.out.println(lormDetasigterapeutaCriteria.uniqueDetasigterapeuta());

        System.out.println("Retrieving Asigdispterapeuta_detasigterapeuta by Asigdispterapeuta_detasigterapeutaCriteria");
        orm.Asigdispterapeuta_detasigterapeutaCriteria lormAsigdispterapeuta_detasigterapeutaCriteria = new orm.Asigdispterapeuta_detasigterapeutaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormAsigdispterapeuta_detasigterapeutaCriteria.id.eq();
        System.out.println(lormAsigdispterapeuta_detasigterapeutaCriteria.uniqueAsigdispterapeuta_detasigterapeuta());

        System.out.println("Retrieving Detalleasignacion_asigdispcliente by Detalleasignacion_asigdispclienteCriteria");
        orm.Detalleasignacion_asigdispclienteCriteria lormDetalleasignacion_asigdispclienteCriteria = new orm.Detalleasignacion_asigdispclienteCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDetalleasignacion_asigdispclienteCriteria.id.eq();
        System.out.println(lormDetalleasignacion_asigdispclienteCriteria.uniqueDetalleasignacion_asigdispcliente());

        System.out.println("Retrieving Datosdispocitivo_datosdeusodiario by Datosdispocitivo_datosdeusodiarioCriteria");
        orm.Datosdispocitivo_datosdeusodiarioCriteria lormDatosdispocitivo_datosdeusodiarioCriteria = new orm.Datosdispocitivo_datosdeusodiarioCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDatosdispocitivo_datosdeusodiarioCriteria.id.eq();
        System.out.println(lormDatosdispocitivo_datosdeusodiarioCriteria.uniqueDatosdispocitivo_datosdeusodiario());

        System.out.println("Retrieving Datosdispocitivo_juego by Datosdispocitivo_juegoCriteria");
        orm.Datosdispocitivo_juegoCriteria lormDatosdispocitivo_juegoCriteria = new orm.Datosdispocitivo_juegoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormDatosdispocitivo_juegoCriteria.id.eq();
        System.out.println(lormDatosdispocitivo_juegoCriteria.uniqueDatosdispocitivo_juego());

        System.out.println("Retrieving Asigdisporganizacion_dispositivo by Asigdisporganizacion_dispositivoCriteria");
        orm.Asigdisporganizacion_dispositivoCriteria lormAsigdisporganizacion_dispositivoCriteria = new orm.Asigdisporganizacion_dispositivoCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormAsigdisporganizacion_dispositivoCriteria.id.eq();
        System.out.println(lormAsigdisporganizacion_dispositivoCriteria.uniqueAsigdisporganizacion_dispositivo());

        System.out.println("Retrieving Persona by PersonaCriteria");
        orm.PersonaCriteria lormPersonaCriteria = new orm.PersonaCriteria();
        // Please uncomment the follow line and fill in parameter(s)
        //lormPersonaCriteria.id.eq();
        System.out.println(lormPersonaCriteria.uniquePersona());

    }

    public static void main(String[] args) {
        try {
            RetrieveAndUpdateMiProyectoProgramacionAvanzadaData retrieveAndUpdateMiProyectoProgramacionAvanzadaData = new RetrieveAndUpdateMiProyectoProgramacionAvanzadaData();
            try {
                retrieveAndUpdateMiProyectoProgramacionAvanzadaData.retrieveAndUpdateTestData();
                //retrieveAndUpdateMiProyectoProgramacionAvanzadaData.retrieveByCriteria();
            } finally {
                orm.MiProyectoProgramacionAvanzadaPersistentManager.instance().disposePersistentManager();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
